package com.fs.starfarer.api.impl.combat;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.DamagingProjectileAPI;
import com.fs.starfarer.api.combat.OnFireEffectPlugin;
import com.fs.starfarer.api.combat.WeaponAPI;
import org.lwjgl.util.vector.Vector2f;
import thQuest.ThU;

public class ThquestZanmuFire implements OnFireEffectPlugin {
    float time =0.0f;
    @Override
    public void onFire(DamagingProjectileAPI damagingProjectileAPI, WeaponAPI weaponAPI, CombatEngineAPI combatEngineAPI) {
        float newTime =Global.getCombatEngine().getTotalElapsedTime(false);

        if(time!=newTime){
            //back a little and random
            Vector2f position = ThU.multiplyVector(ThU.angleVector(weaponAPI.getCurrAngle()+180),25);
//            position = ThU.AddVector(position,new Vector2f((float) ((Math.random()-.5)*10), (float) ((Math.random()-.5)*10)));
            position = ThU.addVector(weaponAPI.getLocation(),position);
            combatEngineAPI.spawnProjectile(weaponAPI.getShip(), weaponAPI,"thquest_zshot_fake",position,damagingProjectileAPI.getFacing(),weaponAPI.getShip().getVelocity());
//            Log.getLogger().info("spawning star!");
            time=newTime;
        }
    }
}
