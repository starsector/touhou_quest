package com.fs.starfarer.api.impl.combat;

import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.util.Misc;
import com.fs.starfarer.combat.ai.missile.MissileAI;
import com.fs.starfarer.combat.entities.Missile;
import org.apache.log4j.Logger;
import org.lazywizard.lazylib.VectorUtils;
import org.lazywizard.lazylib.combat.AIUtils;
import org.lwjgl.util.vector.Vector2f;
import thQuest.Log;
import thQuest.ThU;

import java.awt.*;

public class ThquestFantasySealAI implements MissileAIPlugin, GuidedMissileAI {
    MissileAPI missile;
    CombatEntityAPI target;
    float goal=35f;
    public ThquestFantasySealAI(Missile missile, Vector2f vector2f, boolean b) {
        this.missile=missile;
    }
//temporary probably
    public ThquestFantasySealAI(MissileAPI missile, ShipAPI launchingShip) {
        this.missile=missile;
    }
    @Override
    public CombatEntityAPI getTarget() {
        return target;
    }

    @Override
    public void setTarget(CombatEntityAPI combatEntityAPI) {
        this.target=target;
    }

    @Override
    public void advance(float v) {
        Logger logger = Log.getLogger();
        missile.setFacing(VectorUtils.getAngle(missile.getLocation(),getMissileCenterBarrelLocation())+105);
        if(Misc.getDistance(missile.getLocation(),getMissileCenterBarrelLocation())>goal){
            missile.giveCommand(ShipCommand.STRAFE_RIGHT);
        }else{
            missile.giveCommand(ShipCommand.ACCELERATE);
        }
       //     missile.giveCommand(ShipCommand.STRAFE_RIGHT);
       // }else if(Misc.getDistance(missile.getLocation(),missile.getWeapon().getLocation())<15){
    }
    public Vector2f getMissileCenterBarrelLocation(){
        float angle = missile.getWeapon().getCurrAngle();
        return ThU.addVector(ThU.angleVector(angle),missile.getWeapon().getFirePoint(5));
    }
}
