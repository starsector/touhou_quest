package com.fs.starfarer.api.impl.combat;

import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.util.Misc;
import com.fs.starfarer.combat.entities.BaseEntity;
import org.lwjgl.util.vector.Vector2f;
import thQuest.ThU;

public class ThquestIcicleFall implements OnFireEffectPlugin {
    //list of projectiles slowing down, then the will be turned, set to the right speed, and removed from this list
    private final float SPEED_BASE = 50f;
    private final String TIME_KEY = "thquestitime";
    private final String STAGE_KEY="thquestbkey";
    private final float dist = 3f;
    //private final String SEQUENCE="thquestiseq";
    private final int maxStage =11;
    @Override
    public void onFire(DamagingProjectileAPI damagingProjectileAPI, WeaponAPI weaponAPI, CombatEngineAPI combatEngineAPI) {
    //AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
        float timepass=100f;
        int stage = 0;
        if(!combatEngineAPI.getCustomData().containsKey(TIME_KEY+weaponAPI.getId())){
            combatEngineAPI.getCustomData().put(TIME_KEY+weaponAPI.getId(),combatEngineAPI.getTotalElapsedTime(false));
        }else{
            timepass=combatEngineAPI.getTotalElapsedTime(false)-(Float)combatEngineAPI.getCustomData().get(TIME_KEY+weaponAPI.getId());
            combatEngineAPI.getCustomData().put(TIME_KEY+weaponAPI.getId(),combatEngineAPI.getTotalElapsedTime(false));
        }
        if(!combatEngineAPI.getCustomData().containsKey(STAGE_KEY+weaponAPI.getId())){
            combatEngineAPI.getCustomData().put(STAGE_KEY+weaponAPI.getId(), 0);
        }else{
            stage = 1+(Integer) combatEngineAPI.getCustomData().get(STAGE_KEY+weaponAPI.getId());

            if(stage==maxStage){
                stage=0;
            }
            if(timepass>1f){
                stage=0;
            }
            combatEngineAPI.getCustomData().put(STAGE_KEY+weaponAPI.getId(),stage);
        }
        //stage->angle
        //angle->position
        float angle = 100f-stage*5;
        float angler = weaponAPI.getCurrAngle()+angle;
        float anglel = weaponAPI.getCurrAngle()-angle;
        Vector2f angleVectorr = Misc.getUnitVectorAtDegreeAngle(angler);
        Vector2f angleVectorl = Misc.getUnitVectorAtDegreeAngle(anglel);
        angleVectorr= new Vector2f(angleVectorr.x*dist,angleVectorr.y*dist);
        angleVectorl=new Vector2f(angleVectorl.y*dist,angleVectorl.y*dist);
        angleVectorr=ThU.addVector(angleVectorr,weaponAPI.getFirePoint(0));
        angleVectorl=ThU.addVector(angleVectorl,weaponAPI.getFirePoint(0));

        combatEngineAPI.removeEntity(damagingProjectileAPI);

        spawnBullets(weaponAPI,combatEngineAPI,angleVectorr,angler);
        spawnBullets(weaponAPI,combatEngineAPI,angleVectorl,anglel);

    }
    public void spawnBullets(WeaponAPI weaponAPI, CombatEngineAPI combatEngineAPI,Vector2f location,Float angle){
        //1 deci
        CombatEntityAPI p0=combatEngineAPI.spawnProjectile(weaponAPI.getShip(), weaponAPI,"thquest_icicle_fall_fake",location,angle,weaponAPI.getShip().getVelocity());
        CombatEntityAPI p1=combatEngineAPI.spawnProjectile(weaponAPI.getShip(), weaponAPI,"thquest_icicle_fall_fake",location,angle,weaponAPI.getShip().getVelocity());
        CombatEntityAPI p2=combatEngineAPI.spawnProjectile(weaponAPI.getShip(), weaponAPI,"thquest_icicle_fall_fake",location,angle,weaponAPI.getShip().getVelocity());
        CombatEntityAPI p3=combatEngineAPI.spawnProjectile(weaponAPI.getShip(), weaponAPI,"thquest_icicle_fall_fake",location,angle,weaponAPI.getShip().getVelocity());
        //what speed to go
        p0.setCustomData("thquest_iorder",new Integer(0));
        p1.setCustomData("thquest_iorder",new Integer(1));
        p2.setCustomData("thquest_iorder",new Integer(2));
        p3.setCustomData("thquest_iorder",new Integer(3));
        //velocity from ship
        p0.setCustomData("thquest_ibasevelocity",weaponAPI.getShip().getVelocity());
        p1.setCustomData("thquest_ibasevelocity",weaponAPI.getShip().getVelocity());
        p2.setCustomData("thquest_ibasevelocity",weaponAPI.getShip().getVelocity());
        p3.setCustomData("thquest_ibasevelocity",weaponAPI.getShip().getVelocity());

        //time since launch
//        p0.setCustomData("thquest_uptime",new Float(0));
//        p1.setCustomData("thquest_uptime",new Float(0));
//        p2.setCustomData("thquest_uptime",new Float(0));
//        p3.setCustomData("thquest_uptime",new Float(0));
        if(ThU.isAngleRightRelitive(weaponAPI.getCurrAngle(), angle)){//right side
            p0.setCustomData("thquest_iside",new Integer(0));
            p1.setCustomData("thquest_iside",new Integer(0));
            p2.setCustomData("thquest_iside",new Integer(0));
            p3.setCustomData("thquest_iside",new Integer(0));
        }else{//left side
            p0.setCustomData("thquest_iside",new Integer(1));
            p1.setCustomData("thquest_iside",new Integer(1));
            p2.setCustomData("thquest_iside",new Integer(1));
            p3.setCustomData("thquest_iside",new Integer(1));
        }
        //set speed for each projectile
        setSpeed(p0);
        setSpeed(p1);
        setSpeed(p2);
        setSpeed(p3);

        ((ThquestIcicleFallPlugin)weaponAPI.getEffectPlugin()).projs.add(p0);
        ((ThquestIcicleFallPlugin)weaponAPI.getEffectPlugin()).projs.add(p1);
        ((ThquestIcicleFallPlugin)weaponAPI.getEffectPlugin()).projs.add(p2);
        ((ThquestIcicleFallPlugin)weaponAPI.getEffectPlugin()).projs.add(p3);

    }
    public void setSpeed(CombatEntityAPI proj){
        Integer multiplier = (Integer) proj.getCustomData().get("thquest_iorder");
        multiplier=multiplier+1;
        Vector2f velocity=((BaseEntity)proj).getVelocity();
        Vector2f shipVelocity= (Vector2f) proj.getCustomData().get("thquest_ibasevelocity");
        Vector2f fvelocity =new Vector2f(velocity.x-shipVelocity.x,velocity.y-shipVelocity.y);
        Vector2f facing =ThU.angleVector(proj.getFacing());
        ((BaseEntity) proj).setVel(new Vector2f(facing.x*multiplier*SPEED_BASE+shipVelocity.x,facing.y*multiplier*SPEED_BASE+shipVelocity.y));

    }
    public void stage2(CombatEntityAPI proj){

    }
}
