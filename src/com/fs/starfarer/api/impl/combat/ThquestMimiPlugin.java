package com.fs.starfarer.api.impl.combat;

import com.fs.starfarer.api.combat.*;
import org.lazywizard.lazylib.VectorUtils;
import thQuest.ThU;

import java.util.ArrayList;
import java.util.List;

public class ThquestMimiPlugin implements EveryFrameWeaponEffectPlugin, OnFireEffectPlugin {
    public List<MissileAPI> missiles = new ArrayList<>();
    private final float LARGE_CHANCE =6f;
    private final float MEDIUM_CHANCE =12f;
    private final float SMALL_CHANCE =18f;
    private final float ANGLE =30f;
    private final float SPIN =600f;

    @Override
    public void advance(float v, CombatEngineAPI combatEngineAPI, WeaponAPI weaponAPI) {
    for(int i = missiles.size()-1;i>-1;i--){
        MissileAPI m = missiles.get(i);
        if(m==null||m.isExpired()||m.isFizzling()||m.didDamage()){
            missiles.remove(missiles.get(i));
        }else{
        if(Math.random()<LARGE_CHANCE*v){
            //engine.spawnProjectile(weapon.getShip(), weapon,"thquest_blue_medium",weapon.getFirePoint(0),countdown*180+(float) Math.random()*15,b1);
            combatEngineAPI.spawnProjectile(weaponAPI.getShip(),weaponAPI,"thquest_star_large",m.getLocation(),
                    (float) (m.getFacing()+180+(Math.random()-0.5f)*ANGLE),ThU.modifyVector(m.getVelocity(),
                            (float) Math.random())).setAngularVelocity((float) ((Math.random()-.5)*SPIN));
        }
        if(Math.random()<MEDIUM_CHANCE*v){
            combatEngineAPI.spawnProjectile(weaponAPI.getShip(),weaponAPI,"thquest_star_medium",m.getLocation(),
                    (float) (m.getFacing()+180+(Math.random()-0.5f)*ANGLE),ThU.modifyVector(m.getVelocity(),
                            (float) Math.random())).setAngularVelocity((float) ((Math.random()-.5)*SPIN));

        }
        if(Math.random()<SMALL_CHANCE*v){
            combatEngineAPI.spawnProjectile(weaponAPI.getShip(),weaponAPI,"thquest_star_small",m.getLocation(),
                    (float) (m.getFacing()+180+(Math.random()-0.5f)*ANGLE), ThU.modifyVector(m.getVelocity(),
                            (float) Math.random())).setAngularVelocity((float) ((Math.random()-.5)*SPIN));
        }
        }
    }
    }

    @Override
    public void onFire(DamagingProjectileAPI damagingProjectileAPI, WeaponAPI weaponAPI, CombatEngineAPI combatEngineAPI) {
        missiles.add((MissileAPI) damagingProjectileAPI);
    }
}
