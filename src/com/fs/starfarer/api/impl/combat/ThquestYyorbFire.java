package com.fs.starfarer.api.impl.combat;

import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.DamagingProjectileAPI;
import com.fs.starfarer.api.combat.OnFireEffectPlugin;
import com.fs.starfarer.api.combat.WeaponAPI;

public class ThquestYyorbFire implements OnFireEffectPlugin {
    @Override
    public void onFire(DamagingProjectileAPI damagingProjectileAPI, WeaponAPI weaponAPI, CombatEngineAPI combatEngineAPI) {
        damagingProjectileAPI.setCustomData("thquest_parent",weaponAPI.getShip());
        ((ThquestYyorbPlugin)weaponAPI.getEffectPlugin()).addProj(damagingProjectileAPI);
    }
}
