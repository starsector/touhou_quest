package com.fs.starfarer.api.impl.combat;

import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.util.IntervalUtil;
import com.fs.starfarer.api.util.Misc;
import com.fs.starfarer.combat.entities.BaseEntity;
import org.lazywizard.lazylib.VectorUtils;
import org.lazywizard.lazylib.combat.AIUtils;
import org.lwjgl.util.vector.Vector2f;
import thQuest.ThU;

import java.util.ArrayList;
import java.util.List;

public class ThquestCheesePlugin2 implements EveryFrameWeaponEffectPlugin {
    static float s1slowdown = .85f;
    static float s1FinalSpeed = 10f;
    static float s2InitialSpeed = 150f;
    static float s2AngleError = 1f;
    static float segSep =0.005f;
    static float s2TurnSpeed = 50f;
    static float s2Acceleration = 100f;
    static IntervalUtil recharge = new IntervalUtil(0.05f,0.05f);

    //stage 0: initial slowdown,
    //stage 1: speedup
    //stage 3: terminal
    List<DamagingProjectileAPI> projectileList = new ArrayList<>();
    @Override
    public void advance(float v, CombatEngineAPI combatEngineAPI, WeaponAPI weaponAPI) {
        List<DamagingProjectileAPI> removeList = new ArrayList<>();
        for(DamagingProjectileAPI proj: projectileList) {
            if (proj == null || proj.isExpired()) {
                removeList.add(proj);
            } else {
                //record velocity and frame step of lead bullet
                //this game is bad and EVIL. DO NOT STORE CUSTOM CLASSES IN CUSTOM DATA IN PROJECTILES at least
                ArrayList<Float> tRecord = (ArrayList<Float>) proj.getCustomData().get("thquest_times_for_velocity_record");
                ArrayList<Float> xRecord = (ArrayList<Float>) proj.getCustomData().get("thquest_x_record");
                ArrayList<Float> yRecord = (ArrayList<Float>) proj.getCustomData().get("thquest_y_record");

                xRecord.add(proj.getVelocity().x);
                yRecord.add(proj.getVelocity().y);
                tRecord.add(proj.getElapsed());
                proj.setCustomData("thquest_x_record",xRecord);
                proj.setCustomData("thquest_y_record",yRecord);
                proj.setCustomData("thquest_times_for_velocity_record",tRecord);

                //set trail bullets to follow the recorded velocity of the lead bullet
                int amt= (int)proj.getCustomData().get("thquest_spawn_more_amount");
                if(amt>0){
                    IntervalUtil interval = (IntervalUtil) proj.getCustomData().get("thquest_spawn_more_interval");
                    interval.advance(v);
                    if(interval.intervalElapsed()){
                        proj.getCustomData().put("thquest_spawn_more_amount", amt-1);
                        float angleAtFire = (float)proj.getCustomData().get("thquest_angle_at_fire");
                        Vector2f posAtFire = (Vector2f) proj.getCustomData().get("thquest_loc_at_fire");
                        DamagingProjectileAPI p = (DamagingProjectileAPI) combatEngineAPI.spawnProjectile(weaponAPI.getShip(),weaponAPI,"thquest_cheese_fake",posAtFire,angleAtFire,weaponAPI.getShip().getVelocity());
                        ((List<DamagingProjectileAPI>)proj.getCustomData().get("thquest_followers")).add(p);

                    }
                }
                //trail of projs
                List<DamagingProjectileAPI> subProjs=((List<DamagingProjectileAPI>)proj.getCustomData().get("thquest_followers"));
                List<DamagingProjectileAPI> removeSub = new ArrayList<>();
                DamagingProjectileAPI lastProj = proj;


                int j = 0;
                int lastIndex = 0;
                for(int i=subProjs.size()-1;i>-1;i--){
                    DamagingProjectileAPI subProj = subProjs.get(i);
                    if (subProj == null || subProj.isExpired()) {
                        removeSub.add(subProj);
                    }else{
                        float uptime = subProj.getElapsed();
                        //cheese data should be in order. So should the subprojs be
                        while(tRecord.get(j)<uptime) {
                            lastIndex = j;
                            if(j == tRecord.size()-1){
                                break;
                            }else{
                                j++;
                            }
                        }
                        //mult one is multiplied by the first data point, two is multiplied by the second. This gets the projected vector at that position
                        float velocityMult1 = (tRecord.get(j) - uptime) / (tRecord.get(j) - tRecord.get(lastIndex));
                        float velocityMult2 = Math.abs(velocityMult1-1);//TODO if mult ends up being NAN
                        if(Float.isNaN(velocityMult1)){
                            velocityMult1=1;
                            velocityMult2=0;
                        }

                        Vector2f vector1 = ThU.modifyVector(new Vector2f(xRecord.get(lastIndex),yRecord.get(lastIndex)),velocityMult1);
                        Vector2f vector2 = ThU.modifyVector(new Vector2f(xRecord.get(j),yRecord.get(j)),velocityMult2);
                        Vector2f finalVector = ThU.addVector(vector1,vector2);
                        ((BaseEntity)subProj).setVel(finalVector);


                            if (Misc.getDistance(subProj.getLocation(), subProj.getWeapon().getShip().getLocation()) > subProj.getWeapon().getRange()+100 &&
                                    Misc.getDistance(subProj.getLocation(), subProj.getSpawnLocation()) > subProj.getWeapon().getRange()+100) {
                                combatEngineAPI.removeEntity(subProj);
                                removeSub.add(subProj);
                            }
                    }
                }

                subProjs.removeAll(removeSub);

                //removing projectiles based on
                List<DamagingProjectileAPI> remove2 = new ArrayList<>();




                proj.setCustomData("thquest_followers", subProjs);

                int stage = (int) proj.getCustomData().get("thquest_cheese_stage");
                if (stage == 0) {
                    ((BaseEntity) proj).setVel(ThU.multiplyVector(proj.getVelocity(), 1 - (float) s1slowdown * v));
                    if (ThU.vectorToSpeed(proj.getVelocity()) < s1FinalSpeed) {
                        proj.setCustomData("thquest_cheese_stage", 1);
                        //yeet!
                        ((BaseEntity) proj).setVel(new Vector2f(ThU.modifyVector(ThU.angleVector(proj.getFacing()),s2InitialSpeed)));
                    }
                } else if (stage == 1) {
                    ShipAPI target = (ShipAPI) proj.getCustomData().get("thquest_target");
                    if (target == null||!target.isAlive()||target.isExpired()) {//TODO removed || target.isAlly() would have targeting issues with player allys
                        target = AIUtils.getNearestEnemy(proj);
                    }
                    float desiredAngle;
                    if (target != null) {
                        desiredAngle = VectorUtils.getAngle(proj.getLocation(), target.getLocation());
                    } else {
                        desiredAngle = proj.getFacing();
                    }

                    if (Misc.getAngleDiff(desiredAngle, proj.getFacing()) > s2AngleError) {
                        //nudge facing closer
                        if (ThU.isAngleRightRelitive(desiredAngle, proj.getFacing())) {
                            proj.setFacing(proj.getFacing() - s2TurnSpeed * v);
                        } else {
                            proj.setFacing(proj.getFacing() + s2TurnSpeed * v);
                        }
                    }
                    //velocity should be closely controlled
                    Vector2f velocity = ThU.angleVector(proj.getFacing());
                    //acceleration
                    velocity = ThU.modifyVector(velocity, (float) proj.getCustomData().get("thquest_target_speed"));
                    ((BaseEntity) proj).setVel(velocity);
                    proj.setCustomData("thquest_target_speed", (float) proj.getCustomData().get("thquest_target_speed") * 1+(s2Acceleration * v));
                }
            }
        }
        projectileList.removeAll(removeList);
    }
    public void addProj(DamagingProjectileAPI damagingProjectileAPI){
        damagingProjectileAPI.setCustomData("thquest_cheese_stage",0);
        damagingProjectileAPI.setCustomData("thquest_target_speed",s2InitialSpeed);
        damagingProjectileAPI.setCustomData("thquest_target",damagingProjectileAPI.getWeapon().getShip().getShipTarget());
        damagingProjectileAPI.setCustomData("thquest_spawn_more_interval",new IntervalUtil(segSep,segSep));
        damagingProjectileAPI.setCustomData("thquest_spawn_more_amount",30);
        damagingProjectileAPI.setCustomData("thquest_followers", new ArrayList<DamagingProjectileAPI>());
        damagingProjectileAPI.setCustomData("thquest_angle_at_fire",damagingProjectileAPI.getFacing());
        damagingProjectileAPI.setCustomData("thquest_loc_at_fire",damagingProjectileAPI.getSpawnLocation());
        damagingProjectileAPI.setCustomData("thquest_x_record",new ArrayList<Float>());
        damagingProjectileAPI.setCustomData("thquest_y_record",new ArrayList<Float>());
        damagingProjectileAPI.setCustomData("thquest_times_for_velocity_record",new ArrayList<Float>());

        projectileList.add(damagingProjectileAPI);
    }
}