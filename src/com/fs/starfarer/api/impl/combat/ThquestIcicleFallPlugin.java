package com.fs.starfarer.api.impl.combat;

import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.combat.entities.BaseEntity;
import org.lwjgl.util.vector.Vector2f;
import thQuest.ThU;

import java.util.ArrayList;
import java.util.List;

public class ThquestIcicleFallPlugin implements EveryFrameWeaponEffectPlugin {
    public List<CombatEntityAPI> projs= new ArrayList<>();
    private final float DECEL_BASE = .5f;
    private final float SECOND_PHASE_LAUNCH_SPEED = 100f;
    @Override
    public void advance(float v, CombatEngineAPI combatEngineAPI, WeaponAPI weaponAPI) {
        for(int i =projs.size()-1;i>-1;i--){
            if(projs.get(i)==null){
                projs.remove(i);
                continue;
            }
            CombatEntityAPI proj = projs.get(i);
            //change speed based on uptime and iorder
            Vector2f velocity=((BaseEntity)proj).getVelocity();
            double rad= Math.toRadians(((BaseEntity)proj).getFacing());
            Vector2f reverseFacingVector = new Vector2f(-(float) Math.cos(rad), -(float) Math.sin(rad));
            Integer multiplier = (Integer) proj.getCustomData().get("thquest_iorder");
            multiplier=multiplier+1;
            ((BaseEntity)proj).setVel(new Vector2f(velocity.x+reverseFacingVector.x*DECEL_BASE*multiplier,velocity.y+reverseFacingVector.y*DECEL_BASE*multiplier));
            //if speed is too small, rotate and set speed.
            velocity=((BaseEntity)proj).getVelocity();

            Vector2f shipVelocity= (Vector2f) proj.getCustomData().get("thquest_ibasevelocity");
            Vector2f fvelocity =new Vector2f(velocity.x-shipVelocity.x,velocity.y-shipVelocity.y);
            if(fvelocity.getX()*reverseFacingVector.x>0||fvelocity.y*reverseFacingVector.y>0){
                if((Integer) proj.getCustomData().get("thquest_iside")==0){
                    proj.setFacing(proj.getFacing()-90);
                }else{
                    proj.setFacing(proj.getFacing()+90);
                }
                Vector2f facingVector = new Vector2f(ThU.angleVector(proj.getFacing()));
                ((BaseEntity) proj).setVel(new Vector2f(shipVelocity.x+facingVector.x*SECOND_PHASE_LAUNCH_SPEED,shipVelocity.y+facingVector.y*SECOND_PHASE_LAUNCH_SPEED));
                projs.remove(proj);
            }
        }

    }

}
