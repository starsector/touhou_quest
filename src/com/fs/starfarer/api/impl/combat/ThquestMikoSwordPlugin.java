package com.fs.starfarer.api.impl.combat;

import com.fs.starfarer.api.GameState;
import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.loading.WeaponGroupSpec;
import com.fs.starfarer.api.loading.WeaponGroupType;
import com.fs.starfarer.api.util.IntervalUtil;
import com.fs.starfarer.api.util.Misc;
import org.lazywizard.lazylib.VectorUtils;
import org.lwjgl.util.vector.Vector2f;
import thQuest.ThU;

import java.util.ArrayList;
import java.util.logging.Logger;

public class ThquestMikoSwordPlugin implements EveryFrameWeaponEffectPlugin {
    int cycle = -1;//before first additional laser
    final IntervalUtil TIME_BETWEEN_FRAMES = new IntervalUtil(0.15f,0.15f);
    ArrayList<ShipAPI> drones = new ArrayList<>();
    final int CYCLES = 6;
    Logger logger = Logger.getLogger(this.getClass().getName());
    final float ANGLE = 1f;
    final Vector2f POS_OFFSET= new Vector2f(-5,-5);
    final Vector2f OTHER_OFFSET = new Vector2f(-30,0);
//        final Vector2f POS_OFFSET= new Vector2f(-20,-20);
    boolean ready=true;

    //    @Override
//    public void advance(float v, CombatEngineAPI combatEngineAPI, BeamAPI beamAPI) {
//
//        //1.spawn drones on timer
//        //2. when their beam runs out, despawn them
//    }

    @Override
    public void advance(float v, CombatEngineAPI combatEngineAPI, WeaponAPI weaponAPI) {
//        if(Global.getCurrentState()== GameState.COMBAT){
            //drone behaviour
        if(Global.getCurrentState().equals(GameState.COMBAT)){//this is to stop horrible lag in the refit screen
            if(drones.isEmpty()){
//                logger.info("decided to create drones");
                createDrones(weaponAPI);
            }
        for(int i=0;i<CYCLES*2;i++){
//                logger.info("moving drone"+i);
                int position;
                Vector2f offset;
                if(i%2==0){
                    position= (i+2)/2+1;
                    offset=ThU.addVector(ThU.multiplyVector(POS_OFFSET,position), weaponAPI.getFirePoint(0));
                }else{
//                    position= (i+1)/2*-1;
                    position=(i+1)/2+1;
//                    position= (i+2)/2;
                    Vector2f offset2=POS_OFFSET;
                    offset2.setY(offset2.getY()*-1);//WHY DOES THIS WORK???
                    offset=ThU.addVector(ThU.multiplyVector(offset2,position), weaponAPI.getFirePoint(0));
                }
                ShipAPI drone = drones.get(i);
            Vector2f location;
                    location=VectorUtils.rotateAroundPivot(ThU.addVector(OTHER_OFFSET,offset), weaponAPI.getFirePoint(0),weaponAPI.getCurrAngle());
                     float angle=0f;
                     //TODO improve
                    if(ThU.isAngleRightRelitive(VectorUtils.getAngle(weaponAPI.getLocation(),drone.getLocation()),weaponAPI.getCurrAngle())){
//                         angle= weaponAPI.getCurrAngle()-position* ANGLE*-1;
                        angle= weaponAPI.getCurrAngle()-(position-1)* ANGLE;
                    }else{
                        angle= weaponAPI.getCurrAngle()+(position-1)* ANGLE;
                    }
                drone.getLocation().set(location);
                drone.setFacing(angle);
                drone.getVelocity().set(weaponAPI.getShip().getVelocity());
                drone.setAngularVelocity(weaponAPI.getShip().getAngularVelocity());
                Vector2f dir = Misc.getUnitVectorAtDegreeAngle(angle);
                dir.scale(1000.0F);
                Vector2f.add(dir, location, dir);
                drone.getMouseTarget().set(dir);
                WeaponAPI laser = (WeaponAPI)((WeaponGroupAPI)drone.getWeaponGroupsCopy().get(0)).getWeaponsCopy().get(0);
                laser.setFacing(angle);
                laser.setKeepBeamTargetWhileChargingDown(true);
                laser.setScaleBeamGlowBasedOnDamageEffectiveness(false);//?????
                laser.updateBeamFromPoints();
            }
            if(cycle==-1 && weaponAPI.isFiring()){
                //new cycle
                cycle=0;
            }if( cycle >=0 ){
                TIME_BETWEEN_FRAMES.advance(v);
                if(TIME_BETWEEN_FRAMES.intervalElapsed()){
                    //fire next drones
                    ShipAPI drone = drones.get(cycle*2);
                    Vector2f target=ThU.angleVector(drone.getFacing());
                    drone.giveCommand(ShipCommand.FIRE, target, 0);//fire at itself???
                    drone = drones.get(cycle*2+1);
                    drone.giveCommand(ShipCommand.FIRE, target, 0);//fire at itself???
                    cycle++;
                    if(cycle==CYCLES){
                        cycle=-1;
                    }
                }

            }
        }
    }
    public void createDrones(WeaponAPI weaponAPI){
        for(int i = 0; i<CYCLES*2;i++) {
//            logger.info("creating drone"+i);
            //cycles * 2
            ShipHullSpecAPI spec = Global.getSettings().getHullSpec("dem_drone");
            ShipVariantAPI v = Global.getSettings().createEmptyVariant("dem_drone", spec);
            v.addWeapon("WS 000", "thquest_star_sword_fake");
            WeaponGroupSpec g = new WeaponGroupSpec(WeaponGroupType.LINKED);
            g.addSlot("WS 000");
            v.addWeaponGroup(g);
            ShipAPI drone = Global.getCombatEngine().createFXDrone(v);
            drone.setLayer(CombatEngineLayers.ABOVE_SHIPS_AND_MISSILES_LAYER);
            drone.setOwner(weaponAPI.getShip().getOriginalOwner());
//        drone.getMutableStats().getBeamWeaponRangeBonus().modifyFlat("dem", this.targetingLaserRange);
            drone.getMutableStats().getHullDamageTakenMult().modifyMult("thquest_miko_sword", 0.0F);
            drone.setDrone(true);
            ShipAPI ship = weaponAPI.getShip();
            drone.getAIFlags().setFlag(ShipwideAIFlags.AIFlags.DRONE_MOTHERSHIP, 100000.0F, ship);
            drone.getMutableStats().getEnergyWeaponDamageMult().applyMods(ship.getMutableStats().getEnergyWeaponDamageMult());
            drone.getMutableStats().getEnergyWeaponRangeBonus().applyMods(ship.getMutableStats().getEnergyWeaponRangeBonus());
            drone.getMutableStats().getBeamWeaponRangeBonus().applyMods(ship.getMutableStats().getBeamWeaponRangeBonus());
            drone.getMutableStats().getBeamWeaponDamageMult().applyMods(ship.getMutableStats().getBeamWeaponDamageMult());
            drone.setCollisionClass(CollisionClass.NONE);
            drone.giveCommand(ShipCommand.SELECT_GROUP, (Object) null, 0);
            Global.getCombatEngine().addEntity(drone);
            drones.add(drone);
        }
    }
}
