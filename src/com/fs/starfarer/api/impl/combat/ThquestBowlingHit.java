package com.fs.starfarer.api.impl.combat;

import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.combat.listeners.ApplyDamageResultAPI;
import org.lwjgl.util.vector.Vector2f;

public class ThquestBowlingHit implements OnHitEffectPlugin {
    @Override
    public void onHit(DamagingProjectileAPI damagingProjectileAPI, CombatEntityAPI combatEntityAPI, Vector2f vector2f, boolean b, ApplyDamageResultAPI applyDamageResultAPI, CombatEngineAPI combatEngineAPI) {
        if(combatEntityAPI.getCollisionClass().equals(CollisionClass.FIGHTER)||combatEntityAPI.getCollisionClass().equals(CollisionClass.MISSILE_FF)||combatEntityAPI.getCollisionClass().equals(CollisionClass.MISSILE_NO_FF)){
            combatEngineAPI.applyDamage(combatEntityAPI,vector2f,25f,DamageType.KINETIC,0f,false,false,damagingProjectileAPI);
        }
    }
}