package com.fs.starfarer.api.impl.combat;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.*;
import org.lwjgl.util.vector.Vector2f;
import org.magiclib.util.MagicRender;
import thQuest.ThU;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class ThquestFireworks implements EveryFrameWeaponEffectPlugin, OnFireEffectPlugin {
    public List<MissileAPI> missiles = new ArrayList<>();
    private List<Vector2f> blueCircles = new ArrayList<>();
    private List<Vector2f> redCircles = new ArrayList<>();
    private List<Float> angles = new ArrayList<>();
    private float threshold = -1f;//disabled
    private float cycle = .2f;
    public ThquestFireworks() {
        blueCircles.add(new Vector2f(12.5f,20));
        blueCircles.add(new Vector2f(-12.5f,20));
        blueCircles.add(new Vector2f(12.5f,-20));
        blueCircles.add(new Vector2f(-12.5f,-20));
        redCircles.add(new Vector2f(25,0));
        redCircles.add(new Vector2f(-25,0));
        angles.add(0f);
        angles.add(45f);
        angles.add(90f);
        angles.add(135f);
        angles.add(180f);
        angles.add(225f);
        angles.add(270f);
        angles.add(315f);
    }

    @Override
    public void advance(float v, CombatEngineAPI combatEngineAPI, WeaponAPI weaponAPI) {
        for(int i = missiles.size()-1;i>-1;i--){
            MissileAPI m = missiles.get(i);
            if(m==null||m.isExpired()||m.isFizzling()||m.didDamage()){
                missiles.remove(missiles.get(i));
            }else{
            //projs here!
                for(Vector2f c : blueCircles){
                    MagicRender.singleframe(Global.getSettings().getSprite("misc","thquest_magic_circle"),ThU.addVector(m.getLocation(),c),new Vector2f(12,12),0f, Color.WHITE,true);
                }
                for(Vector2f c : redCircles){
                    MagicRender.singleframe(Global.getSettings().getSprite("misc","thquest_magic_circle"),ThU.addVector(m.getLocation(),c),new Vector2f(12,12),0f, Color.WHITE,true);
                }
                float carry = (float)m.getCustomData().get("thquest_carry");
                carry=carry+v;
                while(carry>cycle){
                    carry=carry-cycle;
                    Global.getSoundPlayer().playSound("thquest_tan00",1,.4f,m.getLocation(),m.getVelocity());
                    for(int j = 0;j<blueCircles.size();j++){
                    Vector2f c=blueCircles.get(j);
                        for(float f : angles){
                            if(m.getFlightTime()<threshold){
                                combatEngineAPI.spawnProjectile(m.getWeapon().getShip(), m.getWeapon(),"thquest_keine_blue",ThU.addVector(m.getLocation(),c),f,m.getVelocity());
                            }else{
                        combatEngineAPI.spawnProjectile(m.getWeapon().getShip(), m.getWeapon(),"thquest_keine_blue",ThU.addVector(m.getLocation(),c),f+((m.getFlightTime()-threshold)*900*(j%2*2-1)),m.getVelocity());
                            }
                        }
                    }
                    for(int j = 0;j<redCircles.size();j++){
                        Vector2f c=redCircles.get(j);
                        for(float f : angles){
                            if(m.getFlightTime()<threshold){
                                combatEngineAPI.spawnProjectile(m.getWeapon().getShip(), m.getWeapon(),"thquest_keine_red",ThU.addVector(m.getLocation(),c),f,m.getVelocity());
                            }else {
                                combatEngineAPI.spawnProjectile(m.getWeapon().getShip(), m.getWeapon(), "thquest_keine_red", ThU.addVector(m.getLocation(), c), f+((m.getFlightTime()-threshold)*900*(j%2*2-1)), m.getVelocity());
                            }
                        }
                    }

                }
                m.setCustomData("thquest_carry",carry);
                //MagicRender.singleframe(Global.getSettings().getSprite("misc","thquest_magic_circle"),m.getLocation(),new Vector2f(12,12),0f, Color.WHITE,true);
                //MagicRender.battlespace(Global.getSettings().getSprite("misc","thquest_magic_circle"),m.getLocation(),m.getVelocity(),);
            }
        }
    }

    @Override
    public void onFire(DamagingProjectileAPI damagingProjectileAPI, WeaponAPI weaponAPI, CombatEngineAPI combatEngineAPI) {
        damagingProjectileAPI.setCustomData("thquest_carry",0f);
        missiles.add((MissileAPI) damagingProjectileAPI);
    }}
