package com.fs.starfarer.api.impl.combat;

import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.DamagingProjectileAPI;
import com.fs.starfarer.api.combat.OnFireEffectPlugin;
import com.fs.starfarer.api.combat.WeaponAPI;
import org.lazywizard.lazylib.VectorUtils;
import org.lwjgl.util.vector.Vector2f;
import thQuest.ThU;

public class ThquestRemigunFire implements OnFireEffectPlugin {
    @Override
    public void onFire(DamagingProjectileAPI projectile, WeaponAPI weapon, CombatEngineAPI engine) {
        Vector2f shipSpeed=weapon.getShip().getVelocity();
        Vector2f projSpeed= new Vector2f(0,0);
        float angle = 0;
        float wangle = VectorUtils.getAngle(weapon.getFirePoint(0),projectile.getLocation());
        //bigger bullets
        for(int i=0;i<8;i++){
            angle= (float) ((Math.random()-.5)*(5+1.5*weapon.getCurrSpread()));
            //            engine.spawnProjectile(weapon.getShip(), weapon,"thquest_remi_gun_medium_red",weapon.getFirePoint(0), wangle+angle,projSpeed);
//            ThU.spawnProjectileModifySpeedAndApplyRefreshKey(engine,weapon,"thquest_remi_gun_medium_red",wangle+angle,(float) (Math.random()*15),90,weapon.getRange(),true,40);
            ThU.modifyRangeAndSpawnProjectile(engine,weapon,"thquest_remi_gun_medium_red",wangle+angle, (float) (1-(Math.random()-0.5)*0.1),true);
        }
        //smaller bullets
        for(int i=0;i<20;i++){
            angle= (float) ((Math.random()-.5)*(10+2*weapon.getCurrSpread()));
////            engine.spawnProjectile(weapon.getShip(), weapon,"thquest_remi_gun_small_red",weapon.getFirePoint(0),wangle+angle,projSpeed);
//            ThU.spawnProjectileModifySpeedAndApplyRefreshKey(engine,weapon,"thquest_remi_gun_small_red",wangle+angle,(float) (Math.random()*100),90,weapon.getRange(),true,50);
            ThU.modifyRangeAndSpawnProjectile(engine,weapon,"thquest_remi_gun_small_red",wangle+angle, (float) (1-(Math.random()-0.5)*0.1),true);
        }
    }
}
