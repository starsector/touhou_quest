package com.fs.starfarer.api.impl.combat;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.util.IntervalUtil;
import com.fs.starfarer.api.util.Misc;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.VectorUtils;
import org.lazywizard.lazylib.combat.AIUtils;
import org.lwjgl.util.vector.Vector2f;
import org.magiclib.util.MagicRender;
import thQuest.ThU;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class ThquestKillingDollPlugin implements EveryFrameWeaponEffectPlugin {
    public List<DamagingProjectileAPI> projs= new ArrayList<>();
    public List<Float> projTimes= new ArrayList<>();
    public List<Float> initialFacing= new ArrayList<>();
    public IntervalUtil shotInterval = new IntervalUtil(0.2f,0.2f);
    public float time;
    boolean blue = false;
    boolean start = false;
    float timeToSrartSpin = 1f;
    float spinSpeed = 100f;
    float slowdownAtSpinStart = 100f;
    float timeToLookStopSpinning = 2f;
    ShipAPI target;

    @Override
    public void advance(float v, CombatEngineAPI combatEngineAPI, WeaponAPI weaponAPI) {
         if(start && !combatEngineAPI.isPaused()){
            //1. spawn projectiles in an alternating  manner
            //2. move projectiles around
            //3. stop manipulating projectiles
//             v = v*weaponAPI.getShip().getMutableStats().getTimeMult().modified;
//             v = v * weaponAPI.getShip().getMutableStats().getTimeMult().modified;
            time += v;
            shotInterval.advance(v);
            if(shotInterval.intervalElapsed()){
                ShipAPI target = weaponAPI.getShip().getShipTarget();
                if(target==null){
                    target =AIUtils.getNearestEnemy(weaponAPI.getShip());
                }
                ShipAPI ship = weaponAPI.getShip();
                float targetAngle = ship.getFacing();
                     MagicRender.battlespace(ship.getSpriteAPI(),ship.getLocation(),ThU.multiplyVector(ship.getVelocity(),7),new Vector2f(37,66),new Vector2f(), targetAngle,ship.getAngularVelocity(),new Color(255,255,255,255),false,0.05f,0.1f,0.05f);
//                ship.addAfterimage(Color.WHITE,20,20,10,10,0,1,1,1,true,true,true);
                spawnProj(combatEngineAPI,weaponAPI);
            }
            List<Integer> ints = new ArrayList<>();
            for(int i = 0; i<projs.size();i++){
                DamagingProjectileAPI proj =projs.get(i);
                proj.getVelocity().set(ThU.modifyVector(proj.getVelocity(),1-v));
            }
            if(time>5f){
                if(target!=null) {
                    for (int i = 0; i < projs.size(); i++) {
                        DamagingProjectileAPI proj = projs.get(i);
                        float angle = Misc.getAngleInDegrees(proj.getLocation(),target.getLocation());
                        proj.setFacing(angle);
                        proj.getVelocity().set(ThU.multiplyVector(ThU.angleVector(angle),2000));
                    }
                }
                start=false;
                projs= new ArrayList<>();
                projTimes=new ArrayList<>();
                }
//            for(int i = 0;i<projs.size();i++){
//                DamagingProjectileAPI proj =projs.get(i);
//                Vector2f location = proj.getLocation();
//                float facing = proj.getFacing();
//                projTimes.set(i,projTimes.get(i)+v);
//                if(projTimes.get(i)>timeToLookStopSpinning){
////                    float distanceToTargetAngle = ThU.angleDiff(facing,)
//                //todo do smooth slowdwn?
//                    float angleToTarget = facing;
//                    if(target!=null) {
//                        angleToTarget = VectorUtils.getAngle(location, target.getLocation());//todo null pointer exception can be here
//                    }
//                    //if angle is between what it is and where it will be next frame, set the angle
//                    if(angleToTarget>facing && angleToTarget<facing+v*spinSpeed || ((facing+v*spinSpeed>360) && angleToTarget>facing && angleToTarget+360<(facing+v*spinSpeed))){
//                    proj.setFacing(angleToTarget);
//                        ints.add(i);
//                        break;
//                    }else{
//                        proj.setFacing(proj.getFacing()+spinSpeed*v);
//                    }
//                }else if (projTimes.get(i)>timeToSrartSpin){
//                    proj.setFacing(proj.getFacing()+spinSpeed*v);
//                }
//                //do movement
//                float speed = proj.getMoveSpeed();
//                Float angle = initialFacing.get(i);
//                Vector2f vector = ThU.modifyVector(ThU.angleVector(angle),speed);
//                location.set(ThU.addVector(vector,location));
//
//            }
        }
    }
    public void start(ShipAPI shipAPI){
        projs= new ArrayList<>();
        start = true;
        time = 0f;
        target = shipAPI.getShipTarget();
        if(target==null){
            target = AIUtils.getNearestEnemy(shipAPI);
        }
    }
    public void spawnProj(CombatEngineAPI combatEngineAPI, WeaponAPI weaponAPI){
        String weaponId;
        if(blue){
            weaponId = "thquest_big_knife_blue";
        }else{
            weaponId = "thquest_big_knife_purple";
        }
        blue = !blue;
        float angle = time *360;
        weaponAPI.setCurrAngle(angle);
        projs.add((DamagingProjectileAPI) combatEngineAPI.spawnProjectile(weaponAPI.getShip(),weaponAPI,weaponId,weaponAPI.getFirePoint(0),angle,weaponAPI.getShip().getVelocity()));
        projs.add((DamagingProjectileAPI) combatEngineAPI.spawnProjectile(weaponAPI.getShip(),weaponAPI,weaponId,weaponAPI.getFirePoint(1),angle,weaponAPI.getShip().getVelocity()));
        projTimes.add(0f);
        projTimes.add(0f);
        initialFacing.add(angle);
        initialFacing.add(angle);
    }
}
