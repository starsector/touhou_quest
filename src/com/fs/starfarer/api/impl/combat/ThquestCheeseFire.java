package com.fs.starfarer.api.impl.combat;

import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.DamagingProjectileAPI;
import com.fs.starfarer.api.combat.OnFireEffectPlugin;
import com.fs.starfarer.api.combat.WeaponAPI;

public class ThquestCheeseFire implements OnFireEffectPlugin {
    @Override
    public void onFire(DamagingProjectileAPI damagingProjectileAPI, WeaponAPI weaponAPI, CombatEngineAPI combatEngineAPI) {
        combatEngineAPI.getCustomData().put("thquest_cheese_gun_active",true);
        ((ThquestCheesePlugin2)(weaponAPI.getEffectPlugin())).addProj(damagingProjectileAPI);
    }
}
