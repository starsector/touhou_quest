package com.fs.starfarer.api.impl.combat;

import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.loading.ProjectileSpecAPI;
import com.fs.starfarer.api.util.Misc;
import com.fs.starfarer.combat.entities.BaseEntity;
import org.lazywizard.lazylib.VectorUtils;
import org.lazywizard.lazylib.combat.AIUtils;
import org.lwjgl.util.vector.Vector2f;
import thQuest.ThU;

import java.util.ArrayList;
import java.util.List;

public class ThquestYyorbPlugin implements EveryFrameWeaponEffectPlugin {
    List<DamagingProjectileAPI> projs = new ArrayList<>();
    CombatEntityAPI target;
    @Override
    public void advance(float v, CombatEngineAPI combatEngineAPI, WeaponAPI weaponAPI) {
        float topSpeed=weaponAPI.getProjectileSpeed();
        float speedupDistance=topSpeed/2;
        float bonusMag=topSpeed/4;
        float baseMag=topSpeed/20;

        for(int i =projs.size()-1;i>-1;i--){
            if(projs.get(i)==null){
                projs.remove(i);
                continue;
            }
            CombatEntityAPI proj = projs.get(i);
            CombatEntityAPI target = (CombatEntityAPI) proj.getCustomData().get("thquest_target");
            if(target instanceof ShipAPI){
                if(!((ShipAPI)target).isAlive()){
                    target=null;
                }
            }
            if(target==null){
                ShipAPI launchingShip = (ShipAPI) proj.getCustomData().get("thquest_parent");
                target=launchingShip.getShipTarget();
                if(target==null||target.getOwner()==launchingShip.getOwner()){
                    target= AIUtils.getNearestEnemy(proj);
                    if(target==null){
                        return;
                    }
                }
            }
            proj.setCustomData("thquest_target",target);
            //target should not be null
            float distance = Misc.getDistance(proj.getLocation(),target.getLocation());
            float magnitude =(speedupDistance-distance)/speedupDistance;
            if(magnitude<0){
                magnitude=0;
            }
            //1. base acceleration + magnitude bonus towards target
            float targetAngle = VectorUtils.getAngle(proj.getLocation(),target.getLocation());
            //proj.setFacing(targetAngle);
            Vector2f targetVector =ThU.angleVector(targetAngle);
            Vector2f velocity = proj.getVelocity();
            ((BaseEntity)proj).setVel(new Vector2f(velocity.x+targetVector.getX()*(magnitude*bonusMag+baseMag),velocity.y+targetVector.getY()*(magnitude*bonusMag+baseMag)));
            velocity = proj.getVelocity();
            float speed =ThU.vectorToSpeed(velocity);
            if(speed>topSpeed){
                magnitude = topSpeed/speed;
                ((BaseEntity)proj).setVel(new Vector2f(velocity.x*magnitude,velocity.y*magnitude));
            }
            proj.setFacing(VectorUtils.getFacing(proj.getVelocity()));
        }
    }
    public void addProj(DamagingProjectileAPI projectileAPI){
        projs.add(projectileAPI);
    }
}
