package com.fs.starfarer.api.impl.combat;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.combat.listeners.ApplyDamageResultAPI;
import org.magiclib.plugins.MagicTrailPlugin;
import org.apache.log4j.Logger;
import org.lazywizard.lazylib.VectorUtils;
import org.lwjgl.util.vector.Vector2f;
import thQuest.Log;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ThquestVengefulSpiritPlugin implements OnFireEffectPlugin, EveryFrameWeaponEffectPlugin {
    ArrayList<MissileAPI> missiles = new ArrayList<>();
    ArrayList<Float> timers = new ArrayList<>();

    Map<MissileAPI,Float> key = new HashMap<>();
    float countdown = 1.0f;
    @Override
    public void advance(float v, CombatEngineAPI combatEngineAPI, WeaponAPI weaponAPI) {
        for(int i =missiles.size()-1;i>-1;i--){
            if(!missiles.get(i).equals(null)){
                if(missiles.get(i).isFading()||missiles.get(i).isExpired()||missiles.get(i).isFizzling()){
                    //destroyed or timed out
                    revengeBullets(missiles.get(i),combatEngineAPI);
                    missiles.remove(i);
                    timers.remove(i);
                    continue;
                }
                //1. draw effect
                drawFire(missiles.get(i));
                //2.shoot
                timers.set(i,timers.get(i)-v);
                timers.set(i,shoot(missiles.get(i),timers.get(i),combatEngineAPI));
                //3. if it exploded, spawn bullets
            }else{

            }

        }
    }

    @Override
    public void onFire(DamagingProjectileAPI damagingProjectileAPI, WeaponAPI weaponAPI, CombatEngineAPI combatEngineAPI) {
        missiles.add((MissileAPI) damagingProjectileAPI);
        key.put((MissileAPI) damagingProjectileAPI, MagicTrailPlugin.getUniqueID());
        timers.add(new Float(countdown));
    }
    public void revengeBullets(MissileAPI m,CombatEngineAPI combatEngineAPI){
        for(int i=0;i<16;i++){
            combatEngineAPI.spawnProjectile(m.getSourceAPI(),m.getWeapon(),"thquest_vengeful_spirit_1",m.getLocation(), (float) (m.getFacing()+22.5*i),m.getVelocity());
            combatEngineAPI.spawnProjectile(m.getSourceAPI(),m.getWeapon(),"thquest_vengeful_spirit_2",m.getLocation(), (float) (m.getFacing()+22.5*i),m.getVelocity());
            combatEngineAPI.spawnProjectile(m.getSourceAPI(),m.getWeapon(),"thquest_vengeful_spirit_3",m.getLocation(), (float) (m.getFacing()+22.5*i),m.getVelocity());
        }
        Global.getSoundPlayer().playSound("thquest_tan00", (float) 0.3,0.3f,m.getLocation(),m.getVelocity());
    }
    public float shoot(MissileAPI m,float timer,CombatEngineAPI combatEngineAPI){
        while(timer<0){
            timer=timer+countdown;
            for(int i=0;i<16;i++){
                combatEngineAPI.spawnProjectile(m.getSourceAPI(),m.getWeapon(),"thquest_vengeful_spirit_1",m.getLocation(), (float) (m.getFacing()+22.5*i),m.getVelocity());
            }
            Global.getSoundPlayer().playSound("thquest_tan00", (float) 0.3,0.3f,m.getLocation(),m.getVelocity());
        }
        return timer;
    }
    public void drawFire(MissileAPI m){
        double rad= Math.toRadians(m.getFacing());
        Vector2f forwardFacingVector = new Vector2f((float) Math.cos(rad), (float) Math.sin(rad));
        Vector2f velocity = m.getVelocity();
        Vector2f finalVector = new Vector2f((float) ((Math.random()-.5)*25+forwardFacingVector.getX()*100+velocity.getX()), (float) ((Math.random()-.5)*25+forwardFacingVector.getY()*100+velocity.getY()));
        MagicTrailPlugin.addTrailMemberAdvanced(
                m,
                key.get(m),
                Global.getSettings().getSprite("base_trail_smoke"),
                m.getLocation(),
                0f,
                0f,
                //VectorUtils.getAngleStrict(finalVector,ship.getVelocity()),
                VectorUtils.getAngle(finalVector,m.getVelocity()),
                //ship.getFacing(),
                .3f,
                .9f, //if too high, causes sprite to fold in on itself
                8f,//start size
                0f,//end size
                Color.blue,
                Color.white,
                1f,
                0f,
                .05f,
                .1f,
                true,
                -1f,
                0f,
                (float) Math.random()*20,//texture offset
                finalVector,//offset vilocity
                null, //advanced options
                CombatEngineLayers.ABOVE_SHIPS_AND_MISSILES_LAYER,
                0f
        );
    }

}
