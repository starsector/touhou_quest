package com.fs.starfarer.api.impl.combat;

import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.combat.listeners.ApplyDamageResultAPI;
import org.lwjgl.util.vector.Vector2f;

public class ThquestZanmuHit implements OnHitEffectPlugin {
    @Override
    public void onHit(DamagingProjectileAPI damagingProjectileAPI, CombatEntityAPI combatEntityAPI, Vector2f vector2f, boolean b, ApplyDamageResultAPI applyDamageResultAPI, CombatEngineAPI combatEngineAPI) {
        if(b){
            damagingProjectileAPI.setDamageAmount(1f);
        }
    }
}
