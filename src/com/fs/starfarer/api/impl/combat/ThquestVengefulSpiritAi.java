package data.scripts.ai;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.CombatEntityAPI;
import com.fs.starfarer.api.combat.GuidedMissileAI;
import com.fs.starfarer.api.combat.MissileAIPlugin;
import com.fs.starfarer.api.combat.MissileAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipCommand;
import data.scripts.util.MagicTargeting;
import data.scripts.util.MagicTargeting.targetSeeking;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.VectorUtils;
import org.lazywizard.lazylib.combat.AIUtils;
import org.lwjgl.util.vector.Vector2f;

public class ThquestVengefulSpiritAi implements MissileAIPlugin, GuidedMissileAI {
    private final float OVERSHOT_ANGLE = 60.0F;
    private final float WAVE_TIME = 2.0F;
    private final float WAVE_AMPLITUDE = 15.0F;
    private final float DAMPING = 0.1F;
    private final boolean OVERSTEER = false;
    private final boolean TARGET_SWITCH = true;
    private final MagicTargeting.targetSeeking seeking;
    private final int fighters;
    private final int frigates;
    private final int destroyers;
    private final int cruisers;
    private final int capitals;
    private final int SEARCH_CONE;
    private final int MAX_SEARCH_RANGE;
    private final boolean FAILSAFE;
    private float PRECISION_RANGE;
    private final boolean LEADING;
    private float ECCM;
    private final float MAX_SPEED;
    private final float OFFSET;
    private CombatEngineAPI engine;
    private final MissileAPI MISSILE;
    private CombatEntityAPI target;
    private Vector2f lead;
    private boolean launch;
    private float timer;
    private float check;
    private ShipAPI launchingship;

    public ThquestVengefulSpiritAi(MissileAPI missile, ShipAPI launchingShip) {
        this.launchingship = launchingShip;
        this.seeking = targetSeeking.FULL_RANDOM;
        this.fighters = 4;
        this.frigates = 3;
        this.destroyers = 2;
        this.cruisers = 1;
        this.capitals = 0;
        this.SEARCH_CONE = 360;
        this.MAX_SEARCH_RANGE = 1500;
        this.FAILSAFE = false;
        this.PRECISION_RANGE = 500.0F;
        this.LEADING = true;
        this.ECCM = 3.0F;
        this.lead = new Vector2f();
        this.launch = true;
        this.timer = 0.0F;
        this.check = 0.0F;
        this.MISSILE = missile;
        this.MAX_SPEED = missile.getMaxSpeed();
        if (missile.getSource().getVariant().getHullMods().contains("eccm")) {
            this.ECCM = 1.0F;
        }

        this.PRECISION_RANGE = (float)Math.pow((double)(2.0F * this.PRECISION_RANGE), 2.0);
        this.OFFSET = (float)(Math.random() * 3.1415927410125732 * 2.0);
    }

    public void advance(float amount) {
        if (this.engine != Global.getCombatEngine()) {
            this.engine = Global.getCombatEngine();
        }

        if (!Global.getCombatEngine().isPaused() && !this.MISSILE.isFading() && !this.MISSILE.isFizzling()) {
            if (this.target != null && (!(this.target instanceof ShipAPI) || ((ShipAPI)this.target).isAlive()) && this.engine.isEntityInPlay(this.target)) {
                this.timer += amount;
                if (this.launch || this.timer >= this.check) {
                    this.launch = false;
                    this.timer -= this.check;
                    this.check = Math.min(0.25F, Math.max(0.05F, MathUtils.getDistanceSquared(this.MISSILE.getLocation(), this.target.getLocation()) / this.PRECISION_RANGE));
                    this.lead = AIUtils.getBestInterceptPoint(this.MISSILE.getLocation(), this.MAX_SPEED * this.ECCM, this.target.getLocation(), this.target.getVelocity());
                    if (this.lead == null) {
                        this.lead = this.target.getLocation();
                    }
                    //changing lead into something completely incorrect for intercept

                }

                float correctAngle = VectorUtils.getAngle(this.MISSILE.getLocation(), this.lead);
                if(this.getClass().hashCode()%2==0){
                    correctAngle=correctAngle+10;
                }else{
                    correctAngle=correctAngle-10;
                }

                float aimAngle = 1.0F;
                if (this.ECCM <= 1.0F) {
                    aimAngle = 0.3F;
                }

                correctAngle = (float)((double)correctAngle + (double)(aimAngle * 15.0F * this.check) * Math.cos((double)(this.OFFSET + this.MISSILE.getElapsed() * 3.1415927F)));
                aimAngle = MathUtils.getShortestRotation(this.MISSILE.getFacing(), correctAngle);
                if (Math.abs(aimAngle) < 60.0F) {
                    this.MISSILE.giveCommand(ShipCommand.ACCELERATE);
                }

                if (aimAngle < 0.0F) {
                    this.MISSILE.giveCommand(ShipCommand.TURN_RIGHT);
                } else {
                    this.MISSILE.giveCommand(ShipCommand.TURN_LEFT);
                }

                if (Math.abs(aimAngle) < Math.abs(this.MISSILE.getAngularVelocity()) * 0.1F) {
                    this.MISSILE.setAngularVelocity(aimAngle / 0.1F);
                }

            } else {
                if(this.launchingship!=null&&this.launchingship.getShipTarget()!=null&&launchingship.getShipTarget().getOwner()!=launchingship.getOwner()){
                    this.setTarget(this.launchingship.getShipTarget());
                }else {
                    this.setTarget(MagicTargeting.pickTarget(this.MISSILE, this.seeking, 1500, 360, 0, 1, 2, 3, 4, false));
                }
                this.MISSILE.giveCommand(ShipCommand.ACCELERATE);
            }
        }
    }

    public CombatEntityAPI getTarget() {
        return this.target;
    }

    public void setTarget(CombatEntityAPI target) {
        this.target = target;
    }

    public void init(CombatEngineAPI engine) {
    }
}
