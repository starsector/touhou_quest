package com.fs.starfarer.api.impl.combat;

import com.fs.starfarer.api.combat.*;

public class ThquestFlowerTankGunFire implements OnFireEffectPlugin {
    float speedMultForSecondWave = 0.7f;
    float speedMultForSecondary = 0.9f;
    float angleClusters = 30f;
    float angleSecondary = 10f;
    @Override
    public void onFire(DamagingProjectileAPI damagingProjectileAPI, WeaponAPI weaponAPI, CombatEngineAPI combatEngineAPI) {
        //TODO spawn projectiles from thquest_flower_tank_gun_fake
        //60 degree angles,
        //4 groups of 3. One in front and two behind to the left and right. Two groups per side directly behind each other
        float angle = weaponAPI.getCurrAngle();
        //first wave
        spawnSeries(combatEngineAPI, weaponAPI, angle + angleClusters, 1f);
        spawnSeries(combatEngineAPI, weaponAPI, angle - angleClusters, 1f);
        //second wave
        spawnSeries(combatEngineAPI, weaponAPI, angle + angleClusters, speedMultForSecondWave);
        spawnSeries(combatEngineAPI, weaponAPI, angle - angleClusters, speedMultForSecondWave);
    }
    public void spawnSeries(CombatEngineAPI combatEngineAPI,WeaponAPI weaponAPI, float angle,float speedMult){
        spawnBullet(combatEngineAPI,weaponAPI,angle,speedMult);
        spawnBullet(combatEngineAPI,weaponAPI,angle + angleSecondary,speedMult * speedMultForSecondary);
        spawnBullet(combatEngineAPI,weaponAPI,angle - angleSecondary,speedMult  *speedMultForSecondary);
    }
    public void spawnBullet(CombatEngineAPI combatEngine,WeaponAPI weaponAPI, float angle,float speedMult){
        ShipAPI ship = weaponAPI.getShip();
        ship.getMutableStats().getBallisticProjectileSpeedMult().modifyMult(this.getClass().getName(),speedMult);
        combatEngine.spawnProjectile(ship,weaponAPI,"thquest_flower_tank_gun_fake",weaponAPI.getFirePoint(0),angle,ship.getVelocity());
        ship.getMutableStats().getBallisticProjectileSpeedMult().unmodify(this.getClass().getName());
    }
}
