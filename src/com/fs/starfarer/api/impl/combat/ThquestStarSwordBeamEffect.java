package com.fs.starfarer.api.impl.combat;

import com.fs.starfarer.api.combat.BeamAPI;
import com.fs.starfarer.api.combat.BeamEffectPlugin;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.CombatEntityAPI;
import org.lazywizard.lazylib.VectorUtils;
import org.lwjgl.util.vector.Vector2f;
import thQuest.ThU;

public class ThquestStarSwordBeamEffect implements BeamEffectPlugin {
    private final float PUSHFORCE =30000f;
    @Override
    public void advance(float v, CombatEngineAPI combatEngineAPI, BeamAPI beam) {
        CombatEntityAPI target = beam.getDamageTarget();
        if (target != null) {
            Vector2f targetVector = target.getVelocity();
            Vector2f pushVector = ThU.angleVector(VectorUtils.getAngle(beam.getFrom(), beam.getTo()));
            pushVector = ThU.multiplyVector(pushVector, PUSHFORCE * v/target.getMass());
            pushVector = ThU.addVector(targetVector, pushVector);
            target.getVelocity().set(pushVector.getX(), pushVector.getY());
        }
    }
}
