package com.fs.starfarer.api.impl.combat;

import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.EveryFrameWeaponEffectPlugin;
import com.fs.starfarer.api.combat.WeaponAPI;

public class ThquestShinkiGunPlugin implements EveryFrameWeaponEffectPlugin {
    float arrowAngle=0f;
    boolean arrowSeekDirection=true;
    float arrowSeekSpeed=10f;
    float arrowShootSpeed=.1f;
    float arrowCarry=0f;

    float beamCarry=0f;
    float beamAngle=0f;
    boolean beamSeekDirection=true;
    float beamSeekSpeed=4f;

    float maxAngle =10f;

    float arrowAngleAppart=60f;

    boolean reversed=false;
    @Override
    public void advance(float v, CombatEngineAPI engine, WeaponAPI weapon) {
    if(weapon.getChargeLevel()==0f){
        return;
    }
    float arcFacing=weapon.getShip().getFacing();//anchored to ship facing

    if(weapon.getSlot().getId().equals("WS0011")||weapon.getSlot().getId().equals("WS0014")){
        reversed=true;
    }
    //seek arrows
    if(reversed ^ arrowSeekDirection){
        //"forwards"
        arrowAngle=arrowAngle+arrowSeekSpeed*v;
        if(arrowAngle>maxAngle){
            arrowSeekDirection=!arrowSeekDirection;
            arrowAngle=arrowAngle-(arrowAngle-maxAngle);
        }
    }else{
        //"backwards"
        arrowAngle=arrowAngle-arrowSeekSpeed*v;
        if(arrowAngle<-maxAngle){
            arrowSeekDirection=!arrowSeekDirection;
            arrowAngle=arrowAngle+(arrowAngle+maxAngle);
        }
    }
    //seek turret

    if(reversed ^ beamSeekDirection){
        //"forwards"
        beamAngle=beamAngle+beamSeekSpeed*v;
        if(beamAngle>maxAngle){
            beamSeekDirection=!beamSeekDirection;
            beamAngle=beamAngle-(beamAngle-maxAngle);
        }
    }else{
        //"backwards"
        beamAngle=beamAngle-beamSeekSpeed*v;
        if(beamAngle<-maxAngle){
            beamSeekDirection=!beamSeekDirection;
            beamAngle=beamAngle+(beamAngle+maxAngle);
        }

    }
    //rotate turret
    weapon.setCurrAngle(arcFacing+beamAngle);

        //spawn arrows
    while(arrowCarry+v>arrowShootSpeed){
        arrowCarry=arrowCarry-arrowShootSpeed;
        engine.spawnProjectile(weapon.getShip(), weapon,"thquest_shinki_fake",weapon.getFirePoint(0),arrowAngle+arcFacing,weapon.getShip().getVelocity());
        engine.spawnProjectile(weapon.getShip(), weapon,"thquest_shinki_fake",weapon.getFirePoint(0),arrowAngle+arrowAngleAppart+arcFacing,weapon.getShip().getVelocity());
        engine.spawnProjectile(weapon.getShip(), weapon,"thquest_shinki_fake",weapon.getFirePoint(0),arrowAngle-arrowAngleAppart+arcFacing,weapon.getShip().getVelocity());
    }
    arrowCarry=arrowCarry+v;


    }
}
