package com.fs.starfarer.api.impl.combat;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.loading.DamagingExplosionSpec;
import com.fs.starfarer.api.loading.ShotBehaviorSpecAPI;
import com.fs.starfarer.combat.E.OoOO;
import com.fs.starfarer.combat.ai.ProximityFuseAI;
import com.fs.starfarer.combat.entities.Missile;
import com.fs.starfarer.combat.entities.Ship;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.lazywizard.lazylib.combat.AIUtils;
import org.lwjgl.util.vector.Vector2f;

public class ThquestMagicMissileAi implements ProximityFuseAIAPI, MissileAIPlugin{
    private float range;
    private float vsMissileRange;
    private DamagingExplosionSpec explosionSpec;
    private static Logger log = Logger.getLogger(ProximityFuseAI.class);
    private boolean slowToMaxSpeed = false;
    ShipAPI launchingship;

    Missile projectile;
    ShotBehaviorSpecAPI spec;
    public ThquestMagicMissileAi(Missile m, ShotBehaviorSpecAPI behaviorSpec,ShipAPI launchingship) {
        this.projectile = m;
        this.spec = behaviorSpec;
        this.launchingship = launchingship;
    }

    @Override
    public void advance(float v) {
        projectile.giveCommand(ShipCommand.ACCELERATE);

        //removed :
        //s.getCollisionClass().equals(CollisionClass.SHIP)||
        //to directly hit ships.
        for(ShipAPI s: AIUtils.getNearbyEnemies(projectile,range)){
            if(s.getCollisionClass().equals(CollisionClass.FIGHTER)){
                explode();
            }
        }
        for(MissileAPI m :AIUtils.getNearbyEnemyMissiles(projectile,vsMissileRange)){
            if(m.getCollisionClass()!=CollisionClass.NONE){
                explode();
            }
        }

        }

    @Override
    public void updateDamage() {
        try {
            this.slowToMaxSpeed = this.spec.getParams().optBoolean("slowToMaxSpeed", false);
            this.range = (float)this.spec.getParams().getDouble("range");
            if (this.spec.getParams().has("vsMissileRange")) {
                this.vsMissileRange = (float)this.spec.getParams().getDouble("vsMissileRange");
            } else {
                this.vsMissileRange = this.range;
            }

            if (this.spec.getParams().has("explosionSpec")) {
                this.explosionSpec = DamagingExplosionSpec.loadFromJSON(this.spec.getParams().getJSONObject("explosionSpec"));
                this.explosionSpec.setDamageType(this.projectile.getDamageType());
                this.explosionSpec.setMaxDamage(this.projectile.getDamageAmount());
                this.explosionSpec.setMinDamage(this.projectile.getDamageAmount()/ 2.0F);
                this.explosionSpec.setEffect(this.spec.getOnHitEffect());
            }
        } catch (JSONException var2) {
            log.error(var2, var2);
        }

    }
    public void explode(){
//        Global.getCombatEngine().spawnDamagingExplosion(explosionSpec,launchingship,projectile.getLocation());
        projectile.explode();//todo is this necessary???
        Global.getCombatEngine().removeEntity(projectile);
    }
}
