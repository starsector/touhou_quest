package com.fs.starfarer.api.impl.combat;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.combat.ai.missile.MissileAI;
import com.fs.starfarer.combat.entities.Missile;
import data.scripts.ai.ThquestFantasySealAI2;
import thQuest.Log;

import java.util.ArrayList;
import java.util.List;

public class ThquestFantasySealPlugin implements EveryFrameWeaponEffectPlugin {
    public List<MissileAPI> missiles = new ArrayList<>();
    private float limit = 3f;
    private float counter = 0f;
    public float lastFired =0f;
    @Override
    public void advance(float v, CombatEngineAPI combatEngineAPI, WeaponAPI weaponAPI) {
        if (missiles.size() == 0) {
            counter=0f;
            return;
        }
        counter += v;

        if (counter > limit) {
            counter-=.2;
            //Log.getLogger().info("setting new AI for missiles");
            MissileAPI m = missiles.get((int) (Math.random()*missiles.size()-1));
            m.setMissileAI(new ThquestFantasySealAI2((Missile) m,weaponAPI.getShip()));
            missiles.remove(m);
            m.setMaxFlightTime(m.getFlightTime()+4.8f);

        }
    }
}
