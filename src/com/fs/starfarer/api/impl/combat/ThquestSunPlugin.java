package com.fs.starfarer.api.impl.combat;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.input.InputEventAPI;
import com.fs.starfarer.api.loading.ProjectileSpecAPI;
import com.fs.starfarer.combat.CombatEngine;
import com.fs.starfarer.combat.entities.BallisticProjectile;
import com.fs.starfarer.combat.entities.BaseEntity;
import org.lazywizard.lazylib.combat.CombatUtils;
import org.lwjgl.util.vector.Vector2f;
import thQuest.Log;
import thQuest.ThU;

import java.util.List;

public class ThquestSunPlugin implements EveryFrameWeaponEffectPlugin {
    float countdown=0;
    float angle = 0;
    float cycleTime = 0.005f; // one cycle spawns 3 projectiles
    float carry =0;
    Vector2f b1=new Vector2f(0,0);
    Vector2f b2=new Vector2f(0,0);
    Vector2f b3=new Vector2f(0,0);

    public void startScript(){
        countdown=3;
    }
    @Override
    public void advance(float amount, CombatEngineAPI engine, WeaponAPI weapon) {
        if(countdown>0){
            countdown=countdown-amount;
            Vector2f speed=weapon.getShip().getVelocity();
            while(amount+carry>cycleTime){
                b1.set((float) (speed.x+(Math.random()-.5)*28), (float) (speed.y+(Math.random()-.5)*28));
                b2.set((float) (speed.x+(Math.random()-.5)*28), (float) (speed.y+(Math.random()-.5)*28));
                b3.set((float) (speed.x+(Math.random()-.5)*28), (float) (speed.y+(Math.random()-.5)*28));
//                DamagingProjectileAPI p= (DamagingProjectileAPI) engine.spawnProjectile(weapon.getShip(), weapon,"thquest_blue_medium",weapon.getFirePoint(0),countdown*180+(float) Math.random()*15,weapon.getShip().getVelocity());
//                ThU.modifySpeedAndApplyRefreshKey(p, (float) (Math.random()*100),180,weapon.getRange());
                ThU.modifyRangeAndSpawnProjectile(engine,weapon,"thquest_blue_medium",countdown*180+(float) (Math.random()-0.5)*10, 1-(float) ((Math.random()-0.5)*0.2),false);
                ThU.modifyRangeAndSpawnProjectile(engine,weapon,"thquest_blue_medium",countdown*180+120+(float) (Math.random()-0.5)*10, 1-(float) ((Math.random()-0.5)*0.2),false);
                ThU.modifyRangeAndSpawnProjectile(engine,weapon,"thquest_blue_medium",countdown*180+240+(float) (Math.random()-0.5)*10, 1-(float) ((Math.random()-0.5)*0.2),false);

                //                p= (DamagingProjectileAPI)engine.spawnProjectile(weapon.getShip(), weapon,"thquest_blue_medium",weapon.getFirePoint(0),countdown*180+120+(float) Math.random()*15,weapon.getShip().getVelocity());
//                ThU.modifySpeedAndApplyRefreshKey(p, (float) (Math.random()*100),180,weapon.getRange());
//                p= (DamagingProjectileAPI)engine.spawnProjectile(weapon.getShip(), weapon,"thquest_blue_medium",weapon.getFirePoint(0),countdown*180+240+(float) Math.random()*15,weapon.getShip().getVelocity());
//                ThU.modifySpeedAndApplyRefreshKey(p, (float) (Math.random()*100),180,weapon.getRange());
                Global.getSoundPlayer().playSound("thquest_tan00",1,.2f,weapon.getFirePoint(0),speed);
                amount= (float) (amount-cycleTime);
            }
            carry=amount+carry;
        }
    }
}
