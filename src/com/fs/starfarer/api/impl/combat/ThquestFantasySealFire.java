package com.fs.starfarer.api.impl.combat;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.*;
import org.lazywizard.lazylib.VectorUtils;

import java.util.ArrayList;
import java.util.List;

public class ThquestFantasySealFire implements OnFireEffectPlugin {
    @Override
    public void onFire(DamagingProjectileAPI damagingProjectileAPI, WeaponAPI weaponAPI, CombatEngineAPI combatEngineAPI) {
        if(combatEngineAPI.getTotalElapsedTime(false) != ((ThquestFantasySealPlugin)weaponAPI.getEffectPlugin()).lastFired){
            List<MissileAPI> missiles = new ArrayList<>();
            missiles.add((MissileAPI) combatEngineAPI.spawnProjectile(weaponAPI.getShip(), weaponAPI,"thquest_fantasy_seal_fake_red",weaponAPI.getFirePoint(0),weaponAPI.getCurrAngle(),weaponAPI.getShip().getVelocity()));
            missiles.add((MissileAPI) combatEngineAPI.spawnProjectile(weaponAPI.getShip(), weaponAPI,"thquest_fantasy_seal_fake_blue",weaponAPI.getFirePoint(2),weaponAPI.getCurrAngle()+45,weaponAPI.getShip().getVelocity()));
            missiles.add((MissileAPI) combatEngineAPI.spawnProjectile(weaponAPI.getShip(), weaponAPI,"thquest_fantasy_seal_fake_green",weaponAPI.getFirePoint(4),weaponAPI.getCurrAngle()+90,weaponAPI.getShip().getVelocity()));
            missiles.add((MissileAPI) combatEngineAPI.spawnProjectile(weaponAPI.getShip(), weaponAPI,"thquest_fantasy_seal_fake_yellow",weaponAPI.getFirePoint(6),weaponAPI.getCurrAngle()+135,weaponAPI.getShip().getVelocity()));
            missiles.add((MissileAPI) combatEngineAPI.spawnProjectile(weaponAPI.getShip(), weaponAPI,"thquest_fantasy_seal_fake_red",weaponAPI.getFirePoint(1),weaponAPI.getCurrAngle()+180,weaponAPI.getShip().getVelocity()));
            missiles.add((MissileAPI) combatEngineAPI.spawnProjectile(weaponAPI.getShip(), weaponAPI,"thquest_fantasy_seal_fake_blue",weaponAPI.getFirePoint(3),weaponAPI.getCurrAngle()+225,weaponAPI.getShip().getVelocity()));
            missiles.add((MissileAPI) combatEngineAPI.spawnProjectile(weaponAPI.getShip(), weaponAPI,"thquest_fantasy_seal_fake_green",weaponAPI.getFirePoint(5),weaponAPI.getCurrAngle()+270,weaponAPI.getShip().getVelocity()));
            missiles.add((MissileAPI) combatEngineAPI.spawnProjectile(weaponAPI.getShip(), weaponAPI,"thquest_fantasy_seal_fake_yellow",weaponAPI.getFirePoint(7),weaponAPI.getCurrAngle()+320,weaponAPI.getShip().getVelocity()));

            //mr1.setJitter(this, new Color(255, 255, 255),10f,1,15f);
            for(MissileAPI missile:missiles){
                missile.setFacing(missile.getFacing()+90);
            }
            ((ThquestFantasySealPlugin)weaponAPI.getEffectPlugin()).missiles.addAll(missiles);
            ((ThquestFantasySealPlugin)weaponAPI.getEffectPlugin()).lastFired=combatEngineAPI.getTotalElapsedTime(false);
        }
    }
}
