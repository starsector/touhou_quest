package com.fs.starfarer.api.impl.combat;

import com.fs.starfarer.api.combat.*;
import org.lazywizard.lazylib.VectorUtils;
import org.lazywizard.lazylib.combat.AIUtils;
import org.lwjgl.util.vector.Vector2f;
import thQuest.Log;

public class ThquestSanaeMissileAI implements MissileAIPlugin, GuidedMissileAI {
    MissileAPI missile;
    ShipAPI launchingShip;
    CombatEntityAPI target;
    Boolean secondStage=false;
    Boolean correctionTurn = false;
    float setAngle;
    public ThquestSanaeMissileAI(MissileAPI missile, ShipAPI launchingShip){
    this.missile=missile;
    this.launchingShip=launchingShip;
    }
    @Override
    public void advance(float amount) {
        if(missile==null){
            return;
        }
        missile.giveCommand(ShipCommand.ACCELERATE);
        if(target==null){
            target=launchingShip.getShipTarget();
            if(target==null||target.getOwner()==launchingShip.getOwner()||target.getOwner()==100){
                target= AIUtils.getNearestEnemy(missile);
                if(target==null){
                    //testing this
                    if(secondStage){
                        missile.setAngularVelocity(0);
                        float facing = missile.getFacing();
                        if(facing<setAngle||facing>setAngle){
                            missile.explode();
                        }

                    }
                    return;
                }
            }
        }
        //todo test this change
//        if(target.getOwner()!=1){
//            target=null;
//        }
        if(secondStage){
            missile.setAngularVelocity(0);
            float facing = missile.getFacing();
            if(facing<setAngle||facing>setAngle){
                missile.explode();
            }
            return;
        }
        if(missile.getLocation()==null){
            return;
        }
        //this fixed a crash WHYYYYYY TARGET SHOULD NOT BE NULL!!!!!????!?!?!!?
        if(target==null||target.getLocation()==null){
            return;
        }
        double angle=VectorUtils.getAngle(missile.getLocation(),target.getLocation());
        //Log.getLogger().info(missile.getFacing());
        Vector2f v=VectorUtils.getDirectionalVector(missile.getLocation(),target.getLocation());
        //VectorUtils.

    missile.giveCommand(ShipCommand.ACCELERATE);
        float reverseVector = missile.getFacing()-180;
        if(reverseVector<0){
        reverseVector=reverseVector+360;
        }
        float leftRange1 = missile.getFacing()-95;
    float leftRange2 = missile.getFacing()-85;
    if(leftRange1<0){
        leftRange1=360+leftRange1;
    }
    if(leftRange2<0){
        leftRange2=360+leftRange2;
    }
    float rightRange1 = missile.getFacing()+95;
    float rightRange2 = missile.getFacing()+85;
        if(rightRange1>360){
        rightRange1=rightRange1-360;
    }
    if(rightRange2>360){
        rightRange2=rightRange2-360;
    }

    if((angle>leftRange1&&angle<leftRange2)||(leftRange1>350&&angle>leftRange1)){
        missile.setFacing((float) (angle));
        missile.setAngularVelocity(0);
        //missile.giveCommand(ShipCommand.ACCELERATE);
        setAngle= (float) angle;
        secondStage=true;
        //Log.getLogger().info("angle detected! missile should go right!");
    }
    else if((angle<rightRange1&&angle>rightRange2)||(rightRange1<10&&angle<rightRange1)){
        missile.setFacing((float) (angle));
        missile.setAngularVelocity(0);
        //missile.giveCommand(ShipCommand.ACCELERATE);
        setAngle= (float) angle;
        secondStage=true;
        //Log.getLogger().info("angle detected! missile should go left!");

    }
    //check for free turns
        if(!correctionTurn){
            if((angle>reverseVector&&angle<leftRange1)||(reverseVector>275&&angle>reverseVector)){
                missile.setFacing((float) (angle)+30);
                missile.setAngularVelocity(0);
                //missile.giveCommand(ShipCommand.ACCELERATE);
                setAngle= (float) angle+30;

                correctionTurn=true;
            }
            else if((angle<reverseVector&&angle>rightRange1)||(reverseVector<85&&angle<reverseVector)){
                missile.setFacing((float) (angle)+30);
                missile.setAngularVelocity(0);
                //missile.giveCommand(ShipCommand.ACCELERATE);
                setAngle= (float) angle-30;
                correctionTurn=true;
            }

        }

    else if (missile.isFizzling()) {
        secondStage=true;
    }
    }

    @Override
    public CombatEntityAPI getTarget() {
        return target;
    }

    @Override
    public void setTarget(CombatEntityAPI target) {
    this.target=target;
    }
}
