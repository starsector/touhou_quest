package com.fs.starfarer.api.impl.campaign.plugins;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CampaignEngineLayers;
import com.fs.starfarer.api.campaign.CustomEntitySpecAPI;
import com.fs.starfarer.api.campaign.PlanetAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.combat.ViewportAPI;
import com.fs.starfarer.api.graphics.SpriteAPI;
import com.fs.starfarer.api.impl.campaign.BaseCustomEntityPlugin;
import com.fs.starfarer.api.util.FlickerUtilV2;
import com.fs.starfarer.api.util.Misc;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import java.awt.Color;

public class ThquestLamp extends BaseCustomEntityPlugin {
    public static Color GLOW_COLOR = new Color(255, 130, 130, 255);
    public static Color LIGHT_COLOR = new Color(255, 130, 130, 255);
    public static String GLOW_COLOR_KEY = "$core_lampGlowColor";
    public static String LIGHT_COLOR_KEY = "$core_lampLightColor";
    public static float GLOW_FREQUENCY = 0.2F;
    private transient SpriteAPI sprite;
    private transient SpriteAPI glow;
    protected float phase = 0.0F;
    protected FlickerUtilV2 flicker = new FlickerUtilV2();

    public ThquestLamp() {
    }

    public void init(SectorEntityToken entity, Object pluginParams) {
        super.init(entity, pluginParams);
        entity.setDetectionRangeDetailsOverrideMult(0.75F);
        this.readResolve();
    }

    Object readResolve() {
        this.glow = Global.getSettings().getSprite("campaignEntities", "fusion_lamp_glow");
        return this;
    }

    public void advance(float amount) {
        for(this.phase += amount * GLOW_FREQUENCY; this.phase > 1.0F; --this.phase) {
        }

        this.flicker.advance(amount * 1.0F);
        SectorEntityToken focus = this.entity.getOrbitFocus();
        if (focus instanceof PlanetAPI) {
            PlanetAPI planet = (PlanetAPI)focus;
            float lightAlpha = this.getLightAlpha();
            lightAlpha *= this.entity.getSensorFaderBrightness();
            lightAlpha *= this.entity.getSensorContactFaderBrightness();
            planet.setSecondLight(new Vector3f(this.entity.getLocation().x, this.entity.getLocation().y, this.entity.getCircularOrbitRadius() * 0.75F), Misc.scaleColor(this.getLightColor(), lightAlpha));
        }

    }

    public float getFlickerBasedMult() {
        float shortage = 0;
        shortage *= 0.33F;
        if (shortage <= 0.0F) {
            return 1.0F;
        } else {
            float f = 1.0F - shortage * this.flicker.getBrightness();
            return f;
        }
    }

    public float getGlowAlpha() {
        float glowAlpha = 0.0F;
        if (this.phase < 0.5F) {
            glowAlpha = this.phase * 2.0F;
        }

        if (this.phase >= 0.5F) {
            glowAlpha = 1.0F - (this.phase - 0.5F) * 2.0F;
        }

        glowAlpha = 0.75F + glowAlpha * 0.25F;
        glowAlpha *= this.getFlickerBasedMult();
        if (glowAlpha < 0.0F) {
            glowAlpha = 0.0F;
        }

        if (glowAlpha > 1.0F) {
            glowAlpha = 1.0F;
        }

        return glowAlpha;
    }

    public float getLightAlpha() {
        float lightAlpha = 0.0F;
        if (this.phase < 0.5F) {
            lightAlpha = this.phase * 2.0F;
        }

        if (this.phase >= 0.5F) {
            lightAlpha = 1.0F - (this.phase - 0.5F) * 2.0F;
        }

        lightAlpha = 0.5F + lightAlpha * 0.5F;
        lightAlpha *= this.getFlickerBasedMult();
        if (lightAlpha < 0.0F) {
            lightAlpha = 0.0F;
        }

        if (lightAlpha > 1.0F) {
            lightAlpha = 1.0F;
        }

        return lightAlpha;
    }

    public Color getGlowColor() {
        Color glowColor = GLOW_COLOR;
        if (this.entity.getMemoryWithoutUpdate().contains(GLOW_COLOR_KEY)) {
            glowColor = (Color)this.entity.getMemoryWithoutUpdate().get(GLOW_COLOR_KEY);
        }

        return glowColor;
    }

    public Color getLightColor() {
        Color lightColor = LIGHT_COLOR;
        if (this.entity.getMemoryWithoutUpdate().contains(LIGHT_COLOR_KEY)) {
            lightColor = (Color)this.entity.getMemoryWithoutUpdate().get(LIGHT_COLOR_KEY);
        }

        return lightColor;
    }

    public void setGlowColor(Color color) {
        this.entity.getMemoryWithoutUpdate().set(GLOW_COLOR_KEY, color);
    }

    public void setLightColor(Color color) {
        this.entity.getMemoryWithoutUpdate().set(LIGHT_COLOR_KEY, color);
    }

    public float getRenderRange() {
        return this.entity.getRadius() + 1200.0F;
    }

    public void render(CampaignEngineLayers layer, ViewportAPI viewport) {
        float alphaMult = viewport.getAlphaMult();
        alphaMult *= this.entity.getSensorFaderBrightness();
        alphaMult *= this.entity.getSensorContactFaderBrightness();
        if (!(alphaMult <= 0.0F)) {
            CustomEntitySpecAPI spec = this.entity.getCustomEntitySpec();
            if (spec != null) {
                float w = spec.getSpriteWidth();
                float h = spec.getSpriteHeight();
                Vector2f loc = this.entity.getLocation();
                if (this.sprite != null) {
                    this.sprite.setAngle(this.entity.getFacing() - 90.0F);
                    this.sprite.setSize(w, h);
                    this.sprite.setAlphaMult(alphaMult);
                    this.sprite.setNormalBlend();
                    this.sprite.renderAtCenter(loc.x, loc.y);
                }

                float glowAlpha = this.getGlowAlpha();
                float glowAngle1 = (this.phase * 1.3F % 1.0F - 0.5F) * 12.0F;
                float glowAngle2 = (this.phase * 1.9F % 1.0F - 0.5F) * 12.0F;
                this.glow.setColor(this.getGlowColor());
                w = 600.0F;
                h = 600.0F;
                this.glow.setSize(w, h);
                this.glow.setAlphaMult(alphaMult * glowAlpha * 0.5F);
                this.glow.setAdditiveBlend();
                this.glow.renderAtCenter(loc.x, loc.y);

                for(int i = 0; i < 5; ++i) {
                    w *= 0.3F;
                    h *= 0.3F;
                    this.glow.setSize(w, h);
                    this.glow.setAlphaMult(alphaMult * glowAlpha * 0.67F);
                    this.glow.renderAtCenter(loc.x, loc.y);
                }
            }
        }
    }
}
