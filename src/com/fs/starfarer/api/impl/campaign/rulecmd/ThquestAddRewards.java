package com.fs.starfarer.api.impl.campaign.rulecmd;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.*;
import com.fs.starfarer.api.campaign.comm.CommMessageAPI;
import com.fs.starfarer.api.campaign.rules.CommandPlugin;
import com.fs.starfarer.api.campaign.rules.MemKeys;
import com.fs.starfarer.api.campaign.rules.MemoryAPI;
import com.fs.starfarer.api.characters.PersonAPI;
import com.fs.starfarer.api.impl.campaign.CoreReputationPlugin;
import com.fs.starfarer.api.util.Misc;
import thQuest.LostGirlData;
import thQuest.LostTouhou;
import thQuest.ThquestRewards;
import thQuest.ThquestStrings;
import thQuest.intel.FantasyResurgenceEventIntel;
import thQuest.intel.FrFactor;

import java.util.List;
import java.util.Map;

public class ThquestAddRewards implements CommandPlugin {
    @Override
    public boolean execute(String s, InteractionDialogAPI interactionDialogAPI, List<Misc.Token> list, Map<String, MemoryAPI> map) {
        String girlId;
        if(list.size()>0){
            girlId = list.get(0).string;
        }else{
            girlId = map.get(MemKeys.LOCAL).getString("$thquest_lost_touhou");
        }
//        LostGirlData lostGirlData=(LostGirlData)map.get(MemKeys.GLOBAL).get("$thquestlostgirldata");
//        LostTouhou lostTouhou=lostGirlData.getGirl(girlId);
        ThquestRewards rewards = ThquestRewards.getLgReward(girlId);
        CargoAPI playerCargo = Global.getSector().getPlayerFleet().getCargo();
        //TODO notifications
        if(rewards.credits>0){
            playerCargo.getCredits().add(rewards.credits);
            AddRemoveCommodity.addCreditsGainText(rewards.credits, interactionDialogAPI.getTextPanel());
        }
        if(rewards.commodities != null) {
            for (String key : rewards.commodities.keySet()) {
                playerCargo.addCommodity(key, rewards.commodities.get(key));
                AddRemoveCommodity.addCommodityGainText(key, rewards.commodities.get(key),interactionDialogAPI.getTextPanel());
            }
        }
        if(rewards.fairyWings != null) {
            for (String fairyWing : rewards.fairyWings.keySet()) {
                playerCargo.addSpecial(new SpecialItemData(fairyWing, null), rewards.fairyWings.get(fairyWing));
                AddRemoveCommodity.addItemGainText(new SpecialItemData(fairyWing, null), rewards.fairyWings.get(fairyWing),interactionDialogAPI.getTextPanel());
            }
        }
        if(rewards.wings!=null) {
            for (String wing : rewards.wings.keySet()) {
                playerCargo.addFighters(wing, rewards.wings.get(wing));
                AddRemoveCommodity.addFighterGainText(wing, rewards.wings.get(wing),interactionDialogAPI.getTextPanel());
            }
        }
        if(rewards.weapons!=null) {
            for (String weapon : rewards.weapons.keySet()) {
                playerCargo.addWeapons(weapon, rewards.weapons.get(weapon));
                AddRemoveCommodity.addWeaponGainText(weapon,rewards.weapons.get(weapon),interactionDialogAPI.getTextPanel());
            }
        }
        if(rewards.specialItems!=null){
            for(SpecialItemData item : rewards.specialItems){
                playerCargo.addSpecial(item,1f);
                AddRemoveCommodity.addItemGainText(item,1,interactionDialogAPI.getTextPanel());
            }
        }
        if(rewards.mayohigaPactRep>0){
            //thquest_gensokyo COOPERATIVE 5
            //from reputation
            String factionId = ThquestStrings.gensokyoFaction;

            ReputationActionResponsePlugin.ReputationAdjustmentResult result;
            CoreReputationPlugin.CustomRepImpact impact = new CoreReputationPlugin.CustomRepImpact();
            impact.delta = rewards.mayohigaPactRep * 0.01F;
            result = Global.getSector().adjustPlayerReputation(new CoreReputationPlugin.RepActionEnvelope(CoreReputationPlugin.RepActions.CUSTOM, impact, (CommMessageAPI)null, interactionDialogAPI.getTextPanel(), true), factionId);
        }

        return false;
    }

    @Override
    public boolean doesCommandAddOptions() {
        return false;
    }

    @Override
    public int getOptionOrder(List<Misc.Token> list, Map<String, MemoryAPI> map) {
        return 0;
    }
}
