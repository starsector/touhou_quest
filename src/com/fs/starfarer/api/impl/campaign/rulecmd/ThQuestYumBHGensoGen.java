package com.fs.starfarer.api.impl.campaign.rulecmd;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.InteractionDialogAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.comm.IntelInfoPlugin;
import com.fs.starfarer.api.campaign.rules.CommandPlugin;
import com.fs.starfarer.api.campaign.rules.MemoryAPI;
import com.fs.starfarer.api.util.Misc;
import thQuest.locations.GenerateGensokyo;
import thQuest.Log;
import thQuest.intel.FantasyResurgenceEventIntel;
import thQuest.intel.FrFactor;
import thQuest.intel.YumBHIntel;

import java.util.List;
import java.util.Map;

public class ThQuestYumBHGensoGen implements CommandPlugin {
    @Override
    public boolean execute(String ruleId, InteractionDialogAPI dialog, List<Misc.Token> params, Map<String, MemoryAPI> memoryMap) {
        Global.getSector().getMemory().set("$touhouquestybh","2");
        List<IntelInfoPlugin> i =Global.getSector().getIntelManager().getIntel();
        for(IntelInfoPlugin a:i){
            if(a.getClass()== YumBHIntel.class){
                ((YumBHIntel) a).endImmediately();
            }
        }

        GenerateGensokyo.createGensokyo();

        //create pointer for dummies :-)
        SectorEntityToken gensokyo=Global.getSector().getEntityById("thquest_gensokyo");
        SectorEntityToken gilead=Global.getSector().getEntityById("gilead");
        Global.getSector().getIntelManager().addIntel(new thQuest.intel.GensokyoPointer(gilead,gensokyo));

        //add intel event for fantasy resurgance
        String text="This event is the beginning of things that will lead to Gensokyo and the Persean sector to be forever changed. For better or worse.";
        if(FantasyResurgenceEventIntel.get()!=null){
            text="This is a major event that will lead to Gensokyo and the Persean sector to be forever changed. For better or worse.";
        }
        FantasyResurgenceEventIntel.addFactorCreateIfNecessary(new FrFactor(10,"Yumemi and Chiyuri are back to Gensokyo!",text),null);
        for (Object o:Global.getSector().getIntelManager().getIntel()){
            Log.getLogger().info(o.getClass().getName());
        }
        return false;
    }

    @Override
    public boolean doesCommandAddOptions() {
        return false;
    }

    @Override
    public int getOptionOrder(List<Misc.Token> params, Map<String, MemoryAPI> memoryMap) {
        return 0;
    }
}
