package com.fs.starfarer.api.impl.campaign.rulecmd;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CampaignClockAPI;
import com.fs.starfarer.api.campaign.InteractionDialogAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.rules.CommandPlugin;
import com.fs.starfarer.api.campaign.rules.MemKeys;
import com.fs.starfarer.api.campaign.rules.MemoryAPI;
import com.fs.starfarer.api.util.Misc;
import thQuest.LostGirlData;
import thQuest.LostTouhou;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
//this will handle gensokyo quests
public class ThquestGensokyoQuests implements CommandPlugin {
    @Override
    public boolean execute(String s, InteractionDialogAPI interactionDialogAPI, List<Misc.Token> list, Map<String, MemoryAPI> map) {
        //Lost girl bar quest in Gensokyo
        SectorEntityToken gensokyo = Global.getSector().getEntityById("thquest_gensokyo");
        LostGirlData lostGirlData=(LostGirlData)map.get(MemKeys.GLOBAL).get("$thquestlostgirldata");
        if(gensokyo!=null&&lostGirlData!=null) {
            //MarketAPI gensokyo =et.getMarket();
            //roll for bar quest

            //psudorandomizer that decides whether quest is on offer on a particular month. Will always pick the first girl in the list
            CampaignClockAPI clockAPI = Global.getSector().getClock();
            //look for if all ongoing LG locations are available
            List<LostTouhou> remove = new ArrayList<>();
            for(LostTouhou l:lostGirlData.notFound){
                if(l.getActualLocation()==null&&Global.getSector().getEntityById(l.getActualLocation().getId())!=null){
                    remove.add(l);
                }
            }
            lostGirlData.notFound.removeAll(remove);
            if(clockAPI.getMonth()*clockAPI.getCycle()*13421%2==0&& lostGirlData.getNotFound().size() > 0&&lostGirlData.getPointer().size()==0) {
                LostTouhou touhou = lostGirlData.getNotFound().get(0);
                gensokyo.getMemory().set("$thquestChiyuriGirlSearch", touhou);
                gensokyo.getMemory().set("$thquestLGSystem", touhou.getActualLocation().getStarSystem().getName());
                if (touhou.getLocationEnum() == LostTouhou.Location.STASH) {
                    gensokyo.getMemory().set("$thquestLGOnOrIn", "in");
                    gensokyo.getMemory().set("$thquestLGLocation", "stash");
                }
                if (touhou.getLocationEnum() == LostTouhou.Location.MINING_STATION) {
                    gensokyo.getMemory().set("$thquestLGOnOrIn", "on");
                    gensokyo.getMemory().set("$thquestLGLocation", "station");
                }
                if (touhou.getLocationEnum() == LostTouhou.Location.HABITAT) {
                    gensokyo.getMemory().set("$thquestLGOnOrIn", "on");
                    gensokyo.getMemory().set("$thquestLGLocation", "station");
                }
                if (touhou.getLocationEnum() == LostTouhou.Location.DECIV) {
                    gensokyo.getMemory().set("$thquestLGOnOrIn", "on");
                    gensokyo.getMemory().set("$thquestLGLocation", "planet");
                }
                if (touhou.getLocationEnum() == LostTouhou.Location.RUINS) {
                    gensokyo.getMemory().set("$thquestLGOnOrIn", "on");
                    gensokyo.getMemory().set("$thquestLGLocation", "planet");
                }
                if (touhou.getLocationEnum() == LostTouhou.Location.DERILICT) {
                    gensokyo.getMemory().set("$thquestLGOnOrIn", "in");
                    gensokyo.getMemory().set("$thquestLGLocation", "derilict ship");
                }
                if (touhou.getLocationEnum() == LostTouhou.Location.CIV_PLANET) {
                    gensokyo.getMemory().set("$thquestLGOnOrIn", "on");
                    gensokyo.getMemory().set("$thquestLGLocation", "civilized planet");
                }
                gensokyo.getMemory().set("$thquestLGName", touhou.getFullName());
            }
        }
            return false;
    }

    @Override
    public boolean doesCommandAddOptions() {
        return false;
    }

    @Override
    public int getOptionOrder(List<Misc.Token> list, Map<String, MemoryAPI> map) {
        return 0;
    }
}
