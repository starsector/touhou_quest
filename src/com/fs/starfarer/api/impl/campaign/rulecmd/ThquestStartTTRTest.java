package com.fs.starfarer.api.impl.campaign.rulecmd;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.InteractionDialogAPI;
import com.fs.starfarer.api.campaign.rules.*;
import com.fs.starfarer.api.util.Misc;

import java.util.List;
import java.util.Map;

public class ThquestStartTTRTest implements CommandPlugin {
    @Override
    public boolean execute(String ruleId, InteractionDialogAPI dialog, List<Misc.Token> params, Map<String, MemoryAPI> memoryMap) {
        //GenerateMayohiga.colonizeZeta();
        // spawn fleet
//        MarketAPI source = Global.getSector().getEconomy().getMarket("thquest_habitat_kappa");
//        MarketAPI target = Global.getSector().getEconomy().getMarket("epiphany");
//        float fp = InvasionFleetManager.getWantedFleetSize(source.getFaction(), target, 0.2f, false);
//        fp *= InvasionFleetManager.RAID_SIZE_MULT;
//        fp *= InvasionFleetManager.getInvasionSizeMult(source.getFactionId());
//        fp *= MathUtils.getRandomNumberInRange(0.8f, 1.2f);
//        fp=1200f;
//        NexRaidIntel intel = new NexRaidIntel(Global.getSector().getFaction("thquest_gensokyo"), source, target, fp, 1);
//        intel.init();

//FINAL YR FLEET
//        MarketAPI source = Global.getSector().getEconomy().getMarket("thquest_habitat_kappa");
//        FleetParamsV3 fleetParamsV3 = new FleetParamsV3(new Vector2f(0, 0), "thquest_youkai",
//                1.0f,
//                FleetTypes.TASK_FORCE,
//                350, // combatPts
//                0, // freighterPts
//                0, // tankerPts
//                0f, // transportPts
//                0f, // linerPts
//                0, // utilityPts
//                Misc.getShipQuality(source, "thquest_youkai") // qualityMod
//        );
//        fleetParamsV3.modeOverride= FactionAPI.ShipPickMode.PRIORITY_THEN_ALL;
//        Log.getLogger().info("ship quality = "+Misc.getShipQuality(source, "thquest_youkai"));
//        CampaignFleetAPI fleet = FleetFactoryV3.createFleet(fleetParamsV3);
//        Vector2f location = Global.getSector().getPlayerFleet().getLocation();
//        source.getPrimaryEntity().getContainingLocation().addEntity(fleet);
//        fleet.setLocation(source.getPrimaryEntity().getLocation().x, source.getPrimaryEntity().getLocation().y);
//
//        fleet.addAssignment(FleetAssignment.DEFEND_LOCATION,Global.getSector().getEntityById("thquest_habitat_kappa"),125000f);
//
//        fleet.setLocation(location.getX(), location.getY());
        Global.getSector().getMemory().set("$thquestttr",1);
//        PersonAPI yamame = Global.getFactory().createPerson();
//        yamame.setName(new FullName("Yamame","Kurodani", FullName.Gender.FEMALE));
//        yamame.setPortraitSprite("graphics/thQuest/portraits/yamame.png");
//        yamame.getStats().setSkillLevel("thquest_yamame_skill", 2);
//        OfficerLevelupPlugin plugin = (OfficerLevelupPlugin) Global.getSettings().getPlugin("officerLevelUp");
//        yamame.getStats().addXP(plugin.getXPForLevel(1));
//        yamame.setPersonality(Personalities.STEADY);
//      Global.getSector().getPlayerFleet().getFleetData().addOfficer(yamame);
        return true;
    }


    @Override
    public boolean doesCommandAddOptions() {
        return false;
    }

    @Override
    public int getOptionOrder(List<Misc.Token> params, Map<String, MemoryAPI> memoryMap) {
        return 0;
    }
}
