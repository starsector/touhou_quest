package com.fs.starfarer.api.impl.campaign.rulecmd;

import com.fs.starfarer.api.campaign.InteractionDialogAPI;
import com.fs.starfarer.api.campaign.rules.CommandPlugin;
import com.fs.starfarer.api.campaign.rules.MemoryAPI;
import com.fs.starfarer.api.util.Misc;
import thQuest.intel.FantasyResurgenceEventIntel;
import thQuest.intel.FrFactor;

import java.util.List;
import java.util.Map;

public class ThquestFrFactor implements CommandPlugin {
    @Override
    public boolean execute(String s, InteractionDialogAPI interactionDialogAPI, List<Misc.Token> list, Map<String, MemoryAPI> map) {
        String shortString = "default short text";
        String longString = "default long text";
        switch(Integer.parseInt(list.get(1).string)){
            case 0:
                //for HoSZ
                shortString = "You have given the M device to the Mayohiga Pact";
                longString = "This will probably allow them to 'communicate' with hyperspace entities.";
                break;
            case 1:
                //for GoG
                shortString = "You have helped Yumemi repair her ship!";
                longString = "This will help her to travel through time, or maybe even all of Gensokyo once again, when the repairs are completed.";
                break;
            case 2:
                //for hidden systems
                shortString = "You have discoverd a hidden Mayohiga Pact system";
                longString = "This will help her to travel through time, or maybe even all of Gensokyo once again, when the repairs are completed.";
            default:
                break;
        }

        FantasyResurgenceEventIntel.addFactorCreateIfNecessary(new FrFactor(Integer.parseInt(list.get(0).string),shortString,longString),null);
        return true;
    }

    @Override
    public boolean doesCommandAddOptions() {
        return false;
    }

    @Override
    public int getOptionOrder(List<Misc.Token> list, Map<String, MemoryAPI> map) {
        return 0;
    }

    //        FantasyResurgenceEventIntel.addFactorCreateIfNecessary(new FrFactor(10,touhou.getNameString()+" rescued",personTooltip),null);
}
