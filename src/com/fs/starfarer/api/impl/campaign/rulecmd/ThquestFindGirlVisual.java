package com.fs.starfarer.api.impl.campaign.rulecmd;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.InteractionDialogAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.rules.CommandPlugin;
import com.fs.starfarer.api.campaign.rules.MemoryAPI;
import com.fs.starfarer.api.characters.PersonAPI;
import com.fs.starfarer.api.util.Misc;

import java.util.List;
import java.util.Map;

public class ThquestFindGirlVisual implements CommandPlugin {
    @Override
    public boolean execute(String s, InteractionDialogAPI dialog, List<Misc.Token> params, Map<String, MemoryAPI> memoryMap) {
        SectorEntityToken target = dialog.getInteractionTarget();
        boolean minimal = false;
        if (params.size() > 0) {
            minimal = ((Misc.Token)params.get(0)).getBoolean(memoryMap);
        }

        if (params.size() > 1) {
            String id = "thquest_"+((Misc.Token)params.get(1)).getString(memoryMap);
            PersonAPI person = Global.getSector().getImportantPeople().getData(id).getPerson();
            dialog.getVisualPanel().showPersonInfo(person, minimal);
        } else if (target.getActivePerson() != null) {
            dialog.getVisualPanel().showPersonInfo(target.getActivePerson(), minimal);
        } else if (target instanceof CampaignFleetAPI) {
            CampaignFleetAPI fleet = (CampaignFleetAPI)target;
            if (fleet.getCommander() != null) {
                dialog.getVisualPanel().showPersonInfo(fleet.getCommander(), minimal);
            }
        }

        return true;    }

    @Override
    public boolean doesCommandAddOptions() {
        return false;
    }

    @Override
    public int getOptionOrder(List<Misc.Token> list, Map<String, MemoryAPI> map) {
        return 0;
    }
}
