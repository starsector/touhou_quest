package com.fs.starfarer.api.impl.campaign.rulecmd;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.FactionAPI;
import com.fs.starfarer.api.campaign.FleetAssignment;
import com.fs.starfarer.api.campaign.InteractionDialogAPI;
import com.fs.starfarer.api.campaign.comm.IntelInfoPlugin;
import com.fs.starfarer.api.campaign.comm.IntelManagerAPI;
import com.fs.starfarer.api.campaign.rules.CommandPlugin;
import com.fs.starfarer.api.campaign.rules.MemKeys;
import com.fs.starfarer.api.campaign.rules.MemoryAPI;
import com.fs.starfarer.api.impl.campaign.fleets.FleetFactoryV3;
import com.fs.starfarer.api.impl.campaign.fleets.FleetParamsV3;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.impl.campaign.ids.FleetTypes;
import com.fs.starfarer.api.impl.campaign.ids.MemFlags;
import com.fs.starfarer.api.impl.campaign.intel.BaseIntelPlugin;
import com.fs.starfarer.api.util.Misc;
import org.lwjgl.util.vector.Vector2f;
import thQuest.ThquestStrings;
import thQuest.intel.YumBHIntel;

import java.util.List;
import java.util.Map;

//this is stage 1 regardless of where it starts...
public class ThQuestYumBH implements CommandPlugin {
    @Override
    public boolean execute(String ruleId, InteractionDialogAPI dialog, List<Misc.Token> params, Map<String, MemoryAPI> memoryMap) {
        List<IntelInfoPlugin> i = Global.getSector().getIntelManager().getIntel();
        YumBHIntel yumIntel = null;
        for(IntelInfoPlugin a:i){
            if(a.getClass() == YumBHIntel.class){
                yumIntel= (YumBHIntel) a;
            }
        }
        YumBHIntel b;

        if(memoryMap.get(MemKeys.LOCAL).get("$id").equals("thquestYum")){
            //this should remove yumemi from intel board
            Global.getSector().getEntityById("station_galatia_academy").getMarket().getCommDirectory().removePerson(Global.getSector().getImportantPeople().getPerson("thquestYum"));
            if(yumIntel==null) {
                b = new YumBHIntel(Global.getSector().getEntityById("station_galatia_academy"), Global.getSector().getEntityById("Gilead"));
                b.setAlternateDescription(true);
                Global.getSector().getIntelManager().addIntel(b);
            }else{
                yumIntel.setAlternateDescription(true);
                yumIntel.setDestination(Global.getSector().getEntityById("Gilead"));
            }
            Global.getSector().getMemory().set("$touhouquestybh", "3");
            spawnFleet();
            dialog.dismiss();
        }else{
            if(yumIntel==null) {
                b = new YumBHIntel(Global.getSector().getEntityById("thquest_habitat_kappa"), Global.getSector().getEntityById("station_galatia_academy"));
                Global.getSector().getMemory().set("$touhouquestybh", "1");
                Global.getSector().getIntelManager().addIntel(b);
            }
        }
        return false;
    }
    @Override
    public boolean doesCommandAddOptions() {
        return false;
    }

    @Override
    public int getOptionOrder(List<Misc.Token> params, Map<String, MemoryAPI> memoryMap) {
        return 0;
    }
    //todo
    public void spawnFleet(){
        //this spawns a YR fleet after the GA stage
        float fp = (float) (70f + (Math.random() - .5) * 50f);
        FleetParamsV3 fleetParamsV3 = new FleetParamsV3(new Vector2f(0, 0), ThquestStrings.youkaiFaction,
                1.0f,
                FleetTypes.TASK_FORCE,
                fp, // combatPts
                fp / 20, // freighterPts
                fp / 20, // tankerPts
                0f, // transportPts
                0f, // linerPts
                0, // utilityPts
                .45f // qualityMod
        );
        //TODO fix. Does not traverse pontus gravity weell
        fleetParamsV3.modeOverride = FactionAPI.ShipPickMode.PRIORITY_THEN_ALL;
        CampaignFleetAPI fleet = FleetFactoryV3.createFleet(fleetParamsV3);
        fleet.addAssignment(FleetAssignment.INTERCEPT, Global.getSector().getPlayerFleet(), 30f);
        Global.getSector().getHyperspace().addEntity(fleet);
        fleet.setLocation(2500,-12000); //this is near pontus
        fleet.setFaction(ThquestStrings.youkaiFaction);
        fleet.getFlagship().setCaptain(Global.getSector().getImportantPeople().getPerson("thquest_meira"));
        fleet.setCommander(Global.getSector().getImportantPeople().getPerson("thquest_meira"));
        Misc.setFlagWithReason(fleet.getMemoryWithoutUpdate(), MemFlags.MEMORY_KEY_MAKE_HOSTILE, "thquest_ybh", true, 999);
    }
}
