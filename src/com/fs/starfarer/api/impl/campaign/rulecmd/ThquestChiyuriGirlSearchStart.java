package com.fs.starfarer.api.impl.campaign.rulecmd;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.InteractionDialogAPI;
import com.fs.starfarer.api.campaign.rules.CommandPlugin;
import com.fs.starfarer.api.campaign.rules.MemKeys;
import com.fs.starfarer.api.campaign.rules.MemoryAPI;
import com.fs.starfarer.api.util.Misc;
import thQuest.LostGirlData;
import thQuest.LostTouhou;
import thQuest.intel.FantasyResurgenceEventIntel;
import thQuest.intel.FrFactor;
import thQuest.intel.ThquestLostGirlIntel;

import java.util.List;
import java.util.Map;

public class ThquestChiyuriGirlSearchStart implements CommandPlugin {
    @Override
    public boolean execute(String s, InteractionDialogAPI interactionDialogAPI, List<Misc.Token> list, Map<String, MemoryAPI> map) {
        //give intel

        //move data
        LostGirlData lostGirlData=(LostGirlData) map.get(MemKeys.GLOBAL).get("$thquestlostgirldata");
        LostTouhou lostTouhou= (LostTouhou) map.get(MemKeys.LOCAL).get("$thquestChiyuriGirlSearch");
        lostGirlData.getNotFound().remove(lostTouhou);
        lostGirlData.getPointer().add(lostTouhou);
        //idk what is the right way to point to it
        ThquestLostGirlIntel fgi =new ThquestLostGirlIntel(lostTouhou.getActualLocation(),lostTouhou.getActualLocation());
        StringBuilder sb = new StringBuilder(lostTouhou.getName());
        sb.setCharAt(0,Character.toUpperCase(sb.charAt(0)));
        fgi.setGirlName(sb+" "+lostTouhou.getSirname());
        fgi.setLocationType(lostTouhou.getLocationEnum());
        Global.getSector().getIntelManager().addIntel(fgi);
        return false;
    }

    @Override
    public boolean doesCommandAddOptions() {
        return false;
    }

    @Override
    public int getOptionOrder(List<Misc.Token> list, Map<String, MemoryAPI> map) {
        return 0;
    }
}
