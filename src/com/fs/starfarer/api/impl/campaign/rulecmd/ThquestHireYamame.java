package com.fs.starfarer.api.impl.campaign.rulecmd;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.InteractionDialogAPI;
import com.fs.starfarer.api.campaign.rules.CommandPlugin;
import com.fs.starfarer.api.campaign.rules.MemoryAPI;
import com.fs.starfarer.api.characters.FullName;
import com.fs.starfarer.api.characters.PersonAPI;
import com.fs.starfarer.api.impl.campaign.ids.Personalities;
import com.fs.starfarer.api.plugins.OfficerLevelupPlugin;
import com.fs.starfarer.api.util.Misc;

import java.util.List;
import java.util.Map;

public class ThquestHireYamame implements CommandPlugin {
    @Override
    public boolean execute(String s, InteractionDialogAPI interactionDialogAPI, List<Misc.Token> list, Map<String, MemoryAPI> map) {
        PersonAPI yamame = Global.getFactory().createPerson();
        yamame.setName(new FullName("Yamame","Kurodani", FullName.Gender.FEMALE));
        yamame.setPortraitSprite("graphics/thQuest/portraits/yamame.png");
        yamame.getStats().setSkillLevel("thquest_yamame_skill", 2);
        OfficerLevelupPlugin plugin = (OfficerLevelupPlugin) Global.getSettings().getPlugin("officerLevelUp");
        yamame.getStats().addXP(plugin.getXPForLevel(1));
        yamame.setPersonality(Personalities.STEADY);
        Global.getSector().getPlayerFleet().getFleetData().addOfficer(yamame);
        return true;
    }

    @Override
    public boolean doesCommandAddOptions() {
        return false;
    }

    @Override
    public int getOptionOrder(List<Misc.Token> list, Map<String, MemoryAPI> map) {
        return 0;
    }
}
