package com.fs.starfarer.api.impl.campaign.rulecmd;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.InteractionDialogAPI;
import com.fs.starfarer.api.campaign.LocationAPI;
import com.fs.starfarer.api.campaign.comm.IntelInfoPlugin;
import com.fs.starfarer.api.campaign.comm.IntelManagerAPI;
import com.fs.starfarer.api.campaign.rules.CommandPlugin;
import com.fs.starfarer.api.campaign.rules.MemKeys;
import com.fs.starfarer.api.campaign.rules.MemoryAPI;
import com.fs.starfarer.api.characters.PersonAPI;
import com.fs.starfarer.api.util.Misc;
import com.fs.starfarer.rpg.Person;
import thQuest.LostGirlData;
import thQuest.LostTouhou;
import thQuest.intel.FantasyResurgenceEventIntel;
import thQuest.intel.FrFactor;
import thQuest.intel.ThquestLostGirlIntel;
import thQuest.intel.YumBHIntel;

import java.util.List;
import java.util.Map;

public class ThquestFindGirl implements CommandPlugin {

    @Override
    public boolean execute(String s, InteractionDialogAPI interactionDialogAPI, List<Misc.Token> list, Map<String, MemoryAPI> map) {
        String id = "thquest_"+map.get(MemKeys.LOCAL).getString("$thquest_lost_touhou");
        PersonAPI touhou =Global.getSector().getImportantPeople().getPerson(id);
        String personTooltip;
        if(YumBHIntel.isComplete()){
            personTooltip="This will help further Yukari's goals";
        }else{
            personTooltip="This person seems to be involved with Gensokyo.";
        }
        //StringBuilder sb = new StringBuilder(map.get(MemKeys.LOCAL).getString("$thquest_lost_touhou"));
        //sb.setCharAt(0,Character.toUpperCase(sb.charAt(0)));
        //run after girl found
        FantasyResurgenceEventIntel.addFactorCreateIfNecessary(new FrFactor(10,touhou.getNameString()+" rescued",personTooltip),null);
        LostGirlData lostGirlData=(LostGirlData)map.get(MemKeys.GLOBAL).get("$thquestlostgirldata");
        LostTouhou lostTouhou=lostGirlData.getGirl(map.get(MemKeys.LOCAL).getString("$thquest_lost_touhou"));
        lostGirlData.getNotFound().remove(lostTouhou);
        lostGirlData.getPointer().remove(lostTouhou);
        lostGirlData.getFound().add(lostTouhou);
        List<IntelInfoPlugin> m = Global.getSector().getIntelManager().getIntel();
        for(IntelInfoPlugin i:m){
            if(ThquestLostGirlIntel.class==i.getClass()){
                ((ThquestLostGirlIntel) i).endImmediately();//TODO changed from remove to end
                break;
            }
        }
        return false;
    }

    @Override
    public boolean doesCommandAddOptions() {
        return false;
    }

    @Override
    public int getOptionOrder(List<Misc.Token> list, Map<String, MemoryAPI> map) {
        return 0;
    }
}
