package com.fs.starfarer.api.impl.campaign.rulecmd;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.InteractionDialogAPI;
import com.fs.starfarer.api.campaign.SpecialItemData;
import com.fs.starfarer.api.campaign.rules.CommandPlugin;
import com.fs.starfarer.api.campaign.rules.MemoryAPI;
import com.fs.starfarer.api.util.Misc;

import java.util.List;
import java.util.Map;

public class ThquestAddSpecialItem implements CommandPlugin {
    @Override
    public boolean execute(String s, InteractionDialogAPI interactionDialogAPI, List<Misc.Token> list, Map<String, MemoryAPI> map) {
        String id = list.get(1).string;
        String data = list.get(1).string;
        Global.getSector().getPlayerFleet().getCargo().addSpecial(new SpecialItemData(id, data), 1f);
        AddRemoveCommodity.addItemGainText(new SpecialItemData(id,data),1,interactionDialogAPI.getTextPanel());
        return false;
    }

    @Override
    public boolean doesCommandAddOptions() {
        return false;
    }

    @Override
    public int getOptionOrder(List<Misc.Token> list, Map<String, MemoryAPI> map) {
        return 0;
    }
}
