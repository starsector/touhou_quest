package com.fs.starfarer.api.impl.campaign.rulecmd;

import com.fs.starfarer.api.campaign.InteractionDialogAPI;
import com.fs.starfarer.api.campaign.rules.CommandPlugin;
import com.fs.starfarer.api.campaign.rules.MemKeys;
import com.fs.starfarer.api.campaign.rules.MemoryAPI;
import com.fs.starfarer.api.util.Misc;
import thQuest.LostGirlData;
import thQuest.LostTouhou;

import java.util.List;
import java.util.Map;

public class ThquestLGActiveCheck implements CommandPlugin {
    @Override
    public boolean execute(String s, InteractionDialogAPI interactionDialogAPI, List<Misc.Token> list, Map<String, MemoryAPI> map) {
        LostGirlData lostGirlData=(LostGirlData)map.get(MemKeys.GLOBAL).get("$thquestlostgirldata");
        String girlName =list.get(0).string;
        LostTouhou touhou = lostGirlData.getGirl(girlName);
        return lostGirlData.pointer.contains(touhou);
    }

    @Override
    public boolean doesCommandAddOptions() {
        return false;
    }

    @Override
    public int getOptionOrder(List<Misc.Token> list, Map<String, MemoryAPI> map) {
        return 0;
    }
}
