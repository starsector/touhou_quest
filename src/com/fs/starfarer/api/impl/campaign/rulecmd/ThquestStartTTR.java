package com.fs.starfarer.api.impl.campaign.rulecmd;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.InteractionDialogAPI;
import com.fs.starfarer.api.campaign.rules.CommandPlugin;
import com.fs.starfarer.api.campaign.rules.MemoryAPI;
import com.fs.starfarer.api.util.Misc;
import thQuest.locations.GenerateReimaden;
import thQuest.TTR.ThquestDancerScript;
import thQuest.fleets.FleetUtil;
import thQuest.fleets.TTRDefFleetBehaviourSpec;
import thQuest.intel.TriTachyonRikakoIntel;

import java.util.List;
import java.util.Map;

public class ThquestStartTTR implements CommandPlugin {
    @Override
    public boolean execute(String s, InteractionDialogAPI interactionDialogAPI, List<Misc.Token> list, Map<String, MemoryAPI> map) {
        Global.getSector().getScripts().add(new ThquestDancerScript());
        GenerateReimaden.createReimaden();
        FleetUtil fu = (FleetUtil) Global.getSector().getMemory().get("$thquestFleetUtil");
        fu.addFleetBehaviourSpec(new TTRDefFleetBehaviourSpec());
        Global.getSector().getIntelManager().addIntel(new TriTachyonRikakoIntel(Global.getSector().getEntityById("thquest_gensokyo"),Global.getSector().getEntityById("thquest_reimaden_IIIa")));
        return false;
    }

    @Override
    public boolean doesCommandAddOptions() {
        return false;
    }

    @Override
    public int getOptionOrder(List<Misc.Token> list, Map<String, MemoryAPI> map) {
        return 0;
    }
}
