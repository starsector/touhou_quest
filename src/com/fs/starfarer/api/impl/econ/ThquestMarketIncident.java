package com.fs.starfarer.api.impl.econ;

import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.impl.campaign.econ.BaseMarketConditionPlugin;
import com.fs.starfarer.api.util.IntervalUtil;

public class ThquestMarketIncident extends BaseMarketConditionPlugin {
    String key = "thquest_scarlet_mist";
    String name = "Scarlet aaaa";
    IntervalUtil interval = new IntervalUtil(300,600);
        @Override
    public void advance(float amount) {
        super.advance(amount);
        interval.advance(amount);
        if(interval.intervalElapsed()){
            this.market.removeCondition(key);
        }
    }
    public boolean isIncidentMarketConditionMitigated(MarketAPI m){
        if(m.hasIndustry("thquest_shrine")){
            return true;
        }
        return false;
    }

}
