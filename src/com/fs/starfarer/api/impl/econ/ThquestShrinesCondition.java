package com.fs.starfarer.api.impl.econ;

import com.fs.starfarer.api.impl.campaign.econ.BaseMarketConditionPlugin;
import com.fs.starfarer.api.ui.TooltipMakerAPI;
import com.fs.starfarer.api.util.IntervalUtil;
import com.fs.starfarer.api.util.Misc;
import thQuest.ThquestStrings;

import java.awt.*;

public class ThquestShrinesCondition extends BaseMarketConditionPlugin {
    String key = "thquest_shrines";
    IntervalUtil interval = new IntervalUtil(300,600);
    @Override
    public void advance(float amount) {
        super.advance(amount);
        interval.advance(amount);
        if(interval.intervalElapsed()){
            this.market.removeCondition(key);
        }
    }
    public void createTooltipAfterDescription(TooltipMakerAPI tooltip, boolean expanded) {
        Color h = Misc.getHighlightColor();
        Color n = Misc.getNegativeHighlightColor();
        float pad = 3.0F;
        float small = 5.0F;
        float opad = 10.0F;
            tooltip.addPara("The people of the sector are generally, and Luddics particularly, hostile to this new religion. meaning that these shrines will likely be %s.", opad, n,"removed in short order");
    }
}
