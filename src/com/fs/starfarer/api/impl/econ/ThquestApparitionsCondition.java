package com.fs.starfarer.api.impl.econ;

import com.fs.starfarer.api.impl.campaign.econ.BaseMarketConditionPlugin;
import com.fs.starfarer.api.ui.TooltipMakerAPI;
import com.fs.starfarer.api.util.IntervalUtil;
import com.fs.starfarer.api.util.Misc;
import thQuest.ThquestStrings;
import thQuest.ThU;

import java.awt.*;

public class ThquestApparitionsCondition extends ThquestMarketIncident {

    String key = "thquest_apparitions";
    String name = "Apparitions";
    IntervalUtil interval = new IntervalUtil(300,600);
    float stability = -1;
    @Override
    public void apply(String id) {
        super.apply(id);
        if(!this.isIncidentMarketConditionMitigated(market)) {
            market.getStability().modifyFlat(key, stability,name);
        }
    }

    @Override
    public void unapply(String id) {
        super.unapply(id);
        market.getStability().unmodify(key);
    }
    @Override
    public void createTooltipAfterDescription(TooltipMakerAPI tooltip, boolean expanded) {
        Color h = Misc.getHighlightColor();
        Color n = Misc.getNegativeHighlightColor();
        float pad = 3.0F;
        float small = 5.0F;
        float opad = 10.0F;
        if(this.isIncidentMarketConditionMitigated(market)){
            tooltip.addPara(ThquestStrings.incidentMitigated, opad, h);
        }else{
            tooltip.addPara("%s stability", opad, n, String.valueOf((int)stability));
        }
    }
}
