package com.fs.starfarer.api.impl.econ;

import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.campaign.econ.MarketImmigrationModifier;
import com.fs.starfarer.api.impl.campaign.econ.BaseMarketConditionPlugin;
import com.fs.starfarer.api.impl.campaign.population.PopulationComposition;
import com.fs.starfarer.api.ui.TooltipMakerAPI;
import com.fs.starfarer.api.util.Misc;
import thQuest.intel.YrIntel2;

import java.awt.*;
import java.util.Map;

public class ThquestYoukaiRaidersMarketCondition extends BaseMarketConditionPlugin implements MarketImmigrationModifier {
    float popGrowth;
    float accessibility=0.0f;
    float stability=0.0f;

    YrIntel2.Status status;
    public ThquestYoukaiRaidersMarketCondition() {
    }
    public void setParam(Object param) {
        status = (YrIntel2.Status)param;
    }

    public void apply(String id) {
        accessibility=0.0f;
        stability=0.0f;
        popGrowth=0.0f;
        if(status!=null) {
            switch (status) {
                case V1:
                    accessibility = .0f;
                    stability = 1f;
                    popGrowth = 10f;
                    break;
                case V2:
                    accessibility = .1f;
                    stability = 2f;
                    popGrowth = 20f;
                    break;
                case V3:
                    accessibility = .2f;
                    stability = 3f;
                    popGrowth = 30f;
                    break;
                default:
                    break;
            }
        }
        String name = "Youkai activity";
        if (accessibility != 0.0F) {
            this.market.getAccessibilityMod().modifyFlat(id, -accessibility, name);
        }

        if (stability != 0.0F) {
            this.market.getStability().modifyFlat(id, -stability, name);
        }
        if(popGrowth != 0.0f){
            this.market.addTransientImmigrationModifier(this);
        }

    }

    public void unapply(String id) {
        this.market.getAccessibilityMod().unmodifyFlat(id);
        this.market.getStability().unmodifyFlat(id);
        this.market.removeTransientImmigrationModifier(this);
    }

    public void advance(float amount) {
    }

    public Map<String, String> getTokenReplacements() {
        return super.getTokenReplacements();
    }

    public boolean isTransient() {
        return true;
    }

    public void createTooltipAfterDescription(TooltipMakerAPI tooltip, boolean expanded) {
        Color h = Misc.getHighlightColor();
        Color n = Misc.getNegativeHighlightColor();
        float pad = 3.0F;
        float small = 5.0F;
        float opad = 10.0F;
        if (stability != 0.0F && accessibility != 0.0F&&popGrowth !=0.0f) {
            tooltip.addPara("%s stability, %s accessibility, %s population growth.", opad, h, new String[]{"-" + (int)stability, "-" + Math.round(accessibility * 100.0F) + "%","-" + Math.round(popGrowth)});
        } else if (stability != 0.0F&&popGrowth !=0.0f) {
            tooltip.addPara("%s stability, %s population growth.", opad, h, new String[]{"-" + (int)stability,"-" + Math.round(popGrowth)});
        } else {
            tooltip.addPara("No perceptible impact on operations.", opad);
        }
        //todo add reference to add decay text
//        if(decay){
//            tooltip.addPara("With the raid now over, these effects will reduce by one level of severity a month.", opad);
//        }

    }

    public float getTooltipWidth() {
        return super.getTooltipWidth();
    }

    public boolean hasCustomTooltip() {
        return true;
    }

    public boolean isTooltipExpandable() {
        return super.isTooltipExpandable();
    }

    @Override
    public void modifyIncoming(MarketAPI marketAPI, PopulationComposition populationComposition) {
        populationComposition.add("pirates", (float)market.getSize());//TODO coppied from closed immigration
        populationComposition.getWeight().modifyFlat(this.getModId(), -popGrowth, "Youkai raiders");

    }
}
