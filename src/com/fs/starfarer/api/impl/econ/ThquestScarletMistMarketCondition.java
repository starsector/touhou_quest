package com.fs.starfarer.api.impl.econ;

import com.fs.starfarer.api.impl.campaign.econ.BaseMarketConditionPlugin;
import com.fs.starfarer.api.ui.TooltipMakerAPI;
import com.fs.starfarer.api.util.IntervalUtil;
import com.fs.starfarer.api.util.Misc;
import thQuest.Log;
import thQuest.ThquestStrings;
import thQuest.ThU;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ThquestScarletMistMarketCondition extends ThquestMarketIncident {
    String key = "thquest_scarlet_mist";
    String name = "Scarlet Mist";
    float stability = -1;
    float production = -3;
    List<String> effectedIndustries = new ArrayList<>(Arrays.asList("farming","aquaculture"));
    @Override
    public void apply(String id) {
        super.apply(id);
        if(!this.isIncidentMarketConditionMitigated(market)) {
            market.getStability().modifyFlat(key, stability,name);
            for (String string : effectedIndustries) {
                if (market.hasIndustry(string)) {
                    market.getIndustry(string).getSupplyBonusFromOther().modifyMult(key, production,name);
                }
            }
        }
    }

    @Override
    public void unapply(String id) {
        super.unapply(id);
        market.getStability().unmodify(key);
        for(String string : effectedIndustries){
            if(market.hasIndustry(string)){
                market.getIndustry(string).getSupplyBonusFromOther().unmodify(key);
            }
        }
    }

    @Override
    public boolean isTransient() {
        return true;
    }

    public void createTooltipAfterDescription(TooltipMakerAPI tooltip, boolean expanded) {
        Color h = Misc.getHighlightColor();
        Color n = Misc.getNegativeHighlightColor();
        float pad = 3.0F;
        float small = 5.0F;
        float opad = 10.0F;
        if(this.isIncidentMarketConditionMitigated(market)){
            tooltip.addPara(ThquestStrings.incidentMitigated, opad, h);
        }else{
            tooltip.addPara("%s stability, %s food production.", opad, n, String.valueOf((int)stability), String.valueOf((int)production));
        }
    }
}
