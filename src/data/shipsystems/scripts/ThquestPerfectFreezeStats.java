package data.shipsystems.scripts;

import com.fs.starfarer.api.GameState;
import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.WeaponAPI;
import com.fs.starfarer.api.impl.combat.BaseShipSystemScript;

public class ThquestPerfectFreezeStats extends BaseShipSystemScript {
    boolean run = false;
    @Override
    public void apply(MutableShipStatsAPI stats, String id, State state, float effectLevel) {
        super.apply(stats, id, state, effectLevel);
        //start shooting stuff.
        if (stats.getEntity() instanceof ShipAPI && Global.getCurrentState() == GameState.COMBAT&&!run) {
            ShipAPI ship = (ShipAPI) stats.getEntity();
            WeaponAPI weapon = null;
            for(WeaponAPI w : ship.getAllWeapons()){
                if(w.getSlot().isHidden()){
                    weapon = w;
                }
            }
            Global.getCombatEngine().addPlugin(new ThquestPerfectFreezeSelfAdvancingScript(ship,weapon));
        run=true;
        }
    }

    @Override
    public void unapply(MutableShipStatsAPI stats, String id) {
        super.unapply(stats, id);
        run = false;
    }
}
