package data.shipsystems.ai;

import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.util.IntervalUtil;
import org.lazywizard.lazylib.combat.AIUtils;
import org.lwjgl.util.vector.Vector2f;

public class ThquestShipDeploymentAi implements ShipSystemAIScript {

    private ShipAPI ship;
    private ShipSystemAPI system;

    private final IntervalUtil tracker = new IntervalUtil(0.17f, 0.24f);

    @Override
    public void init(ShipAPI shipAPI, ShipSystemAPI shipSystemAPI, ShipwideAIFlags shipwideAIFlags, CombatEngineAPI combatEngineAPI) {
        this.ship = shipAPI;
        this.system = shipSystemAPI;
    }

    @Override
    public void advance(float v, Vector2f vector2f, Vector2f vector2f1, ShipAPI shipAPI) {
        tracker.advance(v);
        if (tracker.intervalElapsed()) {
            if(system.canBeActivated()&&ship!=null) {
                ship.useSystem();
            }

            }
    }
}
