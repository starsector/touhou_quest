package data.shipsystems.scripts.ai;

import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.util.IntervalUtil;
import com.fs.starfarer.api.util.Misc;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.combat.AIUtils;
import org.lwjgl.util.vector.Vector2f;
import thQuest.ThU;

import java.util.logging.Logger;

public class ThquestTeleportAI implements ShipSystemAIScript {
    private ShipAPI ship;
    //    private float shipRange;
    private ShipSystemAPI system;
    float shipRange = 200;
    private final IntervalUtil tracker = new IntervalUtil(0.17f, 0.24f);

    @Override
    public void init(ShipAPI ship, ShipSystemAPI shipSystemAPI, ShipwideAIFlags shipwideAIFlags, CombatEngineAPI combatEngineAPI) {
        this.ship = ship;
        this.system = shipSystemAPI;

//        for (WeaponAPI curW : ship.getUsableWeapons()){
//            if(!curW.getType().equals(WeaponAPI.WeaponType.MISSILE) &&
////                    !curW.getSpec().getAIHints().contains(WeaponAPI.AIHints.PD) &&
//                    !curW.isDecorative()){
//                if (curW.getRange() > shipRange) shipRange = curW.getRange();
//            }
//        }
//        shipRange -= 150;//-150 so it jumps into comfortably close range
//        this.shipRange = shipRange;
    }

    @Override
    public void advance(float amount, Vector2f missileDangerDir, Vector2f collisionDangerDir, ShipAPI target) {
        tracker.advance(amount);
        if (tracker.intervalElapsed()) {
            //        for (WeaponAPI curW : ship.getUsableWeapons()){
//            if(!curW.getType().equals(WeaponAPI.WeaponType.MISSILE) &&
////                    !curW.getSpec().getAIHints().contains(WeaponAPI.AIHints.PD) &&
//                    !curW.isDecorative()){
//                if (curW.getRange() > shipRange) shipRange = curW.getRange();
//            }
//        }
//        shipRange -= 150;//-150 so it jumps into comfortably close range
//        this.shipRange = shipRange;

//            boolean isInRange = AIUtils.getNearestEnemy(ship)!=null || Misc.getDistance(AIUtils.getNearestEnemy(ship).getLocation(),ship.getLocation())>shipRange+150;
//            //enemy not close
//            if (ship.get!ship.getAIFlags().hasFlag(ShipwideAIFlags.AIFlags.)&&!ship.getAIFlags().hasFlag(ShipwideAIFlags.AIFlags.BACKING_OFF)&&){
//                ship.giveCommand(ShipCommand.USE_SYSTEM,null,0);
//                //enemy close
//            } else if (ship.getAIFlags().hasFlag()) {
//
//            }
            if(ship.getSystem().canBeActivated()){
                //AI 2
                //seems to not go where it should go
                //in simulation teleports backwards at the start
//                float angle;
//                Vector2f targetVector = ship.getMouseTarget();
//                ShipEngineControllerAPI engine = ship.getEngineController();
//
//                if(engine.isAccelerating()){
//                    angle=0;
//                    if(engine.isStrafingLeft()){
//                        angle = angle - 45;
//                    }else if (engine.isStrafingRight()){
//                        angle = angle + 45;
//                    }
//                }else{
//                    angle=180;
//                    if(engine.isStrafingLeft()){
//                        angle = angle + 45;
//                    }else if (engine.isStrafingRight()){
//                        angle = angle - 45;
//                    }
//
//                }
//                angle = angle + ship.getFacing();
//                targetVector = ThU.modifyVector(ThU.angleVector(angle),2000);
//                ship.giveCommand(ShipCommand.USE_SYSTEM, targetVector, 0);


                //AI 1
                Vector2f targetVector = ship.getMouseTarget();
                if (ship.getAIFlags().hasFlag(ShipwideAIFlags.AIFlags.BACKING_OFF)
                ) {
                    Vector2f directonVector = new Vector2f();
                    float angle = Misc.getAngleInDegrees(ship.getLocation(),ship.getMouseTarget());
                    angle = angle + 180;
                    directonVector = ThU.modifyVector(ThU.angleVector(angle),2000);
                    targetVector = ThU.addVector(directonVector,ship.getMouseTarget());
                }
                ship.giveCommand(ShipCommand.USE_SYSTEM, targetVector, 0);

            }

        }
    }
}
