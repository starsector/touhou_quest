package data.shipsystems.ai;

import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.prototype.Utils;
import org.lazywizard.lazylib.combat.AIUtils;
import org.lazywizard.lazylib.combat.CombatUtils;
import org.lazywizard.lazylib.combat.DefenseUtils;
import org.lazywizard.lazylib.combat.WeaponUtils;
import org.lwjgl.util.vector.Vector2f;

public class ThquestYoumuNonAI implements ShipSystemAIScript {
    private ShipAPI ship;
    private CombatEngineAPI engine;
    private ShipwideAIFlags flags;
    private ShipSystemAPI system;
    @Override
    public void init(ShipAPI ship, ShipSystemAPI system, ShipwideAIFlags flags, CombatEngineAPI engine) {

        this.ship = ship;
        this.flags = flags;
        this.engine = engine;
        this.system = system;
    }

    @Override
    public void advance(float v, Vector2f vector2f, Vector2f vector2f1, ShipAPI target) {
        if(target==null){
            return;
        }
        if(!AIUtils.canUseSystemThisFrame(ship)){
            return;
        }
            Vector2f targetloc=target.getLocation();
            Vector2f me=ship.getLocation();
            //range of 700
            if(Math.sqrt(Math.abs(Math.pow(targetloc.getY()-me.getY(),2)+Math.pow(targetloc.getY()-me.getY(),2)))<700){
                ship.useSystem();
            }
            FluxTrackerAPI f = ship.getFluxTracker();
            if(f.getHardFlux()>f.getMaxFlux()*.8f){
                ship.useSystem();
            }
    }
}
