package data.shipsystems.scripts;

import com.fs.starfarer.api.GameState;
import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.impl.combat.BaseShipSystemScript;
import com.fs.starfarer.api.impl.combat.ThquestKillingDollPlugin;
import com.fs.starfarer.api.util.IntervalUtil;
import com.fs.starfarer.api.util.Misc;
import org.magiclib.util.MagicRender;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ThquestKillingDollStats extends BaseShipSystemScript {
    float shipTimeMult = 1000f;//todo change
    boolean run = false;
//    Map<ShipAPI,List<IntervalUtil>> intervalData1 = new HashMap<>();
//    Map<ShipAPI,List<IntervalUtil>> intervalData2 = new HashMap<>();
    @Override
    public void apply(MutableShipStatsAPI stats, String id, State state, float effectLevel) {
        super.apply(stats, id, state, effectLevel);

        ShipAPI ship = (ShipAPI) stats.getEntity();

        stats.getTimeMult().modifyMult("thquestKillingDoll",shipTimeMult);
        boolean player = ship == Global.getCombatEngine().getPlayerShip();
        ship.getAIFlags().setFlag(ShipwideAIFlags.AIFlags.HARASS_MOVE_IN);
        if (player) {
            Global.getCombatEngine().getTimeMult().modifyMult("thquestKillingDoll", 1.0F / shipTimeMult);
        } else {
            Global.getCombatEngine().getTimeMult().unmodify("thquestKillingDoll");
        }
//        draw UI bonus
           if(player) {
               Global.getCombatEngine().maintainStatusForPlayerShip(this.getClass(), Global.getSettings().getSpriteName("ui", "icon_op"), "time multiplier", Misc.getRoundedValue(shipTimeMult), false);
           }

        if (stats.getEntity() instanceof ShipAPI && Global.getCurrentState() == GameState.COMBAT) {
            if (!run) {
                WeaponAPI weapon = null;
                for (WeaponAPI w : ship.getAllWeapons()) {
                    if (w.getSlot().isHidden()) {
                        weapon = w;
                    }
                }
//                if (!projData.containsKey(ship)) {
//                    projData.put(ship, new ArrayList<DamagingProjectileAPI>());
//                    IntervalUtil interval1 = new IntervalUtil(0.1f, 0.1f);
//                    IntervalUtil interval2 = new IntervalUtil(5f, 5f);
//                    intervalData1.put(ship, new ArrayList<IntervalUtil>());
//                    intervalData2.put(ship, new ArrayList<IntervalUtil>());
//                }
                //script is in weapon

                //TODO uncomment
                ((ThquestKillingDollPlugin)weapon.getEffectPlugin()).start(weapon.getShip());


//            Global.getCombatEngine().addPlugin(new ThquestKillingDollSelfAdvancingScript(ship,weapon));
                //run script
                run = true;
            }
//
        }
    }


    @Override
    public void unapply(MutableShipStatsAPI stats, String id) {
        super.unapply(stats, id);
        stats.getTimeMult().unmodify("thquestKillingDoll");
        Global.getCombatEngine().getTimeMult().unmodify("thquestKillingDoll");
        run = false;
    }
}
