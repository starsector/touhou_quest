package data.shipsystems.scripts;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.impl.combat.BaseShipSystemScript;
import com.fs.starfarer.api.plugins.ShipSystemStatsScript;
import com.fs.starfarer.combat.entities.Ship;
import org.lwjgl.util.vector.Vector2f;
import org.magiclib.util.MagicRender;
import thQuest.ThU;

import java.awt.*;

public class ThquestSanaeStats extends BaseShipSystemScript {
    String timeKey = "thquestSanaeStatsKey";
    String soundKey = "thquestSanaeSoundKey";
    public void apply(MutableShipStatsAPI stats, String id, ShipSystemStatsScript.State state, float effectLevel) {
        //three circles appear around Sanae, teal top, blue bottom left, pink bottom right.
        //green sparks fly out.
        //bubbles expand a great deal, doing massive EMP damage to everything.
        //first expands slowly, then quickly
        //after fadeout, still see green EMP arcs in area

        //stats
        stats.getShieldDamageTakenMult().modifyMult(id,0f);
        stats.getArmorDamageTakenMult().modifyMult(id,0f);
        stats.getHullDamageTakenMult().modifyMult(id,0f);

        if(stats.getEntity()!=null){
            Ship ship = (Ship) stats.getEntity();
            CombatEngineAPI combatEngine = Global.getCombatEngine();
            float v = combatEngine.getElapsedInLastFrame();
            float flux_decrease = 1000;
            ship.getFluxTracker().decreaseFlux(v*flux_decrease);

            float arcsPerSecondPerShip = 5;
            float progress = (Float)ship.getCustomData().get(timeKey);
            boolean playSound = (Boolean)ship.getCustomData().get(soundKey);
            float baseDirection = ship.getFacing();
            float distance = 25f;
            Vector2f greenPos = ThU.addVector(ThU.multiplyVector(ThU.angleVector(baseDirection),distance),ship.getLocation());
            Vector2f purplePos = ThU.addVector(ThU.multiplyVector(ThU.angleVector(baseDirection+120),distance),ship.getLocation());
            Vector2f bluePos = ThU.addVector(ThU.multiplyVector(ThU.angleVector(baseDirection+240),distance),ship.getLocation());

            //1. 1s fade in
            //2. grow lineraly 2s
            //3. slow down a little 0.2s
            //4. grow exponentially 2s
            float defaultSize = 50f;
            float p1growth = 10f;
            float p2growth = 5f;
            float exgrowth = 10f;

            float size;
            float opacity;
            if(progress<1){
                opacity = progress/2;
                size = defaultSize;
            }else if(progress<3){
                opacity = 0.5f;
                size = defaultSize + p1growth*(progress-1);
            }else if(progress<3.2){
                opacity = 0.5f;
                size = defaultSize + p1growth*2 + p2growth *(progress-3);
            }else if(progress<5.2){
                opacity = 0.5f;
                size = (float) (defaultSize + p1growth * 2 + p2growth * 0.2f + Math.pow(progress-3.2f,exgrowth));
                if(!playSound && progress>5){
                    Global.getSoundPlayer().playSound("thquest_se_enep02",1,.3f,ship.getLocation(),ship.getVelocity());
                    ship.setCustomData(soundKey,true);
                }
            }else{
                opacity = (float) ((float) 1- (progress - 5.2))/2;
                if(opacity<0){
                    opacity = 0f;
                }
                size = (float) (defaultSize + p1growth * 2 + p2growth * 0.2f + Math.pow(progress-3.2f,exgrowth));
            }

            MagicRender.singleframe(Global.getSettings().getSprite("systemMap","radar_mask"), greenPos, new Vector2f(size,size),0, new Color(0, 1, .45f,opacity), true );
            MagicRender.singleframe(Global.getSettings().getSprite("systemMap","radar_mask"), purplePos, new Vector2f(size,size),0, new Color(.894f, 0.6745f, 1,opacity), true );
            MagicRender.singleframe(Global.getSettings().getSprite("systemMap","radar_mask"), bluePos, new Vector2f(size,size),0, new Color(0.3137f, 0.4314f, 1,opacity), true );

            //effect of the bubble
            float rand = (float)Math.random();
            int times = 0;
            while(rand<v*arcsPerSecondPerShip){
                times++;
                rand +=v;
            }

            for(ShipAPI s : combatEngine.getShips()){
                if(s.getOwner()!= ship.getOwner() && !s.isHulk()){
                    if(ThU.vectorToSpeed(ThU.minusVector(s.getLocation(),ship.getLocation()))<size/2) {
                        for (int i = 0; i < times; i++) {
                            float randomDirection = (float) Math.random() * 365;
                            Vector2f randomLocation = ThU.addVector(ThU.multiplyVector(ThU.multiplyVector(ThU.angleVector(randomDirection), s.getCollisionRadius()), (float) Math.random()), s.getLocation());
                            combatEngine.spawnEmpArcPierceShields(ship, randomLocation, s, s, DamageType.ENERGY, 25, 500, (float) (s.getCollisionRadius() * Math.random()), "system_emp_emitter_impact", 10, new Color(0.3f, 0.86f, 0.46f, 0.5f), new Color(1f, 1f, 1f, 0.5f));
                        }
                    }
                }
            }


            ship.setCustomData(timeKey,v+progress);

        }

    }
    @Override
    public void unapply(MutableShipStatsAPI stats, String id) {
        super.unapply(stats, id);
        if(stats.getEntity()!=null) {
            Ship ship = (Ship) stats.getEntity();
            ship.setCustomData(timeKey,0f);
            ship.setCustomData(soundKey,false);
        }
        stats.getShieldDamageTakenMult().unmodify(id);
        stats.getArmorDamageTakenMult().unmodify(id);
        stats.getHullDamageTakenMult().unmodify(id);
        }
}
