package data.shipsystems.scripts;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.impl.combat.BaseShipSystemScript;
import com.fs.starfarer.api.plugins.ShipSystemStatsScript;

import java.awt.*;

public class ThquestFocusStats extends BaseShipSystemScript {
    public static final float TURNSPEEDDEBUFF = .5F;
    public static final float TURNACCELERATIONDEBUFF = 1F;
    public static final float ACCELERATIONDEBUFF = 1F;
    public static final float SPEEDDEBUFF = .5F;

    public void apply(MutableShipStatsAPI stats, String id, ShipSystemStatsScript.State state, float effectLevel) {
    }

    public void unapply(MutableShipStatsAPI stats, String id) {
        stats.getTurnAcceleration().unmodify(id);
        stats.getMaxTurnRate().unmodify(id);
        stats.getAcceleration().unmodify(id);
        stats.getMaxSpeed().unmodify(id);
    }

    public ShipSystemStatsScript.StatusData getStatusData(int index, ShipSystemStatsScript.State state, float effectLevel) {
        float shipTimeMult = 1.0F + 2.0F * effectLevel;
        return index == 0 ? new ShipSystemStatsScript.StatusData("speed lowered", false) : null;
    }
}
