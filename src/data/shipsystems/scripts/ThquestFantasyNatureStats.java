package data.shipsystems.scripts;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.graphics.SpriteAPI;
import com.fs.starfarer.api.impl.combat.BaseShipSystemScript;
import com.fs.starfarer.api.plugins.ShipSystemStatsScript;
import com.fs.starfarer.api.util.Misc;
import com.fs.starfarer.combat.entities.BaseEntity;
import com.fs.starfarer.combat.entities.Missile;
import org.lwjgl.util.vector.Vector2f;
import org.magiclib.util.MagicRender;
import thQuest.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ThquestFantasyNatureStats extends BaseShipSystemScript {
    public void apply(MutableShipStatsAPI stats, String id, ShipSystemStatsScript.State state, float effectLevel) {
        final float EFFECTRADIUS = 2000f;
        if (stats.getEntity() instanceof ShipAPI) {
            ShipAPI ship = (ShipAPI) stats.getEntity();
            ship.setCollisionClass(CollisionClass.NONE); //reimu become invulnerable!
            stats.getHullDamageTakenMult().modifyMult(id,0);
            stats.getArmorDamageTakenMult().modifyMult(id,0);
            stats.getShieldDamageTakenMult().modifyMult(id,0);
            List<WeaponAPI> weapons =ship.getAllWeapons();
            //fire fantasy seal
            for(WeaponAPI w:weapons){
                if(w.getSlot().isHidden()){
                    if(w.getCooldownRemaining()==0f){
                        //remove projs
                        int shipSide = ship.getOwner();
                        Vector2f origin = ship.getLocation();
                        for(DamagingProjectileAPI p : Global.getCombatEngine().getProjectiles()){
                            if(p.getOwner()!=shipSide&& Misc.getDistance(p.getLocation(),origin)<EFFECTRADIUS){
                                Global.getCombatEngine().removeEntity(p);
                            }
                        }
                        for(MissileAPI m : Global.getCombatEngine().getMissiles()){
                            if(m.getOwner()!=shipSide&&Misc.getDistance(m.getLocation(),origin)<EFFECTRADIUS){
                                m.explode();
                            }
                        }
                        for(BeamAPI b :Global.getCombatEngine().getBeams()){
                            if(b.getWeapon().getShip().getOwner()!=shipSide&&Misc.getDistance(b.getTo(),origin)<EFFECTRADIUS||Misc.getDistance(b.getFrom(),origin)<EFFECTRADIUS){
                                b.getWeapon().setForceNoFireOneFrame(true);
                            }
                        }
                        //make a big thingy
                    }
                    w.setForceFireOneFrame(true);
                }
            }
        }
    }
    public void unapply(MutableShipStatsAPI stats, String id) {
        if (stats.getEntity() instanceof ShipAPI) {
            //set fantasy seal to be ready to fire again.
            ShipAPI ship = (ShipAPI) stats.getEntity();
            List<WeaponAPI> weapons =ship.getAllWeapons();
            for(WeaponAPI w:weapons){
                if(w.getSlot().isHidden()) {
                w.setRemainingCooldownTo(0f);
                w.setAmmo(8);
                }
                }
            ship.setCollisionClass(CollisionClass.SHIP); //reimu become uninvulnerable!
            stats.getHullDamageTakenMult().unmodify(id);
            stats.getArmorDamageTakenMult().unmodify(id);
            stats.getShieldDamageTakenMult().unmodify(id);
        }
    }

    }
