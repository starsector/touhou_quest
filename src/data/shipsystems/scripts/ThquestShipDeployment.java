package data.shipsystems.scripts;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.impl.combat.BaseShipSystemScript;
import com.fs.starfarer.api.loading.WeaponSlotAPI;
import com.fs.starfarer.api.util.Misc;
import com.fs.starfarer.combat.entities.Ship;
import org.lwjgl.util.vector.Vector2f;
import thQuest.ThU;

import java.util.Arrays;
import java.util.List;

public class ThquestShipDeployment extends BaseShipSystemScript {
    public static String launchPhaseKey = "thquestLaunchPhase";
    public static String weaponSlotKey = "thquestWeaponSlotPrevious";
    public static String targetWeaponSlotKey = "thquestTargetWeaponSlot";
    public static String timeTrackerKey = "thquestTimeTrackerKey";
    int order = -1;

    @Override
    public void unapply(MutableShipStatsAPI stats, String id) {
        super.unapply(stats, id);
        order++;
    }

    @Override
    public void apply(MutableShipStatsAPI stats, String id, State state, float effectLevel) {
        float v = Global.getCombatEngine().getElapsedInLastFrame();
        super.apply(stats, id, state, effectLevel);
        CombatEngineAPI combatEngine = Global.getCombatEngine();
        if(stats==null||combatEngine==null) return;
        ShipAPI shipAPI = (ShipAPI) stats.getEntity();
        Ship ship = (Ship) stats.getEntity();
        if (ship != null && ship.getChildModules().size() > 0) {
            ShipAPI module = ((ShipAPI) stats.getEntity()).getChildModulesCopy().get(0);

//                for(ShipEngineControllerAPI.ShipEngineAPI e : module.getEngineController().getShipEngines()){
//                    e.
//                }

            //causes game crash
            //((Ship)stats.getEntity()).childModules.remove(((Ship)stats.getEntity()).childModules.get(0))

            //detach module
            //if it is just setParentStation the AI breaks
            //if it is just setStationSlot then the map breaks and AI does not work right

//Ship deploying logic
            if(ship.getChildModules().size()>order) {
                Ship detachingShip = ship.getChildModules().get(order);
                if (detachingShip.getStationSlot() != null && detachingShip.getParentStation() != null) {
                    //TODO if ships are destroyed early, then it breaks

                    String ws = detachingShip.getStationSlot().getId();
                    detachingShip.setCustomData(weaponSlotKey, ws);

                    detachingShip.setParentStation(null);
                    detachingShip.setCollisionClass(CollisionClass.FIGHTER);
                    detachingShip.setStationSlot(null);
                    FleetMemberAPI detachingFleetMember = detachingShip.getFleetMember();
                    detachingFleetMember.getCrewComposition().addCrew(detachingFleetMember.getNeededCrew());
                    detachingShip.setCustomData(launchPhaseKey, 0);
                    detachingShip.setCustomData(timeTrackerKey, 0f);

                    List<String> leftList = Arrays.asList("SHIP BAY 6", "SHIP BAY 4", "SHIP BAY 9", "SHIP BAY 12");
                    List<String> rightList = Arrays.asList("SHIP BAY 7", "SHIP BAY 5", "SHIP BAY 10", "SHIP BAY 13");
                    List<String> centerList = Arrays.asList("SHIP BAY 2", "SHIP BAY 3", "SHIP BAY 8", "SHIP BAY 11"); //not all center. Means skip phase 1 and move to the center.

                    //doesn't matter just can't be empty
                    detachingShip.setCustomData(targetWeaponSlotKey, "SHIP BAY 1");

                    if (ws.equals("SHIP BAY 1")) {
                        //skip to launch
                        detachingShip.setCustomData(launchPhaseKey, 2);
                    } else if (centerList.contains(ws)) {
                        detachingShip.setCustomData(launchPhaseKey, 1);
                        detachingShip.setCustomData(targetWeaponSlotKey, "SHIP BAY 1");
                    } else if (leftList.contains(ws)) {
                        detachingShip.setCustomData(launchPhaseKey, 0);
                        detachingShip.setCustomData(targetWeaponSlotKey, "SHIP BAY 2");
                    } else if (rightList.contains(ws)) {
                        //skip to
                        detachingShip.setCustomData(launchPhaseKey, 0);
                        detachingShip.setCustomData(targetWeaponSlotKey, "SHIP BAY 3");
                    }
                }
                //TODO moving and launching logic
                //movement logic
                detachingShip.setFacing(ship.getFacing() + 180);
                if (detachingShip.getCustomData().containsKey(launchPhaseKey)) {
                    int phase = (int) detachingShip.getCustomData().get(launchPhaseKey);
                    float time = (float) detachingShip.getCustomData().get(timeTrackerKey);
                    float secondsPerPhase = 1f;
                    time += combatEngine.getElapsedInLastFrame();
                    detachingShip.setCustomData(timeTrackerKey, time);
                    float progress = time / secondsPerPhase;
                    Vector2f source = ship.getVariant().getSlot((String) detachingShip.getCustomData().get(weaponSlotKey)).computePosition(ship);
                    Vector2f target = ship.getVariant().getSlot((String) detachingShip.getCustomData().get(targetWeaponSlotKey)).computePosition(ship);
                    if (phase < 2) {
                        Vector2f nv = nudgeLoc(source, target, progress);
                        detachingShip.setLoc(nv);
                        if (nv.equals(target)) {
                            phase++;
                            if (phase == 1) {
                                detachingShip.setCustomData(weaponSlotKey, detachingShip.getCustomData().get(targetWeaponSlotKey));
                                detachingShip.setCustomData(targetWeaponSlotKey, "SHIP BAY 1");
                                detachingShip.setCustomData(timeTrackerKey, 0f);
                            }
                        }
                    }
                    if (phase == 2) {
                        //launch!
                        //basic logic

                        detachingShip.setRenderEngines(true);
                        detachingShip.setControlsLocked(false);

                        Vector2f slot1pos = ship.getVariant().getSlot("SHIP BAY 1").computePosition(ship);
                        float dist = Misc.getDistance(detachingShip.getLocation(), slot1pos);
                        if (Misc.getDistance(detachingShip.getLocation(), ship.getLocation()) < ship.getCollisionRadius()) {
                            //ship must stay in the line directly behind ship bay 1
                            Vector2f vectorAngle = ThU.angleVector(ship.getFacing() + 180);
                            Vector2f locationOverride = ThU.multiplyVector(vectorAngle, dist);
                            if (ThU.vectorToSpeed(locationOverride) < 175) {
                                detachingShip.setLoc(ThU.addVector(slot1pos, locationOverride));
                            }

                            detachingShip.getTravelDrive().forceState(ShipSystemAPI.SystemState.IN, 1f);
                            float facing = detachingShip.getFacing();
                            detachingShip.getVelocity().set(ThU.addVector(ThU.multiplyVector(ThU.angleVector(facing), 10), detachingShip.getVelocity()));
                        } else {
                            ship.setLayer(CombatEngineLayers.FRIGATES_LAYER);
                            detachingShip.setCollisionClass(CollisionClass.SHIP);
                        }
                    }
                    detachingShip.setCustomData(launchPhaseKey, phase);
                }
            }
            //old buggy logic that made the ships fly everywhere
//                Vector2f relativePos = ThU.getReversePositionRelative(ship.getFacing()-90,ship.getLocation(),detachingShip.getLocation());
//                Vector2f zero = ship.getVariant().getSlot("SHIP BAY 1").getLocation();
//                float trackSpeed = 20;
//
//                if((int)detachingShip.getCustomData().get("thquestLaunchPhase")==0){
//                    //set front/back position
//                    if(relativePos.getX()<zero.getX()-0.2){
//                        relativePos.setX(relativePos.getX()+v*trackSpeed);
//                        if(relativePos.getX()>zero.getX()){
//                            relativePos.setX(zero.getX());
//                        }
//                    }
//                    if(relativePos.getX()>zero.getX()+0.2) {
//                        relativePos.setX(relativePos.getX()-v*trackSpeed);
//                        if(relativePos.getX()<zero.getX()){
//                            relativePos.setX(zero.getX());
//                        }
//                    }
//                    if(relativePos.getX()>zero.getX()-0.2 && relativePos.getX()<zero.getX()+0.2){
//                        detachingShip.setCustomData("thquestLaunchPhase",1);
//                    }
//                }
//                if((int)detachingShip.getCustomData().get("thquestLaunchPhase")==1){
//                    relativePos.setX(zero.getX());
//                    //set left/right position
//                    if(relativePos.getY()<zero.getY()-0.2){
//                        relativePos.setY(relativePos.getY()+v*trackSpeed);
//                        if(relativePos.getY()>zero.getY()){
//                            relativePos.setY(zero.getY());
//                        }
//                    }
//                    if(relativePos.getY()>zero.getY()+0.2) {
//                        relativePos.setY(relativePos.getY()-v*trackSpeed);
//                        if(relativePos.getY()<zero.getY()){
//                            relativePos.setY(zero.getY());
//                        }
//                    }
//                    if(relativePos.getY()>zero.getY()-0.2 && relativePos.getY()<zero.getY()+0.2){
//                        detachingShip.setCustomData("thquestLaunchPhase",2);
//                    }
//                }
//                detachingShip.setLoc(ThU.getPositionRelative(ship.getFacing()-90,ship.getLocation(),relativePos));








            //retreval logic
//else{
//    ShipAPI detachingShip = ship.getChildModules().get(0);
//    WeaponSlotAPI ws = (WeaponSlotAPI) detachingShip.getCustomData().get("bcomWeaponSlotPrevious");
//    detachingShip.setParentStation(ship);
//    detachingShip.setStationSlot(ws);
//    detachingShip.getLocation().set(ws.computePosition(ship));
//}


            //ship.childModules.set(0,Global.getCombatEngine().getShips().get(2))

            // //spawn new ship
            //		CampaignFleetAPI emptyFleet = Global.getFactory().createEmptyFleet(faction.getId(), "Reinforcements", true);
            //		FleetMemberAPI member = emptyFleet.getFleetData().addFleetMember(variant);
            //		member.getVariant().addTag(Tags.SHIP_LIMITED_TOOLTIP);
            //		emptyFleet.getMemoryWithoutUpdate().set(MemFlags.MEMORY_KEY_NO_SHIP_RECOVERY, true);
            //		member.setShipName(name);
            //		//member.setAlly(true);
            //		member.setOwner(objective.getOwner());
            //		ShipAPI moduleShip = engine.getFleetManager(objective.getOwner()).spawnFleetMember(member, Misc.getPointAtRadius(objective.getLocation(), 750f), 0f, 0f);
            //			moduleShip.setAlly(ship.isAlly()); //yow no way one of these is backing the player up!
            // //attach ship

//            combatEngine.getShips().get(5).setStationSlot(ship.getChildModules().get(0).getStationSlot());
//            combatEngine.getShips().get(5).setParentStation(ship);
//            ship.childModules.set(0,combatEngine.getShips().get(5))

////spawn new ship
//            CampaignFleetAPI emptyFleet = Global.getFactory().createEmptyFleet(Factions.PIRATES, "Reinforcements", true); //TODO change to something sensible
//            FleetMemberAPI member = emptyFleet.getFleetData().addFleetMember("thquest_marisa_standard");
//            member.getVariant().addTag(Tags.SHIP_LIMITED_TOOLTIP);
//            emptyFleet.getMemoryWithoutUpdate().set(MemFlags.MEMORY_KEY_NO_SHIP_RECOVERY, true);
//            member.setShipName("foobar");
////member.setAlly(true);
//            member.setOwner(ship.getOwner());
//            ShipAPI moduleShip = combatEngine.getFleetManager(ship.getOwner()).spawnFleetMember(member, Misc.getPointAtRadius(ship.getLocation(), 0f), 0f, 0f);
//            moduleShip.setAlly(ship.isAlly()); //no way one of these is backing the player up!
//// attach ship
//
//            ship.getChildModules().set(0,(Ship)moduleShip);
//            moduleShip.setStationSlot(ship.getChildModules().get(0).getStationSlot());
//            moduleShip.setParentStation(ship);
////            ((Ship)moduleShip).station=true;



            //TODO
            //try to have a module pre-loaded. Then see if once let go, the module plays as a ship. And if possible, can be replaced.
            //module moves from under the ship, then the ship is released.
        }
    }
    public Vector2f nudgeLoc(Vector2f v1, Vector2f v2, float progress){
        if(progress==0){
            return v1;
        }else if(progress>1){
            return v2;
        }
        return ThU.addVector(ThU.modifyVector(v1,Math.abs(1-progress)),ThU.modifyVector(v2,progress));
    }
}
