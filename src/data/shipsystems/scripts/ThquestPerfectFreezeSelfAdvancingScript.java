package data.shipsystems.scripts;

import com.fs.starfarer.api.EveryFrameScript;
import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.input.InputEventAPI;
import com.fs.starfarer.api.loading.ProjectileSpecAPI;
import com.fs.starfarer.api.util.IntervalUtil;
import com.fs.starfarer.combat.entities.BaseEntity;
import org.lwjgl.util.vector.Vector2f;
import org.magiclib.util.MagicRender;
import thQuest.ThU;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ThquestPerfectFreezeSelfAdvancingScript extends BaseEveryFrameCombatPlugin {
    float timer = 0f;
    float chanceOfBulletsPhase1 = 20;
    ShipAPI ship;
    WeaponAPI weapon;
    float slowdownAmount = 5f;
    boolean frozenBulletsStarted=false;
    IntervalUtil p2Interval = new IntervalUtil(0.5f,0.5f);
    int p2tracker = 0;
    List<DamagingProjectileAPI> phase1projectiles = new ArrayList<>();
    List<DamagingProjectileAPI> phase2projectiles = new ArrayList<>();
    String spriteKey = "thquestPerfectFreezeSprite";

    public ThquestPerfectFreezeSelfAdvancingScript(ShipAPI ship, WeaponAPI weapon) {
        this.ship = ship;
        this.weapon = weapon;
    }

    @Override
    public void advance(float v,List<InputEventAPI> events) {
        CombatEngineAPI engine = Global.getCombatEngine();
        if (ship != null && ship.isAlive()&&!engine.isPaused()) {
            timer = timer + v;
            if (timer < 3f) {
                float chance = v * chanceOfBulletsPhase1;
                int number = (int) chance; //for over 100% chance
                chance = chance - number;
                if (chance < Math.random()) {
                    number++;
                }
                String weaponId;
                String sprite = "thquest_red_bullet";
                for (int i = 0; i < chance; i++) {
                    int randomNumber = ((int) (Math.random() * 10000)) % 4;
                    switch (randomNumber) {
                        case 0:
                            sprite = "thquest_red_bullet";
                            break;
                        case 1:
                            sprite = "thquest_yellow_bullet";
                            break;
                        case 2:
                            sprite = "thquest_blue_bullet";
                            break;
                        default:
                            sprite = "thquest_green_bullet";
                            break;
                    }
                    weaponId = "thquest_perfect_freeze_white";
                    DamagingProjectileAPI proj = (DamagingProjectileAPI) ThU.modifyRangeAndSpawnProjectile(engine, weapon, weaponId, (float) (360 * Math.random()), (float) ((Math.random() - 0.5) * 0.2)+1, true);
                    proj.setCustomData(spriteKey,sprite);
                    phase1projectiles.add(proj);
                    Global.getSoundPlayer().playSound("thquest_tan00", (float) 1,0.5f,ship.getLocation(),ship.getVelocity());
                }
                //render colored bullet over white one
                renderFakeProjs(phase1projectiles,v);
                //phase 1, multicolor bullets
            } else if (timer <=5){
                //keep rendering projs
                renderFakeProjs(phase1projectiles,v);
            }else if (timer > 5 && timer < 6) {
                //shots freeze, instant stop
                List<DamagingProjectileAPI> removeList = new ArrayList<>();
                for (DamagingProjectileAPI p : phase1projectiles) {
                    if (p == null || p.isExpired()) {
                        removeList.add(p);
                    } else {
//                        DamagingProjectileAPI p2proj =(DamagingProjectileAPI) ThU.modifyRangeAndSpawnProjectileAtLocation(engine, weapon, "thquest_perfect_freeze_white", (float) (360 * Math.random()), 1f, false,p.getLocation());
                        ((BaseEntity)p).setVel(ThU.modifyVector(ThU.angleVector(p.getFacing()),0f));
                        p.setFacing((float) (Math.random()*365));
//                        ((BaseEntity)p2proj).setVel(ThU.modifyVector(ThU.angleVector(p2proj.getFacing()),0.000001f));
//                        ((BaseEntity)p2proj).getA
//                        p2proj.getProjectileSpec().
                        phase2projectiles.add(p);
//                        engine.removeEntity(p);
                        removeList.add(p);
                    }
                    //((BaseEntity)proj)
                }
                phase1projectiles.removeAll(removeList);
                //when they stop, make white bullets
            } else if (timer > 6 && timer < 10) {
                //shoot blue bullets
                if (p2tracker < 4) {
                    p2Interval.advance(v);
                    if (p2Interval.intervalElapsed()) {
                        p2tracker++;
                        float angleBetween = 15f;
                        int shots = 8;
                        float startingAngle = (float) (ship.getFacing() - (shots-0.5)/2*angleBetween);
                        int i = 0;
                        for(;i<shots;i++){
                            spawnProjSeries(ship,weapon,engine,startingAngle + i * angleBetween);
                            Global.getSoundPlayer().playSound("thquest_tan00", (float) 0.7,0.5f,ship.getLocation(),ship.getVelocity());
                        }
                        //8 different things
                        // 80 degree angle
                        //3 shots per thing
                        //4 vollies
                    }
                }
            } else if (timer > 10&& timer <20) {
                //start frozen bullets
                if(!frozenBulletsStarted){
                    frozenBulletsStarted=true;
                    float startingSpeed = 200f;
                    for(DamagingProjectileAPI p:phase2projectiles){
                        ((BaseEntity)p).setVel(ThU.modifyVector(ThU.angleVector(p.getFacing()),startingSpeed));
                    }
                }
            }else if(timer >20){
                engine.removePlugin(this);
            }
        }
    }
    public void spawnProjSeries(ShipAPI ship,WeaponAPI weapon, CombatEngineAPI engine, float angle){
        ThU.modifyRangeAndSpawnProjectile(engine,weapon,"thquest_perfect_freeze_blue",angle,1f,true);
        ThU.modifyRangeAndSpawnProjectile(engine,weapon,"thquest_perfect_freeze_blue",angle,0.95f,true);
        ThU.modifyRangeAndSpawnProjectile(engine,weapon,"thquest_perfect_freeze_blue",angle,0.9f,true);
    }
    public void renderFakeProjs(List<DamagingProjectileAPI> projs,float v){
        for(DamagingProjectileAPI proj : projs) {
            if (!proj.isExpired()&&!proj.didDamage()) {
                MagicRender.singleframe(Global.getSettings().getSprite("misc", (String) proj.getCustomData().get(spriteKey)), ThU.addVector(proj.getLocation(), ThU.modifyVector(ThU.angleVector(proj.getFacing()), proj.getMoveSpeed() * v)), new Vector2f(18, 18), proj.getFacing(), Color.WHITE, true);
            }
        }
    }
}
