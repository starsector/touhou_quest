package data.shipsystems.scripts;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.graphics.SpriteAPI;
import com.fs.starfarer.api.impl.combat.BaseShipSystemScript;
import com.fs.starfarer.api.plugins.ShipSystemStatsScript;
import com.fs.starfarer.api.util.Misc;
import com.fs.starfarer.combat.entities.BaseEntity;
import com.fs.starfarer.combat.entities.Missile;
import org.lwjgl.util.vector.Vector2f;
import org.magiclib.util.MagicRender;
import thQuest.ThU;

import java.awt.*;

public class ThquestReverseStats extends BaseShipSystemScript {
    private final float EFFECTRADIUS = 1000f;

    public void apply(MutableShipStatsAPI stats, final String id, ShipSystemStatsScript.State state, float effectLevel) {
    //turn all projectiles(or maybe just enemy) around in a radius. The missiles change allegences and target the ship that launched them
        ShipAPI ship = null;
        if (stats.getEntity() instanceof ShipAPI) {
            ship = (ShipAPI) stats.getEntity();
            int shipSide = ship.getOwner();
            Vector2f origin = ship.getLocation();
            float effectRadius = (EFFECTRADIUS*stats.getSystemRangeBonus().getBonusMult())+stats.getSystemRangeBonus().getFlatBonus();
            for(DamagingProjectileAPI p : Global.getCombatEngine().getProjectiles()){
                if(p.getOwner()!=shipSide&& Misc.getDistance(p.getLocation(),origin)<effectRadius){
                p.setFacing(p.getFacing()+180f);
                if(p instanceof BaseEntity){
                    BaseEntity p2=((BaseEntity)p);
                    Vector2f vel = p2.getVelocity();
                    Vector2f vel2 = new Vector2f(vel.x*-1,vel.y*-1);
                    ((BaseEntity)p).setVel(vel2);
                }
                p.setOwner(shipSide);
                p.setSource(ship);
                }
            }
            for(MissileAPI m : Global.getCombatEngine().getMissiles()){
                if(m.getOwner()!=shipSide&&Misc.getDistance(m.getLocation(),origin)<EFFECTRADIUS){
                    m.setFacing(m.getFacing()+180f);
                    Vector2f vel = m.getVelocity();
                    Vector2f vel2 = new Vector2f(vel.x*-1,vel.y*-1);
                    ((Missile)m).setVel(vel2);
                    m.setOwner(shipSide);
                    if(m.getUnwrappedMissileAI() instanceof GuidedMissileAI){
                        ((GuidedMissileAI) m.getUnwrappedMissileAI()).setTarget(m.getSourceAPI());
                    }
                    m.setSource(ship);
                }
            }

            //draw a circle
            //"sortIcon":"graphics/ui/buttons/arrow_down2.png",
            float angle = 5;
            boolean reverseAngle = false;
            Color color = new Color(1,1,1,effectLevel);
            if(shipSide==0){
                color = new Color(0,1,0,effectLevel);
            }else if (shipSide==1){
                color = new Color(1,0,0,effectLevel);
            }
            for(int i = 0; i<360;i+=angle){
                SpriteAPI arrow = Global.getSettings().getSprite("ui","sortIcon");
                float targetAngle = i;
                if(reverseAngle){
                targetAngle+=180f;
                }
                reverseAngle = !reverseAngle;
//                MagicRender.battlespace(arrow,ThU.addVector(ship.getLocation(),ThU.modifyVector(ThU.angleVector(targetAngle),effectRadius)),ship.getVelocity(),new Vector2f(20,20),new Vector2f(),targetAngle,0f,Color.WHITE,true,0.1f,1f,1f);
                MagicRender.singleframe(
                        arrow,//sprite
                        ThU.addVector(ship.getLocation(), ThU.modifyVector(ThU.angleVector(i+ship.getFacing()), effectRadius)),//location
                        new Vector2f(20,20),//sprite size
                        targetAngle+ship.getFacing()+90,//angle
                        color,true);
            }

            //TODO improve
            //uno reverse cards. Generate eight cards in a circle halfway to the collision sphere. In a random spot. random spin, random fadeout
            SpriteAPI unoBlue = Global.getSettings().getSprite("misc","thquest_uno_blue");
            SpriteAPI unoGreen = Global.getSettings().getSprite("misc","thquest_uno_green");
            SpriteAPI unoRed = Global.getSettings().getSprite("misc","thquest_uno_red");
            SpriteAPI unoYellow = Global.getSettings().getSprite("misc","thquest_uno_yellow");
            SpriteAPI chosen = null;



            int random = (int) ((Math.random()*1000)%35);
            if(random==1){
                chosen=unoBlue;
            }else if(random==2){
                chosen=unoGreen;
            }else if(random==3){
                chosen=unoRed;
            }else if(random==4){
                chosen=unoYellow;
            }
            if(chosen!=null){
                MagicRender.battlespace(chosen,
                        ThU.addVector(ship.getLocation(),new Vector2f((float) ((Math.random()-.5)*100), (float) ((Math.random()-.5)*100))),
                        ThU.addVector(ship.getVelocity(),
                                new Vector2f((float) ((Math.random()-.5)*250), (float) ((Math.random()-.5)*250))),
                        new Vector2f(42,62),
                        new Vector2f(-4.2f,-6.2f),
                        (float) ((Math.random()-.5)*180),//angle
                        (float) ((Math.random()-.5)*200),//spin
                        Color.WHITE,false,
                        .5f,1f,1f );
            }
        }
        }
    }
