package data.skills;

import com.fs.starfarer.api.characters.ShipSkillEffect;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;

public class YamameSkill {
    //1. armour damage taken
public static float ARMOUR_DAMAGE_REDUCTION=25f;
    //2. flux cap
    public static float FLUX_CAP_INCREASE=20f;


    public static class Level1 implements ShipSkillEffect {
        @Override
        public void apply(MutableShipStatsAPI mutableShipStatsAPI, ShipAPI.HullSize hullSize, String s, float v) {
        mutableShipStatsAPI.getArmorDamageTakenMult().modifyPercent(s,-ARMOUR_DAMAGE_REDUCTION);
        }

        @Override
        public void unapply(MutableShipStatsAPI mutableShipStatsAPI, ShipAPI.HullSize hullSize, String s) {
        mutableShipStatsAPI.getArmorDamageTakenMult().unmodify(s);
        }

        @Override
        public String getEffectDescription(float v) {
            return "-"+(int)+ARMOUR_DAMAGE_REDUCTION+"% armour damage taken";
        }

        @Override
        public String getEffectPerLevelDescription() {
            return null;
        }

        @Override
        public ScopeDescription getScopeDescription() {
            return ScopeDescription.PILOTED_SHIP;
        }
    }
    public static class Level2 implements ShipSkillEffect {
        @Override
        public void apply(MutableShipStatsAPI mutableShipStatsAPI, ShipAPI.HullSize hullSize, String s, float v) {
        mutableShipStatsAPI.getFluxCapacity().modifyPercent(s,FLUX_CAP_INCREASE);
        }

        @Override
        public void unapply(MutableShipStatsAPI mutableShipStatsAPI, ShipAPI.HullSize hullSize, String s) {
        mutableShipStatsAPI.getFluxCapacity().unmodify(s);
        }

        @Override
        public String getEffectDescription(float v) {
            return "+"+(int)+FLUX_CAP_INCREASE+"% flux capacity";

        }

        @Override
        public String getEffectPerLevelDescription() {
            return null;
        }

        @Override
        public ScopeDescription getScopeDescription() {
            return ScopeDescription.PILOTED_SHIP;
        }
    }
}
