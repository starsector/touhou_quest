package data.missions.thquesttowho;

import com.fs.starfarer.api.fleet.FleetGoal;
import com.fs.starfarer.api.fleet.FleetMemberType;
import com.fs.starfarer.api.impl.campaign.ids.BattleObjectives;
import com.fs.starfarer.api.impl.campaign.ids.StarTypes;
import com.fs.starfarer.api.mission.FleetSide;
import com.fs.starfarer.api.mission.MissionDefinitionAPI;
import com.fs.starfarer.api.mission.MissionDefinitionPlugin;

public class MissionDefinition implements MissionDefinitionPlugin {

    public void defineMission(MissionDefinitionAPI api) {

        // Set up the fleets so we can add ships and fighter wings to them.
        // In this scenario, the fleets are attacking each other, but
        // in other scenarios, a fleet may be defending or trying to escape
        api.initFleet(FleetSide.PLAYER, "GDF", FleetGoal.ATTACK, false);
        api.initFleet(FleetSide.ENEMY, "ISS", FleetGoal.ATTACK, true);

//		api.getDefaultCommander(FleetSide.PLAYER).getStats().setSkillLevel(Skills.COORDINATED_MANEUVERS, 3);
//		api.getDefaultCommander(FleetSide.PLAYER).getStats().setSkillLevel(Skills.ELECTRONIC_WARFARE, 3);

        // Set a small blurb for each fleet that shows up on the mission detail and
        // mission results screens to identify each side.
        api.setFleetTagline(FleetSide.PLAYER, "Mayohiga Pact picket");
        api.setFleetTagline(FleetSide.ENEMY, "Enemy Mercs with captured Mayohiga Pact ships");

        // These show up as items in the bulleted list under
        // "Tactical Objectives" on the mission detail screen
        api.addBriefingItem("Defeat the enemy and help your fellow Kappa!");
        api.addBriefingItem("Don't lose the flagship.");


        // Set up the player's fleet.  Variant names come from the
        // files in data/variants and data/variants/fighters
        //api.addToFleet(FleetSide.PLAYER, "station_small_Standard", FleetMemberType.SHIP, "Test Station", false);

        api.addToFleet(FleetSide.PLAYER, "thquest_aurora_myon_assault", FleetMemberType.SHIP, "GDF Phantom Fumo", true);
        api.addToFleet(FleetSide.PLAYER, "thquest_snc_standard", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.PLAYER, "thquest_palanquin_ship_standard", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.PLAYER, "thquest_moon_rocket_2_Standard", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.PLAYER, "thquest_moon_rocket_2_Standard", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.PLAYER, "kite_Standard", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.PLAYER, "wolf_Assault", FleetMemberType.SHIP, false);
        // Set up the enemy fleet.
        api.addToFleet(FleetSide.ENEMY, "eradicator_Assault", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.ENEMY, "thquest_snc_standard", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.ENEMY, "thquest_palanquin_ship_so", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.ENEMY, "hammerhead_Balanced", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.ENEMY, "manticore_Support", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.ENEMY, "tempest_Attack", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.ENEMY, "thquest_moon_rocket_2_Standard", FleetMemberType.SHIP, false);



        api.defeatOnShipLoss("GDF Phantom Fumo");

        // Set up the map.
        float width = 12000f;
        float height = 12000f;


        api.initMap((float)-width/2f, (float)width/2f, (float)-height/2f, (float)height/2f);

        float minX = -width/2;
        float minY = -height/2;

        // Add an asteroid field
        api.addAsteroidField(minX, minY + height / 2, 0, 8000f,
                20f, 70f, 100);

        api.addPlanet(0, 0, 80f, StarTypes.BROWN_DWARF, 250f, true);

    }

}