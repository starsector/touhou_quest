package data.missions.thquestDevTest;

import com.fs.starfarer.api.fleet.FleetGoal;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.fleet.FleetMemberType;
import com.fs.starfarer.api.mission.FleetSide;
import com.fs.starfarer.api.mission.MissionDefinitionAPI;
import com.fs.starfarer.api.mission.MissionDefinitionPlugin;

public class MissionDefinition implements MissionDefinitionPlugin {

    public void defineMission(MissionDefinitionAPI api) {

        // Set up the fleets
        api.initFleet(FleetSide.PLAYER, "GDF", FleetGoal.ATTACK, false, 2);
        api.initFleet(FleetSide.ENEMY, "ISS", FleetGoal.ATTACK, true, 3);

        // Set a blurb for each fleet
        api.setFleetTagline(FleetSide.PLAYER, "Hello World!");
        api.setFleetTagline(FleetSide.ENEMY, "Various ships");

        // These show up as items in the bulleted list under
        // "Tactical Objectives" on the mission detail screen
        api.addBriefingItem("Test out Mayohiga Pact ships");

        // Set up the player's fleet
        api.addToFleet(FleetSide.PLAYER, "thquest_halfgon_standard", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.PLAYER, "thquest_psh_standard", FleetMemberType.SHIP, true);
        api.addToFleet(FleetSide.PLAYER, "thquest_aurora_myon_assault", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.PLAYER, "thquest_snc_standard", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.PLAYER, "thquest_palanquin_ship_so", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.PLAYER, "thquest_palanquin_ship_standard", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.PLAYER, "thquest_moon_rocket_2_Standard", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.PLAYER, "thquest_reimu_elite", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.PLAYER, "thquest_marisa_standard", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.PLAYER, "thquest_sakuya_standard", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.PLAYER, "thquest_cirno_standard", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.PLAYER, "thquest_sanae_standard", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.PLAYER, "thquest_ruukoto_standard", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.PLAYER, "thquest_yamabiko_standard", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.PLAYER, "thquest_dex_standard", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.PLAYER, "thquest_baku_standard", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.PLAYER, "thquest_eagle_yr_assault", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.PLAYER, "thquest_hisoutensoku_standard", FleetMemberType.SHIP, false);

        // Set up the enemy fleet
        api.addToFleet(FleetSide.ENEMY, "onslaught_Outdated", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.ENEMY, "heron_Strike", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.ENEMY, "shrike_Support", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.ENEMY, "condor_Strike", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.ENEMY, "hammerhead_Balanced", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.ENEMY, "mule_d_pirates_Standard", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.ENEMY, "hound_Standard", FleetMemberType.SHIP,  false);
        api.addToFleet(FleetSide.ENEMY, "thquest_condor_yr_standard", FleetMemberType.SHIP,  false);
        api.addToFleet(FleetSide.ENEMY, "thquest_yamabiko_standard", FleetMemberType.SHIP,  false);
        api.addToFleet(FleetSide.ENEMY, "thquest_baku_standard", FleetMemberType.SHIP,  false);
        api.addToFleet(FleetSide.ENEMY, "thquest_eagle_yr_assault", FleetMemberType.SHIP,  false);
        api.addToFleet(FleetSide.ENEMY, "thquest_eagle_yr_assault", FleetMemberType.SHIP,  false);



        // Set up the map.
        float width = 24000f;
        float height = 18000f;
        api.initMap((float)-width/2f, (float)width/2f, (float)-height/2f, (float)height/2f);

        float minX = -width/2;
        float minY = -height/2;

        // All the addXXX methods take a pair of coordinates followed by data for
        // whatever object is being added.

        // Add two big nebula clouds
        api.addNebula(minX + width * 0.66f, minY + height * 0.5f, 2000);
        api.addNebula(minX + width * 0.25f, minY + height * 0.6f, 1000);
        api.addNebula(minX + width * 0.25f, minY + height * 0.4f, 1000);

        // And a few random ones to spice up the playing field.
        for (int i = 0; i < 5; i++) {
            float x = (float) Math.random() * width - width/2;
            float y = (float) Math.random() * height - height/2;
            float radius = 100f + (float) Math.random() * 400f;
            api.addNebula(x, y, radius);
        }

        // add objectives
        api.addObjective(minX + width * 0.25f + 2000f, minY + height * 0.5f,
                "sensor_array");
        api.addObjective(minX + width * 0.75f - 2000f, minY + height * 0.5f,
                "comm_relay");
        api.addObjective(minX + width * 0.33f + 2000f, minY + height * 0.4f,
                "nav_buoy");
        api.addObjective(minX + width * 0.66f - 2000f, minY + height * 0.6f,
                "nav_buoy");


        api.addAsteroidField(-(minY + height), minY + height, -45, 2000f,
                20f, 70f, 100);

    }

}





