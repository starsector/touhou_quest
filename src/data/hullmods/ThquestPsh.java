package data.hullmods;

import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.ui.TooltipMakerAPI;
import com.fs.starfarer.api.util.CampaignEngineGlowIndividualEngine;
import org.magiclib.util.MagicRender;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ThquestPsh extends BaseHullMod {
    @Override
    public void applyEffectsBeforeShipCreation(ShipAPI.HullSize hullSize, MutableShipStatsAPI stats, String id) {
        stats.getZeroFluxMinimumFluxLevel().modifyFlat(id, -1f);
        stats.getMaxSpeed().modifyMult(id,0);
    }
    @Override
    public void advanceInCombat(ShipAPI ship, float amount) {

        Set<String> modifiers = new HashSet<>();
        MutableShipStatsAPI stats =ship.getMutableStats();
        modifiers.addAll( stats.getMaxSpeed().getMultMods().keySet());
        modifiers.addAll( stats.getMaxSpeed().getPercentMods().keySet());
        modifiers.addAll( stats.getMaxSpeed().getFlatMods().keySet());
        for(String s : modifiers){
            stats.getMaxSpeed().unmodify(s);
        }
    }

}
