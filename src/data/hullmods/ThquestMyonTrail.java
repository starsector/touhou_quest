package data.hullmods;

import com.fs.starfarer.api.GameState;
import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.CombatEngineLayers;
import com.fs.starfarer.api.combat.ShipAPI;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.VectorUtils;
import org.lwjgl.util.vector.Vector2f;
import org.magiclib.plugins.MagicTrailPlugin;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;

public class ThquestMyonTrail extends BaseHullMod {
    float id =0f;
    float carry = 0f;
    Map<ShipAPI,Float> key = new HashMap<>();
    Map<ShipAPI,Float> position = new HashMap<>();
    Map<ShipAPI,Float> pressure = new HashMap<>();
    @Override
    public void applyEffectsAfterShipCreation(ShipAPI ship, java.lang.String id) {
        key.put(ship, MagicTrailPlugin.getUniqueID());
        position.put(ship, 0f);
        pressure.put(ship, 2f);
        if (Global.getCurrentState() == GameState.COMBAT) {
        //swap sprite
            ship.setSprite("thquest_myon", "0");
        }
    }
    @Override
    public void advanceInCombat(ShipAPI ship, float amount) {
        position.put(ship,position.get(ship)+amount);
        float time=position.get(ship);
        double speed = Math.abs(ship.getVelocity().getX())+Math.abs(ship.getVelocity().getY());
        //System.out.println(speed);
        float wiggleEffect= 60*(float) Math.sin(time*10);
        //position.put(ship,position.get(ship)+pressure.get(ship));
        //    if (wiggleEffect > 0f && pressure.get(ship) > -10f ) {
        //        pressure.put(ship, pressure.get(ship) - amount * 45);
        //    } else if (wiggleEffect < 0f && pressure.get(ship) < 10f) {
        //        pressure.put(ship, pressure.get(ship) + amount * 45);
        //    }


        //lil push backward
        double rad= Math.toRadians(ship.getFacing());
        Vector2f reverseFacingVector = new Vector2f(-(float) Math.cos(rad), -(float) Math.sin(rad));



        float movementDir=VectorUtils.getFacing(ship.getVelocity());
        movementDir=movementDir+90;
        movementDir= (float) Math.toRadians(movementDir);
        float xOff = (float) Math.cos(movementDir);
        float yOff = (float) Math.sin(movementDir);

            Vector2f finalVector = new Vector2f(wiggleEffect*xOff+reverseFacingVector.getX()*100,wiggleEffect*yOff+reverseFacingVector.getY()*100);
            MagicTrailPlugin.addTrailMemberAdvanced(
                    ship,
                    key.get(ship),
                    Global.getSettings().getSprite("base_trail_smoke"),
                    ship.getShieldCenterEvenIfNoShield(),
                    0f,
                    0f,
                    //VectorUtils.getAngleStrict(finalVector,ship.getVelocity()),
                    VectorUtils.getAngle(finalVector,ship.getVelocity()),
                    //ship.getFacing(),
                    .3f,
                    .9f, //if too high, causes sprite to fold in on itself
                    15f,//start size
                    0f,//end size
                    Color.white,
                    Color.white,
                    1f,
                    0f,
                    .4f,
                    .12f,
                    true,
                    -1f,
                    0f,
                    (float) Math.random()*20,//texture offset
                    finalVector,//offset vilocity
                    null, //advanced options
                    CombatEngineLayers.ABOVE_SHIPS_AND_MISSILES_LAYER,
                    0f
                    );
//VectorUtils.rotateAroundPivot(new Vector2f((float) Math.random()*5,(float) Math.random()),ship.getLocation(),VectorUtils.getFacing(ship.getVelocity()))
            carry=carry-.02f;
        //}
    }


}
