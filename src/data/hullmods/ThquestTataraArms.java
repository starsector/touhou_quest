package data.hullmods;

import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;

public class ThquestTataraArms extends BaseHullMod {
    public static final float COST_REDUCTION = 50.0F;
    public static final float DAMAGE_REDUCTION = 50.5f;

    public void applyEffectsBeforeShipCreation(ShipAPI.HullSize hullSize, MutableShipStatsAPI stats, String id) {
        stats.getDynamic().getMod("large_ballistic_mod").modifyMult(id, 1f-(COST_REDUCTION/100f));
        stats.getDynamic().getMod("medium_ballistic_mod").modifyMult(id, 1f-(COST_REDUCTION/100f));
        stats.getDynamic().getMod("small_ballistic_mod").modifyMult(id, 1f-(COST_REDUCTION/100f));
        stats.getWeaponDamageTakenMult().modifyMult(id, DAMAGE_REDUCTION/100f);

    }


    public String getDescriptionParam(int index, ShipAPI.HullSize hullSize) {
        switch (index){
            case 0:
                return (int)COST_REDUCTION+"%";
            case 1:
                return (int)DAMAGE_REDUCTION+"%";
            default:
                return null;
        }
    }

    public boolean affectsOPCosts() {
        return true;
    }

}
