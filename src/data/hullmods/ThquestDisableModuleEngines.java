package data.hullmods;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.*;

public class ThquestDisableModuleEngines extends BaseHullMod {

    //apply before/after combat does not work
    @Override
    public void advanceInCombat(ShipAPI ship, float amount) {
        super.advanceInCombat(ship, amount);
            for (ShipAPI s : ship.getChildModulesCopy()) {
                if(s.getParentStation()!=null){
                    s.setRenderEngines(false);
                    for(WeaponAPI w : s.getAllWeapons()){
                        w.setForceNoFireOneFrame(true);
                    }
                }



        }
    }
}
