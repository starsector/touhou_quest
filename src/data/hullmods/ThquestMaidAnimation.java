package data.hullmods;

import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipEngineControllerAPI;
import com.fs.starfarer.api.util.Misc;
import org.lazywizard.lazylib.VectorUtils;
import thQuest.ThU;

public class ThquestMaidAnimation extends BaseHullMod {

    private double turn= 0.0;
    private int frame =0;
    private int newFrame =0;
    @Override
    public void advanceInCombat(ShipAPI ship, float amount) {

    ShipEngineControllerAPI c =ship.getEngineController();
        float angledif =Misc.getAngleDiff(Misc.normalizeAngle(VectorUtils.getFacing(ship.getVelocity())),ship.getFacing());
    if(ship.getAngularVelocity()>100){
        newFrame = 2;
    }else if(ship.getAngularVelocity()<-100){
        newFrame = 1;
    }else if(angledif>30f&&angledif<150){
        if(ThU.isAngleRightRelitive(VectorUtils.getFacing(ship.getVelocity()),ship.getFacing())){
            newFrame = 2;
        }else{
            newFrame = 1;
        }
    }else if(angledif<-30&&angledif>-150){
        newFrame = 1;
    }else{
        newFrame = 0;
    }
    if(newFrame!=frame){
        frame = newFrame;
        if(frame==0){
        ship.setSprite("thquest_animation",ship.getHullSpec().getHullId()+"_normal");
        }else if(frame==1){
            ship.setSprite("thquest_animation",ship.getHullSpec().getHullId()+"_left");
        }else if(frame==2){
            ship.setSprite("thquest_animation",ship.getHullSpec().getHullId()+"_right");
        }
    }
//        if(c.isTurningLeft()||c.isStrafingLeft()){
//            turn=turn-amount;
//        }else if (c.isTurningRight()||c.isStrafingRight()){
//            turn=turn+amount;
//        }else{
//            if(turn>0){
//                turn=turn-amount;
//            }else{
//                turn=turn+amount;
//            }
//        }

//        if(turn<-.05&&frame!=1){
//            frame=1;
//            turn= -.1;
//        ship.setSprite("thquest_animation",ship.getHullSpec().getHullId()+"_left");
//    }else if (turn>.05&&frame!=2){
//        frame=2;
//        turn= .1;
//        ship.setSprite("thquest_animation",ship.getHullSpec().getHullId()+"_right");
//    }else if(turn<.05&&turn>-.05&&frame!=0){
//        frame=0;
//        turn= 0.0F;
//        ship.setSprite("thquest_animation",ship.getHullSpec().getHullId()+"_normal");
//    }
//        if(turn<-.1){
//            turn= -.1;
//        }else if(turn>.1){
//            turn = .1;
//        }
    }
}
