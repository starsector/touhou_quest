package data.hullmods;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.WeaponAPI;
import com.fs.starfarer.api.util.Misc;

public class ThquestSpreadFire extends BaseHullMod {
    public static final float MAX_RECOIL_DEBUFF=8f;
    public static final float DAMAGE_SCALING=60f;
    @Override
    public void applyEffectsBeforeShipCreation(ShipAPI.HullSize hullSize, MutableShipStatsAPI stats, String id) {
        stats.getMaxRecoilMult().modifyFlat(id,MAX_RECOIL_DEBUFF);
    }

    @Override
    public void	advanceInCombat(ShipAPI ship, float amount){
        //bonus based on average spread of the weapons
        float totalSpread = 0f;
        int weaponsweightedtotal=0;
        int weaponweight=0;
        for(WeaponAPI w:ship.getAllWeapons()){
            if(w.getType()== WeaponAPI.WeaponType.MISSILE||w.getType()== WeaponAPI.WeaponType.SYSTEM||w.getType()== WeaponAPI.WeaponType.DECORATIVE){
                continue;
            }
            if(WeaponAPI.WeaponSize.SMALL==w.getSize()){
                weaponweight=1;
            } else if (WeaponAPI.WeaponSize.MEDIUM==w.getSize()) {
                weaponweight=2;
            } else if (WeaponAPI.WeaponSize.LARGE==w.getSize()) {
                weaponweight=4;
            }
            totalSpread+=w.getCurrSpread()*weaponweight;
            weaponsweightedtotal+=weaponweight;
        }
        float magnitude = (totalSpread/weaponsweightedtotal)/DAMAGE_SCALING ;
        //if(magnitude>1.5){
        //    magnitude=;
        //}
        MutableShipStatsAPI stats=ship.getMutableStats();
        stats.getBallisticWeaponDamageMult().modifyFlat("thquest_spread",magnitude+1);//todo changed
        stats.getEnergyWeaponDamageMult().modifyFlat("thquest_spread",magnitude+1);

        //draw UI bonus
        if(ship.equals(Global.getCombatEngine().getPlayerShip())) {
            Global.getCombatEngine().maintainStatusForPlayerShip(this.getClass(), Global.getSettings().getSpriteName("ui", "icon_op"), "spreadfire damage multiplier", Misc.getRoundedValue(magnitude + 1), false);
        }
    }
    @Override
    public boolean isApplicableToShip(ShipAPI ship) {
        return (!ship.getVariant().getHullMods().contains("thquestfocus"));
    }

    @Override
    public String getUnapplicableReason(ShipAPI ship) {
        if (ship != null && ship.getVariant().getHullMods().contains("thquestfocus")) {
            return "Incompatable with Focus Danmaku";
        }
        return null;
    }
    public String getDescriptionParam(int index, ShipAPI.HullSize hullSize){
        if(index==0){
            return ""+(int)MAX_RECOIL_DEBUFF;
        }if(index==1){
            return ""+(int)DAMAGE_SCALING;
        }
        return null;
    }
}

