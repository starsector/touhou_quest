package data.hullmods;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.CombatEngineLayers;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.graphics.SpriteAPI;
import org.lwjgl.util.vector.Vector2f;
import org.magiclib.util.MagicRender;
import thQuest.ThU;

import java.awt.*;

public class ThquestHisoutesnoku extends BaseHullMod {
    public SpriteAPI rArm1 = Global.getSettings().getSprite("thquest_hisoutensoku","r_arm_1");
    public SpriteAPI rArm2 = Global.getSettings().getSprite("thquest_hisoutensoku","r_arm_2");
    public SpriteAPI rFist = Global.getSettings().getSprite("thquest_hisoutensoku","r_fist");
    public SpriteAPI rFoot = Global.getSettings().getSprite("thquest_hisoutensoku","r_foot");
    public SpriteAPI rLeg1 = Global.getSettings().getSprite("thquest_hisoutensoku","r_leg_1");
    public SpriteAPI rLeg2 = Global.getSettings().getSprite("thquest_hisoutensoku","r_leg_2");

    public SpriteAPI lArm1 = Global.getSettings().getSprite("thquest_hisoutensoku","l_arm_1");
    public SpriteAPI lArm2 = Global.getSettings().getSprite("thquest_hisoutensoku","l_arm_2");
    public SpriteAPI lFist = Global.getSettings().getSprite("thquest_hisoutensoku","l_fist");
    public SpriteAPI lFoot = Global.getSettings().getSprite("thquest_hisoutensoku","l_foot");
    public SpriteAPI lLeg1 = Global.getSettings().getSprite("thquest_hisoutensoku","l_leg_1");
    public SpriteAPI lLeg2 = Global.getSettings().getSprite("thquest_hisoutensoku","l_leg_2");


    public void advanceInCombat(ShipAPI ship, float amount) {

        //TODO parmaterize later
        Vector2f arm1Size = new Vector2f(102,80);//Vector2f arm1Size = new Vector2f(102,193);
        Vector2f arm1Offset = new Vector2f(155,-140);

        arm1Offset.setX(arm1Offset.getX()-arm1Size.getY()*0.1f);

        Vector2f arm2Size = new Vector2f(69,189);
        Vector2f arm2Offset = new Vector2f(arm1Offset);
        arm2Offset.setX(arm2Offset.getX() + arm2Size.getY());

        arm2Offset.setX(arm2Offset.getX()-arm1Size.getY()*0.1f);

        Vector2f fistSize = new Vector2f(69,69);
        Vector2f fistOffset = new Vector2f(arm2Offset);
        fistOffset.setX(fistOffset.getX() + arm2Size.getY()/4 + fistSize.getY());


        arm1Size.setY(arm1Size.getY()*0.8f);

        float arm1Angle = ThU.vectorAngle(arm1Offset);
       float arm1Dist = ThU.vectorToSpeed(arm1Offset);
       Vector2f arm1pos = ThU.multiplyVector(ThU.angleVector(arm1Angle+ship.getFacing()),arm1Dist);

        float arm2Angle = ThU.vectorAngle(arm2Offset);
        float arm2Dist = ThU.vectorToSpeed(arm2Offset);
        Vector2f arm2pos = ThU.multiplyVector(ThU.angleVector(arm2Angle+ship.getFacing()),arm2Dist);

        float fistAngle = ThU.vectorAngle(fistOffset);
        float fistDist = ThU.vectorToSpeed(fistOffset);
        Vector2f fistpos = ThU.multiplyVector(ThU.angleVector(fistAngle+ship.getFacing()),fistDist);


        //renders in this order.
        MagicRender.singleframe(rArm1, ThU.addVector(ship.getLocation(),arm1pos),arm1Size,ship.getFacing()-90, Color.WHITE ,false,CombatEngineLayers.BELOW_SHIPS_LAYER);

        MagicRender.singleframe(rArm2, ThU.addVector(ship.getLocation(),arm2pos),arm2Size,ship.getFacing()-90, Color.WHITE ,false,CombatEngineLayers.BELOW_SHIPS_LAYER);

        MagicRender.singleframe(rFist, ThU.addVector(ship.getLocation(),fistpos),fistSize,ship.getFacing()-90, Color.WHITE ,false,CombatEngineLayers.BELOW_SHIPS_LAYER);
        //        rArm1.renderAtCenter(ship.getLocation().getX(), ship.getLocation().getY());
//        MagicRender.sin
//        ship.getExactBounds().addSegment();
//        ship.getAllWeapons().get(0).getSprite().setWidth(50);
//        ship.getExactBounds().segment
    }
}
