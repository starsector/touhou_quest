package data.hullmods;

import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipEngineControllerAPI;
import com.fs.starfarer.api.combat.WeaponAPI;
import org.lazywizard.lazylib.VectorUtils;
import thQuest.ThU;

public class ThquestSailMovement extends BaseHullMod {
    float degreesPerSecond = 10f;
    float maxAngle = 90f;
    @Override
    public void advanceInCombat(ShipAPI ship, float amount) {
        for(WeaponAPI w :ship.getAllWeapons()){
            if(w.getId().equals("thquest_pqship_deco_sail")){
                float angle = w.getCurrAngle(); //this is absolute
                ShipEngineControllerAPI secAPI = ship.getEngineController();
                float desiredAngle = VectorUtils.getFacing(ship.getVelocity());
//                float desiredAngle = VectorUtils.getFacing(ship.getVelocity())+ship.getFacing();
//                        if(secAPI.isStrafingRight()){
//                    if(secAPI.isAccelerating()){
//                        desiredAngle = (desiredAngle+45)/2;
//                    }else{
//                        desiredAngle = (desiredAngle+90)/2;
//                    }
//                } else if (secAPI.isStrafingLeft()) {
//                    if(secAPI.isAccelerating()){
//                        desiredAngle = (desiredAngle-45)/2;
//                    }else{
//                        desiredAngle = (desiredAngle-90)/2;
//                    }
//                }
                if(ThU.vectorToSpeed(ship.getVelocity())>10) {
                    if (ThU.isAngleRightRelitive(angle,desiredAngle)) {
                        w.setCurrAngle(angle + degreesPerSecond * amount);
                    } else {
                        w.setCurrAngle(angle - degreesPerSecond * amount);
                    }
                }
            }
        }
    }

}
