package data.hullmods;

import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.util.Misc;
import org.lazywizard.lazylib.VectorUtils;
import thQuest.AnimationData;
import thQuest.AnimationDataLoader;
import thQuest.Log;
import thQuest.ThU;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

public class ThquestGirlAnimation extends BaseHullMod {
    //todo to use this class, the ship needs an entry in the animation data csv

    private Map<ShipAPI,Double> turn = new HashMap<>();// = 0.0;
    private Map<ShipAPI,Integer> frame = new HashMap<>();// = 0;
    private Map<ShipAPI,Float> carryMap = new HashMap<>();// = 0f;
    private Map<ShipAPI,Integer> frameMap = new HashMap<>();// = 0;
    private Map<ShipAPI,Float> angleForTurn = new HashMap<>();//30f;
    private Map<ShipAPI,Integer> modeMap = new HashMap<>();// = 0;
    @Override
    public void advanceInCombat(ShipAPI ship, float amount) {
        AnimationData shipAnimationData = AnimationDataLoader.getAnimationData(ship.getHullSpec().getHullId());
        if(!carryMap.containsKey(ship)){
            carryMap.put(ship,0f);
            frameMap.put(ship,0);
            modeMap.put(ship,0);
        }
        //getting data from the maps
        float carry = carryMap.get(ship);
        int frame = frameMap.get(ship);
        int oldMode = modeMap.get(ship);
        try {
            if (ship.isAlive()) {
                carry = carry + amount;
                if (carry > 0.1f) {
                    carry = carry - 0.1f;
                    Logger logger = Logger.getLogger(this.getClass().getName());
                    //check where the ship is. Change mode if necessary.
                    //0=standstill
                    //1=left
                    //2=right
                    //forward loops
                    //3=leftTLeft
                    //4=rightTRight
                    //reverse loops
                    //5=leftTRight
                    //6=rightTLeft

                    //                    float angledif = Misc.getAngleDiff(Misc.normalizeAngle(VectorUtils.getFacing(ship.getVelocity())), ship.getFacing());
                    //                    if (ship.getAngularVelocity() > 100) {
                    //                        newFrame = 2;
                    //                        //for transition frames
                    //                    } else if (ship.getAngularVelocity() < -100) {
                    //                        newFrame = 1;
                    //                        //for transition frames
                    //
                    //                    } else if (ThU.vectorToSpeed(ship.getVelocity()) < 30f) {
                    //                        newFrame = 0;
                    //                    } else if (angledif > 30f && angledif < 150) {
                    //                        if (ThU.isAngleRightRelitive(VectorUtils.getFacing(ship.getVelocity()), ship.getFacing())) {
                    //                            newFrame = 2;
                    //                        } else {
                    //                            newFrame = 1;
                    //                        }
                    //                    } else {
                    //                        newFrame = 0;
                    //                    }
                    float angledif = Misc.getAngleDiff(Misc.normalizeAngle(VectorUtils.getFacing(ship.getVelocity())), ship.getFacing());
                    int turn;
                    int newMode;
                    if (ship.getAngularVelocity() > 100) {
                        turn = 2;
                        //for transition frames
                    } else if (ship.getAngularVelocity() < -100) {
                        turn = 1;
                        //for transition frames

                    } else if (ThU.vectorToSpeed(ship.getVelocity()) < 30f) {
                        turn = 0;
                    } else if (angledif > 30f && angledif < 150) {
                        if (ThU.isAngleRightRelitive(VectorUtils.getFacing(ship.getVelocity()), ship.getFacing())) {
                            turn = 2;
                        } else {
                            turn = 1;
                        }
                    } else {
                        turn = 0;
                    }

                    boolean resetFrame = false;

                    if(oldMode == turn){//00,11,22
                        newMode = oldMode;
                    }else if(oldMode == 0 && turn == 1){
                        newMode = 3;
                    }else if(oldMode == 0&& turn == 2){
                        newMode = 4;
                    }else if(oldMode == 1){
                        newMode = 5;
                    }else if(oldMode == 2){
                        newMode = 6;
                    }else if((oldMode == 3 || oldMode == 5) && turn != 1){
                        newMode = 5;
                    }else if((oldMode == 3 || oldMode == 5) && turn == 1){
                        newMode = 3;
                    }else if((oldMode == 4 || oldMode == 6) && turn != 2){
                        newMode = 6;
                    }else if((oldMode == 4 || oldMode == 6) && turn == 2){
                        newMode = 4;
                    }else{
                        logger.info("ANIMATION LOOP INVALID!");
                        newMode = 0;
                    }

                    if(oldMode!=newMode){
                        resetFrame=true;
                    }
                    //check what mode it is in. Depending on it, increment/decrement frame. Also checks for overflow
                    if(resetFrame){
                        if(newMode < 5){
                            frame = 0;
                        }else{
                            frame = shipAnimationData.getTransitionFrames()-1;
                        }
                    }else {
                        //increment
                        if (newMode < 5) {
                            frame++;
                        }else{
                            frame--;
                        }
                    }

                        if (newMode < 5) {//forward loops
                            if(newMode == 0 && shipAnimationData.getForwardFrames()<frame+1){
                                frame = 0;
                            }else if(newMode<3 && newMode!=0 && shipAnimationData.getSideFrames()<frame+1){ //todo test added newmode!=0
                                frame = 0;
                            }else if(newMode > 2 && shipAnimationData.getTransitionFrames()<frame+1){
                                frame = 0;
                                if(newMode == 4){
                                    newMode = 2;
                                }else{
                                    newMode = 1;
                                 }
                            }
                        } else {
                            if(frame<0){
                                frame = 0;
                                newMode = 0;
                            }
                        }

                    //render frame.

                    int cycle = newMode;
                        if(newMode>4){
                        cycle = newMode-2;
                        }

                    ship.setSprite("thquest_animation", ship.getHullSpec().getHullId() + "s" + cycle + "f" + frame);
                        ship.getSpriteAPI().setAngle(ship.getFacing()); //TODO remove?
                    //oldmode gets persisted
                    oldMode = newMode;
                }
                //putting data back in the maps
                carryMap.put(ship, carry);
                frameMap.put(ship, frame);
                modeMap.put(ship,oldMode);

            }
        } catch(Exception e){
            Log.getLogger().info(e.getMessage());
        }
    }
}