package data.hullmods;

import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;

public class ThquestFocusFire extends BaseHullMod {
    public final float RECOIL_BUFF = 50;
    public final float PROJ_SPEED_BUFF = 50;
    public final float ACC_BUFF = 50;
    public final float DAMAGE_DEBUFF = 10;
    public final float S_MOD_RANGE_BUFF = 10;
    @Override
    public void applyEffectsBeforeShipCreation(ShipAPI.HullSize hullSize, MutableShipStatsAPI stats, String id) {
        stats.getMaxRecoilMult().modifyMult(id,1f-RECOIL_BUFF*0.01f);
        stats.getRecoilDecayMult().modifyMult(id,1f+ RECOIL_BUFF*0.01f);
        stats.getRecoilPerShotMult().modifyMult(id,RECOIL_BUFF*0.01f);
        stats.getProjectileSpeedMult().modifyMult(id, 1f+PROJ_SPEED_BUFF*0.01f);
        stats.getAutofireAimAccuracy().modifyMult(id,1f+ACC_BUFF*0.01f);
        stats.getEnergyWeaponDamageMult().modifyMult(id,1f-DAMAGE_DEBUFF*0.01f);
        stats.getBallisticWeaponDamageMult().modifyMult(id,1f-DAMAGE_DEBUFF*0.01f);
        if(this.isSMod(stats)){
            stats.getBallisticWeaponRangeBonus().modifyPercent(id,S_MOD_RANGE_BUFF);
            stats.getEnergyWeaponRangeBonus().modifyPercent(id,S_MOD_RANGE_BUFF);
        }
    }
    @Override
    public boolean isApplicableToShip(ShipAPI ship) {
        return (!ship.getVariant().getHullMods().contains("thquestspread"));
    }

    @Override
    public String getUnapplicableReason(ShipAPI ship) {
        if (ship != null && ship.getVariant().getHullMods().contains("thquestspread")) {
            return "Incompatable with Spread Danmaku";
        }
        return null;
    }

    public String getDescriptionParam(int index, ShipAPI.HullSize hullSize){
        switch(index){
            case 0:
                return (int)RECOIL_BUFF+"%";
            case 1:
                return (int)PROJ_SPEED_BUFF+"%";
            case 2:
                return (int)ACC_BUFF+"%";
            case 3:
                return (int)DAMAGE_DEBUFF+"%";
            default:
                return null;
        }
    }

    @Override
    public String getSModDescriptionParam(int index, ShipAPI.HullSize hullSize, ShipAPI ship) {
        return (int)S_MOD_RANGE_BUFF+"%";
    }

    @Override
    public boolean isSModEffectAPenalty() {
        return false;
    }

    @Override
    public boolean hasSModEffect() {
        return true;
    }
}

