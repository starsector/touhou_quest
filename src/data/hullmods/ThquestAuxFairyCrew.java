package data.hullmods;

import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.impl.campaign.ids.Stats;
import com.fs.starfarer.combat.entities.Ship;

public class ThquestAuxFairyCrew extends BaseHullMod {
    final float CREW_REQ_REDUCTION = 75;
    final float CREW_LOST_OUTSIDE_COMBAT_REDUCTION = 100;
    final float COMBAT_READINESS_DEBUFF = 15;
    final float REPAIR_TIME_DEBUFF = 10;
    final float SUPPLY_UPKEEP_REDUCTION = 10;


    @Override
    public void applyEffectsBeforeShipCreation(ShipAPI.HullSize hullSize, MutableShipStatsAPI stats, String id) {
        stats.getMinCrewMod().modifyMult(id ,1f - CREW_REQ_REDUCTION*0.01f);
//        ((ShipAPI)stats.getEntity()).cre
        stats.getDynamic().getStat(Stats.NON_COMBAT_CREW_LOSS_MULT).modifyMult(id ,1f -CREW_REQ_REDUCTION*0.01f);
        stats.getMinCrewMod().modifyMult(id ,1f +COMBAT_READINESS_DEBUFF*0.01f);
        stats.getCombatEngineRepairTimeMult().modifyMult(id ,1f +REPAIR_TIME_DEBUFF*0.01f);
        stats.getCombatWeaponRepairTimeMult().modifyMult(id ,1f +REPAIR_TIME_DEBUFF*0.01f);
        if(this.isSMod(stats)) {
            stats.getSuppliesPerMonth().modifyMult(id, 1f +SUPPLY_UPKEEP_REDUCTION*0.01f);
        }
    }
    @Override
    public boolean isApplicableToShip(ShipAPI ship) {
        return (!ship.getVariant().getHullMods().contains("thquestMajorityFairyCrew"));
    }

    @Override
    public String getUnapplicableReason(ShipAPI ship) {
        if(ship.getVariant().getHullMods().contains("thquestMajorityFairyCrew")){
            return "Incompatible with Majority Fairy Crew";
        }
        return null;
    }
    @Override
    public String getDescriptionParam(int index, ShipAPI.HullSize hullSize){
        if(index==0){
            return ""+(int)CREW_REQ_REDUCTION+"%";
        }else if(index==1){
            return ""+(int)CREW_LOST_OUTSIDE_COMBAT_REDUCTION+"%";
        }else if(index==2){
            return ""+(int)COMBAT_READINESS_DEBUFF+"%";
        }else if(index==3){
            return ""+(int)REPAIR_TIME_DEBUFF+"%";
        }
        return null;
    }

    @Override
    public String getSModDescriptionParam(int index, ShipAPI.HullSize hullSize) {
        if(index==0){
            return ""+(int)SUPPLY_UPKEEP_REDUCTION+"%";
        }
        return null;
    }

}
