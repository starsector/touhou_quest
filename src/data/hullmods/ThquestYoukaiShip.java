package data.hullmods;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.FighterLaunchBayAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.graphics.SpriteAPI;
import com.fs.starfarer.api.util.IntervalUtil;
import com.fs.starfarer.api.util.Misc;
import org.lwjgl.util.vector.Vector2f;
import org.magiclib.util.MagicRender;
import thQuest.ThU;

import java.awt.*;

public class ThquestYoukaiShip extends BaseHullMod {
    SpriteAPI sprite = Global.getSettings().getSprite("campaignEntities", "fusion_lamp_glow");
    String id ="thquestYoukaiShip";
    //    public ThquestYoukaiShip() {
//        sprite.setColor(Color.YELLOW);
//    }

    @Override
    public void advanceInCombat(ShipAPI ship, float amount) {

        float shipTimeMult = 0.2f + Math.abs( (1.6f*(float) Math.sin(Global.getCombatEngine().getTotalElapsedTime(false))));

        Color color = Color.WHITE;
        float opacity = Math.abs(shipTimeMult-1);
        if(shipTimeMult<1){
            color = new Color(1, 1-opacity, 1-opacity,opacity);
        }else if(shipTimeMult>1){
            color = new Color(1-opacity, 1-opacity, 1,opacity);
        }

        if (ship.isAlive()) {
            super.advanceInCombat(ship, amount);
//        if(amount>Math.random()/5){
//            float angle = (float) (Math.random()*360);
//            float speed = (float) (Math.random()*50);
//            Vector2f vector2f = ThU.modifyVector(ThU.angleVector(angle),speed);
//            vector2f = ThU.addVector(vector2f,ship.getVelocity());
//            sprite.setAlphaMult(.5f);
//            MagicRender.battlespace(sprite,ship.getLocation(),vector2f,new Vector2f(30,30),new Vector2f(0,0),0f,0f,Color.BLUE,true,1f,5f,1f);
//        }

            //        float wiggleEffect= 60*(float) Math.sin(time*10);
            ship.getMutableStats().getTimeMult().modifyMult("thquestYoukaiShip", shipTimeMult);

            boolean player = ship == Global.getCombatEngine().getPlayerShip();
            if (player) {
                Global.getCombatEngine().getTimeMult().modifyMult(id, 1.0F / shipTimeMult);
            } else {
                Global.getCombatEngine().getTimeMult().unmodify(id);
            }

            ship.setJitterUnder(this,color,1f,3,20);

            //draw UI bonus
            if(player) {
                Global.getCombatEngine().maintainStatusForPlayerShip(this.getClass(), Global.getSettings().getSpriteName("ui", "icon_op"), "time multiplier", Misc.getRoundedValue(shipTimeMult), false);
            }
        }
        if(ship.hasLaunchBays()){
            for(FighterLaunchBayAPI fighterLaunchBayAPI : ship.getLaunchBaysCopy()){
                if(fighterLaunchBayAPI!=null&&fighterLaunchBayAPI.getWing()!=null){
                    for(ShipAPI s :fighterLaunchBayAPI.getWing().getWingMembers()){
                        s.getMutableStats().getTimeMult().modifyMult("thquestYoukaiShip", shipTimeMult);
                        s.setJitterUnder(this,color,1f,3,20);
                    }
                }
            }
        }
        if(ship.getChildModulesCopy()!=null){
            for(ShipAPI module : ship.getChildModulesCopy()){
                module.getMutableStats().getTimeMult().modifyMult("thquestYoukaiShip", shipTimeMult);
                module.setJitterUnder(this,color,1f,3,20);
            }
        }
    }
}
