package data.hullmods;

import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.loading.WeaponSlotAPI;
import com.fs.starfarer.api.util.IntervalUtil;

public class ThquestPofv extends BaseHullMod {
    final float ACCELERATION_BUFF=400f;
    final float TURNING_BUFF=25f;
    final float ECMBONUS =10;
    IntervalUtil engineOff = new IntervalUtil(15,60);
    final float DISABLECHANCE =.3f;
    @Override
    public void applyEffectsBeforeShipCreation(ShipAPI.HullSize hullSize, MutableShipStatsAPI stats, String id) {
        stats.getAcceleration().modifyMult(id, 1f +ACCELERATION_BUFF*0.01f);
        stats.getDeceleration().modifyMult(id,1f +ACCELERATION_BUFF*0.01f);
        stats.getTurnAcceleration().modifyMult(id,1f +TURNING_BUFF*0.01f);
        //stats.getMaxTurnRate().modifyPercent(id,TURNING_BUFF);
        //stats.getEccmChance().modifyFlat(id,ECMBONUS);
    }
    /*
    @Override
    public void	advanceInCombat(ShipAPI ship, float amount){
        engineOff.advance(amount);
        if(engineOff.intervalElapsed()){
            if(ship.getEngineController().getShipEngines().size()==0){
             //no engines
                for(WeaponAPI e:ship.getAllWeapons()){
                    if(Math.random()>DISABLECHANCE) {
                        e.disable();
                    }
                }
            }else {
                for (ShipEngineControllerAPI.ShipEngineAPI e : ship.getEngineController().getShipEngines()) {
            //engines
                    if(Math.random()>DISABLECHANCE){
                        e.disable();
                    }
                }
            }
        }
    }
            */
    @Override
    public boolean isApplicableToShip(ShipAPI ship) {
        return (ship.getHullSize() == ShipAPI.HullSize.FRIGATE || ship.getHullSize() == ShipAPI.HullSize.DESTROYER);
    }

    @Override
    public String getUnapplicableReason(ShipAPI ship) {
        if (ship != null && ship.getHullSize() != ShipAPI.HullSize.FRIGATE && ship.getHullSize() != ShipAPI.HullSize.DESTROYER) {
            return "Can only be installed on frigates and destroyers";
        }
        return null;
    }

    public String getDescriptionParam(int index, ShipAPI.HullSize hullSize){
        if(index==0){
            return ""+(int)ACCELERATION_BUFF+"%";
        }else if(index==1){
            return ""+(int)TURNING_BUFF+"%";
        }
        return null;
    }

}
