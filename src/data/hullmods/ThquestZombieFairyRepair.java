package data.hullmods;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.combat.listeners.ApplyDamageResultAPI;
import com.fs.starfarer.api.combat.listeners.DamageListener;
import com.fs.starfarer.api.combat.listeners.HullDamageAboutToBeTakenListener;
import org.lwjgl.util.vector.Vector2f;

import java.awt.*;

public class ThquestZombieFairyRepair extends BaseHullMod {
    public static String hullmodKey = "thquestZombieFairy";
    public float hpRepairRate = 30f;
    public float armorRepairRate = 30f;
    public float ventSpeedRepair = 200f;
    public static float speedMult = 0.05f;
    public static float turnSpeedMult = 0.05f;
    @Override
    public void applyEffectsAfterShipCreation(ShipAPI ship, java.lang.String id) {
        //add the damage listener
        ship.addListener(new ZombieFairyDamageListener());
    }

    @Override
    public void advanceInCombat(ShipAPI ship, float amount) {
        super.advanceInCombat(ship, amount);
        if(ship.getCustomData().containsKey(hullmodKey)&&(boolean)ship.getCustomData().get(hullmodKey)){
            ship.setPhased(true);
            ship.getPhaseCloak().forceState(ShipSystemAPI.SystemState.IN,1f);
            ship.getFluxTracker().decreaseFlux(ventSpeedRepair*amount);
            float hpLevel = ship.getHitpoints()+hpRepairRate*amount;
            if(hpLevel>ship.getMaxHitpoints()){
                hpLevel = ship.getMaxHitpoints();
                ship.removeCustomData(hullmodKey);
                ship.getMutableStats().getMaxSpeed().unmodify(hullmodKey);
                ship.getMutableStats().getMaxTurnRate().unmodify(hullmodKey);
                Global.getCombatEngine().addFloatingText(ship.getLocation(),"Fully Regenerated",20, Color.WHITE,ship,1f,0.1f);

            }
            ship.setHitpoints(hpLevel);
            ArmorGridAPI armor = ship.getArmorGrid();
            int x = armor.getGrid().length;
            int y = armor.getGrid()[0].length;
            for(int i = 0;i<x;i++){
                for(int j = 0;j<y;j++){
                    if(armor.getArmorValue(x,y)<armor.getMaxArmorInCell()){
                        float newValue = armor.getArmorValue(x,y)+armorRepairRate*amount;
                        if(newValue<armor.getMaxArmorInCell()){
                            newValue = armor.getMaxArmorInCell();
                        }
                        armor.setArmorValue(x,y,newValue);
                    }
                }
            }
        }
    }
}
class ZombieFairyDamageListener implements HullDamageAboutToBeTakenListener {
    @Override
    public boolean notifyAboutToTakeHullDamage(Object o, ShipAPI shipAPI, Vector2f vector2f, float v) {
         if(v>=shipAPI.getHitpoints()){
             shipAPI.setHitpoints(1);
             shipAPI.setPhased(true);
             shipAPI.getPhaseCloak().forceState(ShipSystemAPI.SystemState.IN,1f);
             shipAPI.getMutableStats().getMaxSpeed().modifyMult(ThquestZombieFairyRepair.hullmodKey,ThquestZombieFairyRepair.speedMult);
             shipAPI.getMutableStats().getMaxTurnRate().modifyMult(ThquestZombieFairyRepair.hullmodKey,ThquestZombieFairyRepair.turnSpeedMult);
             shipAPI.setCustomData(ThquestZombieFairyRepair.hullmodKey,true);
             Global.getCombatEngine().addFloatingText(shipAPI.getLocation(),"Critical Damage! Regenerating...",20, Color.WHITE,shipAPI,1f,0.1f);
            return true;
        }
        return false;
    }
}
