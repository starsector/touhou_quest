package data.hullmods;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.CombatEngineLayers;
import com.fs.starfarer.api.combat.CombatEntityAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.listeners.ApplyDamageResultAPI;
import com.fs.starfarer.api.combat.listeners.DamageListener;
import com.fs.starfarer.api.graphics.SpriteAPI;
import com.fs.starfarer.api.util.IntervalUtil;
import com.fs.starfarer.api.util.Misc;
import org.apache.log4j.Logger;
import org.lazywizard.lazylib.VectorUtils;
import org.lwjgl.util.vector.Vector2f;
import org.magiclib.util.MagicRender;
import thQuest.ThU;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;

public class ThquestBombDrop extends BaseHullMod implements DamageListener {
    Logger logger = Global.getLogger(this.getClass());
    float probPerDP = 0.04f;
    float topSpeed=10f;
    float speedupDistance=topSpeed/2;
    float bonusMag=topSpeed;
    float baseMag=topSpeed/20;
    float maxTargetDistance = 2000f;
    SpriteAPI bombSprite = Global.getSettings().getSprite("misc","thquest_bomb");
    Vector2f bombSize = new Vector2f(18,18);
    IntervalUtil interval = new IntervalUtil(1,2);
    ArrayList<Vector2f> bombPositions = new ArrayList<>();
    ArrayList<ShipAPI> bombTargets = new ArrayList<>();
    ArrayList<Vector2f> bombVelocities = new ArrayList<>();
    @Override
    public void advanceInCombat(ShipAPI ship, float amount) {
        //add listener to nearby enemy ships.
        //if damage listener detects a fatal hit, create bomb item that moves towards the ship then disappears.
        interval.advance(amount);
        if(interval.intervalElapsed()){
            for(ShipAPI otherShip:Global.getCombatEngine().getShips()){
                if(otherShip.getHullSize()!= ShipAPI.HullSize.FIGHTER&&otherShip.getOwner()!=ship.getOwner()&&otherShip.isAlive()&&otherShip.getListeners(this.getClass()).isEmpty()){
                    //add listener
                    otherShip.addListener(this);
                }
            }
            //check for stale bombs
            HashSet<Integer> remove= new HashSet<>();
            for(int i=0;i<bombTargets.size();i++){
                if(bombTargets.get(i)==null||!bombTargets.get(i).isAlive()){
                    remove.add(i);
                }
            }
            ArrayList<Integer> remove2= new ArrayList<>(remove);
            Collections.sort(remove2);
            for(int i = remove2.size()-1;i>=0;i--){
                bombTargets.remove(i);
                bombPositions.remove(i);
                bombVelocities.remove(i);
            }
        }
        HashSet<Integer> remove= new HashSet<>();
        for(int i=0;i<bombTargets.size();i++) {
            if (bombTargets.get(i)==ship) {
//            logger.info("runing drawing script");
                //1. acceleration based on speed and distance. Simply accelerate towards ship for now
                //get angle

//            bombVelocities.set(i,ThU.AddVector(ThU.modifyVector(ThU.AngleVector(VectorUtils.getAngle(bombPositions.get(i),bombTargets.get(i).getLocation())), (float) (10+1/(Misc.getDistance(bombPositions.get(i),bombTargets.get(i).getLocation())*.01))),bombVelocities.get(i)));
                //TODO coppied from yyorb
                //target should not be null
                float distance = Misc.getDistance(bombPositions.get(i), bombTargets.get(i).getLocation());
                float magnitude = (speedupDistance - distance) / speedupDistance;
                if (magnitude < 0) {
                    magnitude = 0;
                }
                //1. base acceleration + magnitude bonus towards target
                float targetAngle = VectorUtils.getAngle(bombPositions.get(i), bombTargets.get(i).getLocation());
                //proj.setFacing(targetAngle);
                Vector2f targetVector = ThU.angleVector(targetAngle);
                Vector2f velocity = bombVelocities.get(i);
                bombVelocities.set(i, new Vector2f(velocity.x + targetVector.getX() * (magnitude * bonusMag + baseMag), velocity.y + targetVector.getY() * (magnitude * bonusMag + baseMag)));
                velocity = bombVelocities.get(i);
                float speed = ThU.vectorToSpeed(velocity);
//                logger.info("speed="+speed);
                if (speed > topSpeed) {
                    magnitude = topSpeed / speed;
//                    logger.info("speed reduced by="+magnitude);
                    bombVelocities.set(i, new Vector2f(velocity.x * magnitude, velocity.y * magnitude));
                }

                bombPositions.set(i, ThU.addVector(bombPositions.get(i), bombVelocities.get(i)));

                MagicRender.singleframe(bombSprite, bombPositions.get(i), bombSize, 0f, Color.WHITE, false, CombatEngineLayers.FIGHTERS_LAYER);//TODO combat engine layer???
                if (Misc.getDistance(bombTargets.get(i).getLocation(), bombPositions.get(i)) < 30f) {
//                    logger.info("adding item for removal:"+i);
//                    removeBombPositions.add(bombPositions.get(i));
//                    removeBombTargets.add(bombTargets.get(i));
//                    removeBombVelocities.add(bombVelocities.get(i));
                    remove.add(i);
                    ship.getSystem().setAmmo(ship.getSystem().getAmmo() + 1);
                    Global.getSoundPlayer().playSound("thquest_se_item",1,.5f,ship.getLocation(),ship.getVelocity());
                }
            }
        }
        ArrayList<Integer> remove2= new ArrayList<>(remove);
        Collections.sort(remove2);
        for(int i = remove2.size()-1;i>=0;i--){
//            logger.info("removing item:"+remove2.get(i));
            bombTargets.remove((int)remove2.get(i));
            bombPositions.remove((int)remove2.get(i));
            bombVelocities.remove((int)remove2.get(i));
        }


    }

    @Override
    public void reportDamageApplied(Object o, CombatEntityAPI combatEntityAPI, ApplyDamageResultAPI applyDamageResultAPI) {
//        logger.info("damage detected!");
        if(!((ShipAPI)combatEntityAPI).isAlive()){
//            logger.info("kill detected");
            ((ShipAPI) combatEntityAPI).removeListener(this);
//            logger.info("rolled:"+(((ShipAPI)combatEntityAPI).getFleetMember().getDeploymentPointsCost()*probPerDP));
            if(((ShipAPI)combatEntityAPI).getFleetMember()!=null&&((ShipAPI)combatEntityAPI).getFleetMember().getDeploymentPointsCost()*probPerDP >Math.random()){
//            if(true){
//                logger.info("decided to drop bomb");
                //find nearest enemy ship with hullmod
                ShipAPI nearest = null;
                for(ShipAPI ship: Global.getCombatEngine().getShips()){
                    float distance = Misc.getDistance(combatEntityAPI.getLocation(),ship.getLocation());
//                    logger.info("distance="+distance);
                    if(ship.isAlive()&&ship.getVariant().hasHullMod("thquestbomb")&&ship.getOwner()!=combatEntityAPI.getOwner()&&distance<maxTargetDistance){
                        if(nearest!=null){
                            if(distance<Misc.getDistance(nearest.getLocation(),combatEntityAPI.getLocation())){
                                nearest=ship;
//                                logger.info("setting nearest");
                            }
                        }else{
                            nearest=ship;
//                            logger.info("setting nearest");
                        }
                    }
                }
                if(nearest!=null){
//                    logger.info("found position");
                    bombTargets.add(nearest);
                    bombVelocities.add(combatEntityAPI.getVelocity());
                    bombPositions.add(combatEntityAPI.getLocation());
                }
            }
        }
    }
}
