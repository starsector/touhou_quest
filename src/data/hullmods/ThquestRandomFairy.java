package data.hullmods;

import com.fs.starfarer.api.GameState;
import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.SettingsAPI;
import com.fs.starfarer.api.campaign.CoreUITabId;
import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.loading.FighterWingSpecAPI;
import com.fs.starfarer.api.loading.WeaponGroupSpec;
import org.apache.log4j.Logger;
import thQuest.Log;
import java.util.HashMap;
import java.util.Map;

public class ThquestRandomFairy extends BaseHullMod {
    Logger l = Log.getLogger();

    private Map<String,Boolean> hasRun = new HashMap<>();
    @Override
    public void applyEffectsAfterShipCreation(ShipAPI ship, java.lang.String id) {
//        l.info("id="+id);
//        l.info("ship id ="+ship.getId());
        int variant=0;
        if (Global.getCurrentState()==GameState.COMBAT) {
            //in combat
            variant = ((int) (Math.random() * 13421)) % 4;
            //ship.setSprite("thquest_fairy", Integer.toString(variant));
            hasRun.put(ship.getId(), false);
            //int rand = (int) (Math.random()*3);
            //if(rand==0){
            //spawn extra fighters
            switch (variant) {
                case 0:
                    ship.getVariant().addWeapon("WS0001", "thquest_red_small");
                    break;
                case 1:
                    ship.getVariant().addWeapon("WS0001", "thquest_blue_spread");
                    break;
                case 2:
                    ship.getVariant().addWeapon("WS0001", "thquest_green_small");
                    break;
                case 3:
                    ship.getVariant().addWeapon("WS0001", "thquest_laser_yellow_small");
                    break;
            }
            //I know how dumb this looks, but hear me out... It didn't work the way you would think
            for(WeaponAPI w :ship.getAllWeapons()){
                switch(w.getId()){
                    case "thquest_red_small":
                        ship.setSprite("thquest_fairy", String.valueOf(0));
                        break;
                    case "thquest_blue_spread":
                        ship.setSprite("thquest_fairy", String.valueOf(1));
                        break;
                    case "thquest_green_small":
                        ship.setSprite("thquest_fairy", String.valueOf(2));
                        break;
                    case "thquest_laser_yellow_small":
                        ship.setSprite("thquest_fairy", String.valueOf(3));
                        break;
                }
            }
        }else{
                //in the refit, storage, or other menu
                //this will mess up the codex. Do I care?
                    variant =(int)((System.currentTimeMillis()/1000)%4);
            switch (variant) {
                case 0:
                    ship.getVariant().addWeapon("WS0001", "thquest_red_small_fake");
                    break;
                case 1:
                    ship.getVariant().addWeapon("WS0001", "thquest_blue_spread_fake");
                    break;
                case 2:
                    ship.getVariant().addWeapon("WS0001", "thquest_green_small_fake");
                    break;
                case 3:
                    ship.getVariant().addWeapon("WS0001", "thquest_laser_yellow_small_fake");
                    break;
            }
        }

    }
//    @Override
//    public void advanceInCampaign(FleetMemberAPI member, float amount){
//        int variant = ((int) (Math.random() * 100)) % 4;
//        (privatesuper)member.getVariant().
//    }

    public void advanceInCombat(ShipAPI ship, float amount) {
        if (!hasRun.get(ship.getId())) {
            int rand = ((int) (Math.random() * 13421)) % 3;
            if (rand == 0) {
                for (FighterLaunchBayAPI i : ship.getWing().getSourceShip().getLaunchBaysCopy()) {
                    if (i.getWing()==ship.getWing()) {
                        //setup
                        if(i.getExtraDeployments()<i.getWing().getSpec().getNumFighters()){
                            i.setExtraDeployments(i.getWing().getSpec().getNumFighters());
                            i.setExtraDeploymentLimit(i.getWing().getSpec().getNumFighters());
                        }

                    i.setFastReplacements(i.getFastReplacements()+1);
                    i.setExtraDeployments(i.getExtraDeployments()+ 1);
                    i.setExtraDeploymentLimit(i.getExtraDeploymentLimit() + 1);
                    i.setExtraDuration(10000f);
                    //l.info("new extra deployments "+(i.getExtraDeployments()));
                    //l.info("new extra deployment limit "+(i.getExtraDeploymentLimit()));

                    }
                 }
            }
            hasRun.put(ship.getId(), true);
        }
    }
}