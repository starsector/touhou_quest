//package thQuest;
//
//import com.fs.starfarer.api.Global;
//import com.fs.starfarer.api.campaign.*;
//import com.fs.starfarer.api.campaign.econ.MarketAPI;
//import com.fs.starfarer.api.impl.campaign.ids.Conditions;
//import com.fs.starfarer.api.impl.campaign.ids.Factions;
//import com.fs.starfarer.api.impl.campaign.ids.Planets;
//import com.fs.starfarer.api.impl.campaign.ids.Terrain;
//import com.fs.starfarer.api.impl.campaign.terrain.BaseRingTerrain;
//import com.fs.starfarer.api.impl.campaign.terrain.RingSystemTerrainPlugin;
//import com.fs.starfarer.api.impl.campaign.terrain.StarCoronaTerrainPlugin;
//import com.fs.starfarer.api.util.Misc;
//
//import java.awt.*;
//import java.util.ArrayList;
//
//public class GenerateSanzu {
//    public static void createSanzu() {
//
//        //yellow star
//        //asteroid belt
//        //barren world
//        //fantastic blowhole(balkanized arid oni world with caves)
//        //astroid belt
//        //tengu fortress(gas giant, small pop, military)
//        StarSystemAPI system = Global.getSector().createStarSystem("Sanzu");
//        system.setBackgroundTextureFilename("graphics/backgrounds/background6.jpg");//TODO finalize
//        //        PlanetAPI star = system.initStar("thquest_reimaden", "thquest_yellow_black_hole",300f,-35000,-5000,100f);
//        PlanetAPI star = system.initStar("thquest_sanzu", "star_yellow", 810f, 14000, -11000, 500f);
//
//        //PLANET 1
//        PlanetAPI p1 = system.addPlanet("thquest_reimaden_I", star, "Reimaden I", Planets.PLANET_LAVA, 80, 130, 3000, -65);
//        MarketAPI m1 = Global.getFactory().createMarket("thquest_reimaden_I", "thquest_reimaden_I", 0);
//        m1.setPlanetConditionMarketOnly(true);
//        m1.addCondition(Conditions.EXTREME_TECTONIC_ACTIVITY);
//        m1.addCondition(Conditions.VERY_HOT);
//        m1.addCondition(Conditions.METEOR_IMPACTS);
//        m1.addCondition(Conditions.THIN_ATMOSPHERE);
//        m1.addCondition(Conditions.ORE_ULTRARICH);
//        m1.addCondition(Conditions.RARE_ORE_ULTRARICH);
//        m1.addCondition(Conditions.POOR_LIGHT);
//        m1.setPrimaryEntity(p1);
//        p1.setMarket(m1);
//        //ASTEROID BELT
//        system.addRingBand(star, "misc", "rings_asteroids0", 256f, 0, Color.white, 256f, 3300f, -70f);
//        system.addRingBand(star, "misc", "rings_asteroids0", 256f, 3, Color.white, 256f, 3500f, -72f);
//        system.addAsteroidBelt(star, 200, 3400, 400, -40f, -80f, Terrain.ASTEROID_BELT, "Maelstrom");
//        //ICE PLANET
//        PlanetAPI p2 = system.addPlanet("thquest_reimaden_II", star, "Reimaden II", "frozen1", 10, 90, 6000, -90);
//        MarketAPI m2 = Global.getFactory().createMarket("thquest_reimaden_II", "thquest_reimaden_II", 0);
//        m2.setPlanetConditionMarketOnly(true);
//        m2.addCondition(Conditions.DARK);
//        m2.addCondition(Conditions.VERY_COLD);
//        m2.addCondition(Conditions.NO_ATMOSPHERE);
//        m2.addCondition(Conditions.ORE_SPARSE);
//        m2.addCondition(Conditions.VOLATILES_PLENTIFUL);
//        m2.addCondition(Conditions.LOW_GRAVITY);
//        m2.setPrimaryEntity(p2);
//        p2.setMarket(m2);
//        //ICE GIANT & SYSTEM
//        //RING
//        //MOON
//        PlanetAPI p3 = system.addPlanet("thquest_reimaden_III", star, "Reimaden III", "ice_giant", 90, 285, 10000, 140);
//        MarketAPI m3 = Global.getFactory().createMarket("thquest_reimaden_II", "thquest_reimaden_II", 0);
//        m3.setPlanetConditionMarketOnly(true);
//        m3.addCondition(Conditions.DARK);
//        m3.addCondition(Conditions.VERY_COLD);
//        m3.addCondition(Conditions.VOLATILES_TRACE);
//        m3.addCondition(Conditions.HIGH_GRAVITY);
//        m3.setPrimaryEntity(p3);
//        p3.setMarket(m3);
//        //RINGS
//        accretionDisk.add(system.addRingBand(p3, "misc", "rings_special0", 256f, 0, Color.white, 256f, 550, 60f));
//        accretionDisk.add(system.addRingBand(p3, "misc", "rings_ice0", 256f, 1, Color.white, 256f, 750, 65f));
//        SectorEntityToken ring2 = system.addTerrain(Terrain.RING, new BaseRingTerrain.RingParams(250, 650f, null, "Ring System"));
//        ring2.setCircularOrbit(p3, 0, 0, 60);
//
//        //MOON
//        PlanetAPI p4 = system.addPlanet("thquest_reimaden_IIIa", p3, "Reimaden III-A", "frozen2", 10, 80, 1000, 75f);
//        MarketAPI m4 = Global.getFactory().createMarket("thquest_reimaden_IIIa", "thquest_reimaden_III-A", 3);
//        m4.setFactionId(Factions.TRITACHYON);
//        m4.setPlanetConditionMarketOnly(false);
//        m4.addCondition(Conditions.DARK);
//        m4.addCondition(Conditions.VERY_COLD);
//        m4.addCondition(Conditions.NO_ATMOSPHERE);
//        m4.addCondition(Conditions.ORE_RICH);
//        m4.addCondition(Conditions.RARE_ORE_MODERATE);
//        m4.addCondition(Conditions.LOW_GRAVITY);
//        m4.setPrimaryEntity(p4);
//        p4.setMarket(m4);
//
//
//        system.autogenerateHyperspaceJumpPoints(true, true);
//        ThU.clearDeepHyper(system.getHyperspaceAnchor(), 800);
//
//    }
//}
