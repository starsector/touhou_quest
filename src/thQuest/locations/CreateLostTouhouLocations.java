package thQuest.locations;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CustomCampaignEntityAPI;
import com.fs.starfarer.api.campaign.PlanetAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.StarSystemAPI;
import com.fs.starfarer.api.characters.FullName;
import com.fs.starfarer.api.characters.PersonAPI;
import com.fs.starfarer.api.impl.campaign.ids.Ranks;
import com.fs.starfarer.api.util.Misc;
import org.apache.log4j.Logger;
import thQuest.Log;
import thQuest.LostGirlData;
import thQuest.LostTouhou;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class CreateLostTouhouLocations {
//    public static Logger logger = Log.getLogger();
    public static void generate(){
        Logger logger = Log.getLogger();
        LostGirlData lostGirlData = new LostGirlData();
        List<LostTouhou> touhous= new ArrayList<LostTouhou>();
        touhous.add(new LostTouhou("mima","", LostTouhou.Location.DECIV));
        touhous.add(new LostTouhou("kanako","Yasaka", LostTouhou.Location.CIV_PLANET));
        touhous.add(new LostTouhou("koishi","Komeiji", LostTouhou.Location.RUINS));
        touhous.add(new LostTouhou("nue","Houjuu", LostTouhou.Location.HABITAT));
        touhous.add(new LostTouhou("kana","Anaberal", LostTouhou.Location.MINING_STATION));
        touhous.add(new LostTouhou("elis","", LostTouhou.Location.RUINS));
        touhous.add(new LostTouhou("louise","", LostTouhou.Location.DERILICT));
        touhous.add(new LostTouhou("minamitsu","Murasa", LostTouhou.Location.DECIV));
        touhous.add(new LostTouhou("kokoro","Hata", LostTouhou.Location.MINING_STATION));
        touhous.add(new LostTouhou("maribel","Hearn", LostTouhou.Location.DERILICT));
        touhous.add(new LostTouhou("renko","Usami", LostTouhou.Location.RUINS));
        touhous.add(new LostTouhou("seiran","", LostTouhou.Location.MINING_STATION));
        touhous.add(new LostTouhou("ringo","", LostTouhou.Location.DECIV));
        for( LostTouhou touhou: touhous){
            if(touhou.getLocationEnum()== LostTouhou.Location.DECIV){
                decivGenerate(touhou,lostGirlData);
            } else if (touhou.getLocationEnum()== LostTouhou.Location.DEBRIS) {
                debrisFieldGenerate(touhou,lostGirlData);
            } else if (touhou.getLocationEnum()== LostTouhou.Location.DERILICT) {
                derilictGenerate(touhou,lostGirlData);
            } else if (touhou.getLocationEnum()== LostTouhou.Location.HABITAT) {
                habitatGenerate(touhou,lostGirlData);
            } else if (touhou.getLocationEnum()== LostTouhou.Location.MINING_STATION) {
                miningGenerate(touhou,lostGirlData);
            } else if (touhou.getLocationEnum()== LostTouhou.Location.RUINS) {
                ruinsGenerate(touhou,lostGirlData);
            } else if (touhou.getLocationEnum()== LostTouhou.Location.STASH) {
                stashGenerate(touhou,lostGirlData);
            } else if (touhou.getLocationEnum()== LostTouhou.Location.CIV_PLANET) {
                civGenerate(touhou, lostGirlData);
            }
            PersonAPI person = Global.getFactory().createPerson();
            person.setId("thquest_"+touhou.getName());
            StringBuilder sb = new StringBuilder(touhou.getName());
            sb.setCharAt(0,Character.toUpperCase(sb.charAt(0)));
            person.setName(new FullName(sb.toString(),touhou.getSirname(), FullName.Gender.FEMALE));
            person.setPortraitSprite(Global.getSettings().getSpriteName("characters","thQuest_"+touhou.getName()));
            person.setRankId(Ranks.CITIZEN);
            person.setFaction("thquest_gensokyo");
            person.setPostId(Ranks.POST_SPACER);
            Global.getSector().getImportantPeople().addPerson(person);
        }
        Global.getSector().getMemory().set("$thquestlostgirldata",lostGirlData);
    }
    public static void decivGenerate(LostTouhou lostTouhou,LostGirlData lostGirlData) {
        Logger logger = Log.getLogger();
        String name = lostTouhou.getName();
        List<StarSystemAPI> starSystemAPIList = Global.getSector().getStarSystems();
        int timer = 0;
        int limit = 500;
        while(timer<limit){
            timer++;
            StarSystemAPI system =starSystemAPIList.get((int) (starSystemAPIList.size()*Math.random()));
            if(!hasCiv(system)) {
                for (PlanetAPI p : system.getPlanets()) {
                    if (p.getMarket() != null && !p.getMarket().isInvalidMissionTarget() && p.getMarket().hasCondition("decivilized") && p.getMarket().isPlanetConditionMarketOnly() && !p.getMemory().contains("$thquest_lost_touhou")) {
                        p.getMemory().set("$thquest_lost_touhou", name);
                        logger.info("generating lost girl " + name + " at " + p.getStarSystem().getName() + " system");
                        saveLocation(lostTouhou, p, lostGirlData);
                        return;
                    }
                }
            }
        }
        logger.warn("COULD NOT FIND SUITABLE LOCATION FOR "+name);
    }
    public static void ruinsGenerate(LostTouhou lostTouhou,LostGirlData lostGirlData) {
        Logger logger = Log.getLogger();
        String name = lostTouhou.getName();
        List<StarSystemAPI> starSystemAPIList = Global.getSector().getStarSystems();
        int timer = 0;
        int limit = 500;
        while(timer<limit){
            timer++;
            StarSystemAPI system =starSystemAPIList.get((int) (starSystemAPIList.size()*Math.random()));
            if(!hasCiv(system)) {
                for (PlanetAPI p : system.getPlanets()) {
                    if (p.getMarket() != null && !p.getMarket().isInvalidMissionTarget() && p.getMarket().isPlanetConditionMarketOnly()) {
                        if ((p.getMarket().hasCondition("ruins_scattered") ||
                                p.getMarket().hasCondition("ruins_widespread") ||
                                p.getMarket().hasCondition("ruins_extensive") ||
                                p.getMarket().hasCondition("ruins_vast"))&&!p.getMemory().contains("$thquest_lost_touhou")){
                            p.getMemory().set("$thquest_lost_touhou", name);
                            logger.info("generating lost girl " + name + " at " + p.getStarSystem().getName() + " system");
                            saveLocation(lostTouhou, p, lostGirlData);
                            return;
                        }
                    }
                }
            }
        }
        logger.warn("COULD NOT FIND SUITABLE LOCATION FOR "+name);
    }

    public static void debrisFieldGenerate(LostTouhou lostTouhou,LostGirlData lostGirlData){
        Logger logger = Log.getLogger();
        String name = lostTouhou.getName();
        List<StarSystemAPI> starSystemAPIList = Global.getSector().getStarSystems();
        int timer = 0;
        int limit = 500;
        while(timer<limit){
            timer++;
            StarSystemAPI system =starSystemAPIList.get((int) (starSystemAPIList.size()*Math.random()));
            if(!hasCiv(system)){
                        for(SectorEntityToken p : system.getAllEntities()){
                            if(p.hasTag("debris") && !p.getMemory().contains("$thquest_lost_touhou")){
                                p.getMemory().set("$thquest_lost_touhou",name);
                                logger.info("generating lost girl "+ name +" at "+p.getStarSystem().getName()+" system");
                                saveLocation(lostTouhou,p,lostGirlData);
                                return;
                            }
                        }
                }
        }
        logger.warn("COULD NOT FIND SUITABLE LOCATION FOR "+name);
    }
    public static void habitatGenerate(LostTouhou lostTouhou,LostGirlData lostGirlData){
        Logger logger = Log.getLogger();
        String name = lostTouhou.getName();
        List<StarSystemAPI> starSystemAPIList = Global.getSector().getStarSystems();
        int timer = 0;
        int limit = 500;
        while(timer<limit){
            timer++;
            StarSystemAPI system =starSystemAPIList.get((int) (starSystemAPIList.size()*Math.random()));
            if(!hasCiv(system)) {
                for (SectorEntityToken p : system.getCustomEntities()) {
                    if (p.getCustomEntityType().equals("orbital_habitat_remnant")&& !p.getMemory().contains("$thquest_lost_touhou")) {
                        p.getMemory().set("$thquest_lost_touhou", name);
                        logger.info("generating lost girl " + name + " at " + p.getStarSystem().getName() + " system");
                        saveLocation(lostTouhou, p, lostGirlData);
                        return;
                    }
                }
            }
        }
        logger.warn("COULD NOT FIND SUITABLE LOCATION FOR "+name);
    }
    public static void miningGenerate(LostTouhou lostTouhou,LostGirlData lostGirlData){
        Logger logger = Log.getLogger();
        String name = lostTouhou.getName();
        List<StarSystemAPI> starSystemAPIList = Global.getSector().getStarSystems();
        int timer = 0;
        int limit = 500;
        while(timer<limit){
            timer++;
            StarSystemAPI system =starSystemAPIList.get((int) (starSystemAPIList.size()*Math.random()));
            if(!hasCiv(system)) {
                for (CustomCampaignEntityAPI p : system.getCustomEntities()) {
                    if (p.getCustomEntityType().equals("station_mining_remnant")&& !p.getMemory().contains("$thquest_lost_touhou")) {
                        p.getMemory().set("$thquest_lost_touhou", name);
                        logger.info("generating lost girl " + name + " at " + p.getStarSystem().getName() + " system");
                        saveLocation(lostTouhou, p, lostGirlData);
                        return;
                    }
                }
            }
        }
        logger.warn("COULD NOT FIND SUITABLE LOCATION FOR "+name);
    }
    public static void stashGenerate(LostTouhou lostTouhou,LostGirlData lostGirlData){
        Logger logger = Log.getLogger();
        String name = lostTouhou.getName();
        List<StarSystemAPI> starSystemAPIList = Global.getSector().getStarSystems();
        int timer = 0;
        int limit = 500;
        while(timer<limit){
            timer++;
            StarSystemAPI system =starSystemAPIList.get((int) (starSystemAPIList.size()*Math.random()));
            if(!hasCiv(system)) {
                for (CustomCampaignEntityAPI p : system.getCustomEntities()) {
                    if ((p.getCustomEntityType().equals("supply_cache") || p.getCustomEntityType().equals("hidden_cache"))&& !p.getMemory().contains("$thquest_lost_touhou")) { //maybe there needs to be more types here
                        p.getMemory().set("$thquest_lost_touhou", name);
                        logger.info("generating lost girl " + name + " at " + p.getStarSystem().getName() + " system");
                        saveLocation(lostTouhou, p, lostGirlData);
                        return;
                    }
                }
            }
        }
        logger.warn("COULD NOT FIND SUITABLE LOCATION FOR "+name);
    }
    public static void derilictGenerate(LostTouhou lostTouhou,LostGirlData lostGirlData){
        Logger logger = Log.getLogger();
        String name = lostTouhou.getName();
        List<StarSystemAPI> starSystemAPIList = Global.getSector().getStarSystems();
        int timer = 0;
        int limit = 500;
        while(timer<limit){
            timer++;
            StarSystemAPI system =starSystemAPIList.get((int) (starSystemAPIList.size()*Math.random()));
            if(!hasCiv(system)) {
                for (CustomCampaignEntityAPI p : system.getCustomEntities()) {
                    if (p.getCustomEntityType().equals("wreck")&& !p.getMemory().contains("$thquest_lost_touhou")) {
                        p.getMemory().set("$thquest_lost_touhou", name);
                        logger.info("generating lost girl " + name + " at " + p.getStarSystem().getName() + " system");
                        saveLocation(lostTouhou, p, lostGirlData);
                        return;
                    }
                }
            }
        }
        logger.warn("COULD NOT FIND SUITABLE LOCATION FOR "+name);
    }
    public static void civGenerate(LostTouhou lostTouhou,LostGirlData lostGirlData){
        Logger logger = Log.getLogger();
        String name = lostTouhou.getName();
        List<StarSystemAPI> starSystemAPIList = Global.getSector().getStarSystems();
        int timer = 0;
        int limit = 500;
        while(timer<limit){
            timer++;
            StarSystemAPI system =starSystemAPIList.get((int) (starSystemAPIList.size()*Math.random()));
                for (PlanetAPI p : system.getPlanets()) {
                    if (p.getMarket() != null && !p.getMarket().isInvalidMissionTarget() && !p.getMarket().isPlanetConditionMarketOnly()) {
                            p.getMemory().set("$thquest_lost_touhou", name);
                            logger.info("generating lost girl " + name + " at " + p.getStarSystem().getName() + " system");
                            saveLocation(lostTouhou, p, lostGirlData);
                            return;
                    }
                }
        }
        logger.warn("COULD NOT FIND SUITABLE LOCATION FOR "+name);

    }
    public static void saveLocation(LostTouhou touhou, SectorEntityToken e, LostGirlData l){
        touhou.setActualLocation(e);
        l.getNotFound().add(touhou);
    }
    public static boolean hasCiv(StarSystemAPI s){
        return !Misc.getMarketsInLocation(s).isEmpty();
    }
}
