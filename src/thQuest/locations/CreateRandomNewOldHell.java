package thQuest.locations;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.*;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.characters.PersonAPI;
import com.fs.starfarer.api.impl.campaign.ids.*;
import com.fs.starfarer.api.impl.campaign.procgen.*;
import com.fs.starfarer.api.impl.campaign.terrain.BaseTiledTerrain;
import com.fs.starfarer.api.impl.campaign.terrain.NebulaTerrainPlugin;
import com.fs.starfarer.api.impl.campaign.terrain.PulsarBeamTerrainPlugin;
import com.fs.starfarer.api.impl.campaign.terrain.StarCoronaTerrainPlugin;
import com.fs.starfarer.api.util.Misc;
import com.fs.starfarer.campaign.StarSystem;
import org.lwjgl.util.vector.Vector2f;
import thQuest.ThU;

import java.util.*;

public class CreateRandomNewOldHell extends StarSystemGenerator {
    public CreateRandomNewOldHell(CustomConstellationParams params) {
        super(params);
    }

    public void createSystem(){

        //cycle through the constellations
        //pick a suitable nebula, if none is found, have other logic
        //use random star picker
        List<StarSystemAPI> starsystems = Global.getSector().getStarSystems();
        for(StarSystemAPI starSystemAPI:starsystems){
            if(starSystemAPI.getConstellation()!=null&&starSystemAPI.getConstellation().getType()== Constellation.ConstellationType.NEBULA){
                Vector2f location = ThU.starPlacer(starSystemAPI.getLocation(),12000,2000,6000f,100); //it never seems to have enough min distance
                this.generateSystem(location,starSystemAPI.getConstellation());
                break;
            }
        }
    }
    //todo test
    public void generateSystem(Vector2f loc,Constellation constellation2) {
        starAge=(constellation2.getAge());//changed
        constellationAge=starAge;//changed
        this.systemType = StarSystemType.NEBULA;//changed
        String id = "thquest_new_old_hell" ;
        //using greek letter name for nebula
//        String tag = NameGenData.TAG_NEBULA;
////                if (planet.isMoon()) tag = NameGenData.TAG_MOON;
//        String name = ProcgenUsedNames.pickName(tag, null, null).nameWithRomanSuffixIfAny;
////                planet.setName(newName);
//        ProcgenUsedNames.notifyUsed(name);
        String[] greekLetters = new String[]{"Alpha", "Beta", "Gamma", "Delta", "Epsilon", "Zeta", "Eta", "Theta", "Iota", "Kappa", "Lambda", "Mu", "Nu", "Xi", "Omicron", "Pi", "Rho", "Sigma", "Tau", "Upsilon", "Phi", "Chi", "Psi", "Omega"};
        String letter = greekLetters[constellation2.getSystems().size()];
        String name = letter + " " + constellation2.getName();
        if (!this.initSystem(name, loc)) {
            this.cleanup();
        } else {
            this.star = null;
            this.secondary = null;
            this.tertiary = null;
            this.systemCenter = null;
            if (!this.addStars(id)) {
                this.cleanup();
            } else {
                this.updateAgeAfterPickingStar();
                float binaryPad = 1500.0F;
                float maxOrbitRadius = 20000.0F;
                if (this.systemType == StarSystemGenerator.StarSystemType.BINARY_FAR || this.systemType == StarSystemGenerator.StarSystemType.TRINARY_1CLOSE_1FAR || this.systemType == StarSystemGenerator.StarSystemType.TRINARY_2FAR) {
                    float var10000 = maxOrbitRadius - (5000.0F + binaryPad);
                }

                GenResult result = this.addPlanetsAndTerrain(20000.0F);
                float primaryOrbitalRadius = this.star.getRadius();
                if (result != null) {
                    primaryOrbitalRadius = result.orbitalWidth * 0.5F;
                }

                float orbitAngle = random.nextFloat() * 360.0F;
                float baseOrbitRadius = primaryOrbitalRadius + binaryPad;
                float orbitDays = baseOrbitRadius / (3.0F + random.nextFloat() * 2.0F);
                if (this.systemType == StarSystemGenerator.StarSystemType.BINARY_FAR && this.secondary != null) {
                    this.addFarStar(this.secondary, orbitAngle, baseOrbitRadius, orbitDays);
                } else if (this.systemType == StarSystemGenerator.StarSystemType.TRINARY_1CLOSE_1FAR && this.tertiary != null) {
                    this.addFarStar(this.tertiary, orbitAngle, baseOrbitRadius, orbitDays);
                } else if (this.systemType == StarSystemGenerator.StarSystemType.TRINARY_2FAR) {
                    this.addFarStar(this.secondary, orbitAngle, baseOrbitRadius, orbitDays);
                    this.addFarStar(this.tertiary, orbitAngle + 60.0F + 180.0F * random.nextFloat(), baseOrbitRadius, orbitDays);
                }

                if (this.systemType == StarSystemGenerator.StarSystemType.NEBULA) {
                    this.star.setSkipForJumpPointAutoGen(true);
                }

                this.addJumpPoints(result, false);
                if (this.systemType == StarSystemGenerator.StarSystemType.NEBULA) {
                    this.system.removeEntity(this.star);
                    StarCoronaTerrainPlugin coronaPlugin = Misc.getCoronaFor(this.star);
                    if (coronaPlugin != null) {
                        this.system.removeEntity(coronaPlugin.getEntity());
                    }

                    this.system.setStar((PlanetAPI)null);
                    this.system.initNonStarCenter();
                    Iterator var14 = this.system.getAllEntities().iterator();

                    label53:
                    while(true) {
                        SectorEntityToken entity;
                        do {
                            if (!var14.hasNext()) {
                                this.system.getCenter().addTag("ambient_ls");
                                break label53;
                            }

                            entity = (SectorEntityToken)var14.next();
                        } while(entity.getOrbitFocus() != this.star && entity.getOrbitFocus() != this.system.getCenter());

                        entity.setOrbit((OrbitAPI)null);
                    }
                }
                //todo this section was added here before JP generation
                String tag = "";
                for(PlanetAPI p:this.system.getPlanets()){
                    if (!p.isMoon()){
                        tag = NameGenData.TAG_MOON;
                    }
                    String newName = ProcgenUsedNames.pickName(tag, null, null).nameWithRomanSuffixIfAny;
                    p.setName(newName);
                    p.getMarket().setName(newName);
                    ProcgenUsedNames.notifyUsed(name);

                }
                //custom planet
                List<PlanetAPI> eligibleParentPlanets = new ArrayList<>();
                List<PlanetAPI> eligiblePriorityParentPlanets = new ArrayList<>();

                float minDist = 0f;
                for(PlanetAPI planetAPI:system.getPlanets()){
                    if(!planetAPI.isMoon()){
                        eligibleParentPlanets.add(planetAPI);
                        if(planetAPI.isGasGiant()){
                            eligiblePriorityParentPlanets.add(planetAPI);
                        }
                    }
                }
                SectorEntityToken parent;
                if(eligiblePriorityParentPlanets.size()>0){
                    parent = eligiblePriorityParentPlanets.get((int) (Math.random()*eligiblePriorityParentPlanets.size()));
                }else if(eligibleParentPlanets.size()>0){
                    parent = eligibleParentPlanets.get((int) (Math.random()*eligibleParentPlanets.size()));
                }else{
                    parent = system.getCenter();//todo not tested
                }
                for(SectorEntityToken sectorEntityToken:system.getAllEntities()){
                    if(sectorEntityToken.getOrbitFocus()!=null&&sectorEntityToken.getOrbitFocus().equals(parent)){
                        if(sectorEntityToken.getCircularOrbitRadius()>minDist){
                            minDist=sectorEntityToken.getCircularOrbitRadius();
                        }
                    }
                }
                float dist = (float) (minDist+500+Math.random()*500);
                PlanetAPI p1 = system.addPlanet("thquest_new_old_hell", parent, "New Old Hell", Planets.TUNDRA,
                        (float)Math.random()*360,//angle
                        100,//radius
                        dist,//orbitRadius
                        dist/10);//orbit days
                p1.setFaction("thquest_gensokyo");
                                MarketAPI m1 = ThU.addMarketplace("thquest_gensokyo",p1,null,"New Old Hell",4,
                        new ArrayList<String>(Arrays.asList(
//                                Conditions.COLD,
                                Conditions.HABITABLE,
//                                Conditions.DARK,
                                Conditions.FARMLAND_POOR,
                                               Conditions.ORGANICS_ABUNDANT,
                                               Conditions.RUINS_WIDESPREAD,
                                               Conditions.ORE_MODERATE,
                                               Conditions.POPULATION_4
                                       )),//conditions
                                       new ArrayList<String>(Arrays.asList(Submarkets.SUBMARKET_OPEN,Submarkets.SUBMARKET_BLACK,Submarkets.SUBMARKET_STORAGE)),//submarkets
                                       new ArrayList<String>(Arrays.asList(
                                               Industries.POPULATION,
                                               Industries.PATROLHQ,
                                               Industries.MINING,
                                               Industries.SPACEPORT,
                                               Industries.BATTLESTATION
                                       )),//industries
                                       true,
                                       true);
                                m1.getIndustry(Industries.SPACEPORT).setImproved(true);
//                ThU.populateIntelBoard(p1);
                m1.getCommDirectory().addPerson(Global.getSector().getImportantPeople().getPerson("thquestOrin"));
                m1.setAdmin(Global.getSector().getImportantPeople().getPerson("thquestOrin"));

                PersonAPI person = m1.getFaction().createRandomPerson();
                person.setPostId(Ranks.POST_PORTMASTER);
                m1.getCommDirectory().addPerson(person);

                person = m1.getFaction().createRandomPerson();
                person.setPostId(Ranks.POST_SUPPLY_OFFICER);
                m1.getCommDirectory().addPerson(person);
                CustomCampaignEntityAPI star =system.addCustomEntity("thquest_artificial_sun","Artificial Sun","thquest_fusion_lamp", "thquest_gensokyo");
                OrbitAPI orbit = Global.getFactory().createCircularOrbit(p1,100f,200,15f);
                star.setOrbit(orbit);






                for(PlanetAPI p : system.getPlanets()){
                    p.setAutogenJumpPointNameInHyper(p.getName()+ " Gravity Well");
                }

                this.system.autogenerateHyperspaceJumpPoints(true, false);//setting to false false due to issue with naming

                if (this.systemType == StarSystemType.NEBULA) {
                    this.system.setStar(this.star);
                }
                int min = 1;
                int max = 3;
                int num = random.nextInt(max + 1 - min) + min;
                addStableLocations(this.system, num);
                //custom code
                ThU.clearDeepHyper(system.getHyperspaceAnchor(),1000);
                StarSystemGenerator.addSystemwideNebula(system,constellation2.getAge());
                //todo pick a random non-moon and add a moon that is the underground colony
//                transformPlanet.setId("thquest_new_old_hell");
//                transformPlanet.setName("New Old Hell");
//                transformPlanet.setFaction("thquest_gensokyo");
//                transformPlanet.setTypeId(Planets.TUNDRA);
//                transformPlanet.setRadius(100);
//                transformPlanet.getMarket().setName("New Old Hell");
//                transformPlanet.getMemory().set("$planetType",Planets.TUNDRA);


//                m1.addIndustry(Industries.POPULATION, Collections.singletonList(Items.ORBITAL_FUSION_LAMP));
                system.setConstellation(constellation2);//todo WHAT???
                ((StarSystem)system).setDisplayName(system.getBaseName()+" Nebula");
//                system.removeEntity(transformPlanet);
            }
        }
    }
}
