package thQuest.locations;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.*;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.impl.campaign.ids.*;
import com.fs.starfarer.api.impl.campaign.procgen.StarAge;
import com.fs.starfarer.api.impl.campaign.procgen.AccretionDiskGenPlugin;
import com.fs.starfarer.api.impl.campaign.procgen.StarGenDataSpec;
import com.fs.starfarer.api.impl.campaign.terrain.BaseRingTerrain;
import com.fs.starfarer.api.impl.campaign.terrain.EventHorizonPlugin;
import com.fs.starfarer.api.impl.campaign.terrain.RingSystemTerrainPlugin;
import com.fs.starfarer.api.impl.campaign.terrain.StarCoronaTerrainPlugin;
import com.fs.starfarer.api.loading.TerrainSpecAPI;
import com.fs.starfarer.api.util.Misc;
import org.lwjgl.util.vector.Vector2f;
import thQuest.ThU;

import java.awt.*;
import java.util.ArrayList;

public class GenerateReimaden {
    public static void createReimaden(){
        StarSystemAPI system = Global.getSector().createStarSystem("Reimaden");
        system.setBackgroundTextureFilename("graphics/backgrounds/background4.jpg");//TODO finalize
//        PlanetAPI star = system.initStar("thquest_reimaden", "thquest_yellow_black_hole",300f,-35000,-5000,100f);
        Vector2f place = ThU.starPlacer(new Vector2f(-42000,5000),12000,2000,9000f,100);
        if(place==null){
            place=new Vector2f(-45000,5000);
        }
                PlanetAPI star = system.initStar("thquest_reimaden", "thquest_yellow_black_hole",300f, place.x, place.y, 100f);
        Misc.addNebulaFromPNG("data/campaign/terrain/thquest_reimaden_nebula.png", 0.0F, 0.0F, system, "terrain", "nebula", 4, 4,StarAge.OLD);
        StarCoronaTerrainPlugin coronaPlugin = Misc.getCoronaFor(star);
        if (coronaPlugin != null) {
            system.removeEntity(coronaPlugin.getEntity());
        }
        float ehRadius =300f;
        SectorEntityToken eventHorizon = system.addTerrain
                ("event_horizon", new StarCoronaTerrainPlugin.CoronaParams
                        (star.getRadius() + ehRadius,
                                (star.getRadius() + ehRadius) / 2.0F, star, -10,
                        2f, 25));
        eventHorizon.setCircularOrbit(star, 0.0F, 0.0F, 100.0F);
        system.setLightColor(new Color(100,100,50));
        //eh = Global.getSettings().getTerrainSpec("event_horizon");
        //SectorEntityToken eh2 = system.addTerrain("event_horizon",eh);
       // eh2.setLocation(0,0);
        //system.setLightColor(new Color(255,125,125));

        //ECRETION DISK 600-2600
        //PLANET 1(magma) 3000
        //ASTEROIDS 3500
        //PLANET 2(ice) 4000
        //PLANET 3(ice giant) 10000
        //  --SOME SPECIAL RINGS
        //  --ice planet 2
        //  --barren planet
        //ASTEROIDS, ICE 12000-14000
        //NEBULA ALSO

        //ECRETion DISK
        ArrayList<RingBandAPI> accretionDisk = new ArrayList<>();
//        ecretionDisk.add(system.addRingBand(star,"misc","rings_special0",256f,0,Color.white,256f,800f,-20f));
//        ecretionDisk.add(system.addRingBand(star,"misc","rings_ice0",256f,2,Color.white,256f,900f,-20f));
//        ecretionDisk.add(system.addRingBand(star,"misc","rings_ice0",256f,4,Color.white,256f,1000f,-25f));
//        ecretionDisk.add(system.addRingBand(star,"misc","rings_ice0",256f,1,Color.white,256f,1100f,-30f));
        accretionDisk.add(system.addRingBand(star,"misc","rings_dust0",256f,3,Color.white,256f,1200f,-30f));
        accretionDisk.add(system.addRingBand(star,"misc","rings_special0",256f,0,Color.white,256f,1300f,-32f));
        accretionDisk.add(system.addRingBand(star,"misc","rings_dust0",256f,2,Color.white,256f,1400f,-34f));
        accretionDisk.add(system.addRingBand(star,"misc","rings_dust0",256f,0,Color.white,256f,1500f,-36f));
        accretionDisk.add(system.addRingBand(star,"misc","rings_dust0",256f,1,Color.white,256f,1600f,-38f));
        accretionDisk.add(system.addRingBand(star,"misc","rings_dust0",256f,3 ,Color.white,256f,1700f,-40f));
        accretionDisk.add(system.addRingBand(star,"misc","rings_special0",256f,0 ,Color.white,256f,1800f,-42f));
        accretionDisk.add(system.addRingBand(star,"misc","rings_dust0",256f,0,Color.white,256f,1900,-44f));
        accretionDisk.add(system.addRingBand(star,"misc","rings_ice0",256f,2,Color.white,256f,2000,-46f));
        accretionDisk.add(system.addRingBand(star,"misc","rings_dust0",256f,3,Color.white,256f,2100,-48f));
        accretionDisk.add(system.addRingBand(star,"misc","rings_dust0",256f,2,Color.white,256f,2200,-50f));
        accretionDisk.add(system.addRingBand(star,"misc","rings_ice0",256f,1,Color.white,256f,2300,-52f));
        accretionDisk.add(system.addRingBand(star,"misc","rings_dust0",256f,1,Color.white,256f,2400,-54f));
        accretionDisk.add(system.addRingBand(star,"misc","rings_dust0",256f,2,Color.white,256f,2500,-56f));
        accretionDisk.add(system.addRingBand(star,"misc","rings_ice0",256f,1,Color.white,256f,2600,-58f));
        accretionDisk.add(system.addRingBand(star,"misc","rings_ice0",256f,0,Color.white,256f,2800,-60f));
        for(RingBandAPI r:accretionDisk){
            r.setSpiral(true);
            r.setMinSpiralRadius(0);
            r.setSpiralFactor(25);
        }
        SectorEntityToken ring = system.addTerrain("ring", new BaseRingTerrain.RingParams(1200f, 2500 / 2.0F, (SectorEntityToken)star, "Accretion Disk"));
        ring.addTag("accretion_disk");
        if (((CampaignTerrainAPI)ring).getPlugin() instanceof RingSystemTerrainPlugin) {
            ((RingSystemTerrainPlugin)((CampaignTerrainAPI)ring).getPlugin()).setNameForTooltip("Accretion Disk");
        }
        //PLANET 1
        PlanetAPI p1 = system.addPlanet("thquest_reimaden_I",star,"Reimaden I", Planets.PLANET_LAVA,80,130,3000,-65);
        MarketAPI m1 = p1.getMarket();
//        MarketAPI m1 = Global.getFactory().createMarket("thquest_reimaden_I","thquest_reimaden_I",0);
        m1.setPlanetConditionMarketOnly(true);
        m1.addCondition(Conditions.EXTREME_TECTONIC_ACTIVITY);
        m1.addCondition(Conditions.VERY_HOT);
        m1.addCondition(Conditions.METEOR_IMPACTS);
        m1.addCondition(Conditions.THIN_ATMOSPHERE);
        m1.addCondition(Conditions.ORE_ULTRARICH);
        m1.addCondition(Conditions.RARE_ORE_ULTRARICH);
        m1.addCondition(Conditions.POOR_LIGHT);
//        m1.setPrimaryEntity(p1);
//        p1.setMarket(m1);

        //ASTEROID BELT
        system.addRingBand(star,"misc","rings_asteroids0",256f,0,Color.white,256f,3300f,-70f);
        system.addRingBand(star,"misc","rings_asteroids0",256f,3,Color.white,256f,3500f,-72f);
        system.addAsteroidBelt(star,200, 3400, 400, -40f, -80f, Terrain.ASTEROID_BELT, "Maelstrom");
        //ICE PLANET
        PlanetAPI p2 = system.addPlanet("thquest_reimaden_II",star, "Reimaden II","frozen1",10,90,6000,-100);
//        MarketAPI m2 = Global.getFactory().createMarket("thquest_reimaden_II","thquest_reimaden_II",0);
        MarketAPI m2 =p2.getMarket();
        m2.setPlanetConditionMarketOnly(true);
        m2.addCondition(Conditions.DARK);
        m2.addCondition(Conditions.VERY_COLD);
        m2.addCondition(Conditions.NO_ATMOSPHERE);
        m2.addCondition(Conditions.ORE_SPARSE);
        m2.addCondition(Conditions.VOLATILES_PLENTIFUL);
        m2.addCondition(Conditions.LOW_GRAVITY);
//        m2.setPrimaryEntity(p2);
//        p2.setMarket(m2);
        //STABLE POINT
        //ICE GIANT & SYSTEM
        //RING
        //MOON
        PlanetAPI p3 = system.addPlanet("thquest_reimaden_III",star, "Reimaden III","ice_giant",90,285,10000,220);
//        MarketAPI m3 = Global.getFactory().createMarket("thquest_reimaden_II","thquest_reimaden_II",0);
        MarketAPI m3 =p3.getMarket();
        m3.setPlanetConditionMarketOnly(true);
        m3.addCondition(Conditions.DARK);
        m3.addCondition(Conditions.VERY_COLD);
        m3.addCondition(Conditions.VOLATILES_TRACE);
        m3.addCondition(Conditions.HIGH_GRAVITY);
        m3.setPrimaryEntity(p3);
        p3.setMarket(m3);
        //RINGS
        accretionDisk.add(system.addRingBand(p3,"misc","rings_special0",256f,0,Color.white,256f,550,60f));
        accretionDisk.add(system.addRingBand(p3,"misc","rings_ice0",256f,1,Color.white,256f,750,65f));
        SectorEntityToken ring2 = system.addTerrain(Terrain.RING, new BaseRingTerrain.RingParams(250, 650f, null, "Ring System"));
        ring2.setCircularOrbit(p3, 0, 0, 60);

        //MOON
        PlanetAPI p4 = system.addPlanet("thquest_reimaden_IIIa",p3, "Reimaden III-A","frozen2",10,80,1000,75f);
//        p4.setFaction(Factions.TRITACHYON);
//        MarketAPI m4 = Global.getFactory().createMarket("thquest_reimaden_IIIa","thquest_reimaden_III-A",3);
        MarketAPI m4 = p4.getMarket();
//        m4.setFactionId(Factions.TRITACHYON);
        m4.setPlanetConditionMarketOnly(true);
        m4.addCondition(Conditions.DARK);
        m4.addCondition(Conditions.POPULATION_3);
        m4.addCondition(Conditions.VERY_COLD);
        m4.addCondition(Conditions.NO_ATMOSPHERE);
        m4.addCondition(Conditions.ORE_RICH);
        m4.addCondition(Conditions.RARE_ORE_MODERATE);
        m4.addCondition(Conditions.LOW_GRAVITY);
        m4.getMemory().set("$nex_do_not_colonize",true);

        //        m4.setPrimaryEntity(p4);
//        p4.setMarket(m4);

        //BUILDINGS
        SectorEntityToken nav = system.addCustomEntity("thquest_reimaden_buoy","Reimaden Buoy",Entities.NAV_BUOY_MAKESHIFT,Factions.TRITACHYON);
        nav.setCircularOrbit(star,100f,7500,200f);
        SectorEntityToken buoy = system.addCustomEntity("thquest_reimaden_array","Reimaden Sensor Array",Entities.SENSOR_ARRAY_MAKESHIFT,Factions.TRITACHYON);
        buoy.setCircularOrbit(star,5f,10000,220);



        system.autogenerateHyperspaceJumpPoints(true,true);
        ThU.clearDeepHyper(system.getHyperspaceAnchor(),800);

    }
}
