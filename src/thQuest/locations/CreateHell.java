package thQuest.locations;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.PlanetAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.StarSystemAPI;
import com.fs.starfarer.api.campaign.econ.EconomyAPI;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.impl.campaign.econ.Decivilized;
import com.fs.starfarer.api.impl.campaign.ids.Conditions;
import com.fs.starfarer.api.impl.campaign.ids.Industries;
import com.fs.starfarer.api.impl.campaign.ids.Submarkets;
import com.fs.starfarer.api.impl.campaign.intel.deciv.DecivTracker;
import com.fs.starfarer.api.impl.campaign.procgen.Constellation;
import org.lwjgl.util.vector.Vector2f;
import thQuest.ThU;

import java.awt.*;

public class CreateHell {
    public static void generate(boolean inProgressGame){
        EconomyAPI globalEconomy = Global.getSector().getEconomy();
        //star placer and constellation
        Vector2f targetLocation = new Vector2f(24500,-6000);

        Vector2f location= ThU.starPlacer(targetLocation,12000f,1000f,7000f,6100);
        Constellation c = ThU.findNearestConstellationFromStar(location);
        StarSystemAPI system = Global.getSector().createStarSystem("New Old Hell's New Sun");
        system.setBackgroundTextureFilename("graphics/backgrounds/background2.jpg");
        SectorEntityToken star = system.initStar("thquestHell","star_red_dwarf",500f, location.x, location.y, 500f);//4 and 5 are positions //last is corrona
        system.setConstellation(c);
        system.setLightColor(new Color(255, 107, 79));
        //Planet 1: New New Hell
        PlanetAPI p =system.addPlanet("thquest_new_new_hell", system.getStar(), "New New Hell", "lava", 220f, 190, 1000, 250);
        p.setFaction("thquest_gensokyo");
        p.getSpec().setPitch( 90f);
        MarketAPI pm =Global.getFactory().createMarket("thquest_new_new_hell_market",p.getName(),8);
        pm.setFactionId("thquest_gensokyo");
        pm.addCondition(Conditions.VERY_HOT);
        pm.addCondition(Conditions.TOXIC_ATMOSPHERE);
        pm.addCondition(Conditions.METEOR_IMPACTS);
        pm.addCondition(Conditions.THIN_ATMOSPHERE);
        pm.addCondition(Conditions.ORE_SPARSE);
        pm.addCondition(Conditions.RARE_ORE_SPARSE);
        pm.getCondition(Conditions.ORE_SPARSE).setSurveyed(true);
        pm.getCondition(Conditions.RARE_ORE_SPARSE).setSurveyed(true);

        pm.addCondition(Conditions.POPULATION_8);

        pm.getTariff().modifyFlat("default_tariff", pm.getFaction().getTariffFraction());
        pm.setSurveyLevel(MarketAPI.SurveyLevel.FULL);


        pm.addIndustry(Industries.COMMERCE);
        pm.addIndustry(Industries.SPACEPORT);
        pm.addIndustry(Industries.GROUNDDEFENSES);

        pm.addSubmarket(Submarkets.SUBMARKET_OPEN);
        pm.addSubmarket(Submarkets.SUBMARKET_BLACK);
        pm.addSubmarket(Submarkets.SUBMARKET_STORAGE);

        p.setMarket(pm);
        pm.setPrimaryEntity(p);
        if(inProgressGame){
            ThU.populateIntelBoard(p);
        }
        globalEconomy.addMarket(pm,false);

        //hidden stuff
        pm.setEconGroup(pm.getId());
        pm.setHidden(true);
        pm.getMemoryWithoutUpdate().set(DecivTracker.NO_DECIV_KEY, true);

        //Planet 2: New Old Hell
        PlanetAPI p2 = system.addPlanet("thquest_new_old_hell",system.getStar(),"New Old Hell","frozen2",220f,190,5000,250);
        p2.setFaction("thquest_gensokyo");
        MarketAPI pm2 = Global.getFactory().createMarket("thquest_new_old_hell_market",p2.getName(),4);
        pm2.setFactionId("thquest_gensokyo");
        pm2.addCondition(Conditions.VERY_COLD);
        pm2.addCondition(Conditions.THIN_ATMOSPHERE);
        pm2.addCondition(Conditions.VOLATILES_ABUNDANT);
        pm2.addCondition(Conditions.RARE_ORE_MODERATE);
        pm2.getCondition(Conditions.VOLATILES_ABUNDANT).setSurveyed(true);
        pm2.getCondition(Conditions.RARE_ORE_MODERATE).setSurveyed(true);

        pm2.addCondition(Conditions.POPULATION_4);

        pm2.getTariff().modifyFlat("default_tariff", pm2.getFaction().getTariffFraction());
        pm2.setSurveyLevel(MarketAPI.SurveyLevel.FULL);


        pm2.addIndustry(Industries.SPACEPORT);
        pm2.addIndustry(Industries.GROUNDDEFENSES);
        pm2.addIndustry(Industries.MINING);
        pm2.addIndustry(Industries.BATTLESTATION_MID);
        pm2.addIndustry(Industries.PATROLHQ);
        pm2.addIndustry(Industries.LIGHTINDUSTRY);

        pm2.addSubmarket(Submarkets.SUBMARKET_OPEN);
        pm2.addSubmarket(Submarkets.SUBMARKET_BLACK);
        pm2.addSubmarket(Submarkets.SUBMARKET_STORAGE);


        p2.setMarket(pm2);
        pm2.setPrimaryEntity(p2);
        if(inProgressGame){
            ThU.populateIntelBoard(p2);
        }
        globalEconomy.addMarket(pm2,false);

        pm2.setEconGroup(pm2.getId());
        pm2.setHidden(true);
        pm2.getMemoryWithoutUpdate().set(DecivTracker.NO_DECIV_KEY, true);

//        p2.setSensorProfile(1f);
//        p2.setDiscoverable(true);
//        p2.getDetectedRangeMod().modifyFlat("gen", 5000f);
//        pm2.getMemoryWithoutUpdate().set(DecivTracker.NO_DECIV_KEY, true);

        //asteroid field

        //Dante planet


        //hyperspace points
        system.generateAnchorIfNeeded();
        system.autogenerateHyperspaceJumpPoints(true,false);
        ThU.clearDeepHyper(system.getHyperspaceAnchor(),400);

    }
}
