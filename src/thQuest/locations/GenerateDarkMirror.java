package thQuest.locations;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.*;
import com.fs.starfarer.api.campaign.econ.EconomyAPI;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.campaign.econ.SubmarketAPI;
import com.fs.starfarer.api.characters.PersonAPI;
import com.fs.starfarer.api.impl.campaign.ids.*;
import com.fs.starfarer.api.impl.campaign.procgen.Constellation;
import com.fs.starfarer.api.impl.campaign.procgen.StarSystemGenerator;
import com.fs.starfarer.api.impl.campaign.terrain.BaseRingTerrain;
import com.fs.starfarer.api.impl.campaign.terrain.RingSystemTerrainPlugin;
import com.fs.starfarer.api.impl.campaign.terrain.StarCoronaTerrainPlugin;
import com.fs.starfarer.api.util.Misc;
import org.lwjgl.util.vector.Vector2f;
import thQuest.ThU;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class GenerateDarkMirror {
    public static void createDarkMirror(boolean inProgressGame) {
        Vector2f location = new Vector2f( 30000,-10000);
        Constellation c=null;
        location = ThU.starPlacer(location,20000,1600,5000f,100);
        if (location==null){
            location = new Vector2f( 14000,-11000);
        }else{
            c =ThU.findNearestConstellationFromStar(location);
        }
        //todo put a center star in it. Or move the mirrors close together
        EconomyAPI globalEconomy = Global.getSector().getEconomy();
        //binary system DM and LM
        //red dwarf
        //kind of empty, but have unique music?

        //Inner system mugenkan GNS colony, habitat

        //With LM is vina and ice & snow world


        //outer system is Makai Gate
        //and on the oposite side light mirror.


        StarSystemAPI system = Global.getSector().createStarSystem("thquest_mirrors");
        system.setBackgroundTextureFilename("graphics/backgrounds/background6.jpg");//TODO finalize
//        PlanetAPI star = system.initStar("thquest_light_mirror","star_white",170, 14000,-11000,300);
        PlanetAPI star = system.initStar("thquest_light_mirror","star_white",170, location.getX(),location.getY(),300);
        star.setName("Light Mirror");
        system.setLightColor(new Color(200,200,200));
        system.setEnteredByPlayer(true);

        system.getLocation().set(new Vector2f(location.getX(),location.getY()));


        //create center
//        system.initNonStarCenter();
//        SectorEntityToken systemCenter = system.getCenter();

        //primary star
//        PlanetAPI star = system.addPlanet("thquest_light_mirror",systemCenter,"Light Mirror","star_white",355,170,17000,5000);
//        system.addCorona(star,300,5,1,2);
        //corona of 300

        //secondary star
        PlanetAPI s2 = system.addPlanet("thquest_dark_mirror",star, "Dark Mirror","star_red_dwarf", 170,350f,17000, 2000);

        //TODO map is bad
        system.setStar(star);
        system.setSecondary(s2);
        system.setName("Mirrors Star System");
        system.setType(StarSystemGenerator.StarSystemType.BINARY_FAR);
        system.setBaseName("Mirrors");

        //Inner Jump Point
        JumpPointAPI jp1 = Global.getFactory().createJumpPoint("thquest_light_mirror_jump_point","Light Mirror Jump Point");
        OrbitAPI jp1Orbit = Global.getFactory().createCircularOrbit(star,100,4000,600);
        jp1.setOrbit(jp1Orbit);
        jp1.setStandardWormholeToHyperspaceVisual();
        system.addEntity(jp1);

        //mugenkan station

        SectorEntityToken mugenkan = system.addCustomEntity("thquest_mugenkan","Mugenkan Station","station_side07", "thquest_gensokyo");
        mugenkan.setCircularOrbitPointingDown(s2,0,1650,200);
        MarketAPI mugenkanMarket = ThU.addMarketplace("thquest_gensokyo",mugenkan,null,"Mugenkan Station",4,
                new ArrayList<String>(Collections.singleton(Conditions.POPULATION_4)),//conditions
                new ArrayList<String>(Arrays.asList(Submarkets.SUBMARKET_OPEN,Submarkets.SUBMARKET_BLACK,Submarkets.SUBMARKET_STORAGE)),//submarkets
                new ArrayList<String>(Arrays.asList(
                        Industries.POPULATION,
                        Industries.GROUNDDEFENSES,
                        Industries.COMMERCE,
                        Industries.LIGHTINDUSTRY,
                        Industries.SPACEPORT,
                        Industries.BATTLESTATION
                )),//industries
                true,
                true);
//        ThU.populateIntelBoard(mugenkan);
        mugenkanMarket.getCommDirectory().addPerson(Global.getSector().getImportantPeople().getPerson("thquestElly"));
        mugenkanMarket.setAdmin(Global.getSector().getImportantPeople().getPerson("thquestElly"));


        PersonAPI person = mugenkanMarket.getFaction().createRandomPerson();
        person.setPostId(Ranks.POST_PORTMASTER);
        mugenkanMarket.getCommDirectory().addPerson(person);

        person = mugenkanMarket.getFaction().createRandomPerson();
        person.setPostId(Ranks.POST_SUPPLY_OFFICER);
        mugenkanMarket.getCommDirectory().addPerson(person);


        //the only stable location
        SectorEntityToken relay = system.addCustomEntity("thquest_mirror_relay","Dark Mirror Relay", Entities.COMM_RELAY,"thquest_gensokyo");
        relay.setCircularOrbitPointingDown(s2,45f,4000f,400f);


        //Pirate planet
        PlanetAPI p1 = system.addPlanet("thquest_vina", star, "Ruins of Vina", "barren-desert",
                80,//angle
                110,//radius
                2500,//orbitRadius
                300);//orbit days
        p1.setFaction(Factions.PIRATES);
        MarketAPI m1 = ThU.addMarketplace(Factions.PIRATES,p1,null,"Ruins of Vina",3,
                new ArrayList<>(Arrays.asList(
                        Conditions.HOT,
                        Conditions.TOXIC_ATMOSPHERE,
                        Conditions.THIN_ATMOSPHERE,
                        Conditions.RARE_ORE_ABUNDANT,
                        Conditions.ORGANICS_COMMON,
                        Conditions.RUINS_EXTENSIVE,
                        Conditions.FREE_PORT,
                        Conditions.POPULATION_3
                )),//conditions
                new ArrayList<String>(Arrays.asList(Submarkets.SUBMARKET_OPEN,Submarkets.SUBMARKET_BLACK,Submarkets.SUBMARKET_STORAGE)),//submarkets
                new ArrayList<String>(Arrays.asList(
                        Industries.POPULATION,
                        Industries.GROUNDDEFENSES,
                        Industries.PATROLHQ,
                        Industries.MINING,
                        Industries.SPACEPORT
                )),//industries
                true,
                true);
        ThU.populateIntelBoard(p1);

//        m1.setPlanetConditionMarketOnly(true);
//        m1.addCondition(Conditions.TECTONIC_ACTIVITY);
//        m1.addCondition(Conditions.HOT);
//        m1.addCondition(Conditions.TOXIC_ATMOSPHERE);
//        m1.addCondition(Conditions.THIN_ATMOSPHERE);
//        m1.addCondition(Conditions.ORE_RICH);
//        m1.addCondition(Conditions.RARE_ORE_ABUNDANT);
//        m1.addCondition(Conditions.ORGANICS_COMMON);
//        m1.addCondition(Conditions.RUINS_EXTENSIVE);
//        m1.setPrimaryEntity(p1);
//        p1.setMarket(m1);


//        //ASTEROID BELT
//        system.addRingBand(star, "misc", "rings_asteroids0", 256f, 0, Color.white, 256f, 3300f, -70f);
//        system.addRingBand(star, "misc", "rings_asteroids0", 256f, 3, Color.white, 256f, 3500f, -72f);
//        system.addAsteroidBelt(star, 200, 3400, 400, -40f, -80f, Terrain.ASTEROID_BELT, "Maelstrom");

        //ICE PLANET
        PlanetAPI p2 = system.addPlanet("thquest_WIS", star, "World of Ice and Snow", "frozen1", 10, 90, 5000, 800f);
        MarketAPI m2 = Global.getFactory().createMarket("thquest_WIS", "World of Ice and Snow", 0);
        m2.setPlanetConditionMarketOnly(true);
        m2.setSurveyLevel(MarketAPI.SurveyLevel.FULL);
        m2.addCondition(Conditions.POOR_LIGHT);
        m2.addCondition(Conditions.COLD);
        m2.addCondition(Conditions.NO_ATMOSPHERE);
        m2.addCondition(Conditions.VOLATILES_PLENTIFUL);
        m2.getCondition(Conditions.VOLATILES_PLENTIFUL).setSurveyed(true);
        m2.addCondition(Conditions.LOW_GRAVITY);
        m2.setPrimaryEntity(p2);
        p2.setMarket(m2);

//
//        MarketAPI ms = Global.getFactory().createMarket("thquest_light_mirror", "Light Mirror", 0);
//        ms.setPlanetConditionMarketOnly(true);
//        ms.setPrimaryEntity(s2);
//        s2.setMarket(ms);

        system.addCorona(s2,200,5,0,2);

        JumpPointAPI jp2 = Global.getFactory().createJumpPoint("thquest_dark_mirror_jump_point","Dark Mirror Jump Point");
        OrbitAPI jp2Orbit = Global.getFactory().createCircularOrbit(s2,100f,1200,150f);
        jp2.setOrbit(jp2Orbit);
        jp2.setStandardWormholeToHyperspaceVisual();
        system.addEntity(jp2);

        //Makai Gate, opisite of dark mirror, on its own...

        //thquest_makai_station

        SectorEntityToken makai = system.addCustomEntity("thquest_makai_gate","Makai Gate","thquest_makai_station", "thquest_gensokyo");
        makai.setCircularOrbitPointingDown(star,350,17000,2000);
        MarketAPI makaiMarket = ThU.addMarketplace("thquest_gensokyo",makai,null,"Makai Gate",6,
                new ArrayList<String>(Arrays.asList(Conditions.VOLATILES_PLENTIFUL, Conditions.FREE_PORT,Conditions.POPULATION_6)),//conditions
                new ArrayList<String>(Arrays.asList(Submarkets.SUBMARKET_OPEN,Submarkets.SUBMARKET_BLACK,Submarkets.GENERIC_MILITARY,Submarkets.SUBMARKET_STORAGE)),//submarkets
                new ArrayList<String>(Arrays.asList(
                        Industries.POPULATION,
                        Industries.HEAVYBATTERIES,
                        Industries.ORBITALWORKS,
                        Industries.MINING,
                        Industries.MILITARYBASE,
                        Industries.MEGAPORT,
                        Industries.STARFORTRESS_MID,
                        Industries.LIGHTINDUSTRY
                )),//industries
                true,
                true);
//        ThU.populateIntelBoard(makai);
//        PersonAPI person = makaiMarket.getFaction().createRandomPerson();
    //   Ranks.POST_ADMINISTRATOR);
        makaiMarket.getCommDirectory().addPerson(Global.getSector().getImportantPeople().getPerson("thquestShinki"));
        makaiMarket.setAdmin(Global.getSector().getImportantPeople().getPerson("thquestShinki"));


        person = makaiMarket.getFaction().createRandomPerson();
        person.setPostId(Ranks.POST_PORTMASTER);
        makaiMarket.getCommDirectory().addPerson(person);

        person = makaiMarket.getFaction().createRandomPerson();
        person.setPostId(Ranks.POST_SUPPLY_OFFICER);
        makaiMarket.getCommDirectory().addPerson(person);

        JumpPointAPI jp3 = Global.getFactory().createJumpPoint("thquest_makai_jump_point","Makai Gate Jump Point");
        OrbitAPI jp3Orbit = Global.getFactory().createCircularOrbit(makai,230,1500,200);
        jp3.setOrbit(jp3Orbit);
        jp3.setStandardWormholeToHyperspaceVisual();
        system.addEntity(jp3);



        system.autogenerateHyperspaceJumpPoints(true, false);
        ThU.clearDeepHyper(system.getHyperspaceAnchor(), 800);
        if(c!=null){
                    system.setConstellation(c);
        }
    }
}
