//package thQuest.fleets;
//
//import com.fs.starfarer.api.Global;
//import com.fs.starfarer.api.campaign.*;
//import com.fs.starfarer.api.campaign.econ.MarketAPI;
//import com.fs.starfarer.api.impl.campaign.command.WarSimScript;
//import com.fs.starfarer.api.impl.campaign.fleets.FleetFactoryV3;
//import com.fs.starfarer.api.impl.campaign.fleets.FleetParamsV3;
//import com.fs.starfarer.api.impl.campaign.ids.FleetTypes;
//import com.fs.starfarer.api.util.IntervalUtil;
//import com.fs.starfarer.api.util.Misc;
//import com.fs.starfarer.campaign.CampaignClock;
//import org.lwjgl.util.vector.Vector2f;
//import thQuest.Log;
//import thQuest.ThU;
//import thQuest.exceptions.ThquestNoAppropriateSystemFoundException;
//import thQuest.intel.YrIntel;
//
//import java.util.ArrayList;
//import java.util.List;
//
//public class YrFleetBehaviour implements FleetBehaviour{
//    String memkey;
////    List<CampaignFleetAPI> fleets = new ArrayList<>();
//    FactionAPI targetFaction;
//    IntervalUtil update = new IntervalUtil(20,30);
//    YrIntel yrIntel;
//    boolean decay = false;
//    long startTime;
//    int score=0;
//    float effect=0f;
//    CampaignClockAPI c= new CampaignClock();
//    SectorEntityToken target;
//
//
//
//    public YrFleetBehaviour(List<StarSystemAPI> prohibited) throws ThquestNoAppropriateSystemFoundException {
//        //TODO fleet generation, spawning, assignment, and intel management here
//        //memkey is how the fleets will be recognized
//        memkey = "thquestYr"+Global.getSector().genUID();
//        startTime=c.getTimestamp();
//        spawnYrFleet(prohibited);
//        yrIntel = new YrIntel(target,target,this);
//        Global.getSector().getIntelManager().addIntel(yrIntel);
//    }
//    public void spawnYrFleet(List<StarSystemAPI> prohibited) throws ThquestNoAppropriateSystemFoundException {
//        //List<FactionAPI> factions = Global.getSector().getAllFactions();
//        List<MarketAPI> markets =Misc.getNearbyMarkets(new Vector2f(0,0), (float) (Math.random()*25+25));
//        MarketAPI target;
//        int tries=0;
//        while(true){
//            MarketAPI market = markets.get((int)(Math.random()*markets.size()));
//            if(market.getFaction().isHostileTo("thquest_youkai")&&!market.isHidden()&&!prohibited.contains(market.getStarSystem())){
//                target=market;
//                break;
//            }else{
//                markets.remove(market);
//            }
//            if(markets.size()==0){
//                Log.getLogger().info("");
//            }
//            tries++;
//            if (tries>50){
//                throw new ThquestNoAppropriateSystemFoundException();
//            }
//        }
//        this.target=target.getPrimaryEntity();
//        float def = WarSimScript.getEnemyStrength("thquest_youkai", target.getStarSystem());
//        Log.getLogger().info("def="+def);
//        int fp = (int) (((Math.random()+.3)*def*8)+20);
//        if(fp>1000){
//            fp=1000;
//        }
//        int fleets = 1;
//        if(fp>400){
//            fleets = fp/400+1;
//            fp = fp/fleets;
//        }
////        Vector2f player =new Vector2f(-100000000,-100000000);
////        if(Global.getSector().getPlayerFleet().isInHyperspace()){
////            player = Global.getSector().getPlayerFleet().getLocation();
////        }
//        Vector2f randomLocation = ThU.findHyperspaceLocationForFleetSpawn(new Vector2f(0,0),10000);
////        float playerView =Global.getSector().getViewport().getVisibleWidth();
////        do {
////            randomLocation = new Vector2f((float) ((Math.random() - .5f) * 10000), (float) ((Math.random() - .5f) * 10000));//todo less insane numbers was 50k
////        } while (!(Misc.getDistance(player, randomLocation) > playerView*2));
//        for(int i =0;i<fleets;i++){
//            //spawn fleets
//            //get random location
//            Vector2f location = new Vector2f(randomLocation.getX()+(float)(Math.random()- .5f) * 1000,randomLocation.getY()+(float)(Math.random()- .5f) * 1000);
//            FleetParamsV3 fleetParamsV3 = new FleetParamsV3(new Vector2f(0, 0), "thquest_youkai",
//                    1.0f,
//                    FleetTypes.TASK_FORCE,
//                    fp, // combatPts
//                    fp/20, // freighterPts
//                    fp/20, // tankerPts
//                    0f, // transportPts
//                    0f, // linerPts
//                    0, // utilityPts
//                    .45f // qualityMod
//            );
//            fleetParamsV3.modeOverride= FactionAPI.ShipPickMode.PRIORITY_THEN_ALL;
//            CampaignFleetAPI fleet = FleetFactoryV3.createFleet(fleetParamsV3);
//            fleet.addAssignment(FleetAssignment.RAID_SYSTEM,target.getPrimaryEntity(),60f);
//            Global.getSector().getHyperspace().addEntity(fleet);
//            fleet.setLocation(location.getX(), location.y);
////            this.fleets.add(fleet);
//            fleet.addTag(memkey);
//            Log.getLogger().info("spawning YR fleet at "+location.getX()+","+location.getY()+" targeting market "+target.getId());
//            Log.getLogger().info("at "+fp+" fp");
//        }
//        //TODO maybe change this?
//        targetFaction=target.getFaction();
//    }
//
//    @Override
//    public List<CampaignFleetAPI> getFleets() {
//        return getFleetsEverywhere();
//    }
//
//    @Override
//    public IntervalUtil getUpdateInterval() {
//        return update;
//    }
//
//    @Override
//    public boolean isDone() {
//        if(c.getElapsedDaysSince(startTime)>60&&effect<=0){
//            return true;
//        }
//        if(decay&&effect<=0){
//            return true;
//        }
//        return false;
//    }
//
//    @Override
//    public boolean isUpdateOnBattle() {
//        return true;
//    }
//
//    @Override
//    public void updateOnBattle(CampaignFleetAPI primaryWinner, BattleAPI battle) {
//            boolean involved=false;
//        for(CampaignFleetAPI fleet:battle.getSnapshotBothSides()){
//            if(fleet.hasTag(memkey)){
//                involved=true;
//            }
//        }
//        if(involved){//this now can be true, but this still does not run...
//            if(!primaryWinner.getFaction().getId().equals("thquest_youkai")){
//                //bad things happened
//                score--;
//                int count=0;
//                for(CampaignFleetAPI f:getFleetsEverywhere()){
//                    count += f.getFleetSizeCount();
//                }
//                if(count*4<WarSimScript.getEnemyStrength("thquest_youkai", target.getStarSystem())){
//                    score = -99;
//                }
////                if(fleets.size()==1){
////                    score=score-99;
////                }
//            }else{
//                    score++;
//            }
//            this.update(0f);
//        }
//    }
//
//    @Override
//    public boolean isUpdateOnJump() {
//        return false;
//    }
//
//    @Override
//    public void updateOnJump(CampaignFleetAPI fleet, SectorEntityToken from, JumpPointAPI.JumpDestination to) {
//
//    }
//
//    @Override
//    public void update(float v) {
//        if(getFleetsEverywhere().size()==0){
//            yrIntel.endAfterDelay(10);
//            score=-1000;
//        }
//        if(decay){
//            //20-30s per tick
//            effect = effect - v/(Global.getSector().getClock().getSecondsPerDay()*30);//todo test
//            updateMarketConditions();
//        }else if(score<0){
//            for(CampaignFleetAPI fleet:getFleetsEverywhere()){
//                decay=true;
//                //retreat location
//                yrIntel.setStatus(Status.RETREATED);
////                updateMarketConditions(Status.RETREATED);
//
//                removeConditions();
//                List<SectorEntityToken> jumpPoints=target.getStarSystem().getJumpPoints();
//                SectorEntityToken jp = jumpPoints.get((int) (Math.random()*jumpPoints.size()));
//                fleet.addAssignment(FleetAssignment.GO_TO_LOCATION_AND_DESPAWN,jp,30f);
//            }
//            yrIntel.endAfterDelay(10f);
//        }else if(c.getElapsedDaysSince(startTime)>50){
//            yrIntel.endAfterDelay(10f);
//            yrIntel.setStatus(Status.DONE);
////            updateMarketConditions(status.DONE);  it is now decaying
//            decay=true;
//            for(CampaignFleetAPI fleet:getFleetsEverywhere()){
//                //retreat location
//                List<SectorEntityToken> jumpPoints=target.getStarSystem().getJumpPoints();
//                SectorEntityToken jp = jumpPoints.get((int) (Math.random()*jumpPoints.size()));
//                fleet.addAssignment(FleetAssignment.GO_TO_LOCATION_AND_DESPAWN,jp,30f);
//            }
//        }
//        if(score>8){
//            effect = 3f;
//            yrIntel.setStatus(Status.V3);
//            updateMarketConditions();
////            updateMarketConditions(status.V3);
//        }else if(score>4){
//            effect = 2f;
//            yrIntel.setStatus(Status.V2);
//            updateMarketConditions();
////            updateMarketConditions(status.V2);
//        }else if(score>0){
//            effect = 1f;
//            yrIntel.setStatus(Status.V1);
//            updateMarketConditions();
////            updateMarketConditions(status.V1);
//        }
//    }
//
//    @Override
//    public void cleanup() {
//        yrIntel.endImmediately();
//        //remove market conditions
//        removeConditions();
//    }
//    public void removeConditions(){
//        List<MarketAPI> markets=Misc.getMarketsInLocation(target.getContainingLocation());
//        for(MarketAPI m:markets) {
//            if(m.hasCondition("thquest_youkai_raiders")){
//                m.removeCondition("thquest_youkai_raiders");
//            }
//        }
//
//    }
//    public void updateMarketConditions(){
//        Status s;
//        if(effect <=0f){
//            s=Status.DONE;
//            //TODO cleanup here?
//
//        } else if (effect <=1f) {
//            s=Status.V1;
//        }else if (effect <=2f) {
//            s=Status.V2;
//        }else{
//            s=Status.V3;
//        }
//        Log.getLogger().info("updatemarketconditions called");
//        List<MarketAPI> markets=Misc.getMarketsInLocation(target.getContainingLocation());
//        for(MarketAPI m:markets){
//            Log.getLogger().info("updatemarketconditions called on market "+m.getId());
//            if(!m.hasCondition("thquest_youkai_raiders")){
//                m.addCondition("thquest_youkai_raiders");
//                m.getCondition("thquest_youkai_raiders").setSurveyed(true);//todo is this neccessary?
//            }
//            m.getCondition("thquest_youkai_raiders").getPlugin().setParam(s);
//        }
//    }
//    public List<CampaignFleetAPI> getFleetsAtTarget(){
//        List<CampaignFleetAPI> fleets = new ArrayList<>();
//        for (CampaignFleetAPI c:target.getContainingLocation().getFleets()){
//            if(c.getCustomData().containsKey(memkey)){
//                fleets.add(c);
//            }
//        }
//        return fleets;
//    }
//    public List<CampaignFleetAPI> getFleetsEverywhere(){
//        List<CampaignFleetAPI> fleets = new ArrayList<>();
//        for (CampaignFleetAPI c:target.getContainingLocation().getFleets()){
//            if(c.hasTag(memkey)){
//                fleets.add(c);
//            }
//        }
//        for (CampaignFleetAPI c:Global.getSector().getHyperspace().getFleets()){
//            if(c.hasTag(memkey)){
//                fleets.add(c);
//            }
//        }
//        return fleets;
//    }
//    public enum Status {
//        STARTING,
//        V1,
//        V2,
//        V3,
//        RETREATED,
//        DEFEATED,
//        DONE
//    }
//    public SectorEntityToken getTarget() {
//        return target;
//    }
//}
