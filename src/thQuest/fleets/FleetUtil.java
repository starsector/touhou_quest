package thQuest.fleets;

import com.fs.starfarer.api.EveryFrameScript;
import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.*;
import com.fs.starfarer.api.campaign.comm.IntelInfoPlugin;
import com.fs.starfarer.api.util.IntervalUtil;
import thQuest.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class  FleetUtil {
    //class manages campaign fleets for raids and quests.
    //maintains list of fleet behavior specs that create fleets and (optionally) intel items
    public List<FleetBehaviourSpec> specs= new ArrayList<>();
    public List <FleetBehaviour> behaviours=new ArrayList<>();
    public void reportBattleOccurred(CampaignFleetAPI primaryWinner, BattleAPI battle) {
        Logger.getLogger(this.getClass().getName()).info("battle occurred");
        for(FleetBehaviour behaviour:behaviours){
            if(behaviour.isUpdateOnBattle()){
                behaviour.updateOnBattle(primaryWinner,battle);
            }
        }
    }
    public void reportFleetJumped(CampaignFleetAPI fleet, SectorEntityToken from, JumpPointAPI.JumpDestination to) {

        for(FleetBehaviour behaviour:behaviours){
            if(behaviour.isUpdateOnJump()){
                behaviour.updateOnJump(fleet,from,to);
            }
        }
    }
    public void advance(float v) {
//        Log.getLogger().info("andvanced with"+v);
        List<FleetBehaviourSpec> removeSpec = new ArrayList<>();
        for(FleetBehaviourSpec spec:specs){
            IntervalUtil interval = spec.getUpdateInterval();
            if(spec.isDone()){
                        Log.getLogger().info("removing spec"+spec.getClass());
                removeSpec.add(spec);
                continue;
            }
            interval.advance(v);
            if(interval.intervalElapsed()){
                List<FleetBehaviour> newBehaviours =spec.update(interval.getIntervalDuration());
                if(!newBehaviours.isEmpty()){
                    this.behaviours.addAll(newBehaviours);
                }
            }
        }
        specs.removeAll(removeSpec);
        List<FleetBehaviour> removeBeh = new ArrayList<>();
        for(FleetBehaviour behaviour : behaviours){
            if(behaviour.isDone()){
                Log.getLogger().info("removing behaviour"+behaviour.getClass());
                behaviour.cleanup();
                removeBeh.add(behaviour);
                continue;
            }
            IntervalUtil interval = behaviour.getUpdateInterval();
            interval.advance(v);
            if(interval.intervalElapsed()){
                behaviour.update(interval.getIntervalDuration());
            }

        }
        behaviours.removeAll(removeBeh);
    }
    public void addFleetBehaviourSpec(FleetBehaviourSpec fleetBehaviourSpec){
        specs.add(fleetBehaviourSpec);
    }
    public void addFleetBehaviour(FleetBehaviour fleetBehaviour){
        behaviours.add(fleetBehaviour);
    }
    public <T> List<T> getPeers(Class<T> clazz){
        List<T> out=new ArrayList<>();
        for(FleetBehaviour b:behaviours){
            if(b.getClass()==clazz){
            out.add((T) b);
            }
        }
        return out;
    }
    //    public static <T> List<T> getIntelByClass(Class<T> clazz){
    //        return (List<T>) Global.getSector().getIntelManager().getIntel(clazz);
    //    }
}
