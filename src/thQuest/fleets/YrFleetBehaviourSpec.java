package thQuest.fleets;

import com.fs.starfarer.api.campaign.StarSystemAPI;
import com.fs.starfarer.api.util.IntervalUtil;
import thQuest.Log;
import thQuest.exceptions.ThquestNoAppropriateSystemFoundException;
import static thQuest.ThU.secondsFromDays;
import java.util.ArrayList;
import java.util.List;

public class YrFleetBehaviourSpec implements FleetBehaviourSpec{
    public IntervalUtil interval = new IntervalUtil(secondsFromDays(60),secondsFromDays(120));
    public IntervalUtil intervalFast = new IntervalUtil(secondsFromDays(30),secondsFromDays(60));
    //test values
//        public IntervalUtil interval = new IntervalUtil(secondsFromDays(30),secondsFromDays(60));
//    public IntervalUtil intervalFast = new IntervalUtil(secondsFromDays(29),secondsFromDays(30));

    List<StarSystemAPI> systems = new ArrayList<>();
    public boolean intensified = false;

    @Override
    public IntervalUtil getUpdateInterval() {
        if(intensified){
            return intervalFast;
        }else{
            return interval;
        }
    }

    @Override
    public boolean isDone() {
        return false;
    }

    @Override
    public List<FleetBehaviour> update(float v){
        //TODO remove probably

//        if(systems.size()>6){
//            systems.remove(0);
//        }
//        Log.getLogger().info("YrFleetBehaviourSpec tick!");
//        List<FleetBehaviour> out = new ArrayList<>();
//        try {
//            FleetBehaviour yr = new YrFleetBehaviour(systems);
//            systems.add(((YrFleetBehaviour) yr).getTarget().getStarSystem());
//            out.add(yr);
//        }catch(ThquestNoAppropriateSystemFoundException e){
//            Log.getLogger().info("No appropriate system found within parameters for Youkai Raiders event!");
//        }
//        return out;
        return null;
    }
}
