package thQuest.fleets;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.*;
import com.fs.starfarer.api.campaign.rules.MemKeys;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.impl.campaign.fleets.FleetFactoryV3;
import com.fs.starfarer.api.impl.campaign.fleets.FleetParamsV3;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.impl.campaign.ids.FleetTypes;
import com.fs.starfarer.api.impl.campaign.ids.MemFlags;
import com.fs.starfarer.api.util.IntervalUtil;
import com.fs.starfarer.api.util.Misc;
import org.apache.log4j.Logger;
import org.lwjgl.util.vector.Vector2f;

import java.util.ArrayList;
import java.util.List;


public class TTRDefFleetBehaviour implements FleetBehaviour {
    IntervalUtil interval = new IntervalUtil(120, 150);
//    Logger logger = Global.getLogger(this.getClass());
    SectorEntityToken home;
    int FLEETS = 2;
    SectorEntityToken hole;

    //TODO not used correctly but broken in such a way that it works lol
    String fleetKey = "$thquestttrfleetpatrol";
    boolean done = false;

    public TTRDefFleetBehaviour() {
        home = Global.getSector().getEntityById("thquest_reimaden_IIIa");
        hole = Global.getSector().getEntityById("thquest_reimaden");
        //initialize one fleet
        spawnFleet();
    }

    @Override
    public List<CampaignFleetAPI> getFleets() {
        ArrayList<CampaignFleetAPI> list = new ArrayList<>();
        for (CampaignFleetAPI f : home.getContainingLocation().getFleets()) {
            if (f.getCustomData().get(fleetKey) != null) {
                list.add(f);
            }
        }
        return list;
    }

    @Override
    public IntervalUtil getUpdateInterval() {
        return interval;
    }

    @Override
    public boolean isDone() {
        return done;
    }

    @Override
    public boolean isUpdateOnBattle() {
        return false;
    }

    @Override
    public void updateOnBattle(CampaignFleetAPI primaryWinner, BattleAPI battle) {

    }

    @Override
    public boolean isUpdateOnJump() {
        return false;
    }

    @Override
    public void updateOnJump(CampaignFleetAPI fleet, SectorEntityToken from, JumpPointAPI.JumpDestination to) {

    }

    @Override
    public void update(float v) {
        //TODO
        //1. look at fleet numbers, if damaged or in black hole, or missing, replace
        //look at memory. If done, set fleets to despawn and set done to true.
        int numFleets = 0;
        for (CampaignFleetAPI fleet : home.getContainingLocation().getFleets()) {
            if (fleet.getMemory().contains(fleetKey)) {
                numFleets++;
                if (Misc.getDistance(fleet.getLocation(), hole.getLocation()) < 300f) {
                    fleet.despawn();
                } else if (fleet.getFleetPoints() < 200) {
                    fleet.addAssignment(FleetAssignment.GO_TO_LOCATION_AND_DESPAWN, home, 10000f);
                }
            }
        }
//        logger.info("removing:" + toRemove.size());
//        fleets.removeAll(toRemove);
//
        if (numFleets < FLEETS) {
//            logger.info("size=" + numFleets);
//            logger.info("fleets=" + FLEETS);
            //spawn one fleet
            spawnFleet();
        }
        if (Integer.parseInt((String) Global.getSector().getMemory().get("$thquestttr")) > 3) {
            done = true;
        }
    }

    @Override
    public void cleanup() {
        for (CampaignFleetAPI fleet : home.getContainingLocation().getFleets()) {
            if (fleet.getCustomData().get(fleetKey) != null) {
                if (Misc.getDistance(fleet.getLocation(), hole.getLocation()) < 300f) {
                    fleet.despawn();
                } else {
                    fleet.addAssignment(FleetAssignment.GO_TO_LOCATION_AND_DESPAWN, home, 100f);
                }
            }
        }
    }

    public void spawnFleet() {
        //TODO test
        float fp = (float) (120f + (Math.random() - .5) * 100f);
        FleetParamsV3 fleetParamsV3 = new FleetParamsV3(new Vector2f(0, 0), Factions.MERCENARY,
                1.0f,
                FleetTypes.MERC_PATROL,
                fp, // combatPts
                fp / 20, // freighterPts
                fp / 20, // tankerPts
                0f, // transportPts
                0f, // linerPts
                0, // utilityPts
                .45f // qualityMod
        );
        fleetParamsV3.modeOverride = FactionAPI.ShipPickMode.PRIORITY_THEN_ALL;
        CampaignFleetAPI fleet = FleetFactoryV3.createFleet(fleetParamsV3);
        fleet.addAssignment(FleetAssignment.PATROL_SYSTEM, home, 100f);
        home.getContainingLocation().addEntity(fleet);
        fleet.setLocation(home.getLocation().getX(), home.getLocation().getY());
        fleet.setFaction(Factions.INDEPENDENT);
        fleet.getMemory().set(fleetKey,true);
        Misc.setFlagWithReason(fleet.getMemoryWithoutUpdate(), MemFlags.MEMORY_KEY_MAKE_HOSTILE, "thquest_ttr", true, 999);
        Misc.setFlagWithReason(fleet.getMemoryWithoutUpdate(), MemFlags.MEMORY_KEY_LOW_REP_IMPACT, "thquest_ttr", true, 999);//TODO maybe this is the right key

    }
}
