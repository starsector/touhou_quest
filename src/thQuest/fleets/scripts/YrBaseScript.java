package thQuest.fleets.scripts;

import com.fs.starfarer.api.EveryFrameScript;
import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.CustomCampaignEntityAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import org.apache.log4j.Logger;
import org.lwjgl.util.vector.Vector2f;
import thQuest.ThU;
import thQuest.fleets.FleetUtil;
import thQuest.fleets.YrBaseFleetBehaviour;
import thQuest.intel.YrBasePointer;

import java.util.List;
import java.util.Objects;

public class YrBaseScript implements EveryFrameScript {
    //TODO will parent here cause a memory leak?
    String parentId;
//    Logger logger;

    public YrBaseScript(String parentId) {
        this.parentId = parentId;
//        this.logger = Logger.getLogger(this.getClass().getName());
        //create base
    }

    @Override
    public boolean isDone() {
        FleetUtil fleetUtil = (FleetUtil) Global.getSector().getMemory().get("$thquestFleetUtil");
        List<YrBaseFleetBehaviour> list = fleetUtil.getPeers(YrBaseFleetBehaviour.class);
        YrBaseFleetBehaviour behaviour = null;
        for(YrBaseFleetBehaviour fb : list){
            if(fb.getId()==parentId){
                behaviour = fb;
            }
        }
        if(behaviour==null){
            return true;
        }
        return behaviour.isDone();
    }

    @Override
    public boolean runWhilePaused() {
        return false;
    }

    @Override
    public void advance(float v) {
        FleetUtil fleetUtil = (FleetUtil) Global.getSector().getMemory().get("$thquestFleetUtil");
        List<YrBaseFleetBehaviour> list = fleetUtil.getPeers(YrBaseFleetBehaviour.class);
        YrBaseFleetBehaviour parent = null;
        for(YrBaseFleetBehaviour fb : list){
            if(fb.getId()==parentId){
                parent = fb;
            }
        }
        if(parent!=null) {
            CampaignFleetAPI mothership = parent.getMothership();
            CampaignFleetAPI playerFleet = Global.getSector().getPlayerFleet();
            if (playerFleet != null && mothership.getVisibilityLevelTo(playerFleet) == SectorEntityToken.VisibilityLevel.COMPOSITION_AND_FACTION_DETAILS &&
                    mothership.getContainingLocation().equals(parent.anchor.getContainingLocation())) {
                List<YrBasePointer> pointers = ThU.getIntelByClass(YrBasePointer.class);
                boolean found = false;
                //todo handle transit and raiding
                //if it is moving, you learn where it was and where it is going? Or nothing?
                //when on raid, you learn nothing
                for (YrBasePointer pointer : pointers) {
                    if (Objects.equals(pointer.fbId, parent.getId())) {
                        //intel item already exists
                        found = true;
                        if (pointer.baseLocation != parent.anchor) {
                            //update this intel item
                            pointer.notifyBaseMoved(parent.anchor);
                        }
                        break;
                    }
                }
                //intel not found for hash. Make a new one
                if (!found) {
                    //todo handle if the player deletes this intel that it doesn't show up again right away
                    YrBasePointer newPointer = new YrBasePointer(parent.anchor, parent.anchor, parent.getId());
                    Global.getSector().getIntelManager().addIntel(newPointer);
                }
            }
        }
    }
}
