package thQuest.fleets;

import com.fs.starfarer.api.campaign.BattleAPI;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.JumpPointAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.fleet.FleetAPI;
import com.fs.starfarer.api.util.IntervalUtil;

import java.util.List;

public interface FleetBehaviour {
    public List<CampaignFleetAPI> getFleets();
    public IntervalUtil getUpdateInterval();
    public boolean isDone();
    public boolean isUpdateOnBattle();
    public void updateOnBattle(CampaignFleetAPI primaryWinner, BattleAPI battle);
    public boolean isUpdateOnJump();
    public void updateOnJump(CampaignFleetAPI fleet, SectorEntityToken from, JumpPointAPI.JumpDestination to);
    public void update(float amount);
    public void cleanup();
}
