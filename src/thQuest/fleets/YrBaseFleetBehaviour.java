package thQuest.fleets;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.*;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.impl.campaign.command.WarSimScript;
import com.fs.starfarer.api.impl.campaign.fleets.FleetFactoryV3;
import com.fs.starfarer.api.impl.campaign.fleets.FleetParamsV3;
import com.fs.starfarer.api.impl.campaign.fleets.RouteLocationCalculator;
import com.fs.starfarer.api.impl.campaign.ids.FleetTypes;
import com.fs.starfarer.api.impl.campaign.ids.MemFlags;
import com.fs.starfarer.api.util.IntervalUtil;
import com.fs.starfarer.api.util.Misc;
import com.fs.starfarer.campaign.Hyperspace;
import org.lwjgl.util.vector.Vector2f;
import thQuest.Log;
import thQuest.ThU;
import thQuest.exceptions.ThquestNoAppropriateSystemFoundException;
import thQuest.fleets.scripts.YrBaseScript;
import thQuest.intel.YrBasePointer;
import thQuest.intel.YrIntel2;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import static thQuest.ThU.findHyperspaceLocationForFleetSpawn;

public class YrBaseFleetBehaviour implements FleetBehaviour{
    //todo
    //YR bases are fleets that are composed of a single large ship(space hulk?)
    //they have several modes.
    //1. travel to destination
    //2. park at destination
    //3. go on Youkai Raid

    //the base must ALSO
    //respond to the player being in system if it is being attacked it retreats
    //leaves clues that can be picked up as intel entries leading the player to the next location

//    Logger logger = Logger.getLogger(this.getClass().getName());
    IntervalUtil intervalToSpawnSubfleet = new IntervalUtil(40,120);//TODO refine numbers?
    String mothershipTag;
    String id = Global.getSector().genUID();;
    String subfleetTag;
    float raidTimeMax = 300f;
    float raidTimer = raidTimeMax;
    public StarSystemAPI yrTargetSystem; //this will be set to null when not raiding.
    public YrPhase phase = YrPhase.TRAVELING_TO_ANCHOR; //this is the state that the fleets are in
    int fleetsWanted = 5;
    int fleetSize = 80;
    int fleetSizeVariance = 30;
//    int raidFleetsWanted = 2;
    int yrScore;
    boolean done = false;
    long timestampLastLeft;
    String anchorTag = "thquestYbAnchor";
    IntervalUtil interval = new IntervalUtil(60,80);
    //TODO it might be a bad idea to put these in memory
    public SectorEntityToken anchor;
    public YrBaseFleetBehaviour() {
        Logger logger = Logger.getLogger(this.getClass().getName());
        mothershipTag = "thquestYbMothership"+this.getId();
        subfleetTag = "thquestYbSubfleet"+this.getId();
        timestampLastLeft = Global.getSector().getClock().getTimestamp();
        anchor = findAnchor(null);
        anchor.addTag(anchorTag);
        //mothership spawn location
        Vector2f spawnLocation = findHyperspaceLocationForFleetSpawn(anchor.getStarSystem().getHyperspaceAnchor().getLocationInHyperspace(),10000);
        //mothership spawning
        FleetParamsV3 fleetParamsV3 = new FleetParamsV3(new Vector2f(0, 0), "thquest_youkai",
                1.0f,
                FleetTypes.TASK_FORCE,//todo YR base fleet type
                100, // combatPts
                0, // freighterPts
                0, // tankerPts
                0f, // transportPts
                0f, // linerPts
                0, // utilityPts
                .45f // qualityMod
        );
        fleetParamsV3.flagshipVariant=Global.getSettings().getVariant("thquest_dex_standard");
        fleetParamsV3.maxNumShips=2; //must be 2 otherwise it will not spawn, I remove the extra ship later
        fleetParamsV3.modeOverride= FactionAPI.ShipPickMode.PRIORITY_THEN_ALL;
        CampaignFleetAPI fleet = FleetFactoryV3.createFleet(fleetParamsV3);
        for(FleetMemberAPI f: fleet.getMembersWithFightersCopy()){
            if(!f.getHullSpec().getHullId().equals(Global.getSettings().getVariant("thquest_dex_standard").getHullSpec().getHullId())){
                fleet.removeFleetMemberWithDestructionFlash(f);
            }
        }



//        thQuest.fleets.scripts.YrBaseArrivedScript script = new thQuest.fleets.scripts.YrBaseArrivedScript();
//        script.idTarget = id;
        Global.getSector().getHyperspace().addEntity(fleet);
        fleet.setLocation(spawnLocation.getX(), spawnLocation.getY());
        fleet.addTag(mothershipTag);
        fleet.addAssignment(FleetAssignment.GO_TO_LOCATION,anchor,300f);
        Misc.setFlagWithReason(fleet.getMemoryWithoutUpdate(),MemFlags.FLEET_IGNORES_OTHER_FLEETS, "thquestYrBase",true,100000000000000000f);
        //TODO re-enable later
//        fleet.addScript(new YrBaseScript(id));

//        //todo testing... setting as player location and making non hostile
//        fleet.setContainingLocation(Global.getSector().getPlayerFleet().getContainingLocation());
//        fleet.setLocation(Global.getSector().getPlayerFleet().getLocation().getX(),Global.getSector().getPlayerFleet().getLocation().getY());

        //create subfleets

        for(int i = 0;i<fleetsWanted;i++){
            spawnSubFleet(spawnLocation,Global.getSector().getHyperspace());
        }

        logger.info("created YR Base at " + spawnLocation +" with ID "+id);
        logger.info("destination system: " + anchor.getStarSystem().getName());
    }

    @Override
    public List<CampaignFleetAPI> getFleets() {
        List<CampaignFleetAPI> list = getSubFleets();
        CampaignFleetAPI ms = getMothership();
        if(ms!=null){
            list.add(ms);
        }
        return list;
    }

    @Override
    public IntervalUtil getUpdateInterval() {
        return interval;
    }

    @Override
    public boolean isDone() {
        return done;
    }

    @Override
    public boolean isUpdateOnBattle() {
        return true;
    }

    @Override
    public void updateOnBattle(CampaignFleetAPI primaryWinner, BattleAPI battle) {

         boolean involvesYr=false;
        for(CampaignFleetAPI fleet:battle.getSnapshotBothSides()) {
            if(fleet.getFaction().getId().equals("thquest_youkai")){
                involvesYr=true;
            }
        }
            if(involvesYr&&phase==YrPhase.ON_RAID){//yr target being null is the indicator of what mode, yrintel determines weather or not it should count yet
                LocationAPI battleLocation = battle.getCombinedOne().getContainingLocation();
                //it counts if it happens within the target system or within 1 LY of it in hyperspace (1000 units)
                //TODO test
                //TODO YR the id matches, but the battle is in the wrong system
                boolean isInRange = yrTargetSystem == battleLocation || (
                        battleLocation.isHyperspace() &&
                                Misc.getDistance(battle.getCombinedOne().getLocation(),yrTargetSystem.getHyperspaceAnchor().getLocation())<1000f);
                if(!primaryWinner.getFaction().getId().equals("thquest_youkai")){
                    //bad things happened
                    yrScore--;
                    int count=0;
                    for(CampaignFleetAPI f:getSubFleets()){
                        count += f.getFleetSizeCount();
                    }
//                    if(count*4< WarSimScript.getEnemyStrength("thquest_youkai", yrTarget.getStarSystem())){
//                        yrScore = -99;
//                    }
                    if(count<3){
                        yrScore = -99;
                    }

//                if(fleets.size()==1){
//                    score=score-99;
//                }
                }else if (isInRange){
                        yrScore++;
                }
//                this.update(0f);
            }
        }

    @Override
    public boolean isUpdateOnJump() {
        return true;
    }

    @Override
    public void updateOnJump(CampaignFleetAPI fleet, SectorEntityToken from, JumpPointAPI.JumpDestination to) {
        //mothership is always passive.
        //subfleets are passive in hyperspace, but active in regular space, to avoid them being distracted
        if(fleet.hasTag(mothershipTag)&&phase==YrPhase.TRAVELING_TO_RAID&&to.getDestination().getStarSystem()==yrTargetSystem){
            raidTimer = raidTimeMax;
            phase = YrPhase.ON_RAID;
//            Misc.setFlagWithReason(fleet.getMemoryWithoutUpdate(),MemFlags.FLEET_IGNORES_OTHER_FLEETS, "thquestYrBase",false,1000f);

            //creating the intel item when the mothership goes to the location
            //checking if player has an accurate base pointer. If so, include that information.

//            List<YrBasePointer> pointers = ThU.getIntelByClass(YrBasePointer.class);
//            SectorEntityToken fromLocation = yrTarget;
//            for(YrBasePointer p : pointers){
//                if(p.fbId.equals(id)&&p.baseLocation==anchor){
//                    fromLocation=anchor;
//                }
//            }
//            YrIntel2 intel = new YrIntel2(yrTarget,fromLocation,id);
//            intel.status = YrIntel2.Status.V0;
//            Global.getSector().getIntelManager().addIntel(intel);
//            Global.getSector().addScript(intel);
        }
        //TODO test MEMORY_KEY_MAKE_HOSTILE
        if(fleet.hasTag(subfleetTag) && to.getDestination().getContainingLocation().isHyperspace()){
            Misc.setFlagWithReason(fleet.getMemoryWithoutUpdate(),MemFlags.FLEET_IGNORES_OTHER_FLEETS, "thquestYrBase",true,1000f);
        }else if(fleet.hasTag(subfleetTag) && !to.getDestination().getContainingLocation().isHyperspace()){
            Misc.setFlagWithReason(fleet.getMemoryWithoutUpdate(),MemFlags.FLEET_IGNORES_OTHER_FLEETS, "thquestYrBase",false,1000f);
            Misc.setFlagWithReason(fleet.getMemoryWithoutUpdate(), MemFlags.MEMORY_KEY_MAKE_HOSTILE, "thquest_yrb", true, 1000000000);
        }
        if(fleet.hasTag(subfleetTag) && phase == YrPhase.TRAVELING_TO_RAID || phase == YrPhase.ON_RAID && to.getDestination().getStarSystem() == yrTargetSystem) {
            //TODO causes null pointer exception DK why
//            fleet.getAI().setActionTextOverride("Raiding System");
        }
        if(fleet.hasTag(mothershipTag) && phase == YrPhase.TRAVELING_TO_ANCHOR&&to.getDestination().getStarSystem()==yrTargetSystem){
            Logger logger = Logger.getLogger(this.getClass().getName());
            logger.info("YR base "+id+" has arrived at its destination.");
            List<CampaignFleetAPI> subFleets = getSubFleets();
            fleet.clearAssignments();
//            Misc.setFlagWithReason(fleet.getMemoryWithoutUpdate(),MemFlags.FLEET_IGNORES_OTHER_FLEETS, "thquestYrBase",true,1000f);

//            fleet.addAssignment(FleetAssignment.ORBIT_AGGRESSIVE, anchor, 300f);//todo fix
            for(CampaignFleetAPI f:subFleets){
                logger.info("YR subfleet "+f.hashCode()+" has received the order and is now patrolling.");
//                f.clearAssignments();
//                f.addAssignment(FleetAssignment.PATROL_SYSTEM, anchor, 300f);
                //Probably unnecessary due to the above.
                Misc.setFlagWithReason(f.getMemoryWithoutUpdate(), MemFlags.MEMORY_KEY_MAKE_HOSTILE, "thquest_yrb", true, 1000000000);
                Misc.setFlagWithReason(f.getMemoryWithoutUpdate(),MemFlags.FLEET_IGNORES_OTHER_FLEETS, "thquestYrBase",false,1000f);
                f.getAI().setActionTextOverride("Raiding System");
                //if I add something to make them raid here they do not!!!
                //TODO update the intel item here to show arrived
            }
        }
    }

    @Override
    public void update(float amount) {
        Logger logger = Logger.getLogger(this.getClass().getName());

        List<YrIntel2> intelList = ThU.getIntelByClass(YrIntel2.class);
        YrIntel2 yrIntel = null;
        for(YrIntel2 intel : intelList){
            if(intel.yrId.equals(id)){
                yrIntel = intel;
            }
        }

    //check if mothership exists
        CampaignFleetAPI mothership =getMothership();
        if(mothership==null||mothership.isExpired()){
            done = true;
            phase = YrPhase.FINISHED;
            if(yrIntel!=null){
                yrIntel.status = YrIntel2.Status.BASE_DESTROYED;
            }
        }else{
            //check if it is on a raid, if so, check score and time elapsed
            if(phase == YrPhase.ON_RAID&&yrIntel!=null){
                List<CampaignFleetAPI> fleets = getSubFleets();
                //TODO test if they actually raid!
                for(CampaignFleetAPI f:fleets) {
                    f.clearAssignments();
                    f.addAssignment(FleetAssignment.PATROL_SYSTEM, yrTargetSystem.getJumpPoints().get(0), 300f);
                    f.getAI().setActionTextOverride("Raiding system");
                }
                if(yrScore<-2){
                    yrIntel.status = YrIntel2.Status.DEFEATED;
                    //raid is lost. Notify intel and retreat back to anchor
                    mothership.clearAssignments();
//                    thQuest.fleets.scripts.YrBaseArrivedScript script = new YrBaseArrivedScript();
//                    script.idTarget = id;
                    mothership.clearAssignments();
                    mothership.addAssignment(FleetAssignment.GO_TO_LOCATION,anchor,300f);
                    List<CampaignFleetAPI> subFleets = getSubFleets();
                    for(CampaignFleetAPI f:subFleets){
                        f.clearAssignments();
                        f.addAssignmentAtStart(FleetAssignment.ORBIT_AGGRESSIVE,mothership,300f,null);//TODO or defend
                        f.getAI().setActionTextOverride("Following Youkai Base");
                    }
                    //values are
                    //8,4,0
                }else if(yrScore<1) {//todo null pointer exception here
                        //nothing ?
                    }
                else if(yrScore<4){//todo null pointer exception here
                    yrIntel.status = YrIntel2.Status.V1;
                    if(yrIntel.lingeringEffect<1.0f){
                        yrIntel.lingeringEffect = 1.5f;
                    }
                }else if(yrScore<8){
                    yrIntel.status = YrIntel2.Status.V2;
                    if(yrIntel.lingeringEffect<2f){
                        yrIntel.lingeringEffect = 2.5f;
                    }
                }else{
                    yrIntel.status = YrIntel2.Status.V3;
                    if(yrIntel.lingeringEffect<2f){
                        yrIntel.lingeringEffect = 3.5f;
                    }
                }
                if(phase==YrPhase.ON_RAID) {
                    raidTimer = raidTimer - amount;
                }
            if(raidTimer <0f){
                endRaid();
            }
            }else
            //check if mothership is due to move
            //check if all the fleets are there
            if(Global.getSector().getClock().getElapsedDaysSince(timestampLastLeft)>60&&phase != YrPhase.AT_ANCHOR){
                timestampLastLeft = Global.getSector().getClock().getTimestamp();
                anchor.removeTag(anchorTag);
                anchor = findAnchor(anchor);
                anchor.addTag(anchorTag);
                mothership.clearAssignments();
//                thQuest.fleets.scripts.YrBaseArrivedScript script = new YrBaseArrivedScript();
//                script.idTarget = id;
                mothership.clearAssignments();
                mothership.addAssignment(FleetAssignment.GO_TO_LOCATION,anchor,300f);
                List<CampaignFleetAPI> subFleets = getSubFleets();
                for(CampaignFleetAPI f:subFleets){
                    f.clearAssignments();
                    f.addAssignmentAtStart(FleetAssignment.ORBIT_AGGRESSIVE,mothership,300f,null);//TODO or defend
                    f.getAI().setActionTextOverride("Following Youkai Base");
                }
                logger.info("YR mothership "+ id + " decided to move to "+ anchor.getStarSystem());
            }

            //Other stuff to check.
            //fleet cycling
            //TODO does not work
            intervalToSpawnSubfleet.advance(amount);
            List<CampaignFleetAPI> fleets = getSubFleets();
            if(intervalToSpawnSubfleet.intervalElapsed()){
                if(fleets.size()<fleetsWanted){
                    spawnSubFleet(mothership.getLocation(),mothership.getContainingLocation());
                }else if(fleets.size()<fleetsWanted+1){//get damaged/random fleet and cycle it out +1 is a failsafe
                    CampaignFleetAPI chosen=null;
                    for(CampaignFleetAPI fleet :fleets){
                        if(fleet.getFleetPoints()<50f){
                            chosen=fleet;
                        }
                    }
                    if(chosen==null){
                        chosen = fleets.get((int) (fleets.size()*Math.random()));
                    }
                    chosen.clearAssignments();
                    chosen.addAssignment(FleetAssignment.GO_TO_LOCATION_AND_DESPAWN,mothership,300f);
                    spawnSubFleet(mothership.getLocation(),mothership.getContainingLocation());
                }
            }
        }
    }

    @Override
    public void cleanup() {
        List<CampaignFleetAPI> subFleets = getSubFleets();
        for(CampaignFleetAPI f:subFleets){
            f.clearAssignments();
            //they go to the mirrors system to despawn
            f.addAssignment(FleetAssignment.GO_TO_LOCATION_AND_DESPAWN,Global.getSector().getEntityById("thquest_makai_gate"),300f);
            f.getAI().setActionTextOverride("Retreating");
        }
    }
    public List<CampaignFleetAPI> getSubFleets() {
        List<CampaignFleetAPI> cf = new ArrayList<>();
        List<SectorEntityToken> tokens = Global.getSector().getEntitiesWithTag(subfleetTag);
        for(SectorEntityToken t : tokens){
            if(t instanceof CampaignFleetAPI){
                cf.add((CampaignFleetAPI) t);
            }
        }
        return cf;
    }
    public CampaignFleetAPI getMothership(){
        List<SectorEntityToken> tokens = Global.getSector().getEntitiesWithTag(mothershipTag);
        if(tokens.isEmpty()){
            return null;
        }else {
            return (CampaignFleetAPI) tokens.get(0);
        }
    }

    public SectorEntityToken findAnchor(SectorEntityToken prevLocation){
        int retries = 100;
        int counter=0;
        //anchorTag

        while(true){
            List<StarSystemAPI> starSystemAPIList;
            if(prevLocation!=null){
                starSystemAPIList = Misc.getNearbyStarSystems(prevLocation,counter + 10);
            }else{
                starSystemAPIList = Global.getSector().getStarSystems();
            }
            StarSystemAPI system = starSystemAPIList.get((int) (Math.random()*starSystemAPIList.size()));

            List<PlanetAPI> planets = new ArrayList<>();
            for(PlanetAPI p : system.getPlanets()){
                if(!p.isStar()){
                    planets.add(p);
                }
            }
            if(Misc.getMarketsInLocation(system.getCenter().getContainingLocation()).size()==0 &&
                    Misc.getNearbyMarkets(system.getLocation(),10f).size() >
                            0&&!system.hasBlackHole()&&!system.hasPulsar()&&system.getEntitiesWithTag(anchorTag).size() == 0&&planets.size() > 0){
                    //these conditions are not too far from civilization, not too close, not already anchor, no black hole, no pulsar, at least one planet
                    return planets.get((int) (Math.random()*planets.size()));
            }else if(counter>retries){
                return null;
            }else{
                counter++;

            }
        }
    }
    public StarSystemAPI orderGoOnRaid(List<String> restrictedSystems) throws ThquestNoAppropriateSystemFoundException {
        Logger logger = Logger.getLogger(this.getClass().getName());
        if(phase == YrPhase.ON_RAID){
            logger.info("YR mothership "+ id + " ordered to raid. But was already on a raid. Rejecting order.");
            return null;
        }
        phase = YrPhase.TRAVELING_TO_RAID;
        //raid target logic. Put migration on hold until done
        List<MarketAPI> markets =Misc.getNearbyMarkets(new Vector2f(0,0), (float) (Math.random()*25+25));
        MarketAPI target;
        int tries=0;
        while(true){
            MarketAPI market = markets.get((int)(Math.random()*markets.size()));
            if(market.getFaction().isHostileTo("thquest_youkai")&&!market.isHidden()&&!restrictedSystems.contains(market.getStarSystem().getId())){
                target=market;
                break;
            }else{
                markets.remove(market);
            }
            if(markets.size()==0){
                Log.getLogger().info("");
            }
            tries++;
            if (tries>50){
                throw new ThquestNoAppropriateSystemFoundException();
            }
        }
        yrTargetSystem = target.getStarSystem();
//        this.yrTarget=target.getPrimaryEntity();
        yrScore = 0;
        //fleet ordering
        CampaignFleetAPI mothership = getMothership();
        mothership.clearAssignments();
//        thQuest.fleets.scripts.YrBaseRaidScript script = new YrBaseRaidScript();
//        script.idTarget = id;
        mothership.clearAssignments();
        Misc.setFlagWithReason(mothership.getMemoryWithoutUpdate(),MemFlags.FLEET_IGNORES_OTHER_FLEETS, "thquestYrBase",true,1000f);
        //base will hang out in exterior of the star system
        List<SectorEntityToken> tokens = yrTargetSystem.getJumpPoints();
        SectorEntityToken token = tokens.get(0);
        for(SectorEntityToken t: tokens){
            if(t.getCircularOrbitRadius()>token.getCircularOrbitRadius()){
                token = t;
            }
        }



//        this.raidToken = token;
        mothership.addAssignment(FleetAssignment.GO_TO_LOCATION,token,300f);
        List<CampaignFleetAPI> subFleets = getSubFleets();
        for(CampaignFleetAPI f:subFleets){
//            Misc.setFlagWithReason(f.getMemoryWithoutUpdate(),MemFlags.FLEET_IGNORES_OTHER_FLEETS, "thquestYrBase",true,1000f);
            f.clearAssignments();
            //THIS SOMEHOW WORKS
            f.addAssignmentAtStart(FleetAssignment.ORBIT_AGGRESSIVE,mothership,300f,null);
            f.getAI().setActionTextOverride("Following Youkai Base");
        }
        logger.info("YR mothership "+ id + " decided to raid "+ yrTargetSystem);

        //intel item for going on raid

        List<YrBasePointer> pointers = ThU.getIntelByClass(YrBasePointer.class);
        SectorEntityToken fromLocation = yrTargetSystem.getCenter();
        for(YrBasePointer p : pointers){
            if(p.fbId.equals(id)&&p.baseLocation==anchor){
                fromLocation=anchor;
            }
        }
        YrIntel2 intel = new YrIntel2(fromLocation,yrTargetSystem.getCenter(),id);
        intel.status = YrIntel2.Status.STARTING;
        Global.getSector().getIntelManager().addIntel(intel);
        Global.getSector().addScript(intel);




        return target.getStarSystem();
    }
    public void endRaid(){

        //setting raid to done
        List<YrIntel2> intelList = ThU.getIntelByClass(YrIntel2.class);
        YrIntel2 yrIntel = null;
        for(YrIntel2 intel : intelList){
            if(intel.yrId.equals(id)){
                yrIntel = intel;
            }
        }
        yrIntel.status = YrIntel2.Status.DONE;
        phase = YrPhase.TRAVELING_TO_ANCHOR;

        //endraid will return fleets back to course for home
        CampaignFleetAPI mothership = getMothership();
        //TODO copied code. Move to method?
        mothership.clearAssignments();
//        thQuest.fleets.scripts.YrBaseArrivedScript script = new YrBaseArrivedScript();
//        script.idTarget = id;
        mothership.clearAssignments();
        mothership.addAssignment(FleetAssignment.GO_TO_LOCATION,anchor,300f);
        List<CampaignFleetAPI> subFleets = getSubFleets();
        for(CampaignFleetAPI f:subFleets){
            f.clearAssignments();
            f.addAssignmentAtStart(FleetAssignment.FOLLOW,mothership,300f,null);
        }
    }
    //called via callback using fleet script.
    //This is when the mothership moves into a location. NOT on a raid
//    public void arrivedScript(){
//        Logger logger = Logger.getLogger(this.getClass().getName());
//        logger.info("YR base "+id+" has arrived at its destination.");
//        CampaignFleetAPI mothership = getMothership();
//        List<CampaignFleetAPI> subFleets = getSubFleets();
//        mothership.clearAssignments();
//
//        mothership.addAssignment(FleetAssignment.ORBIT_AGGRESSIVE, anchor, 300f);//todo fix
//        for(CampaignFleetAPI f:subFleets){
//            logger.info("YR subfleet "+f.hashCode()+" has received the order and is now patrolling.");
//            f.clearAssignments();
//            f.addAssignment(FleetAssignment.PATROL_SYSTEM, anchor, 300f);
//        }
//    }
    //unused. Raids no longer use external scripts, instead it uses the jump listener.
//    public void arrivedRaidScript(){
//
//        Logger logger = Logger.getLogger(this.getClass().getName());
//    //todo similar to arrived but for YR
//        logger.info("YR base "+id+" has arrived at its raid destination.");
//        CampaignFleetAPI mothership = getMothership();
//        List<CampaignFleetAPI> subFleets = getSubFleets();
//        mothership.clearAssignments();
//
//        //orbits the centeral location in a location 2000 beyond the next thing out.
//        List<SectorEntityToken> sets = yrTargetSystem.getJumpPoints();
//        List<PlanetAPI> planets = yrTargetSystem.getPlanets();
//        float dist = 0f;
//        for(SectorEntityToken set : sets){
//            if(set.getCircularOrbitRadius()>dist){
//                dist = set.getCircularOrbitRadius();
//            }
//        }
//        for(PlanetAPI planet :planets){
//            if(planet.getCircularOrbitRadius()>dist){
//                dist = planet.getCircularOrbitRadius();
//            }
//        }
//        mothership.clearAssignments();
//        mothership.addAssignment(FleetAssignment.ORBIT_PASSIVE, raidToken, dist+2000);
//        for(CampaignFleetAPI f:subFleets){
//            logger.info("YR subfleet "+f.hashCode()+" has received the order and is now raiding.");
//            f.clearAssignments();
//            f.addAssignment(FleetAssignment.PATROL_SYSTEM, yrTarget, 300f);//RAID_SYSTEM makes them leave the system
//        }
//    }
    public void spawnSubFleet(Vector2f spawnPosition, LocationAPI spawnLocation){
        //todo trying this
        float fp = (float) (80f + (Math.random() - .5) * 30f);
        FleetParamsV3 fleetParamsV3new = new FleetParamsV3(new Vector2f(0, 0), "thquest_youkai",
                1.0f,
                FleetTypes.TASK_FORCE,//todo YR base fleet type
                fp, // combatPts
                fp/4, // freighterPts
                fp/4, // tankerPts
                0f, // transportPts
                0f, // linerPts
                0, // utilityPts
                .45f // qualityMod
        );
        fleetParamsV3new.modeOverride = FactionAPI.ShipPickMode.PRIORITY_THEN_ALL;
        CampaignFleetAPI fleet3 = FleetFactoryV3.createFleet(fleetParamsV3new);

//        Misc.setFlagWithReason(fleet3.getMemoryWithoutUpdate(), MemFlags.MEMORY_KEY_MAKE_HOSTILE, "thquest_yrb", true, 1000000000);
        if(phase == YrPhase.ON_RAID ){//currently raiding
            fleet3.addAssignmentAtStart(FleetAssignment.PATROL_SYSTEM,yrTargetSystem.getCenter(),300f,null);
            fleet3.getAI().setActionTextOverride("Raiding System");

            spawnLocation.addEntity(fleet3);
            fleet3.setLocation(spawnPosition.getX(), spawnPosition.getY());
            fleet3.addTag(subfleetTag);
        }else if(getMothership().getContainingLocation()==anchor.getContainingLocation()){//At location
            fleet3.addAssignmentAtStart(FleetAssignment.PATROL_SYSTEM,anchor,300f,null);
            fleet3.getAI().setActionTextOverride("Raiding System");

            spawnLocation.addEntity(fleet3);
            fleet3.setLocation(spawnPosition.getX(), spawnPosition.getY());
            fleet3.addTag(subfleetTag);
        }else{//on the move
            fleet3.addAssignmentAtStart(FleetAssignment.ORBIT_AGGRESSIVE,getMothership(),300f,null);
            fleet3.getAI().setActionTextOverride("Following Youkai Base");

            spawnLocation.addEntity(fleet3);
            fleet3.setLocation(spawnPosition.getX(), spawnPosition.getY());
            fleet3.addTag(subfleetTag);

        }
        //todo test mem key make hostile
        if(spawnLocation.isHyperspace()){
            Misc.setFlagWithReason(fleet3.getMemoryWithoutUpdate(),MemFlags.FLEET_IGNORES_OTHER_FLEETS, "thquestYrBase",true,1000f);
        }else{
            Misc.setFlagWithReason(fleet3.getMemoryWithoutUpdate(),MemFlags.FLEET_IGNORES_OTHER_FLEETS, "thquestYrBase",false,1000f);
            Misc.setFlagWithReason(fleet3.getMemoryWithoutUpdate(), MemFlags.MEMORY_KEY_MAKE_HOSTILE, "thquest_yrb", true, 1000000000);

        }
    }
//    public void spawnRaidSubFleet(float fp, Vector2f location){
//        //spawn fleets
//        //get random location
//        FleetParamsV3 fleetParamsV3 = new FleetParamsV3(new Vector2f(0, 0), "thquest_youkai",
//                1.0f,
//                FleetTypes.TASK_FORCE,
//                fp, // combatPts
//                fp/20, // freighterPts
//                fp/20, // tankerPts
//                0f, // transportPts
//                0f, // linerPts
//                0, // utilityPts
//                .45f // qualityMod
//        );
//        fleetParamsV3.modeOverride= FactionAPI.ShipPickMode.PRIORITY_THEN_ALL;
//        CampaignFleetAPI fleet = FleetFactoryV3.createFleet(fleetParamsV3);
//
//        Misc.setFlagWithReason(fleet.getMemoryWithoutUpdate(), MemFlags.MEMORY_KEY_MAKE_HOSTILE, "thquest_yrb", true, 1000000000);
//
//        fleet.addAssignmentAtStart(FleetAssignment.RAID_SYSTEM,yrTarget,300f,null);
//        Global.getSector().getHyperspace().addEntity(fleet);
//        fleet.setLocation(location.getX(), location.y);
//        fleet.addTag(subfleetTag);
////        Log.getLogger().info("spawning YR fleet at "+location.getX()+","+location.getY()+" targeting market "+yrTarget.getId());
////        Log.getLogger().info("at "+fp+" fp");
//    }
    public String getId() {
        return id;
    }
    public enum YrPhase {
        TRAVELING_TO_ANCHOR,
        TRAVELING_TO_RAID,
        ON_RAID,
        AT_ANCHOR,
        FINISHED
    }
}
