package thQuest.fleets;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.util.IntervalUtil;

import java.util.ArrayList;
import java.util.List;

public class TTRDefFleetBehaviourSpec implements FleetBehaviourSpec{
    IntervalUtil interval = new IntervalUtil(1,2);
    boolean done = false;
    @Override
    public IntervalUtil getUpdateInterval() {
        return interval;
    }

    @Override
    public boolean isDone() {
        return done;
    }

    @Override
    public List<FleetBehaviour> update(float v) {
        if(Integer.parseInt((String) Global.getSector().getMemory().get("$thquestttr"))>3){
            done=true;
        }
        List<FleetBehaviour> fleetbehaviour = new ArrayList<>();
        fleetbehaviour.add(new TTRDefFleetBehaviour());
        done=true;
        return fleetbehaviour;
    }
}
