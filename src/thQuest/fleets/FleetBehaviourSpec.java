package thQuest.fleets;

import com.fs.starfarer.api.util.IntervalUtil;

import java.util.List;

public interface FleetBehaviourSpec {
    public IntervalUtil getUpdateInterval();
    public boolean isDone();
    public List<FleetBehaviour> update(float amount);
}
