package thQuest.fleets;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.util.IntervalUtil;

import java.util.ArrayList;
import java.util.List;

import static thQuest.ThU.secondsFromDays;

public class YrBaseFleetBehaviourSpec implements FleetBehaviourSpec{
    String tag = "thquestRaiderBase";
    int limit = 5;
    boolean newgame = true;
    //TODO testing was 60,90
    public IntervalUtil interval = new IntervalUtil(secondsFromDays(2),secondsFromDays(5));

    @Override
    public IntervalUtil getUpdateInterval() {
        return interval;
    }

    @Override
    public boolean isDone() {
        return false;
    }

    @Override
    public List<FleetBehaviour> update(float amount) {
        List<FleetBehaviour> out = new ArrayList<>();
        if(newgame){
            while(out.size()<limit){
                out.add(new YrBaseFleetBehaviour());
            }
            newgame= false;
            return out;
        }
        if(((FleetUtil) Global.getSector().getMemory().get("$thquestFleetUtil")).getPeers(YrBaseFleetBehaviour.class).size()<limit){
            out.add(new YrBaseFleetBehaviour());
        }
        return out;
    }
}
