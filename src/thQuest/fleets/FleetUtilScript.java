package thQuest.fleets;

import com.fs.starfarer.api.EveryFrameScript;
import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.*;
import thQuest.Log;

import java.util.logging.Logger;

public class FleetUtilScript extends BaseCampaignEventListener implements EveryFrameScript {
//    private FleetUtil fleetUtil;

//    public void setFleetUtil(FleetUtil fleetUtil) {
//        this.fleetUtil = fleetUtil;
//    }

    public FleetUtilScript(boolean permaRegister) {
        super(permaRegister);
    }

    @Override
    public boolean isDone() {
        return false;
    }

    @Override
    public boolean runWhilePaused() {
        return false;
    }

    @Override
    public void advance(float v) {
//        Log.getLogger().info("andvanced with"+v);
        ((FleetUtil) Global.getSector().getMemory().get("$thquestFleetUtil")).advance(v);
    }
    @Override
    public void reportBattleOccurred(CampaignFleetAPI primaryWinner, BattleAPI battle) {
        Logger.getLogger(this.getClass().getName()).info("battle occurred");
        ((FleetUtil) Global.getSector().getMemory().get("$thquestFleetUtil")).reportBattleOccurred(primaryWinner,battle);
    }
    @Override
    public void reportFleetJumped(CampaignFleetAPI fleet, SectorEntityToken from, JumpPointAPI.JumpDestination to) {
        ((FleetUtil) Global.getSector().getMemory().get("$thquestFleetUtil")).reportFleetJumped(fleet, from, to);
    }

    }
