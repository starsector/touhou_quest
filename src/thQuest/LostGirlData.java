package thQuest;

import java.util.ArrayList;
import java.util.List;

public class LostGirlData {
    public List<LostTouhou> notFound= new ArrayList<>();
    public List<LostTouhou> pointer= new ArrayList<>();
    public List<LostTouhou> found= new ArrayList<>();

    public List<LostTouhou> getNotFound() {
        return notFound;
    }

    public List<LostTouhou> getPointer() {
        return pointer;
    }


    public List<LostTouhou> getFound() {
        return found;
    }
    public LostTouhou getGirl(String name){
        for(LostTouhou touhou:notFound){
            if(touhou.getName().equals(name)){
                return touhou;
            }
        }
        for(LostTouhou touhou:pointer){
            if(touhou.getName().equals(name)){
                return touhou;
            }
        }
        for(LostTouhou touhou:found){
            if(touhou.getName().equals(name)){
                return touhou;
            }
        }
        return null;
    }
}
