package thQuest;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CargoTransferHandlerAPI;
import com.fs.starfarer.api.campaign.impl.items.BaseSpecialItemPlugin;
import com.fs.starfarer.api.loading.Description;
import com.fs.starfarer.api.ui.TooltipMakerAPI;

public class FairySpecialItemPlugin extends BaseSpecialItemPlugin {
    @Override
    public void createTooltip(TooltipMakerAPI tooltip, boolean expanded, CargoTransferHandlerAPI transferHandler, Object stackSource, boolean useGray) {
        super.createTooltip(tooltip, expanded, transferHandler, stackSource, useGray);
        float opad = 10.0F;
//        tooltip.addPara("This is a squad of several fairies together with support personnel and supplies. When installed on a carrier, they will create a home for themselves, "+
//                "containing nature to have the ability to reconstitute themselves there in the event of combat losses.",10f);
        tooltip.addPara(Global.getSettings().getDescription(this.itemId.substring(0,this.itemId.length()-10), Description.Type.SHIP).getText1(),10f);//thquest_sunflower_fairy	SHIP
        this.addCostLabel(tooltip, opad, transferHandler, stackSource);
        //TODO maybe mimic what vanilla has
    }
}
