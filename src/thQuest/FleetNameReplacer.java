package thQuest;

import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.FleetInflater;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.campaign.listeners.FleetInflationListener;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.util.Misc;

public class FleetNameReplacer implements FleetInflationListener {
    @Override
    public void reportFleetInflated(CampaignFleetAPI campaignFleetAPI, FleetInflater fleetInflater) {
        //TODO check if it is a GNS fleet that
        //thquest_makai_gate thquest_makai_gate_market
        //thquest_mugenkan thquest_mugenkan_market
        //thquest_new_old_hell_market thquest_new_old_hell_market
        //thquest_habitat_kappa
        MarketAPI sourceMarket = Misc.getSourceMarket(campaignFleetAPI);
        if(sourceMarket!=null && sourceMarket.getFaction().getId().equals(ThquestStrings.gensokyoFaction) ){
            String id = sourceMarket.getId();
            if( id.equals("thquest_makai_gate_market") || id.equals("thquest_mugenkan_market")||id.equals("thquest_WIS_market")||id.equals("thquest_vina_market")){
            for(FleetMemberAPI ship : campaignFleetAPI.getFleetData().getMembersInPriorityOrder()){
                renameShipToPrefix(ship,ThquestStrings.gensokyoShipPrefix, ThquestStrings.makaiShipPrefix); //Makai Space Navy
            }
            }if(id.equals("thquest_new_old_hell_market")){
                for(FleetMemberAPI ship : campaignFleetAPI.getFleetData().getMembersInPriorityOrder()){
                    renameShipToPrefix(ship,ThquestStrings.gensokyoShipPrefix, ThquestStrings.oldHellShipPrefix); //Old hell contract space ship
                }
            }
        //makai market  || sourceMarket.getId().equals("thquest_WIS_market")
        }
    }
    public static void renameShipToPrefix(FleetMemberAPI fleetMemberAPI, String from,String to){
        String shipName = fleetMemberAPI.getShipName();
        StringBuilder sb = new StringBuilder(shipName);
        for(int i = 0; i < from.length() ; i++){
            if(sb.charAt(i)!=from.charAt(i)){
                return;
            }
        }
        sb.delete(0,from.length());
        sb.insert(0,to);
        fleetMemberAPI.setShipName(sb.toString());
    }
}
