package thQuest;

import com.fs.starfarer.api.EveryFrameScript;
import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CargoAPI;
import com.fs.starfarer.api.campaign.StarSystemAPI;
import thQuest.fleets.FleetUtil;
import thQuest.fleets.YrBaseFleetBehaviour;

import java.util.ArrayList;
import java.util.List;

public class YRClockScript implements EveryFrameScript {
    int pos = 0;
    List<String> recentlyRaidedSystems = new ArrayList<>();
    float current = 0f;
    float baseValue = 1000f;//todo change to 1000 or whatever second value
    public float x = baseValue;
    float rand = (float)Math.random();
    @Override
    public boolean isDone() {
        return false;
    }

    @Override
    public boolean runWhilePaused() {
        return false;
    }

    @Override
    public void advance(float v) {
        current = current+v;
    if(current>x/2+x*rand){
        if(recentlyRaidedSystems.size()>6){
            recentlyRaidedSystems.remove(0);
        }
        //trigger YR, reset rand and current
        rand = (float) Math.random();
        FleetUtil fu = (FleetUtil) Global.getSector().getMemory().get("$thquestFleetUtil");
        List<YrBaseFleetBehaviour> yrBaseFleetList = fu.getPeers(YrBaseFleetBehaviour.class);
        if(yrBaseFleetList.size()>0){
            if(pos>=yrBaseFleetList.size()){
                pos=0;
            }
            try {
                StarSystemAPI system = yrBaseFleetList.get(pos).orderGoOnRaid(recentlyRaidedSystems);
                recentlyRaidedSystems.add(system.getId());
                current= 0f;
                //TODO test
                pos++;
            }catch (Exception e){
                Log.getLogger().info("could not spawn Youkai Raid. No appropriate system found");
            }
        }
    }
    }
}
