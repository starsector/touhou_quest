package thQuest;

public class ThquestStrings {
    public static String gensokyoFaction = "thquest_gensokyo";
    public static String youkaiFaction = "thquest_youkai";
    public static String incidentMitigated = "Negative effects are mitigated due to the presence of a shrine on $marketName";
    public static String gensokyoShipPrefix = "GDF";
    public static String makaiShipPrefix = "MSN";
    public static String oldHellShipPrefix = "OHCSS";
}
