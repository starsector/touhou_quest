package thQuest;

import com.fs.starfarer.api.EveryFrameScript;
import com.fs.starfarer.api.GameState;
import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.*;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.campaign.econ.SubmarketAPI;
import com.fs.starfarer.api.campaign.listeners.CoreUITabListener;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.campaign.fleet.CargoData;

import java.util.Arrays;
import java.util.List;

public class CoreUiListener implements CoreUITabListener, EveryFrameScript {
    List<String> fighterIds = Arrays.asList("thquest_fairy_maid_wing","thquest_sunflower_fairy_wing", "thquest_fairy_wing");
    List<String> variantIds = Arrays.asList("thquest_Fairy_Maid","thquest_sunflower_fairy_standard", "thquest_fairy_standard");
    boolean readyToTransform=false;
    @Override
    public void reportAboutToOpenCoreTab(CoreUITabId coreUITabId, Object o) {
    Log.getLogger();
    if(coreUITabId==CoreUITabId.REFIT){
        if(Global.getCurrentState()==GameState.CAMPAIGN){
            toLpc(Global.getSector().getPlayerFleet().getCargo());
            CampaignUIAPI campaignUI = Global.getSector().getCampaignUI();
            if(campaignUI.getCurrentInteractionDialog()!=null&&campaignUI.getCurrentInteractionDialog().getInteractionTarget()!=null&&campaignUI.getCurrentInteractionDialog().getInteractionTarget().getMarket()!=null){
                MarketAPI m = campaignUI.getCurrentInteractionDialog().getInteractionTarget().getMarket();
                List<SubmarketAPI> submarkets = m.getSubmarketsCopy();
                for(SubmarketAPI s : submarkets){
                    toLpc(s.getCargo());
                }
            }
        }
        //transform to LPC
    }else if (coreUITabId==CoreUITabId.FLEET) {
    //TODO name transformations
        if(Global.getCurrentState()==GameState.CAMPAIGN){
            CampaignUIAPI campaignUI = Global.getSector().getCampaignUI();
            if(campaignUI.getCurrentInteractionDialog()!=null&&campaignUI.getCurrentInteractionDialog().getInteractionTarget()!=null&&campaignUI.getCurrentInteractionDialog().getInteractionTarget().getMarket()!=null) {
                MarketAPI m = campaignUI.getCurrentInteractionDialog().getInteractionTarget().getMarket();
                String from = ThquestStrings.gensokyoShipPrefix;
                String to = "";
                String id = m.getId();
                if(m.getFaction().getId().equals(ThquestStrings.gensokyoFaction) && (id.equals("thquest_makai_gate_market") || id.equals("thquest_mugenkan_market")||id.equals("thquest_WIS_market")||id.equals("thquest_vina_market"))) {
                to = ThquestStrings.makaiShipPrefix;
                } else if (m.getFaction().getId().equals(ThquestStrings.gensokyoFaction) && id.equals("thquest_new_old_hell_market")) {
                    to = ThquestStrings.oldHellShipPrefix;
                }else {
                return;
                }
                List<SubmarketAPI> submarkets = m.getSubmarketsCopy();
                for (SubmarketAPI s : submarkets) {
                    FleetDataAPI d = s.getCargo().getMothballedShips();
                    for (FleetMemberAPI ship : d.getMembersInPriorityOrder()) {
                        FleetNameReplacer.renameShipToPrefix(ship,from,to );
                    }
                }
            }
            }
    }else{
        if(Global.getCurrentState()==GameState.CAMPAIGN) {
            toItem(Global.getSector().getPlayerFleet().getCargo());
            CampaignUIAPI campaignUI = Global.getSector().getCampaignUI();
            if(campaignUI.getCurrentInteractionDialog()!=null&&campaignUI.getCurrentInteractionDialog().getInteractionTarget()!=null&&campaignUI.getCurrentInteractionDialog().getInteractionTarget().getMarket()!=null){
                MarketAPI m = campaignUI.getCurrentInteractionDialog().getInteractionTarget().getMarket();
                List<SubmarketAPI> submarkets = m.getSubmarketsCopy();
                for(SubmarketAPI s : submarkets){
                    toItem(s.getCargo());
                }
            }
        }
            //transform to item
    }
    //after tag, get everyframe to skip
    }

    @Override
    public boolean isDone() {
        return false;
    }

    @Override
    public boolean runWhilePaused() {
        return false;
    }
    @Override
    public void advance(float v) {
        if(readyToTransform){
            //go from LPC->item
        }
    }
    public void toLpc(CargoAPI c){
        List<CargoStackAPI> stacks =c.getStacksCopy();
        for(CargoStackAPI stack : stacks){
            for(int i = 0 ; i<fighterIds.size(); i++){
                String id = fighterIds.get(i);
                if(stack.getData()!=null && stack.getData().getClass() == SpecialItemData.class && (id+"_item").equals(((SpecialItemData)stack.getData()).getId())){
                    int size =(int) stack.getSize();
                    c.removeStack(stack);
                    c.addFighters(fighterIds.get(i), size);
                }
            }
        }
    }
    public void toItem(CargoAPI c){
        List<CargoAPI.CargoItemQuantity<String>> stacks =c.getFighters();
        for(CargoAPI.CargoItemQuantity<String> stack : stacks){
            for(int i = 0 ; i<fighterIds.size(); i++){
                String id = fighterIds.get(i);
                if(stack.getItem()!=null && id.equals(stack.getItem())){
                    int size = stack.getCount();
                    c.removeFighters(id,stack.getCount());
                    c.addSpecial(new SpecialItemData(id+"_item", null),size);
                }
            }
        }
    }
}
