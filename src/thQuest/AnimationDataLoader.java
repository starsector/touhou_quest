package thQuest;

import com.fs.starfarer.api.Global;
import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class AnimationDataLoader {

    static Map<String, AnimationData> animationMap;
    public static AnimationData getAnimationData(String id){
        return animationMap.get(id);
    }
    public static void loadAnimationData() throws JSONException, IOException {
        animationMap = new HashMap<String, AnimationData>();
     JSONArray data = Global.getSettings().getMergedSpreadsheetDataForMod("id","data/config/thquest/animationFrames.csv", "thquest");
     for(int i=0;i<data.length();i++){
         AnimationData next = new AnimationData();
         next.setForwardFrames(Integer.parseInt(data.getJSONObject(i).getString("front_frames")));
         next.setSideFrames(Integer.parseInt(data.getJSONObject(i).getString("side_frames")));
         next.setTransitionFrames(Integer.parseInt((data.getJSONObject(i).getString("transition_frames"))));
         animationMap.put((data.getJSONObject(i).getString("id")),next);
     }
    }
}
