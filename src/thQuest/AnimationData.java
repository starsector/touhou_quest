package thQuest;

public class AnimationData {
    int forwardFrames;
    int sideFrames;
    int transitionFrames;

    public int getForwardFrames() {
        return forwardFrames;
    }

    public void setForwardFrames(int forwardFrames) {
        this.forwardFrames = forwardFrames;
    }

    public int getSideFrames() {
        return sideFrames;
    }

    public void setSideFrames(int sideFrames) {
        this.sideFrames = sideFrames;
    }

    public int getTransitionFrames() {
        return transitionFrames;
    }

    public void setTransitionFrames(int transitionFrames) {
        this.transitionFrames = transitionFrames;
    }
}
