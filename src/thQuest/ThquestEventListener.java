package thQuest;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.*;
import com.fs.starfarer.api.campaign.comm.IntelInfoPlugin;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.util.Misc;
import thQuest.intel.FantasyResurgenceEventIntel;
import thQuest.intel.FrFactor;
import thQuest.intel.MirrorsForceDiscover;
import thQuest.locations.GenerateMayohiga;

import java.util.List;

public class ThquestEventListener extends BaseCampaignEventListener {
    Long zetaInitTime;
    Long frTimeTracker;
    public static final float YR_CHANCE = .2f;//TODO change to .2
    Boolean zetaDone;
    public ThquestEventListener(boolean permaRegister) {
        super(permaRegister);
        zetaDone=false;
        zetaInitTime =null;
        frTimeTracker = Global.getSector().getClock().getTimestamp();
    }
    @Override
    public void reportEconomyTick(int interIndex){
        //YR!
        if(Global.getSector().getMemory().contains("$thquest_yr")&&Math.random()<YR_CHANCE){

//          if(true){
//              for(int i=0;i<10;i++) {
                  //spawnYrFleet();
//              }
        }

        //Log.getLogger().info("TICK");
        if(zetaInitTime !=null){
            if(Global.getSector().getClock().getElapsedDaysSince(zetaInitTime)>60&&!zetaDone){
                zetaDone=true;
                GenerateMayohiga.colonizeZeta();
                zetaInitTime =null;
            }
        }
        List<MarketAPI> mList =Misc.getFactionMarkets("thquest_gensokyo");
        boolean gnsHelp = true;
        for(MarketAPI m : mList){
            if(!m.isHidden()&&m.isInEconomy()){
                gnsHelp = false;
            }
        }
            if(gnsHelp&&!(boolean)Global.getSector().getMemoryWithoutUpdate().contains("$thquestMirrorsDiscovered")){
            //if mirrors not discovered, discover mirrors
                //todo test, changed
            ThU.discoverMarket("thquest_mugenkan_market",true);
            ThU.discoverMarket("thquest_vina_market",true);
            ThU.discoverMarket("thquest_makai_gate_market",true);
            Global.getSector().getMemoryWithoutUpdate().set("$thquestMirrorsDiscovered",true);
            StarSystemAPI system = Global.getSector().getStarSystem("thquest_mirrors");
            system.setEnteredByPlayer(true);
                IntelInfoPlugin i = new MirrorsForceDiscover(Global.getSector().getEntityById("thquest_makai_gate"),Global.getSector().getEntityById("thquest_makai_gate"));
            i.setImportant(true);
                Global.getSector().getIntelManager().addIntel(i);
        }
            //TODO rename ships in faction markets if necessary


        //YR progress
        if(Global.getSector().getClock().getElapsedDaysSince(frTimeTracker)>120){
            frTimeTracker = Global.getSector().getClock().getTimestamp();
            FantasyResurgenceEventIntel.addFactorCreateIfNecessary(new FrFactor(10,"time factor","The presence of the Mayohiga Pact and Youkai Raiders in the sector is leading to more \"fantastical activity\""),null);
        }


    }



    public void startZetaClock(){
        zetaInitTime =Global.getSector().getClock().getTimestamp();
    }
}
