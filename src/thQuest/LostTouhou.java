package thQuest;

import com.fs.starfarer.api.campaign.SectorEntityToken;

import java.util.List;
import java.util.Map;

public class LostTouhou {
    private String name;
    private String sirname;
    private Location locationEnum;
    private SectorEntityToken actualLocation;

    public SectorEntityToken getActualLocation() {
        return actualLocation;
    }

    public void setActualLocation(SectorEntityToken actualLocation) {
        this.actualLocation = actualLocation;
    }

    public LostTouhou(String name, String sirname, Location locationEnum) {
        this.name = name;
        this.sirname = sirname;
        this.locationEnum = locationEnum;
    }

    public String getSirname() {
        return sirname;
    }

    public void setSirname(String sirname) {
        this.sirname = sirname;
    }

    public String getName() {
        return name;
    }
    public String getFullName(){
        StringBuilder sb = new StringBuilder(name);
        sb.setCharAt(0,Character.toUpperCase(sb.charAt(0)));
        return(sb+" "+sirname);
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLocationEnum(Location locationEnum) {
        this.locationEnum = locationEnum;
    }

    public Location getLocationEnum() {
        return locationEnum;
    }

    public enum Location {
    DECIV,
    RUINS,
    DEBRIS,
    DERILICT,
    STASH,
    HABITAT,
    MINING_STATION,
     CIV_PLANET,
}

}