package thQuest;

import com.fs.starfarer.api.EveryFrameScript;
import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.StarSystemAPI;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.util.IntervalUtil;
import com.fs.starfarer.api.util.Misc;
import org.lwjgl.util.vector.Vector2f;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class IncidentClockScript implements EveryFrameScript {
    //TODO reduce time
    IntervalUtil interval = new IntervalUtil(10f,20f);
    List<String> recentlyPickedMarkets = new ArrayList<>();
    List<String> incidentConditionsHabitable = new ArrayList<>(Arrays.asList("thquest_scarlet_mist","thquest_unseasonal_weather","thquest_shrines","thquest_scarlet_mist","thquest_unseasonal_weather","thquest_apparitions"));
    List<String> incidentConditions = new ArrayList<>(Arrays.asList("thquest_shrines","thquest_apparitions"));

    @Override
    public boolean isDone() {
        return false;
    }

    @Override
    public boolean runWhilePaused() {
        return false;
    }

    @Override
    public void advance(float v) {
        interval.advance(v);
        if(interval.intervalElapsed()){
            List<MarketAPI> marketList = Misc.getNearbyMarkets(new Vector2f(0,0),100);
            int tries = 100;
            int x=0;
            MarketAPI m=null;
            while (x<tries){
                m = marketList.get((int) (Math.random()* marketList.size()));
                String c;
                if(m.hasCondition("habitable")){
                    c = incidentConditionsHabitable.get((int) (Math.random()* incidentConditionsHabitable.size()));
                }
                else{
                    c = incidentConditions.get((int) (Math.random()* incidentConditions.size()));
                }
                if(!m.hasCondition(c)){
                    m.addCondition(c);
                    Log.getLogger().info("added incident condition " + c + " on "+m.getId());
                    break;
                }
                x++;
            }
            x=0;
        }
    }
}
