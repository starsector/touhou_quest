package thQuest.TTR;
import com.fs.starfarer.api.util.Misc;
import org.apache.log4j.Logger;
import com.fs.starfarer.api.EveryFrameScript;
import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.FactionAPI;
import com.fs.starfarer.api.impl.campaign.fleets.FleetFactoryV3;
import com.fs.starfarer.api.impl.campaign.fleets.FleetParamsV3;
import com.fs.starfarer.api.impl.campaign.ids.FleetTypes;
import com.fs.starfarer.api.util.IntervalUtil;
import org.lwjgl.util.vector.Vector2f;
import thQuest.Log;

public class ThquestDancerScript implements EveryFrameScript {
    public IntervalUtil interval =new IntervalUtil(1,2);
    boolean done = false;
//    Logger logger = Logger.getLogger(this.getClass().getName());
    @Override
    public boolean isDone() {
        //after dialogue trigger, true
        return done;
    }

    @Override
    public boolean runWhilePaused() {
        return false;
    }

    @Override
    public void advance(float v) {
        interval.advance(v);
        if (interval.intervalElapsed()) {
            //1. check if it is the correct quest stage.
            //2. check if player is in hyperspace
            //3. check if the distance is good
            if(Integer.parseInt((String)Global.getSector().getMemory().get("$thquestttr"))!=2) {//stage 2 is go to reimaden
                done = true;
//                logger.warn("wrong quest stage! aborting TTR Dancer intercept script!");
                return;
            }
            Vector2f location = Global.getSector().getEntityById("thquest_reimaden").getLocationInHyperspace();
            if(Global.getSector().getPlayerFleet().isInHyperspace()&& Misc.getDistance(Global.getSector().getPlayerFleet().getLocation(),location)<10000f){
                //All good!
                FleetParamsV3 fleetParamsV3 = new FleetParamsV3(new Vector2f(0, 0), "thquest_youkai",
                        1.0f,
                        FleetTypes.TASK_FORCE,
                        10, // combatPts
                        0, // freighterPts
                        0, // tankerPts
                        0f, // transportPts
                        0f, // linerPts
                        0, // utilityPts
                        .45f // qualityMod
                );
                fleetParamsV3.modeOverride = FactionAPI.ShipPickMode.PRIORITY_THEN_ALL;
                CampaignFleetAPI target = FleetFactoryV3.createFleet(fleetParamsV3);
                Global.getSector().getHyperspace().addEntity(target);
                target.setLocation(100000, 100000);
                target.setId("ThquestTTRDancersTarget");
                Global.getSector().getCampaignUI().showInteractionDialog(target);
                //TODO too soon???
                target.despawn();
                done = true;
            }
        }
    }
}
