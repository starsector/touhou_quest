package thQuest;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.StarSystemAPI;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class GenerateYrBases {
    static int numBases = 5;
    public static String tag = "thquest_yr_base";
    public static void createYrBases(){
        List<StarSystemAPI> chosenSystems = new ArrayList<>();
        for(int i=0;i<numBases;i++){
            List<StarSystemAPI> starSystems = Global.getSector().getStarSystems();
            boolean done = false;
            StarSystemAPI randomSystem;
            while(!done){
                randomSystem = starSystems.get((int) (Math.random()*starSystems.size()));
            if(randomSystem.getConstellation() != null && randomSystem.getStar().isStar() &&
                    randomSystem.isProcgen() && !chosenSystems.contains(randomSystem)){//system is good parameters
                chosenSystems.add(randomSystem);
                //choose a location not occupied by orbiting thing
                if(randomSystem.getPlanets().size() > 0){
                //system with planets
                }else{
                //system without planets
                    List<Float> positions = new ArrayList<>();
                    for(SectorEntityToken s:randomSystem.getAllEntities()){
                        positions.add(s.getCircularOrbitRadius());
                    }
                    Collections.sort(positions);
                    //find a gap
                }

                done=true;
            }
            }
        }

    }
}
