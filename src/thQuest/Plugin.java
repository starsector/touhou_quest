package thQuest;

import com.fs.starfarer.api.BaseModPlugin;
import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.PluginPick;
import com.fs.starfarer.api.campaign.*;
import com.fs.starfarer.api.characters.FullName;
import com.fs.starfarer.api.characters.MutableCharacterStatsAPI;
import com.fs.starfarer.api.characters.PersonAPI;
import com.fs.starfarer.api.combat.MissileAIPlugin;
import com.fs.starfarer.api.combat.MissileAPI;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.impl.campaign.ids.Personalities;
import com.fs.starfarer.api.impl.campaign.ids.Ranks;
import com.fs.starfarer.api.impl.campaign.ids.Skills;
import com.fs.starfarer.api.impl.campaign.procgen.StarAge;
import com.fs.starfarer.api.impl.campaign.procgen.StarSystemGenerator;
import com.fs.starfarer.api.impl.combat.ThquestFantasySealAI;
import com.fs.starfarer.api.impl.combat.ThquestMagicMissileAi;
import com.fs.starfarer.api.impl.combat.ThquestSanaeMissileAI;
import com.fs.starfarer.api.loading.Description;
import com.fs.starfarer.api.plugins.OfficerLevelupPlugin;
import com.fs.starfarer.combat.entities.Missile;
import data.scripts.ai.ThquestFantasySealAI2;
import data.scripts.ai.ThquestVengefulSpiritAi;
import exerelin.campaign.SectorManager;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import thQuest.fleets.FleetUtil;
import thQuest.fleets.FleetUtilScript;
import thQuest.fleets.YrBaseFleetBehaviourSpec;
import thQuest.intel.FantasyResurgenceEventIntel;
import thQuest.locations.CreateLostTouhouLocations;
import thQuest.locations.CreateRandomNewOldHell;
import thQuest.locations.GenerateDarkMirror;
import thQuest.locations.GenerateMayohiga;

import java.io.IOException;
import java.util.Objects;

public class Plugin extends BaseModPlugin {

    @Override
    public void onNewGame() {
    }

    @Override
    public void onDevModeF8Reload() {
        super.onDevModeF8Reload();
        try {
            AnimationDataLoader.loadAnimationData();
            ThquestSettings.loadSettings();
        } catch (JSONException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onGameLoad(boolean newGame) {
        FleetUtilScript fleetUtilScript = new FleetUtilScript(false);
//        fleetUtilScript.setFleetUtil((FleetUtil) Global.getSector().getMemory().get("$thquestFleetUtil"));
//        Global.getSector().getListenerManager().addListener(fleetUtilScript,false);
        Global.getSector().addTransientListener(fleetUtilScript);
        Global.getSector().addTransientScript(fleetUtilScript);
        Global.getSector().getListenerManager().addListener(new CoreUiListener());
        Global.getSector().getListenerManager().addListener(new FleetNameReplacer());
        //TODO update descriptions based on FR percentage
        //for now, we aren't going to bother with setting them back to before FR
        ThquestScripts.updateDescriptions();
    }
    public void onApplicationLoad() throws JSONException, IOException {
        AnimationDataLoader.loadAnimationData();
        ThquestSettings.loadSettings();
        ThquestScripts.updateDescriptions();
    }

    @Override
    public void onEnabled(boolean wasEnabledBefore){

            if(!wasEnabledBefore){
                //the script that will call Youkai Raiders
                Global.getSector().addScript(new YRClockScript());
                //script that makes random conditions
                //TODO the following line activating incident is unstable, fix later
//                Global.getSector().addScript(new IncidentClockScript());
            //script setup
            Global.getSector().getListenerManager().addListener(new ThquestEventListener(true));
            FleetUtil fleetUtil = new FleetUtil();
            //TODO separate the responsibilities of the scripts data in the scripts. Data is loaded, the script won't be
            Global.getSector().getMemory().set("$thquestFleetUtil",fleetUtil);
//            Global.getSector().getListenerManager().addListener(fleetUtil);
//            Global.getSector().getScripts().add(fleetUtil);
            //faction setup

                //todo remove maybe
                 fleetUtil = (FleetUtil) Global.getSector().getMemory().get("$thquestFleetUtil");
                 //replaced by base behaviour
//                fleetUtil.addFleetBehaviourSpec(new YrFleetBehaviourSpec());
                fleetUtil.addFleetBehaviourSpec(new YrBaseFleetBehaviourSpec());

                Global.getSector().getFaction("thquest_gensokyo").setRelationship(Factions.PIRATES,-0.50f);
            Global.getSector().getFaction("thquest_gensokyo").setRelationship(Factions.LUDDIC_PATH,-0.50f);
                Global.getSector().getFaction("thquest_gensokyo").setRelationship("thquest_youkai",-0.50f);
                FactionAPI youkaiFaction =Global.getSector().getFaction("thquest_youkai");
            for(FactionAPI factionAPI:Global.getSector().getAllFactions()){
                if(!Objects.equals(factionAPI.getId(), "thquest_youkai")) {
                    youkaiFaction.setRelationship(factionAPI.getId(), -0.5f);
                }
            }
            youkaiFaction.setRelationship("thquest_gensokyo", .75f);
            youkaiFaction.setRelationship("thquest_youkai", 1f);

            //person setup

            //Nex compatibility setting, if there is no nex or corvus mode(Nex), generate system and quest stuff
            boolean haveNexerelin = Global.getSettings().getModManager().isModEnabled("nexerelin");
            if (!haveNexerelin || SectorManager.getManager().isCorvusMode()) {
                setup(true);
            }
            Global.getSector().getMemory().set("$touhouquestybh","0");
        }
    }
    @Override
    public PluginPick<MissileAIPlugin> pickMissileAI(MissileAPI missile, ShipAPI launchingShip){
        String missileSpec = missile.getProjectileSpecId();
        if(missileSpec.equals("thquestSnake")){
            return new PluginPick<MissileAIPlugin>(new ThquestSanaeMissileAI(missile,launchingShip), CampaignPlugin.PickPriority.MOD_SPECIFIC);
        }else if(missileSpec.equals("thquest_fantasy_seal_red")||missileSpec.equals("thquest_fantasy_seal_blue")||missileSpec.equals("thquest_fantasy_seal_yellow")||missileSpec.equals("thquest_fantasy_seal_green")){
            return new PluginPick<MissileAIPlugin>(new ThquestFantasySealAI(missile,launchingShip), CampaignPlugin.PickPriority.MOD_SPECIFIC);
            //phase 2 fantasy seal likes to circle around small targets. Not good for small projectiles...
        } else if (missileSpec.equals("thquest_ofuda_reimu_blue")) {//yin yang orb
            return new PluginPick<MissileAIPlugin>(new ThquestFantasySealAI2(missile,launchingShip), CampaignPlugin.PickPriority.MOD_SPECIFIC);
        } else if (missileSpec.equals("thquest_vengeful_spirit")) {
            return new PluginPick<MissileAIPlugin>(new ThquestVengefulSpiritAi((Missile) missile, launchingShip), CampaignPlugin.PickPriority.MOD_SPECIFIC);
        } else if (missileSpec.equals("thquest_magic_missile_proj")){
            return new PluginPick<MissileAIPlugin>(new ThquestMagicMissileAi((Missile) missile, missile.getSpec().getBehaviorSpec(),launchingShip), CampaignPlugin.PickPriority.MOD_SPECIFIC);
        }
        return null;
    }
    public void setup(boolean inProgressGame){
        //System.out.println("SETUP CALLED THQUEST");
        PersonAPI yum = Global.getFactory().createPerson();
        yum.setId("thquestYum");
        yum.setName(new FullName("Yumemi","Okazaki", FullName.Gender.FEMALE));
        yum.setPortraitSprite(Global.getSettings().getSpriteName("characters","thQuest_yum"));
        yum.setRankId(Ranks.CITIZEN);
        yum.setFaction(Factions.NEUTRAL);
        yum.setPostId(Ranks.POST_SCIENTIST);
        SectorEntityToken s =Global.getSector().getStarSystem("galatia").getEntityById("station_galatia_academy");
        Global.getSector().getImportantPeople().addPerson(yum);
        //Yes, I can't pass in yum object
        s.getMarket().getCommDirectory().addPerson(Global.getSector().getImportantPeople().getPerson("thquestYum"));

        PersonAPI chi = Global.getFactory().createPerson();
        chi.setId("thquestChi");
        chi.setName(new FullName("Chiyuri","Kitashirakawa", FullName.Gender.FEMALE));
        chi.setPortraitSprite(Global.getSettings().getSpriteName("characters","thQuest_chi"));
        chi.setRankId(Ranks.CITIZEN);
        chi.setFaction(Factions.NEUTRAL);
        chi.setPostId(Ranks.POST_SPACER);
        Global.getSector().getImportantPeople().addPerson(chi);

        PersonAPI rei = Global.getFactory().createPerson();
        rei.setId("thquestRei");
        rei.setName(new FullName("Reimu","Hakurei", FullName.Gender.FEMALE));
        rei.setPortraitSprite(Global.getSettings().getSpriteName("characters","thQuest_rei"));
        rei.setRankId(Ranks.CITIZEN);
        rei.setFaction("thquest_gensokyo");
        rei.setPostId(Ranks.POST_CITIZEN);
        Global.getSector().getImportantPeople().addPerson(rei);


        PersonAPI yuk = Global.getFactory().createPerson();
        yuk.setId("thquestYuk");
        yuk.setName(new FullName("Yukari","Yakumo", FullName.Gender.FEMALE));
        yuk.setPortraitSprite(Global.getSettings().getSpriteName("characters","thQuest_yuk"));
        //need to add sage rank
        yuk.setRankId(Ranks.CITIZEN);
        yuk.setFaction("thquest_gensokyo");
        yuk.setPostId(Ranks.POST_CITIZEN);
        Global.getSector().getImportantPeople().addPerson(yuk);
        //TTR quest
        //ran
        PersonAPI ran = Global.getFactory().createPerson();
        ran.setId("thquestRan");
        ran.setName(new FullName("?????","", FullName.Gender.FEMALE));//at first you do not know who she is
        ran.setPortraitSprite(Global.getSettings().getSpriteName("characters","thQuest_ran"));
        ran.setRankId(Ranks.SPECIAL_AGENT);//TODO special agent is kinda cool.
        ran.setFaction("thquest_gensokyo");
        ran.setPostId(Ranks.POST_SPECIAL_AGENT);
        Global.getSector().getImportantPeople().addPerson(ran);
        //rikako
        PersonAPI rik = Global.getFactory().createPerson();
        rik.setId("thquestRik");
        rik.setName(new FullName("Rikako", "Asakura", FullName.Gender.FEMALE));
        rik.setPortraitSprite(Global.getSettings().getSpriteName("characters","thQuest_rikako"));
        rik.setRankId(Ranks.CITIZEN);
        rik.setFaction(Factions.TRITACHYON);
        rik.setPostId(Ranks.POST_SCIENTIST);
        Global.getSector().getImportantPeople().addPerson(rik);
        //Orin(new old hell)
        PersonAPI orin = Global.getFactory().createPerson();
        orin.setId("thquestOrin");
        orin.setName(new FullName("Rin", "Kaenbyou", FullName.Gender.FEMALE));
        orin.setPortraitSprite(Global.getSettings().getSpriteName("characters","thQuest_orin"));
        orin.setRankId(Ranks.CITIZEN);
        orin.setFaction("thquest_gensokyo");
        orin.setPostId(Ranks.POST_ADMINISTRATOR);
        Global.getSector().getImportantPeople().addPerson(orin);
        //Shinki(makai gate)
        PersonAPI shinki = Global.getFactory().createPerson();
        shinki.setId("thquestShinki");
        shinki.setName(new FullName("Shinki", "", FullName.Gender.FEMALE));
        shinki.setPortraitSprite(Global.getSettings().getSpriteName("characters","thQuest_shinki"));
        shinki.setRankId(Ranks.UNKNOWN);
        shinki.setFaction("thquest_gensokyo");
//        shinki.setPostId(Ranks.POST_UNKNOWN);
        shinki.setPostId(Ranks.POST_ADMINISTRATOR);
        shinki.getStats().setSkillLevel(Skills.INDUSTRIAL_PLANNING,3);
        Global.getSector().getImportantPeople().addPerson(shinki);

        //Ellen(Mugenkan station)
        PersonAPI elly = Global.getFactory().createPerson();
        elly.setId("thquestElly");
        elly.setName(new FullName("Elly","", FullName.Gender.FEMALE));
        elly.setPortraitSprite(Global.getSettings().getSpriteName("characters","thQuest_elly"));
        elly.setRankId(Ranks.UNKNOWN);
        elly.setFaction("thquest_gensokyo");
        elly.setPostId(Ranks.POST_ADMINISTRATOR);
        elly.getStats().setSkillLevel(Skills.INDUSTRIAL_PLANNING,3);

        Global.getSector().getImportantPeople().addPerson(elly);

        //meira "thquest_meira"
        PersonAPI meira = Global.getFactory().createPerson();
        meira.setId("thquest_meira");
        meira.setName(new FullName("Meira","", FullName.Gender.FEMALE));
        meira.setPortraitSprite(Global.getSettings().getSpriteName("characters","thQuest_meira"));
        meira.setFaction(ThquestStrings.youkaiFaction);
        meira.setRankId(Ranks.SPACE_CAPTAIN);
        meira.setPersonality(Personalities.AGGRESSIVE);
        MutableCharacterStatsAPI meiraStats = meira.getStats();
        OfficerLevelupPlugin plugin = (OfficerLevelupPlugin) Global.getSettings().getPlugin("officerLevelUp");
        meiraStats.addXP(plugin.getXPForLevel(5));
        meiraStats.setSkillLevel(Skills.HELMSMANSHIP,2);
        meiraStats.setSkillLevel(Skills.IMPACT_MITIGATION,1);
        meiraStats.setSkillLevel(Skills.FIELD_MODULATION,1);
        meiraStats.setSkillLevel(Skills.BALLISTIC_MASTERY,2);
        meiraStats.setSkillLevel(Skills.SYSTEMS_EXPERTISE,1);
        Global.getSector().getImportantPeople().addPerson(meira);

        //create star system
        GenerateMayohiga.createMayohiga(inProgressGame);
        GenerateDarkMirror.createDarkMirror(inProgressGame);
        new CreateRandomNewOldHell(new StarSystemGenerator.CustomConstellationParams(StarAge.OLD)).createSystem();





//        CreateHell.generate(inProgressGame);
        //setup for lost girls quest
        CreateLostTouhouLocations.generate();
    }
}
