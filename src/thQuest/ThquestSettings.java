package thQuest;

import com.fs.starfarer.api.Global;
import org.json.JSONException;
import org.json.JSONObject;
import org.lwjgl.util.vector.Vector2f;

import java.io.IOException;

public class ThquestSettings {
    private static Vector2f mayohigaLocation;
    private static boolean randomMayohiga;

    public static Vector2f getMayohigaLocation() {
        return mayohigaLocation;
    }

    public static boolean isRandomMayohiga() {
        return randomMayohiga;
    }

    public static void loadSettings() throws JSONException, IOException {
        JSONObject thquestSettings = Global.getSettings().getMergedJSONForMod("thquest_config.json","thquest");
        randomMayohiga = thquestSettings.getBoolean("RandomMayohigaLocation");
        mayohigaLocation = new Vector2f(thquestSettings.getInt("MayohigaLocationX"),thquestSettings.getInt("MayohigaLocationY"));

    }
}
