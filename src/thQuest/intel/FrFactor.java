package thQuest.intel;

import com.fs.starfarer.api.impl.campaign.intel.events.*;
import com.fs.starfarer.api.ui.TooltipMakerAPI;
import com.fs.starfarer.api.util.Misc;

import java.awt.*;

public class FrFactor extends BaseOneTimeFactor {
    public static float SHOW_DURATION_DAYS = 1000000000000000000f;//TODO changed
    private String text;
    private String longText;
    public FrFactor(int points,String text, String longText) {
        super(points);
        this.text=text;
        this.longText=longText;
    }

    public String getDesc(BaseEventIntel intel) {
        return text;
    }

    public TooltipMakerAPI.TooltipCreator getMainRowTooltip() {
        return new BaseFactorTooltip() {
            public void createTooltip(TooltipMakerAPI tooltip, boolean expanded, Object tooltipParam) {
                tooltip.addPara(longText, 0.0F, Misc.getHighlightColor(), new String[]{"20"});
            }
        };
    }


}
