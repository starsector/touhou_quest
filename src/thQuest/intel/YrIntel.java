//package thQuest.intel;
//
//import com.fs.starfarer.api.Global;
//import com.fs.starfarer.api.campaign.FactionAPI;
//import com.fs.starfarer.api.campaign.SectorEntityToken;
//import com.fs.starfarer.api.impl.campaign.intel.misc.BreadcrumbIntel;
//import com.fs.starfarer.api.ui.SectorMapAPI;
//import com.fs.starfarer.api.ui.TooltipMakerAPI;
//import thQuest.fleets.YrFleetBehaviour;
//
//import java.util.HashSet;
//import java.util.Set;
//
//public class YrIntel extends BreadcrumbIntel {
//    YrFleetBehaviour.Status status = YrFleetBehaviour.Status.STARTING;
//    YrFleetBehaviour yrFleetBehaviour;
//
//    public YrFleetBehaviour.Status getStatus() {
//        return status;
//    }
//
//    public void setStatus(YrFleetBehaviour.Status status) {
//        this.status = status;
//    }
//
//    public YrIntel(SectorEntityToken foundAt, SectorEntityToken target, YrFleetBehaviour yrFleetBehaviour) {
//        super(foundAt, target);
//        this.yrFleetBehaviour = yrFleetBehaviour;
//    }
//    @Override
//    public String getName() {
//        return "Youkai Raid";
//    }
//
//    @Override
//    public String getText() {
//        return "Youkai Raid";
//    }
//
//    @Override
//    public String getIcon() {
//        return Global.getSettings().getSpriteName("intel","thquest_yr");
//    }
//
//    @Override
//    public boolean hasSmallDescription() {
//        return true;
//    }
//
//    @Override
//    public Set<String> getIntelTags(SectorMapAPI map) {
//        HashSet<String> set= new HashSet<>();
//        set.add("military");
//        set.add("thquest_gensokyo");
//        return set;
//    }
//
//    @Override
//    public FactionAPI getFactionForUIColors() {
//        return Global.getSector().getFaction("thquest_youkai");
//    }
//
//    @Override
//    public void createSmallDescription(TooltipMakerAPI info, float width, float height) {
//        info.addPara("Youkai are raiding the "+target.getStarSystem().getName()+" system.",10);
//        //from fleetbehaviour
//
//        if(status==null){
//            info.addPara("The raid is over due to to Youkai losses. The remaining forces are retreating.",10);
//        }else if(status.equals(YrFleetBehaviour.Status.RETREATED)){
//            info.addPara("The raid is over due to to Youkai losses. The remaining forces are retreating.",10);
//        } else if (status.equals(YrFleetBehaviour.Status.STARTING)) {
//            info.addPara("The raid has no impact as of yet... Most likely the raiding forces are still en route.",10);
//        } else if (status.equals(YrFleetBehaviour.Status.V1)) {
//            info.addPara("The raid is having a low impact.",10);
//        } else if (status.equals(YrFleetBehaviour.Status.V2)) {
//            info.addPara("The raid is having a medium impact.",10);
//        } else if (status.equals(YrFleetBehaviour.Status.V3)) {
//            info.addPara("The raid is having a large impact.",10);
//        } else if (status.equals(YrFleetBehaviour.Status.DONE)){
//            info.addPara("The raid now over.",10);
//        }
//        //info.addButton("Delete", BUTTON_DELETE,300,20,20);
//    }
//
//}
