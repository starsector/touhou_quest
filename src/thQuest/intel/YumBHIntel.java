package thQuest.intel;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.FactionAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.comm.IntelInfoPlugin;
import com.fs.starfarer.api.campaign.rules.MemKeys;
import com.fs.starfarer.api.impl.campaign.ids.Tags;
import com.fs.starfarer.api.impl.campaign.intel.BaseIntelPlugin;
import com.fs.starfarer.api.ui.SectorMapAPI;
import com.fs.starfarer.api.ui.TooltipMakerAPI;
import com.fs.starfarer.api.util.Misc;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class YumBHIntel extends BaseIntelPlugin {
    SectorEntityToken location;
    SectorEntityToken destination;
    Boolean alternateDescription=false; //stage 1 vs stage 2
    public YumBHIntel(SectorEntityToken location, SectorEntityToken destination) {
    this.location = location;
    this.destination = destination;
    this.important=true;
        Misc.makeImportant(destination,"thquestybh");
    }
    public static boolean isComplete(){
        return Global.getSector().getMemory().getInt("touhouquestybh")==4;
    }

    public void setDestination(SectorEntityToken destination) {
        Misc.makeUnimportant(this.destination,"thquestybh");
        this.destination = destination;
        Misc.makeImportant(destination,"thquestybh");
    }
    public void setAlternateDescription(boolean b){
        alternateDescription=b;
    }

    @Override
    public SectorEntityToken getMapLocation(SectorMapAPI map){
        return destination;
    }
    @Override
    public java.lang.String getSmallDescriptionTitle(){
     return "Yumemi back home";
    }
    @Override
    public java.lang.String getIcon(){
            return Global.getSettings().getSpriteName("characters", "thQuest_yum");
    }

    @Override
    public void createSmallDescription(TooltipMakerAPI info,float width,float height){
         if (alternateDescription) {
                info.addPara("Go to Gilead to help Yumemi with some sort of archeology project.", 10);
                addBulletPoints(info, ListInfoMode.IN_DESC);
            } else {
                info.addPara("Go to Galatia academy and talk to Yumemi", 10);
            }
    }

    @Override
    protected void addBulletPoints(TooltipMakerAPI info, ListInfoMode mode) {
        super.addBulletPoints(info, mode);
        Color h=Misc.getHighlightColor();
        Color tc = getBulletColorForMode(mode);
        info.addPara("  -%s reward", 10, tc, h, Misc.getDGSCredits(10000));

    }

    @Override
    public java.util.Set<java.lang.String> getIntelTags(SectorMapAPI map){
        Set<String> s = new HashSet<String>();
        s.add("Story");
        s.add("Accepted");
        s.add(Tags.INTEL_IMPORTANT);
        return s;
    }
    @Override
    public boolean hasSmallDescription(){
    return true;
    }
    @Override
    public void createIntelInfo(TooltipMakerAPI info, IntelInfoPlugin.ListInfoMode mode){
        info.addPara("Yumemi Back home", Color.PINK,10);
            if (alternateDescription) {
                info.addPara("Go to Gilead", 10);
            } else {
                info.addPara("Go to Galatia Academy", 10);
            }
    }
    @Override
    public void endImmediately() {
        Misc.makeUnimportant(this.destination,"thquestybh");
        super.endImmediately();
    }
}
