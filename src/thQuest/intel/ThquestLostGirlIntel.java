package thQuest.intel;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.FactionAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.rules.MemKeys;
import com.fs.starfarer.api.impl.campaign.ids.Tags;
import com.fs.starfarer.api.impl.campaign.intel.misc.BreadcrumbIntel;
import com.fs.starfarer.api.ui.IntelUIAPI;
import com.fs.starfarer.api.ui.SectorMapAPI;
import com.fs.starfarer.api.ui.TooltipMakerAPI;
import com.fs.starfarer.api.util.Misc;
import thQuest.LostGirlData;
import thQuest.LostTouhou;

import java.util.HashSet;
import java.util.Set;

public class ThquestLostGirlIntel extends BreadcrumbIntel {
    public ThquestLostGirlIntel(SectorEntityToken foundAt, SectorEntityToken target) {
        super(foundAt, target);
    }
    public String girlName;
    public LostTouhou.Location locationType;

    public LostTouhou.Location getLocationType() {
        return locationType;
    }

    public void setLocationType(LostTouhou.Location locationType) {
        this.locationType = locationType;
    }

    public String getGirlName() {
        return girlName;
    }

    public void setGirlName(String girlName) {
        this.girlName = girlName;
    }

    @Override
    public String getName() {
        return "Lost Girl";
    }

    @Override
    public String getText() {
        return "Lost Girl";
    }

    @Override
    public String getIcon() {
        return Global.getSettings().getSpriteName("intel","new_planet_info");
    }

    @Override
    public boolean hasSmallDescription() {
        return true;
    }

    @Override
    public Set<String> getIntelTags(SectorMapAPI map) {
        HashSet<String> set= new HashSet<>();
        set.add("Fleet log");
        set.add("Accepted");
        set.add(Tags.INTEL_IMPORTANT);
        set.add("thquest_gensokyo");
        return set;
    }

    @Override
    public FactionAPI getFactionForUIColors() {
        return Global.getSector().getFaction("thquest_gensokyo");
    }

    @Override
    public void createSmallDescription(TooltipMakerAPI info, float width, float height) {
        String location = "on";
        switch (locationType){
            case DECIV:
            case RUINS:
            case CIV_PLANET:
                location="on a planet";
                break;
            case DERILICT:
                location="in a derelict ship";
                break;
            case MINING_STATION:
            case HABITAT:
                location="on an abandoned station";
                break;
            case STASH:
            case DEBRIS:
            default:
                location="at an unknown location";
                break;
        }

        info.addPara("Chiyuri gave you information about "+girlName+"'s whereabouts, who has been missing for some time",10);
        info.addPara("She is located "+location+" in the "+target.getStarSystem().getName()+" system.",10);
        info.addButton("Delete", BUTTON_DELETE,300,20,20);
    }
    @Override
    protected void createDeleteConfirmationPrompt(TooltipMakerAPI prompt) {
        prompt.addPara("Are you sure you want to delete? Please continue if you could not find the location.", Misc.getTextColor(), 0.0F);
    }
    @Override
    public void buttonPressConfirmed(Object buttonId, IntelUIAPI ui) {
        if (buttonId == BUTTON_DELETE) {
            //todo test
            LostGirlData lostGirlData=(LostGirlData)Global.getSector().getMemory().get("$thquestlostgirldata");
        LostTouhou t =lostGirlData.getPointer().get(0);
        lostGirlData.getPointer().remove(t);
        lostGirlData.getNotFound().add(t);
            this.endImmediately();
            ui.recreateIntelUI();
        } else {
            ui.updateUIForItem(this);
        }

    }
    }
