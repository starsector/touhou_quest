package thQuest.intel;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.FactionAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.impl.campaign.fleets.RouteLocationCalculator;
import com.fs.starfarer.api.impl.campaign.intel.misc.BreadcrumbIntel;
import com.fs.starfarer.api.ui.SectorMapAPI;
import com.fs.starfarer.api.ui.TooltipMakerAPI;
import com.fs.starfarer.api.util.IntervalUtil;
import com.fs.starfarer.api.util.Misc;
import thQuest.Log;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class YrIntel2 extends BreadcrumbIntel {
    public Status status = Status.STARTING;
    public Status decayStatus = Status.V0;
    public String yrId;
    boolean decay = false;
    boolean removedConditions = false;
    float timePerStage = 120f;
    int stage = 0;
    float secondsPerLevel = 300;
    public float lingeringEffect;

    private IntervalUtil sanityInterval = new IntervalUtil(10f,30f);
    private float sanityTimeout = 900f;
    private float uptime = 0f;

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public YrIntel2(SectorEntityToken foundAt, SectorEntityToken target, String yrId) {
        super(foundAt, target);
        this.yrId = yrId;
    }

    @Override
    public String getName() {
        return "Youkai Raid";
    }

    @Override
    public String getText() {
        return "Youkai Raid";
    }

    @Override
    public String getIcon() {
        return Global.getSettings().getSpriteName("intel", "thquest_yr");
    }

    @Override
    public boolean hasSmallDescription() {
        return true;
    }

    @Override
    public Set<String> getIntelTags(SectorMapAPI map) {
        HashSet<String> set = new HashSet<>();
        set.add("military");
        set.add("thquest_gensokyo");
        return set;
    }

    @Override
    public void advance(float amount) {
        super.advance(amount);
        if (status == Status.DEFEATED || status == Status.DONE || status == Status.BASE_DESTROYED) {
            lingeringEffect = lingeringEffect - amount / secondsPerLevel;
        }
        if (lingeringEffect < 0 && !removedConditions) {
            removeConditions();
            removedConditions = true;
        }else if(lingeringEffect < -1){
            Global.getSector().removeScript(this);
            //TODO end after delay seems to not work
            this.endImmediately();
        }
        //todo update if status changed, and decay status
        //also updates status on its own, after it gets the done signal
        Status beforeStatus = decayStatus;
        if (lingeringEffect < 0.1) {
            decayStatus = Status.V0;
        } else if (lingeringEffect < 2f) {
            decayStatus = Status.V1;
        } else if (lingeringEffect < 3f) {
            decayStatus = Status.V2;
        } else {
            decayStatus = Status.V3;
        }
        if (decayStatus != beforeStatus) {
            updateMarketConditions();
        }
        //TODO uncomment if there is problems
//        //sanity check...
//        uptime=uptime+amount;
//        sanityInterval.advance(amount);
//        if(sanityInterval.intervalElapsed()||uptime>sanityTimeout) {
//            if (Global.getSector().getEntitiesWithTag("thquestYbMothership" + yrId).size() == 0) {
//                status = Status.DONE;
//            }
//        }
    }

    @Override
    public FactionAPI getFactionForUIColors() {
        return Global.getSector().getFaction("thquest_youkai");
    }

    @Override
    public void createSmallDescription(TooltipMakerAPI info, float width, float height) {
        info.addPara("Youkai are raiding the " + target.getStarSystem().getName() + " system.", 10);
        //from fleetbehaviour

        //    public float getDistAway(){
        //        CampaignFleetAPI mothership = getMothership();
        //        return RouteLocationCalculator.getTravelDays(mothership, yrTarget.getStarSystem().getHyperspaceAnchor());
        //    }
        switch (status) {
            case STARTING:
                info.addPara("The raiding forces are en route.", 10);
                List<SectorEntityToken> mothership = Global.getSector().getEntitiesWithTag("thquestYbMothership"+yrId);
                if(mothership.size()>0) {
                    info.addPara("Estimated " + (int)RouteLocationCalculator.getTravelDays(Global.getSector().getEntitiesWithTag("thquestYbMothership" + yrId).get(0), target) + " days until arrival", 10);
                }
                break;
            case V0:
                info.addPara("The raid is having no impact so far.", 10);
                break;
            case V1:
                info.addPara("The raid is having a low impact.", 10);
                break;
            case V2:
                info.addPara("The raid is having a medium impact.", 10);
                break;
            case V3:
                info.addPara("The raid is having a large impact.", 10);
                break;
            case DEFEATED:
                info.addPara("The raid is over due to to Youkai losses. The remaining forces are retreating.", 10);
                break;
            case DONE:
                info.addPara("The raid now over.", 10);
                break;
            case BASE_DESTROYED:
                info.addPara("The Youkai base has been destroyed. The remaining forces are retreating.", 10);
                break;
        }
        if (status == Status.DEFEATED || status == Status.DONE || status == Status.BASE_DESTROYED) {
            if (lingeringEffect > 0f) {
                info.addPara("The raid is having a lingering effect, which will decrease over time.", 10);
            }
        }
        //info.addButton("Delete", BUTTON_DELETE,300,20,20);
    }

    public enum Status {
        STARTING,
        V0,
        V1,
        V2,
        V3,
        DEFEATED,
        DONE,
        BASE_DESTROYED
    }

    public void updateMarketConditions() {
        Log.getLogger().info("updatemarketconditions called");
        List<MarketAPI> markets = Misc.getMarketsInLocation(target.getContainingLocation());
        for (MarketAPI m : markets) {
            Log.getLogger().info("updatemarketconditions called on market " + m.getId());
            if (!m.hasCondition("thquest_youkai_raiders")) {
                m.addCondition("thquest_youkai_raiders");
                m.getCondition("thquest_youkai_raiders").setSurveyed(true);//todo is this neccessary?
            }
            m.getCondition("thquest_youkai_raiders").getPlugin().setParam(decayStatus);
        }
    }

    public void removeConditions() {
        List<MarketAPI> markets = Misc.getMarketsInLocation(target.getContainingLocation());
        for (MarketAPI m : markets) {
            if (m.hasCondition("thquest_youkai_raiders")) {
                m.removeCondition("thquest_youkai_raiders");
            }
        }

    }
}
