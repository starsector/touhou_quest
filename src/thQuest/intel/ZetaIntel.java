package thQuest.intel;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.FactionAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.impl.campaign.ids.Tags;
import com.fs.starfarer.api.impl.campaign.intel.misc.BreadcrumbIntel;
import com.fs.starfarer.api.ui.SectorMapAPI;
import com.fs.starfarer.api.ui.TooltipMakerAPI;
import com.fs.starfarer.campaign.Faction;

import java.util.HashSet;
import java.util.Set;

public class ZetaIntel extends BreadcrumbIntel {
    public ZetaIntel(SectorEntityToken foundAt, SectorEntityToken target) {
        super(foundAt, target);
    }
    @Override
    public String getName() {
        return "Habitat Zeta Colonization Complete";
    }

    @Override
    public String getText() {
        return "Habitat Zeta Colonization Complete";
    }

    @Override
    public String getIcon() {
        return Global.getSettings().getSpriteName("intel","important");
    }

    @Override
    public boolean hasSmallDescription() {
        return true;
    }

    @Override
    public Set<String> getIntelTags(SectorMapAPI map) {
        HashSet<String> set= new HashSet<>();
        set.add("Fleet log");
        return set;
    }

    @Override
    public FactionAPI getFactionForUIColors() {
        return Global.getSector().getFaction("thquest_gensokyo");
    }

    @Override
    public void createSmallDescription(TooltipMakerAPI info, float width, float height) {
        info.addImage(Global.getSector().getFaction("thquest_gensokyo").getLogo(), width,128,10f);
        info.addPara("The Kickstarter was successful thanks to you and Gensokyo now has a new colony!",10);
        info.addButton("Delete", BUTTON_DELETE,300,20,20);
    }
}
