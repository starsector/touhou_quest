package thQuest.intel;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.comm.IntelInfoPlugin;
import com.fs.starfarer.api.impl.campaign.ids.Tags;
import com.fs.starfarer.api.impl.campaign.intel.BaseIntelPlugin;
import com.fs.starfarer.api.ui.SectorMapAPI;
import com.fs.starfarer.api.ui.TooltipMakerAPI;
import com.fs.starfarer.api.util.Misc;

import java.awt.*;
import java.util.HashSet;
import java.util.Set;

public class TriTachyonRikakoIntel extends BaseIntelPlugin {
    SectorEntityToken location;
    SectorEntityToken destination;
    static String memkey = "$thquestttr";

    //4 versions, + info on dancers based on stage. Beginning, R ending, F ending good & bad
    public TriTachyonRikakoIntel(SectorEntityToken location, SectorEntityToken destination) {
        this.location = location;
        this.destination = destination;
        this.important=true;
        Misc.makeImportant(destination,"thquestttr");

    }
    public static boolean isComplete(){
        return Global.getSector().getMemory().getInt(memkey)>4;
    }
    public void setDestination(SectorEntityToken destination) {
        Misc.makeUnimportant(this.destination,"thquestttr");
        this.destination = destination;
        Misc.makeImportant(destination,"thquestttr");
    }
    @Override
    public java.util.Set<java.lang.String> getIntelTags(SectorMapAPI map){
        Set<String> s = new HashSet<String>();
        s.add("Story");
        s.add("Accepted");
        s.add(Tags.INTEL_IMPORTANT);
        return s;
    }
    @Override
    public SectorEntityToken getMapLocation(SectorMapAPI map){
        return destination;
    }
    @Override
    public java.lang.String getSmallDescriptionTitle(){
        return "Tri-Tachyon Rikako";
    }
    @Override
    public boolean hasSmallDescription(){
        return true;
    }
    @Override
    public java.lang.String getIcon() {
        return Global.getSettings().getSpriteName("characters", "thQuest_rikako");
    }
        public void createIntelInfo(TooltipMakerAPI info, IntelInfoPlugin.ListInfoMode mode){
        //goes 2 = start 4 = have the dancers, 5=fantasy end 6=fantasy bad end, 7= Rikako ending
        int stage = Global.getSector().getMemory().getInt(memkey);
        info.addPara("Tri-Tachyon Rikako", Color.PINK,10);
//        if(stage==2){
//            info.addPara("Go to the Reimaden star system and go to the Tri-tachyon base at "+destination.getName()+".", Color.PINK,10);
//        }else if (stage==4){
//            info.addPara("Go to the Reimaden star system and go to the Tri-tachyon base at "+destination.getName()+".", Color.PINK,10);
//        }else if (stage==5){
//            info.addPara("You have successfully captured Rikako.", Color.PINK,10);
//            info.addPara("THIS IS THE END OF CONTENT FOR NOW. THANK YOU FOR PLAYING!", Color.PINK,10);//TODO remove
//        }else if (stage==6){
//            info.addPara("You have not successfully captured Rikako, however you have destroyed the base and stopped her plot.", Color.PINK,10);
//            info.addPara("THIS IS THE END OF CONTENT FOR NOW. THANK YOU FOR PLAYING!", Color.PINK,10);//TODO remove
//        }else if (stage==7){
//            info.addPara("You have betrayed Yukari and have agreed to take Rikako to Gensokyo.", Color.PINK,10);
//            info.addPara("THIS IS THE END OF CONTENT FOR NOW. THANK YOU FOR PLAYING!", Color.PINK,10);//TODO remove
//        }
    }
    @Override
    public void createSmallDescription(TooltipMakerAPI info,float width,float height) {
        //goes 2 = start 4 = have the dancers, 5=fantasy end 6=fantasy bad end, 7= Rikako ending
        int stage = Global.getSector().getMemory().getInt(memkey);
        info.addImage(Global.getSector().getFaction("thquest_gensokyo").getLogo(), width,128,10f);
        info.addPara("You were tasked to deal with a problem for Yukari. ",10);
        if(stage==2){
            info.addPara("Go to the Reimaden star system and go to the Tri-tachyon base at "+destination.getName()+".",10);
            info.addPara("Ran and Sakuya, who are currently aboard your ship will handle the ground fighting from there.",10);
        }else if (stage==4){
            info.addPara("Go to the Reimaden star system and go to the Tri-tachyon base at "+destination.getName()+".",10);
            info.addPara("Ran and Sakuya, who are currently aboard your ship will handle the ground fighting from there.",10);
            info.addPara("You have two other girls aboard your ship, Mai and Satono, who may also engage in the fighting if you so wish",10);
        }else if (stage==5){
            info.addPara("You have successfully captured Rikako.",10);
            info.addPara("THIS IS THE END OF CONTENT FOR NOW. THANK YOU FOR PLAYING!",10);//TODO remove
        }else if (stage==6){
            info.addPara("You have not successfully captured Rikako, however you have destroyed the base and stopped her plot.",10);
            info.addPara("THIS IS THE END OF CONTENT FOR NOW. THANK YOU FOR PLAYING!",10);//TODO remove
        }else if (stage==7){
            info.addPara("You have betrayed Yukari and have agreed to take Rikako to Gensokyo.",10);
            info.addPara("THIS IS THE END OF CONTENT FOR NOW. THANK YOU FOR PLAYING!",10);//TODO remove
        }
    }
    @Override
    public void endImmediately() {
        Misc.makeUnimportant(this.destination,"thquestttr");
        super.endImmediately();
    }
}
