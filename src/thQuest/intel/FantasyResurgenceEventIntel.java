package thQuest.intel;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.*;
import com.fs.starfarer.api.campaign.rules.MemKeys;
import com.fs.starfarer.api.characters.FullName;
import com.fs.starfarer.api.characters.PersonAPI;
import com.fs.starfarer.api.impl.campaign.ids.Personalities;
import com.fs.starfarer.api.impl.campaign.intel.events.BaseEventIntel;
import com.fs.starfarer.api.impl.campaign.intel.events.BaseFactorTooltip;
import com.fs.starfarer.api.impl.campaign.intel.events.EventFactor;
import com.fs.starfarer.api.impl.campaign.intel.events.ht.HyperspaceTopographyEventIntel;
import com.fs.starfarer.api.plugins.OfficerLevelupPlugin;
import com.fs.starfarer.api.ui.SectorMapAPI;
import com.fs.starfarer.api.ui.TooltipMakerAPI;
import com.fs.starfarer.api.util.Misc;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import thQuest.fleets.FleetBehaviourSpec;
import thQuest.fleets.FleetUtil;
import thQuest.fleets.YrFleetBehaviourSpec;

import java.awt.*;
import java.io.IOException;
import java.util.EnumSet;
import java.util.Set;

public class FantasyResurgenceEventIntel extends BaseEventIntel {
    public static Color BAR_COLOR = Global.getSettings().getColor("progressBarFleetPointsColor");
    public static int PROGRESS_MAX = 90;
    public static int PROGRESS_1 = 30;
    public static int PROGRESS_2 = 60;
    public static String KEY = "$thquest_fre";
    public FantasyResurgenceEventIntel(TextPanelAPI text, boolean withIntelNotification) {
        Global.getSector().getMemoryWithoutUpdate().set(KEY, this);
        this.setMaxProgress(PROGRESS_MAX);
        this.addStage(FantasyResurgenceEventIntel.Stage.START, 0);
        this.addStage(Stage.OFFICER, PROGRESS_1, StageIconSize.MEDIUM);
        this.addStage(Stage.YOUKAI_RAIDERS, PROGRESS_2, StageIconSize.MEDIUM);
        this.addStage(Stage.END, PROGRESS_MAX, true, StageIconSize.LARGE);
       // this.getDataFor(FantasyResurgenceEventIntel.Stage.GIFT).keepIconBrightWhenLaterStageReached = true;
        this.getDataFor(FantasyResurgenceEventIntel.Stage.YOUKAI_RAIDERS).keepIconBrightWhenLaterStageReached = true;
        //this.getDataFor(FantasyResurgenceEventIntel.Stage.OFFICER).keepIconBrightWhenLaterStageReached = true;
        Global.getSector().getIntelManager().addIntel(this, !withIntelNotification, text);
    }
    protected void notifyStageReached(BaseEventIntel.EventStageData stage) {
        //on certain stages, the Mayohiga pact uses more of its technology
        addItemsToFactions(getStage());
        if (stage.id == FantasyResurgenceEventIntel.Stage.OFFICER) {
        //get an officer
        // set flag in gensokyo
        Global.getSector().getMemory().set("$thquestYam",1);
        } else if (stage.id == FantasyResurgenceEventIntel.Stage.YOUKAI_RAIDERS) {
            //trigger faction?
            //raiders intend to terrorize.
            //raiders come from gensokyo colonies to systems with 1. Hostilities W/ Gensokyo, or random weighted, priority to LC, will not target PC
            //effects are fear effect, lowers growth and lowers stability by 1
//            Global.getSector().getMemory().set("$thquest_yr",true);
            //TODO test
            //sets memory key that intensifies YR
    for(FleetBehaviourSpec b:((FleetUtil)Global.getSector().getMemory().get("$thquestFleetUtil")).specs){
        if(b instanceof YrFleetBehaviourSpec ){
            ((YrFleetBehaviourSpec)b).intensified=true;
            break;
        }
    }//            FleetUtil fleetUtil = (FleetUtil) Global.getSector().getMemory().get("$thquestFleetUtil");
//            fleetUtil.addFleetBehaviourSpec(new YrFleetBehaviourSpec());

} else if (stage.id == FantasyResurgenceEventIntel.Stage.END) {
        //ttr quest open
        Global.getSector().getMemory().set("$thquestttr",1);
        }
    }
    @Override
    public FactionAPI getFactionForUIColors(){
        return Global.getSector().getFaction("thquest_gensokyo");
    }

    //Factors are in a different class
    public static void addFactorCreateIfNecessary(EventFactor factor, InteractionDialogAPI dialog) {
        if (get() == null) {
            new FantasyResurgenceEventIntel((TextPanelAPI)null, false);
        }

        if (get() != null) {
            get().addFactor(factor, dialog);
        }

    }
    public static FantasyResurgenceEventIntel get() {
        return (FantasyResurgenceEventIntel)Global.getSector().getMemoryWithoutUpdate().get(KEY);
    }
    public static int getStage(){
        FantasyResurgenceEventIntel f = FantasyResurgenceEventIntel.get();
        int progress = f.progress;
        if(f==null || progress<PROGRESS_1){
            return 0;
        }else if(progress<PROGRESS_2){
            return 1;
        }else if(progress<PROGRESS_MAX){
            return 2;
        }else{
            return 3;
        }
    }
    public void addStageDescriptionText(TooltipMakerAPI info, float width, Object stageId) {
        float opad = 10.0F;
        float small = 0.0F;
        Color h = Misc.getHighlightColor();
        BaseEventIntel.EventStageData stage = this.getDataFor(stageId);
        if (stage != null) {
            if (this.isStageActive(stageId)) {
                this.addStageDesc(info, stageId, small, false);
            }

        }
    }

    public void addStageDesc(TooltipMakerAPI info, Object stageId, float initPad, boolean forTooltip) {
        float opad = 10.0F;
        int progress =this.getProgress();
        boolean yam = false;
        if(Global.getSector().getMemoryWithoutUpdate().contains("$thquestYam")){
            //for whatever reason yam could be int or String
            yam = (Integer.parseInt(Global.getSector().getMemory().get("$thquestYam").toString())==2);
        }
        boolean ybh = false;
        if(Global.getSector().getMemoryWithoutUpdate().contains("$touhouquestybh") && (Integer.parseInt((String) Global.getSector().getMemoryWithoutUpdate().get("$touhouquestybh")) >=4 )){
            ybh=true;
        }
        Color h = Misc.getHighlightColor();
        if (stageId == FantasyResurgenceEventIntel.Stage.START) {
            if(ybh) info.addPara("The sector is changing due to the influence of fantastical things, such as Gensokyo. Ultimately, this should be beneficial to you, and you can speed it up by completing unique quests.", initPad);
            else info.addPara("The sector is changing due to the actions of the Mayohiga Pact. Ultimately this will be beneficial to you, and you can speed it up by completing unique quests.", initPad);
        } else if (stageId == FantasyResurgenceEventIntel.Stage.OFFICER&&progress<PROGRESS_1) {
            info.addPara("The increasing fantasy will surely lead to some good things for you.", initPad);
        } else if (stageId == FantasyResurgenceEventIntel.Stage.OFFICER&&!yam) {
            if(ybh) info.addPara("You have been noticed by some in the Mayohiga pact. An officer recruitment opprotinity awaits you on Gensokyo, at the Hakurei Shrine", initPad);
            else info.addPara("You have been noticed by some in the Mayohiga pact. An officer recruitment opprotinity awaits you on the Mayohiga Pact \"home world\"", initPad);
        }else if (stageId == FantasyResurgenceEventIntel.Stage.OFFICER) {
            info.addPara("You have gotten a new officer from Gensokyo.", initPad);
        } else if (stageId == FantasyResurgenceEventIntel.Stage.YOUKAI_RAIDERS&&progress<PROGRESS_2) {
            info.addPara("...", initPad);
        }else if (stageId == FantasyResurgenceEventIntel.Stage.YOUKAI_RAIDERS) {
            info.addPara("Some Youkai have become restless because of the increasing power of fantasy. They will now raid with increased frequency.", initPad);
        } else if (stageId == FantasyResurgenceEventIntel.Stage.END&&progress<PROGRESS_MAX) {
            info.addPara("At some point things will come to a head and the trajectory of the sector will be altered.", initPad);
        }else if (stageId == FantasyResurgenceEventIntel.Stage.END) {
            if(ybh) info.addPara("The events that have been set in motion are now coming to a head. Go to Gensokyo to go to the next stage.", initPad);
            else  info.addPara("New event unlocked at the Mayohiga Pact \"home world\"", initPad);
        }
    }
        public TooltipMakerAPI.TooltipCreator getStageTooltipImpl (Object stageId){
            final BaseEventIntel.EventStageData esd = this.getDataFor(stageId);
            final int progress =this.getProgress();
            return esd != null && EnumSet.of(FantasyResurgenceEventIntel.Stage.GIFT, FantasyResurgenceEventIntel.Stage.YOUKAI_RAIDERS, FantasyResurgenceEventIntel.Stage.OFFICER, FantasyResurgenceEventIntel.Stage.END).contains(esd.id) ? new BaseFactorTooltip() {
                public void createTooltip(TooltipMakerAPI tooltip, boolean expanded, Object tooltipParam) {
                    float opad = 10.0F;
                    if (esd.id == FantasyResurgenceEventIntel.Stage.YOUKAI_RAIDERS) {
                        if(progress<PROGRESS_2){
                            tooltip.addTitle("Unknown negative event");
                        }else{
                            tooltip.addTitle("Youkai raiders");
                        }
                    } else if (esd.id == FantasyResurgenceEventIntel.Stage.OFFICER) {
                        if(progress<PROGRESS_1){
                            tooltip.addTitle("Unknown positive event");
                        }else{
                            tooltip.addTitle("Recruit officer");
                        }
                    } else if (esd.id == FantasyResurgenceEventIntel.Stage.END) {
                        if(progress<PROGRESS_MAX){
                            tooltip.addTitle("Unknown event");
                        }else{
                            tooltip.addTitle("Gensokyo event");
                        }                    }

                    FantasyResurgenceEventIntel.this.addStageDesc(tooltip, esd.id, opad, true);
                    esd.addProgressReq(tooltip, opad);
                }
            } : null;
        }

    public String getIcon() {
        return Global.getSettings().getSpriteName("intel", "thquest_gns_small_crest");
    }
    protected String getName() {
        return "Fantasy Resurgence";
    }

    protected String getStageIconImpl(Object stageId) {
        BaseEventIntel.EventStageData esd = this.getDataFor(stageId);
        if (esd == null) {
            return null;
        } else {
            //todo change to sprites
            if (esd.id == Stage.START) {
                return Global.getSettings().getSpriteName("events", "stage_unknown_good");
            } else if (esd.id == FantasyResurgenceEventIntel.Stage.YOUKAI_RAIDERS) {
                return Global.getSettings().getSpriteName("events", "stage_unknown_bad");
            } else if (esd.id == FantasyResurgenceEventIntel.Stage.OFFICER) {
                return Global.getSettings().getSpriteName("events", "stage_unknown_good");
            } else if (esd.id == FantasyResurgenceEventIntel.Stage.END) {
                return Global.getSettings().getSpriteName("events", "stage_unknown_neutral");
            }
            return null;
        }
    }

    public Color getBarColor() {
        Color color = BAR_COLOR;
        color = Misc.interpolateColor(color, Color.black, 0.25F);
        return color;
    }


    public Set<String> getIntelTags(SectorMapAPI map) {
        Set<String> tags = super.getIntelTags(map);
        tags.add("Important");
        tags.add("thquest_gensokyo");
        tags.remove("Major events");
        return tags;
    }
    public boolean withMonthlyFactors() {
        return false;
    }
    public static enum Stage {
        START,
        GIFT,
        YOUKAI_RAIDERS,
        OFFICER,
        END;
        private Stage() {
        }
    }
    public void addItemsToFactions(int stage){
        try {
            JSONArray wepAddSpreadsheet = Global.getSettings().getMergedSpreadsheetDataForMod("id","data/weapons/thquest_zz_midgame_weapon_additions.csv","thquest");
            JSONArray wingAddSpreadsheet = Global.getSettings().getMergedSpreadsheetDataForMod("id","data/hulls/thquest_zz_midgame_wing_additions.csv","thquest");
            JSONArray shipAddSpreadsheet = Global.getSettings().getMergedSpreadsheetDataForMod("id","data/hulls/thquest_zz_midgame_ship_additions.csv","thquest");
            JSONArray hullmodAddSpreadsheet = Global.getSettings().getMergedSpreadsheetDataForMod("id","data/hullmods/thquest_zz_midgame_hullmod_additions.csv","thquest");
            for(int i=0;i<wepAddSpreadsheet.length();i++){
                JSONObject row = wepAddSpreadsheet.getJSONObject(i);
                String id = row.getString("id_obj");
                String faction = row.getString("faction");
                int targetStage = row.getInt("stage");
                if(stage >= targetStage){
                    Global.getSector().getFaction(faction).addKnownWeapon(id,false);
                }
            }
            for(int i=0;i<wingAddSpreadsheet.length();i++){
                JSONObject row = wingAddSpreadsheet.getJSONObject(i);
                String id = row.getString("id_obj");
                String faction = row.getString("faction");
                int targetStage = row.getInt("stage");
                if(stage >= targetStage){
                    Global.getSector().getFaction(faction).addKnownFighter(id,false);
                }
            }
            for(int i=0;i<shipAddSpreadsheet.length();i++){
                JSONObject row = shipAddSpreadsheet.getJSONObject(i);
                String id = row.getString("id_obj");
                String faction = row.getString("faction");
                int targetStage = row.getInt("stage");
                if(stage >= targetStage){
                    Global.getSector().getFaction(faction).addKnownShip(id,false);
                }
            }
            for(int i=0;i<hullmodAddSpreadsheet.length();i++){
                JSONObject row = hullmodAddSpreadsheet.getJSONObject(i);
                String id = row.getString("id_obj");
                String faction = row.getString("faction");
                int targetStage = row.getInt("stage");
                if(stage >= targetStage){
                    Global.getSector().getFaction(faction).addKnownHullMod(id);
                }
            }
        } catch (IOException | JSONException e) {
            throw new RuntimeException(e);
        }
    }
}
