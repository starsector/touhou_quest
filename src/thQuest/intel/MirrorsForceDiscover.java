package thQuest.intel;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.FactionAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.impl.campaign.ids.Tags;
import com.fs.starfarer.api.impl.campaign.intel.misc.BreadcrumbIntel;
import com.fs.starfarer.api.ui.SectorMapAPI;
import com.fs.starfarer.api.ui.TooltipMakerAPI;

import java.util.HashSet;
import java.util.Set;

public class MirrorsForceDiscover extends BreadcrumbIntel {
    public MirrorsForceDiscover(SectorEntityToken foundAt, SectorEntityToken target) {
        super(foundAt, target);
    }
    @Override
    public String getName() {
        return "Makai Gate revealed";
    }

    @Override
    public String getText() {
        return "Makai Gate revealed";
    }

    @Override
    public String getIcon() {
//        return Global.getSettings().getSpriteName("intel","new_planet_info");
        return Global.getSector().getFaction("thquest_gensokyo").getCrest();//todo test
    }

    @Override
    public boolean hasSmallDescription() {
        return true;
    }

    @Override
    public Set<String> getIntelTags(SectorMapAPI map) {
        HashSet<String> set= new HashSet<>();
        set.add(Tags.INTEL_FLEET_LOG);
        return set;
    }

    @Override
    public FactionAPI getFactionForUIColors() {
        return Global.getSector().getFaction("thquest_gensokyo");
    }

    @Override
    public void createSmallDescription(TooltipMakerAPI info, float width, float height) {
        info.addPara("Due to outside pressures, the Mayohiga pacts' hidden stations have been revealed.",10);
        info.addButton("Delete", BUTTON_DELETE,300,20,20);
    }

}
