package thQuest.intel;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.FactionAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.impl.campaign.intel.misc.BreadcrumbIntel;
import com.fs.starfarer.api.ui.SectorMapAPI;
import com.fs.starfarer.api.ui.TooltipMakerAPI;

import java.util.HashSet;
import java.util.Set;
//YR base pointer gets data from YrBaseScript.
public class YrBasePointer extends BreadcrumbIntel {
    public String fbId;
    int timeoutDays = 10;
    boolean baseDestroyed = false;
    boolean baseLeft = false;
    public long timestamp = Global.getSector().getClock().getTimestamp();
    long timestampBaseDestroyed;
    long timestampBaseGone;
    public SectorEntityToken baseLocation;//same as target but not private

    public void notifyBaseDestroyed(){
        baseDestroyed = true;
        timestampBaseDestroyed = Global.getSector().getClock().getTimestamp();
    }
    //todo call from rules.csv
    //only if it is stale and there is no clue to it's current location.
    public void notifyBaseGone(){
        baseLeft=true;
        timestampBaseGone = Global.getSector().getClock().getTimestamp();
    }
    public void notifyBaseMoved(SectorEntityToken newLocation){
        this.foundAt = target;
        target = newLocation;
    }

    public YrBasePointer(SectorEntityToken foundAt, SectorEntityToken target, String fbId) {
        super(foundAt, target);
        baseLocation = target;
        this.fbId = fbId;
    }
    @Override
    public String getName() {
        return "Youkai Raid";
    }

    @Override
    public String getText() {
        return "Youkai Raid";
    }

    @Override
    public String getIcon() {
        return Global.getSettings().getSpriteName("intel","thquest_yr");
    }

    @Override
    public boolean hasSmallDescription() {
        return true;
    }

    @Override
    public Set<String> getIntelTags(SectorMapAPI map) {
        HashSet<String> set= new HashSet<>();
        set.add("military");
        set.add("thquest_gensokyo");
        return set;
    }

    @Override
    public FactionAPI getFactionForUIColors() {
        return Global.getSector().getFaction("thquest_youkai");
    }

    @Override
    public void createSmallDescription(TooltipMakerAPI info, float width, float height) {
        info.addPara("Youkai have set up a base in the "+target.getStarSystem().getName()+" system.",10);
        info.addPara("The base is mobile. The older this log is the more inaccurate this intel will be.",10);
        if(baseDestroyed){
            info.addPara("This base has been destroyed.",10);
        }else if(baseLeft){
            info.addPara("The base is now in an unknown location.",10);
        }else{
            info.addPara(""+Global.getSector().getClock().getElapsedDaysSince(timestamp)+" days ago.",10);

        }
        //info.addButton("Delete", BUTTON_DELETE,300,20,20);
    }
    @Override
        public boolean shouldRemoveIntel(){
        //this value is only if the player knows the base was destroyed / can't find.
        return Global.getSector().getClock().getElapsedDaysSince(timestampBaseDestroyed)>timeoutDays||
                Global.getSector().getClock().getElapsedDaysSince(timestampBaseGone)>timeoutDays;
    }
}
