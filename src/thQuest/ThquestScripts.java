package thQuest;

import com.fs.starfarer.api.GameState;
import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.loading.Description;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import thQuest.intel.FantasyResurgenceEventIntel;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ThquestScripts {
    static Map<String,String> replacedDescriptions = new HashMap<>();
    public static void updateDescriptions(){
//        int stage = FantasyResurgenceEventIntel.getStage();
//        if(stage>0) {
            JSONArray replacingDescriptions;
            try {
                replacingDescriptions = Global.getSettings().getMergedSpreadsheetDataForMod("id", "data/strings/thquestCustomDescriptions.csv", "thquest");
            } catch (IOException | JSONException e) {
                throw new RuntimeException(e);
            }
            for (int i = 0; i < replacingDescriptions.length(); i++) {
                try {
                    JSONObject object = (JSONObject) replacingDescriptions.get(i);
//                    int req_stage = object.getInt("fr_stage");
                    String req = object.getString("condition");
                    String id = object.getString("id");
                    Description.Type type = Description.Type.valueOf(object.getString("type"));
                    String text1 = object.getString("text1");
                    if(Global.getCurrentState() == null || (req.equals("gensokyo_faction")&&Global.getSector().getMemoryWithoutUpdate().contains("$thquestGensokyoFactionFullDescription") ) || (req.equals("rika") && Global.getSector().getMemoryWithoutUpdate().contains("$thquestRikaDescriptionPackage"))) {
                        if(!replacedDescriptions.containsKey(id)) {
                            replacedDescriptions.put(id, Global.getSettings().getDescription(id, type).getText1());
                        }
                        Global.getSettings().getDescription(id, type).setText1(text1);
                    }else{
                        if(replacedDescriptions.containsKey(id)){
                            Global.getSettings().getDescription(id, type).setText1(replacedDescriptions.get(id));
                        }
                    }
                } catch (JSONException e) {
                    throw new RuntimeException(e);
                }
            }
//        }
    }
}
