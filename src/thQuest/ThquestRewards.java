package thQuest;

import com.fs.starfarer.api.campaign.SpecialItemData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ThquestRewards {
    //also does FR completion, which is not handled here
    public ThquestRewards(int credits, Map<String, Integer> fairyWings, List<SpecialItemData> specialItems, Map<String, Integer> wings, Map<String, Integer> weapons, Map<String, Integer> commodities, int mayohigaPactRep) {
        this.credits = credits;
        this.fairyWings = fairyWings;
        this.specialItems = specialItems;
        this.wings = wings;
        this.weapons = weapons;
        this.commodities = commodities;
        this.mayohigaPactRep = mayohigaPactRep;
    }

    //    public boolean randomLoot; don't want to do random loot rn
    public int credits;
    public Map<String, Integer> fairyWings;
    public List<SpecialItemData> specialItems;
    public Map<String, Integer> wings;
    public Map<String, Integer> weapons;
    public Map<String, Integer> commodities;
    public int mayohigaPactRep;
    public static ThquestRewards getLgReward(String name){
        ThquestRewards rewards= new ThquestRewards(40000,null,null,null,null,null,1);
        //this list must match what is in CreateLostTouhouLocations!
        switch (name){
            //LG quests
            case "mima":
                List<SpecialItemData> specialItemsMima = new ArrayList<>();
                specialItemsMima.add(new SpecialItemData("weapon_bp","thquest_yyorb"));
                specialItemsMima.add(new SpecialItemData("weapon_bp","thquest_sakuya_orb_medium"));
                specialItemsMima.add(new SpecialItemData("weapon_bp","thquest_sanae_gun"));
                specialItemsMima.add(new SpecialItemData("weapon_bp","thquest_magic_missile"));
                specialItemsMima.add(new SpecialItemData("weapon_bp","thquest_cat_cart"));
                rewards.specialItems=specialItemsMima;
                rewards.credits = 70000;
                rewards.mayohigaPactRep = 0;
                return rewards;
            case "kanako":
                rewards.credits = 50000;
                rewards.mayohigaPactRep = 3;
                List<SpecialItemData> specialItems = new ArrayList<>();
                specialItems.add(new SpecialItemData("ship_bp","thquest_moon_rocket"));
                specialItems.add(new SpecialItemData("ship_bp","thquest_moon_rocket_2"));
                rewards.specialItems=specialItems;
                return rewards;
            case "koishi":
                HashMap<String, Integer> fairiesLow = new HashMap<>();
                fairiesLow.put("thquest_fairy_maid_wing_item",2);
                fairiesLow.put("thquest_fairy_wing_item",2);
                rewards.fairyWings=fairiesLow;
                HashMap<String, Integer> weaponsLow = new HashMap<>();
                weaponsLow.put("thquest_yyorb",6);
                rewards.weapons=weaponsLow;
                List<SpecialItemData> specialItemsKosh = new ArrayList<>();
                specialItemsKosh.add(new SpecialItemData("ship_bp","thquest_cirno"));
                specialItemsKosh.add(new SpecialItemData("weapon_bp","thquest_icicle_fall"));
                rewards.specialItems=specialItemsKosh;
                HashMap<String, Integer> wingsLow = new HashMap<>();
                wingsLow.put("thquest_shanghai_doll_wing",2);
                wingsLow.put("thquest_hourai_doll_wing",2);
                rewards.wings=wingsLow;
                HashMap<String, Integer> hvweapons = new HashMap<>();
                hvweapons.put("hand_weapons",25);
                rewards.commodities = hvweapons;
                return rewards;
                //other quests
            case "nue":
                List<SpecialItemData> specialItemsNue = new ArrayList<>();
                specialItemsNue.add(new SpecialItemData("ship_bp","thquest_sakuya"));
                rewards.specialItems=specialItemsNue;
                return rewards;
            case "kana":
                List<SpecialItemData> specialItemsKana = new ArrayList<>();
                specialItemsKana.add(new SpecialItemData("ship_bp","thquest_psh"));
                specialItemsKana.add(new SpecialItemData("ship_bp","thquest_aurora_myon"));
                specialItemsKana.add(new SpecialItemData("fighter_bp","thquest_myon_fighter"));
                rewards.specialItems=specialItemsKana;
                return rewards;
            case "elis":
                List<SpecialItemData> specialItemsElis = new ArrayList<>();
                specialItemsElis.add(new SpecialItemData("weapon_bp","thquest_zshot"));
                specialItemsElis.add(new SpecialItemData("weapon_bp","thquest_control_rod"));
                specialItemsElis.add(new SpecialItemData("weapon_bp","thquest_fireworks"));
                rewards.specialItems=specialItemsElis;
                return rewards;
            case "louise":
                List<SpecialItemData> specialItems2 = new ArrayList<>();
                specialItems2.add(new SpecialItemData("weapon_bp","thquest_cheese"));
                specialItems2.add(new SpecialItemData("fighter_bp","thquest_shrine_tank"));
                specialItems2.add(new SpecialItemData("fighter_bp","thquest_flower_tank"));
                rewards.specialItems=specialItems2;
                return rewards;
            case "minamitsu":
                List<SpecialItemData> specialItemsMinamitsu = new ArrayList<>();
                specialItemsMinamitsu.add(new SpecialItemData("ship_bp","thquest_palanquin_ship"));
                specialItemsMinamitsu.add(new SpecialItemData("ship_bp","thquest_halfgon"));
                rewards.specialItems=specialItemsMinamitsu;
                return rewards;
            case "kokoro":
                List<SpecialItemData> specialItemsKokoro = new ArrayList<>();
                specialItemsKokoro.add(new SpecialItemData("ship_bp","thquest_snc"));
                rewards.specialItems=specialItemsKokoro;
                return rewards;
            case "maribel":
                List<SpecialItemData> specialItemsMaribel = new ArrayList<>();
                specialItemsMaribel.add(new SpecialItemData("ship_bp","thquest_reimu"));
                specialItemsMaribel.add(new SpecialItemData("weapon_bp","thquest_fantasy_seal"));
                rewards.specialItems=specialItemsMaribel;
                return rewards;
            case "renko":
                List<SpecialItemData> specialItemsRenko = new ArrayList<>();
                specialItemsRenko.add(new SpecialItemData("ship_bp","thquest_marisa"));
                specialItemsRenko.add(new SpecialItemData("weapon_bp","thquest_star_sword"));
                rewards.specialItems=specialItemsRenko;
                return rewards;
            case "seiran":
                List<SpecialItemData> specialItemsSeiran = new ArrayList<>();
                specialItemsSeiran.add(new SpecialItemData("fighter_bp","thquest_hourai"));
                specialItemsSeiran.add(new SpecialItemData("weapon_bp","thquest_remi_gun"));
                rewards.specialItems=specialItemsSeiran;
                return rewards;
            case "ringo":
                List<SpecialItemData> specialItemsRingo = new ArrayList<>();
                specialItemsRingo.add(new SpecialItemData("fighter_bp","thquest_shanghai"));
                specialItemsRingo.add(new SpecialItemData("weapon_bp","thquest_mimi_launcher"));
                rewards.specialItems=specialItemsRingo;
                return rewards;
            case "ybh":
                rewards.credits=50000;
                rewards.mayohigaPactRep=3;
//                HashMap<String, Integer> someWings = new HashMap<>();
//                someWings.put("thquest_shrine_tank",3);
                return rewards;
            case "gog":
                //currently not handled here
                return rewards;
            case "ttr":
                //currently not handled here
                return rewards;
            default:
                return rewards;
        }
    }
}